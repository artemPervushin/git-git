<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Прошивки – интернет-магазин «КаркамЭлектроникс»");
$APPLICATION->SetTitle("Прошивки");
?><main class="container about-page">
<h1><?$APPLICATION->ShowTitle()?></h1>
<h2>Прошивки для автоэлектроники</h2>
<div class="firmware-list row">
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/m2/m2.php">
		<p>
			 КАРКАМ M2
		</p>
 <img src="/podderzhka/m2/M2-supp.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/kombo2/kombo2.php">
		<p>
			 КАРКАМ Комбо 2
		</p>
 <img src="/upload/iblock/90a/90a94a814f4c366b52e76423a28974b2.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/duo.php">
		<p>
			 КАРКАМ ДУО
		</p>
 <img src="/upload/iblock/9bd/9bd18d9c4e0262faf9d3e1e57719e3b2.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/tinys/tinys.php">
		<p>
			 КАРКАМ Tiny S
		</p>
 <img src="/podderzhka/tinys/tinys.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/q7/q7.php">
		<p>
			 КАРКАМ Q7
		</p>
 <img src="/upload/iblock/c77/c77265f251d9691edd7541c5fd38b6fe.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/t2/t2.php">
		<p>
			 КАРКАМ Т2
		</p>
 <img src="/upload/iblock/0d0/0d0288ec79b3daa4896127ebea11283c.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/smart/smart.php">
		<p>
			 КАРКАМ Смарт
		</p>
 <img src="/upload/iblock/bfc/bfc11b899587e3812928a70e9e9b9e07.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/stels/stels3plus.php">
		<p>
			 КАРКАМ Стелс 3+
		</p>
 <img src="/upload/resizer2/1_af964266e86d7ccf23e4692fd0a27316.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/a2/a2.php">
		<p>
			 КАРКАМ ЗЕРКАЛО А2
		</p>
 <img src="/upload/iblock/419/4194b27e7341f4f208aa5a45f6161096.png"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/a7/a7.php">
		<p>
			 КАРКАМ ЗЕРКАЛО А7
		</p>
 <img src="/upload/iblock/7bc/7bc82ad72e6799256d852cae005e2e1d.png"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/a7_duo/a7_duo.php">
		<p>
			 КАРКАМ ЗЕРКАЛО A7 Duo
		</p>
 <img src="/upload/iblock/08a/08a5e39142f039c1704aacd3521cb5da.png"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/combo1/combo1.php">
		<p>
			 КАРКАМ КОМБО
		</p>
 <img src="/upload/iblock/1b1/1b1ed0008f0dfa3c459b76fca19f359a.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/m1/m1.php">
		<p>
			 КАРКАМ M1
		</p>
 <img src="/upload/iblock/422/422b4f76f700afe105245b14a20d8226.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/q2/q2.php">
		<p>
			 КАРКАМ Q2
		</p>
 <img src="/podderzhka/q2/30.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBdWVsbElHakZ6MnM&usp=sharing">
		<p>
			 КАРКАМ QX2
		</p>
 <img src="/upload/iblock/d7e/d7eb15a1116f408c3ccd045f30b87fbd.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="">
		<p>
			 КАРКАМ Q4
		</p>
 <img src="/upload/iblock/283/28363c2c7e0adaca7ae00428185615a2.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBM003ZDFQWC0tRWM&usp=sharing">
		<p>
			 КАРКАМ Q5
		</p>
 <img src="/podderzhka/q5/Q5.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBSXRhTXhCNFdBQ3c&usp=sharing">
		<p>
			 КАРКАМ QL3
		</p>
 <img src="/podderzhka/ql3/ql3.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBazVGTXBERlE5TGM&usp=sharing">
		<p>
			 КАРКАМ QL3 ECO
		</p>
 <img src="/upload/iblock/e71/e711e9f5aec0a0460a95d9153845ed1a.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBNlM3eWhfdmxXZkU&usp=sharing">
		<p>
			 КАРКАМ QL3 MINI
		</p>
 <img src="/upload/iblock/431/4317a68043e587fac06799b2d09f775c.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBZmJTczh1TzlkVWs&usp=sharing">
		<p>
			 КАРКАМ QL3 NEO
		</p>
 <img src="/podderzhka/ql3/ql3_neo.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBZWQxbC1QblU3WWc&usp=sharing">
		<p>
			 КАРКАМ QS3
		</p>
 <img src="/upload/iblock/4f2/4f2b33e5b56360bc98f787c2140b6f43.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBSjd5UGl1aEVnLUU&usp=sharing">
		<p>
			 КАРКАМ QS3 ECO
		</p>
 <img src="/upload/iblock/313/31359f22eb12fdbb23661423b848d42b.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBd3lRaDZCemZ2Qm8&usp=sharing">
		<p>
			 КАРКАМ Tiny
		</p>
 <img src="/upload/iblock/483/483bb3d572e081d6f4ab0cb867625a84.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBSDF4NjZDZkJidEk&usp=sharing">
		<p>
			 КАРКАМ QX3 NEO
		</p>
 <img src="/upload/iblock/195/195d136aa1e3cea3b27b59d2ed5148b0.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBQ3VGYlMwMU1mSW8&usp=sharing">
		<p>
			 КАРКАМ Q3
		</p>
 <img src="/podderzhka/q3/q3.jpg"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBdEt4dlVUOXFQNFU&usp=sharing">
		<p>
			 КАРКАМ Excam One
		</p>
 <img src="/podderzhka/excameone/ExcamOne.png"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/podderzhka/sa_4t/CFCardViewer.zip">
		<p>
			 КАРКАМ SA-4T
		</p>
 <img src="/podderzhka/sa_4t/SA4T.png"></a>
	</div>
</div>
<h2>Прошивки для гибридных видеорегистраторов</h2>
<div class="firmware-list row">
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/open?id=0B4x3h2WFXCZBekRmdU00SVo4azQ&authuser=0">
		<p>
			 КАРКАМ 1004, 6004, 8604, 8608
		</p>
 <img src="/upload/iblock/a2d/a2dd8bfa584fce66149b1efd143ad0b7.png"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/open?id=0B4x3h2WFXCZBMXNpeWx3U2pMcTQ&authuser=0">
		<p>
			 КАРКАМ 9408, 9316
		</p>
 <img src="/podderzhka/9408.png"></a>
	</div>
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="https://drive.google.com/open?id=0B4x3h2WFXCZBalVCRkFwY0F2ZFE&authuser=0">
		<p>
			 КАРКАМ 9516
		</p>
 <img src="/podderzhka/9516.png"></a>
	</div>
</div>
<h2>Прошивки для IP-видеокамер</h2>
<div class="firmware-list row">
	<div class="firmware-item col-lg-2 col-md-3 col-sm-4 col-xs-6">
 <a href="/upload/ipcam_1mp_2014-10-08.rar">
		<p>
			 Для IP камер 1Мп от 08.10.2014
		</p>
 <img src="/upload/iblock/129/12973b6201c55ee42d76b26a92cd221a.jpg"></a>
	</div>
</div>
<h2>Программное обеспечение</h2>
<ul>
	<li><a class="moz-txt-link-freetext" href="/doc/CMS_CARCAM.exe">Программа для просмотра камер видеонаблюдения CMS КАРКАМ</a></li>
	<li>Видеоплеер <a class="moz-txt-link-freetext" href="http://yadi.sk/d/BgdA3rNPKsMmQ">HDPlayer.zip</a></li>
	<li>Пакет кодеков <a class="moz-txt-link-freetext" href="http://codecguide.com/download_kl.htm">http://codecguide.com/download_kl.htm</a></li>
	<li>Registrator Viewer <a class="moz-txt-link-freetext" href="http://www.registratorviewer.com/download.html">http://www.registratorviewer.com/download.html</a></li>
	<li>Радар-детектор <a class="moz-txt-link-freetext" href="https://drive.google.com/folderview?id=0B4x3h2WFXCZBTlpVd2xfbzdLREE&usp=sharing">КАРКАМ Комбо</a></li>
	<li>Автомобильный видеорегистратор <a class="moz-txt-link-freetext" href="https://drive.google.com/open?id=0B4x3h2WFXCZBWklvQmNfSGdtZW8&authuser=0">SA-4T</a></li>
	<li>Автомобильный видеорегистратор <a class="moz-txt-link-freetext" href="https://drive.google.com/open?id=0B4x3h2WFXCZBWFV3MXFYeTRUV1k&authuser=0">КАРКАМ M1</a></li>
	<li><a href="/doc/General_DeviceManage_V2.5.1.0.R.20140814.exe">Программа для поиска и настройки сетевого оборудования под управлением процессора HiSilicon</a></li>
</ul>
 </main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>