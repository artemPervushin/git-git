<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Видеорегистраторы, системы видеонаблюдения, автоэлектроника, GSM сигнализации с Бесплатной доставкой по всей России. Регулярные акции, скидки при оплате картой");
$APPLICATION->SetPageProperty("description", "Видеорегистраторы, системы видеонаблюдения, автоэлектроника, GSM сигнализации с Бесплатной доставкой по всей России. Регулярные акции, скидки при оплате картой");
$APPLICATION->SetPageProperty("title", "КАРКАМ Электроникс – официальный интернет-магазин производителя электроники");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Главная страница");
global $rz_b2_options;
?><main class="home-page" data-page="home-page">
	<h1 class="home-page-h1"><?$APPLICATION->ShowTitle()?></h1>
	<?
	if($rz_b2_options['block_home-main-slider'] == 'Y') {
	?>
	<link rel="stylesheet" href="/slider_master/responsiveslides.css">
	<script src="/slider_master/responsiveslides.min.js"></script>
	<div class="container hidden-xs" style="padding:0;">
		<ul class="rslides" id="slider1">
			<li><a href="/product/carcam-atlas.html"><img src="/images/slider/atlas_ban01_12_2016_1770x531_.jpg" alt="КАРКАМ АТЛАС"></a></li>
			<li><a href="/product/carcam-combo-3.html"><img src="/images/slider/kombo3_ban01_12_2016_1770x531_01.jpg" alt="КАРКАМ КОМБО 3"></a></li>
			<li><a href="/product/carcam-combo-3.html"><img src="/images/slider/kombo3_ban01_12_2016_1770x531_02.jpg" alt="КАРКАМ КОМБО 3"></a></li>
			<li><a href="/product/carcam-combo-3.html"><img src="/images/slider/kombo3_ban01_12_2016_1770x531_03.jpg" alt="КАРКАМ КОМБО 3"></a></li>
		</ul>
	</div>
	<script>$("#slider1").responsiveSlides({ speed: 800 });</script>
	<div style="clear:both;"></div>
	<?	
		//$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/big-slider.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));
	}
	if($rz_b2_options['block_home-rubric'] == 'Y') {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/categories.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));
	}
	if($rz_b2_options['block_home-cool-slider'] == 'Y') {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/cool-slider.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));
	}
	if($rz_b2_options['block_home-specials'] == 'Y') {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/main_spec.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));
	}
	?>

	<? if ('Y' == $rz_b2_options['block_home-feedback']): ?>
		<? \Yenisite\Core\Tools::IncludeArea('index', 'feedback', false, true) ?>
	<? endif ?>
	<? if ($rz_b2_options['block_home-catchbuy'] == 'Y') {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/catchbuy.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));
	}
	?>
	
	<div class="promo-banners container wow fadeIn">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/banner1.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
		<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/banner2.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
	</div>
	
	<div class="text-content container wow fadeIn">
		<div class="row">
			<div class="col-sm-12 col-md-4">
				<div class="about">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/about_title.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"N"));?>
					<div class="hidden-xs"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/about_text.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"N"));?></div>
				</div>
			</div><!-- /.col-sm-6.col-md-5 -->
			<div class="col-sm-6 col-md-4">
				<div class="benefit">
					<div class="img-wrap">
						<span data-picture data-alt="Доставляем!">
				      		<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/delivery.png"></span>
						</span>
					</div>
					<div class="content">
						<header>Быстро и качественно доставляем</header>
						<p>Наша компания производит доставку по всей России и ближнему зарубежью</p>
					</div>
				</div>
				<div class="benefit">
					<div class="img-wrap">
						<span data-picture data-alt="Дружим!">
							<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/agree.png"></span>
						</span>
					</div>
					<div class="content">
						<header>Открытость для покупателей</header>
						<p>Доверие покупателей – самое ценное для нас. Мы делаем все, чтобы завоевать и сохранить его</p>
					</div>
				</div>
				<div class="benefit">
					<div class="img-wrap">
						<span data-picture data-alt="Обмениваем!">
							<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/exchange.png"></span>
						</span>		
					</div>
					<div class="content">
						<header>Возврат товара в течение 30 дней</header>
						<p>У вас есть 30 дней, для того чтобы протестировать вашу покупку</p>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="benefit">
					<div class="img-wrap">
						<span data-picture data-alt="Гарантируем!">
							<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/OK.png"></span>
						</span>
					</div>
					<div class="content">
						<header>Гарантия качества и сервисное обслуживание</header>
						<p>Мы предлагаем только те товары, в качестве которых мы уверены</p>
					</div>
				</div>
				<div class="benefit">
					<div class="img-wrap">
						<span data-picture data-alt="Выгодно!">
							<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/sale.png"></span>
						</span>	
					</div>
					<div class="content">
						<header>Самые выгодные цены только у нас</header>
						<p>Если вы нашли товар по более выгодной цене, мы отдадим его еще дешевле</p>
					</div>
				</div>
				<div class="benefit">
					<div class="img-wrap">
					<span data-picture data-alt="Огромный ассортимент!">
						<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/menu.png"></span>
				    </span>
					</div>
					<div class="content">
						<header>Огромный ассортимент различных товаров</header>
						<p>Более 10 000 наименований товаров ведущих мировых брендов</p>
					</div>
				</div>
			</div>
		</div><!-- /.row -->
		<?if ($rz_b2_options['block_home-brands'] == 'Y'):?>
		<div class="row brands-wrap wow fadeIn" data-brands-view-type="<?= ($rz_b2_options['brands_cloud'] == 'Y') ? 'tags' : 'carousel' ?>">
			<div class="col-sm-12">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/brands.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
			</div><!-- /.col-sm-12 -->
		</div><!-- /.row.brands-wrap -->
		<? endif ?>
	</div><!-- /.text-content.container -->

	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/news.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));?>
			</div>
			<div class="col-sm-6">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/campaigns.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));?>
			</div>
		</div>
	</div>
</main>
<script type="text/javascript">
    window._retag = window._retag || [];
    window._retag.push({code: "9ce88869e8", level: 0});
    (function () {
        var id = "admitad-retag";
        if (document.getElementById(id)) {return;}
        var s = document.createElement("script");
        s.async = true; s.id = id;
        var r = (new Date).getDate();
        s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static/js/retag.js?r="+r;
        var a = document.getElementsByTagName("script")[0]
        a.parentNode.insertBefore(s, a);
    })()
</script>
<!-- /.home-page -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>