<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Yandex market");?>
<?$arrFilter = array("!ID"=> array("6294", "13035", "13030", "13032", "13027", "13370", "13376", "12434", "17852", "17717", "7920", "7910", "7909", "7908", "7923", "12528", "7276", "19252", "8346", "8330" ));?>
<?$APPLICATION->IncludeComponent(
	"yenisite:yandex.market_new", 
	"wikimart", 
	array(
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID_IN" => array(
			0 => "153",
		),
		"IBLOCK_SECTION" => array(
			0 => "0",
		),
		"DO_NOT_INCLUDE_SUBSECTIONS" => "N",
		"SITE" => "carcam.ru",
		"COMPANY" => "\"официальный интернет магазин каркам\"",
		"SKU_NAME" => "PRODUCT_NAME",
		"FILTER_NAME" => "arrFilter",
		"MORE_PHOTO" => "MORE_PHOTO",
		"PRICE_CODE" => array(
		),
		"IBLOCK_ORDER" => "N",
		"CURRENCIES_CONVERT" => "NOT_CONVERT",
		"LOCAL_DELIVERY_COST" => "",
		"NAME_PROP" => "0",
		"DETAIL_TEXT_PRIORITET" => "N",
		"DISCOUNTS" => "DISCOUNT_CUSTOM",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"CACHE_FILTER" => "Y",
		"COMPONENT_TEMPLATE" => "wikimart",
		"IBLOCK_TYPE_LIST" => array(
			0 => "catalog",
		),
		"IBLOCK_CATALOG" => "Y",
		"IBLOCK_AS_CATEGORY" => "Y",
		"CACHE_NON_MANAGED" => "N",
		"SKU_PROPERTY" => "PROPERTY_CML2_LINK",
		"OLD_PRICE_LIST" => "FROM_DISCOUNT",
		"PARAMS" => array(
		),
		"COND_PARAMS" => array(
		),
		"SELF_SALES_NOTES" => "N",
		"SALES_NOTES_NAMES" => "0"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>