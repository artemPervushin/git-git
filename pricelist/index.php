<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Прайс-лист");?>

<main class="container about-page">
	<div class="row">
		<div class="col-xs-12">
			<?$APPLICATION->IncludeComponent(
				"yenisite:catalog.price_generator", 
				".default", 
				array(
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"COMPONENT_TEMPLATE" => ".default",
					"FILE_NAME" => "price",
					"FILE_TYPE" => "xls",
					"FILTER_NAME" => "arrFilter",
					"IBLOCK_ID" => array(
						0 => "28",
						1 => "",
					),
					"IBLOCK_TYPE" => array(
						0 => "catalog",
					),
					"PAGE_ELEMENT_COUNT" => "1",
					"PRICE_CODE" => array(
						0 => "BASE",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
						2 => "",
					)
				),
				false
			);?>
		</div>
	</div>
 </main>
 
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>