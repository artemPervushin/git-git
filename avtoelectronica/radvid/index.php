<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Радар-детекторы+Видеорегистраторы");?> <?$APPLICATION->IncludeComponent(
	"bitrix:catalog",
	"catalog",
	Array(
		"STICKER_NEW" => "0",
		"STICKER_HIT" => "10000",
		"STICKER_BESTSELLER" => "100",
		"BLOCK_VIEW_MODE" => "Всплывашка",
		"LIST_PRICE_SORT" => "CATALOG_PRICE_1",
		"DEFAULT_ELEMENT_SORT_BY" => "PROPERTY_WEEK_COUNTER",
		"DEFAULT_ELEMENT_SORT_ORDER" => "DESC",
		"EXPAND_PROPS" => "N",
		"COMPLETE_SET_PROPERTIES" => array(),
		"COMPLETE_SET_DESCRIPTION" => "PREVIEW_TEXT",
		"COMPLETE_SET_RESIZER_SET" => "5",
		"COMPLETE_SET_NO_INCLUDE_PRICE" => "N",
		"DEFAULT_VIEW" => "block",
		"ACCESSORIES_ON" => "N",
		"ACCESSORIES_LINK" => array(),
		"ACCESSORIES_PROPS" => "",
		"ACCESSORIES_FILTER_NAME" => "arrFilter",
		"ACCESSORIES_PAGE_ELEMENT_COUNT" => "10",
		"SHOW_ANNOUNCE" => "N",
		"SHOW_ELEMENT" => "N",
		"SHOW_SEOBLOCK" => "N",
		"BLOCK_IMG_SMALL" => "3",
		"BLOCK_IMG_BIG" => "4",
		"LIST_IMG" => "3",
		"TABLE_IMG" => "5",
		"DETAIL_IMG_SMALL" => "2",
		"DETAIL_IMG_BIG" => "1",
		"DETAIL_IMG_ICON" => "6",
		"COMPARE_IMG" => "4",
		"RESIZER_BOX" => "N",
		"SECTION_SHOW_DESCRIPTION" => "N",
		"SECTION_SHOW_DESCRIPTION_DOWN" => "N",
		"SECTION_META_SPLITTER" => ",",
		"SECTION_META_H1" => "#NAME#",
		"SECTION_META_H1_FORCE" => "UF_H1",
		"SECTION_META_TITLE_PROP" => "Купить #NAME#",
		"SECTION_META_TITLE_PROP_FORCE" => "UF_TITLE",
		"SECTION_META_KEYWORDS" => "#NAME#",
		"SECTION_META_KEYWORDS_FORCE" => "UF_KEYWORDS",
		"SECTION_META_DESCRIPTION" => "#IBLOCK_NAME# #NAME#",
		"SECTION_META_DESCRIPTION_FORCE" => "UF_DESCRIPTION",
		"DETAIL_META_SPLITTER" => ",",
		"DETAIL_META_H1" => "#NAME#",
		"DETAIL_META_H1_FORCE" => "H1",
		"DETAIL_META_TITLE_PROP" => "Купить #NAME#",
		"DETAIL_META_TITLE_PROP_FORCE" => "TITLE",
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_KEYWORDS_FORCE" => "KEYWORDS",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_META_DESCRIPTION_FORCE" => "DESCRIPTION",
		"TABS_TECH_ENABLE" => "Y",
		"TABS_REVIEWS_ENABLE" => "Y",
		"TABS_VIDEO_ENABLE" => "N",
		"REVIEWS_SITE_ENABLE" => "Y",
		"REVIEWS_YM2_ENABLE" => "N",
		"REVIEWS_YM_ENABLE" => "N",
		"REVIEWS_MR_ENABLE" => "N",
		"REVIEWS_VK_ENABLE" => "N",
		"REVIEWS_FB_ENABLE" => "N",
		"REVIEWS_DQ_ENABLE" => "N",
		"IBLOCK_MAX_VOTE" => "5",
		"IBLOCK_VOTE_NAMES" => array("1", "2", "3", "4", "5"),
		"IBLOCK_SET_STATUS_404" => "N",
		"DISPLAY_AS_RATING" => "rating",
		"FILTER_BY_QUANTITY" => "N",
		"YS_STORES_MUCH_AMOUNT" => "15",
		"SHOW_SKLAD" => "N",
		"SET_STATUS_ABCD" => "N",
		"SETTINGS_HIDE" => array(),
		"HIDE_ORDER_PRICE" => "N",
		"FOR_ORDER_DESCRIPTION" => "",
		"PRODUCT_PROPERTIES" => array(),
		"HIDE_BUY_IF_PROPS" => "Y",
		"AJAX_MODE" => "N",
		"SEF_MODE" => "N",
		"IBLOCK_TYPE" => "s1_autoelectronica",
		"IBLOCK_ID" => "194",
		"USE_FILTER" => "Y",
		"USE_REVIEW" => "Y",
		"USE_COMPARE" => "Y",
		"SHOW_TOP_ELEMENTS" => "Y",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_TOP_DEPTH" => "2",
		"PAGE_ELEMENT_COUNT" => "20",
		"LINE_ELEMENT_COUNT" => "3",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array("PHOTO"),
		"INCLUDE_SUBSECTIONS" => "Y",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"DETAIL_PROPERTY_CODE" => array("PRODUCER", "COUNTRY"),
		"DETAIL_BROWSER_TITLE" => "-",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"PRICE_CODE" => array("BASE"),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"USE_ALSO_BUY" => "N",
		"USE_STORE" => "Y",
		"USE_ELEMENT_COUNTER" => "Y",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Радар-детекторы+Видеорегистраторы",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_FIELD_CODE" => array("NAME"),
		"COMPARE_PROPERTY_CODE" => array("COUNTRY", "PRODUCER"),
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(),
		"FILTER_NAME" => "arrFilter",
		"FILTER_FIELD_CODE" => array(),
		"FILTER_PROPERTY_CODE" => array("PRODUCER", "COUNTRY"),
		"FILTER_PRICE_CODE" => array("BASE"),
		"MESSAGES_PER_PAGE" => "10",
		"USE_CAPTCHA" => "Y",
		"REVIEW_AJAX_POST" => "N",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"FORUM_ID" => "1",
		"URL_TEMPLATES_READ" => "",
		"SHOW_LINK_TO_FORUM" => "Y",
		"USE_STORE_PHONE" => "N",
		"USE_STORE_SCHEDULE" => "N",
		"USE_MIN_AMOUNT" => "Y",
		"MIN_AMOUNT" => "10",
		"STORE_PATH" => "/store/#store_id#",
		"MAIN_TITLE" => "Наличие на складах",
		"HIDE_NOT_AVAILABLE" => "N",
		"CONVERT_CURRENCY" => "N",
		"VARIABLE_ALIASES" => Array(
			"SECTION_ID" => "SECTION_ID",
			"ELEMENT_ID" => "ELEMENT_ID"
		),
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);?> <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>