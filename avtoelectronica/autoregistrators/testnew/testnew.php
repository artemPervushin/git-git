<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test");
?>


<br>
<head>
	<meta charset="UTF-8">
	<title>Back to Top button</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<div class="main">



	<div id="inner-wrapper">
	  <header id="header">
		
			<nav id="menu">
				<ul>
					<li><a href="#ex1">Описание</a></li>
					<li><a href="#ex2">Характеристики</a></li>
					<li><a href="#ex3">Обзоры</a></li>
					<li><a href="#ex4">Поддержка</a></li>
                    <li><a href="#ex5">Видео</a></li>
				</ul>
			</nav>
		</header>
		<div id="content">
			<section id="ex1">
				<h2>Описание</h2>
				<article class="post">
					
					<p> КАРКАМ КОМБО 2 - суперсовременное автомобильное устройство сочетающее 3 в 1 - видео регистратор, радар-детектор, GPS-информер.
Такая комплектация делает его незаменимым комплексом безопасности, оберегая водителя от штрафов и надежно защищая в случае ДТП.<br/><br/>

КАРКАМ КОМБО2 - компактное устройство, занимает под лобовым стеклом минимум места и экономит Ваши средства в сравнении с приобретением авторегистратора, радар-детектора и GPS-информера отдельно. <br/><br/>
Видеорегистратор записывает качественное видео в формате FULL HD с частотой 30 кадров в секунду и углом обзора 140°.

Встроенный радар-детектор способен определять все существующие типы радаров, в том числе Кордон, I-robot, Автодория, Стрелка и Стрелка СТ. Кроме того, есть возможность выбрать необходимую степень чувствительности радар-детектора и выключить отдельные диапазоны.<br/><br/>

Функция GPS-информер предупредит вас об уже известном местоположении фоторадаров, камер контроля скорости, возможных полицейских засадах и многом другом заранее. Эта функция возможна благодаря встроенному GPS/GLONASS-модулю.<br/><br/>

Регистратор может накладывать штамп скорости на видео. 
База камер регулярно обновляется.</p>
				</article>
		
			</section>
			<div class="separator"></div>
			<section id="ex2">
				<h2>Характеристики</h2>
				<div class="post">
				 
					<table width="700" border="0" align="center"> 
  <tbody> 
    <tr bgcolor="#efefef"> <td width="275" align="center">Тип устройства</td> <td width="415" align="center">3 в 1: авторегистратор, радар детектор и gps-информер</td> </tr>
   
    <tr bgcolor="#f9f9f9"> <td align="center">Процессор</td> <td align="center">AIT8427D</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#efefef">Матрица</td> <td align="center" bgcolor="#efefef">OV4689</td> </tr>
   
    <tr bgcolor="#f9f9f9"> <td align="center">Количество камер</td> <td align="center">1</td> </tr>
   
    <tr bgcolor="#f9f9f9"> <td align="center" bgcolor="#efefef">Разрешение видео</td> <td align="center" bgcolor="#efefef">1920x1080@30 к/с / 1920х720@30к/с / 1920х720@60к/с / 640x480@60 к/с</td> </tr>
   
    <tr bgcolor="#f9f9f9"> <td align="center" bgcolor="#f9f9f9">Разрешение в режиме фото</td> <td align="center" bgcolor="#f9f9f9">640X480[VGA] / 1280x960[1.2M] / 2048x1536[3M] / 2560x1920[5M] / 3264x2448[8M] / 4000x3000[12M] / 4352x3564[14M]</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Угол обзора</td> <td align="center">140° (по диагонали)</td> </tr>
   
    <tr bgcolor="#F9F9F9"> <td align="center" bgcolor="#f9f9f9">Поворот на 360°</td> <td align="center" bgcolor="#f9f9f9">Есть</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Объектив</td> <td align="center">1.8F</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">GPS, ГЛОНАСС</td> <td align="center" bgcolor="#f9f9f9">Есть</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">GPS-Информер</td> <td align="center">Есть</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Поддержка карт памяти</td> <td align="center" bgcolor="#f9f9f9">microSD (microSDHC) до 32 Гб</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Принимаемый диапазон</td> <td align="center">X [10475-10575] / Лазер[360] / Диапазон X: 10525 ГГц ± 50МГц / Диапазон Ка: 34,3; 34,7; 34,94 ГГц / Диапазон К 24,150 ГГц ± 125 МГц / К [24050-24250] / Ка [33400-36000]</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Отключаемые диапазоны</td> <td align="center" bgcolor="#f9f9f9">X [10475-10575] / К [24050-24250] / Ка [33400-36000] / Лазер</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Детектирование радаров</td> <td align="center">Барьер / Визир / Арена /Сокол / Радис / Кордон / Крис / Беркут / Искра</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">База GPS</td> <td align="center" bgcolor="#f9f9f9">ПОТОК-С / Стрелка-М / Стрелка-Видеоблок / Стрелка-Ст / Система АВТОДОРИЯ</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Обновление базы данных</td> <td align="center">Раз в 2-3 месяца</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Режим записи</td> <td align="center" bgcolor="#f9f9f9">Циклическая/неприрывная</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Ночной режим</td> <td align="center">ИК-напыление</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Детектор движения</td> <td align="center" bgcolor="#f9f9f9">Есть</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Корпус</td> <td align="center">Пластик</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Голосовые предупреждения</td> <td align="center" bgcolor="#f9f9f9">Есть</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Автоматическое включение / выключение записи после подачи питания</td> <td align="center">Есть</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Функции</td> <td align="center" bgcolor="#f9f9f9">Автостарт записи при включении питания / Питание от бортовой сети автомобиля 12В / GPS - информер / Детектор движения в кадре / GPS/Глонасс / Предупреждение о превышении максимально допустимой скорости / WDR (широкий динамический диапазон) / Автовыключение дисплея ао время записи через заданный промежуток времени / Штамп скорости / Отключение записи звука горячей кнопкой / Запись скорости координат на видео</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Формат записи </td> <td align="center">H.264</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Кодек сжатия</td> <td align="center" bgcolor="#f9f9f9">H.264</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Интерфейс</td> <td align="center">видеокомпозитный</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Размер LCD -экрана</td> <td align="center" bgcolor="#f9f9f9">2,4&quot;</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Рабочая температура</td> <td align="center">-20°C - 60°C</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Размер упаковки</td> <td align="center" bgcolor="#f9f9f9">170х150х70мм</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Вес</td> <td align="center">100г</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center" bgcolor="#f9f9f9">Страна производитель</td> <td align="center" bgcolor="#f9f9f9">Гонконг</td> </tr>
   
    <tr bgcolor="#efefef"> <td align="center">Производитель</td> <td align="center">КАРКАМ</td> </tr>
   </tbody>
 </table>
				</div>
		 
			</section>
			<div class="separator"></div>
			<section id="ex3">
				<h2>Обзоры</h2>
				<article class="post">
					<h3 class="post-title">Post title</h3>
					<p>1</p>
				</article>
			
			</section>
			<div class="separator"></div>
			<section id="ex4">
				<h2>Поддержка</h2>
				<div class="grid">



					<figure class="effect-zoe">
						<img src="/autoelectronica/autoregistrators/podderzhka/img/firmware2.png" alt="Carcam TinyS Support" 

width="480px;"/>
						<figcaption>


<p><a href=" /autoelectronica/autoregistrators/podderzhka/kombo2/Proshivka_i_baza_dannyh_Carcam_Kombo2_ot_07-09-2015.rar"   style="color:#fff; font-size:12px; a:hover > color:#222;">Обновление от 07.09.2015</a></p>
<p><a href=" /autoelectronica/autoregistrators/podderzhka/kombo2/database_update_kombo2.rar"   style="color:#fff; font-size:12px; a:hover > color:#222;">Обновление от 06.07.2015</a></p>

<p><a href="/doc/CarcamPlayer.zip" style="color:#fff; font-size:12px;">Carcam player</a></p>



                            
<!---------<p class="icon-links">
<a href="http://carcam.ru/carcam_tiny_s.html" style="font-size:12px;">Вернуться к странице  товара</a></p>------->
                            



							<p class="description">Официальная прошивка с базой данных для авторегистратора КАРКАМ КОМБО 2</p>



						</figcaption>			
					</figure>
















					<figure class="effect-zoe">
						<img src="/autoelectronica/autoregistrators/podderzhka/img/instructions1.png" alt="Carcam TinyS Support" 

width="480px;"/>
						<figcaption>






							<p><a href="/autoelectronica/autoregistrators/podderzhka/kombo2/Instruction_Carcam_COMBO-2.pdf" style="color:#fff; font-size:12px;">Инструкция и руководство</a></p>

							<br/>

                            
							<!---------<p class="icon-links">
								<a href="http://carcam.ru/carcam_tiny_s.html" style="font-size:12px;">Вернуться к странице 

товара</a>
								<!--<a href="#"><span class="icon-eye"></span></a></p>--->


                            



							<p class="description">Официальная инструкция для авторегистратора<br/> КАРКАМ КОМБО 2</p>



						</figcaption>			
					</figure>




</div>


			</section>
            

            <div class="separator"></div>
			<section id="ex5">
				<h2>Видео</h2>
				<article class="post">
					<h3 class="post-title">Post title</h3>
					<p>111</p>
				</article>
			</section>
		</div>
	

    
    
	</div>
<div class="static_div">111
<div class="static_price">БЛОК С ЦЕНОЙ

</div></div>







</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>