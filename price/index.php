<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->SetTitle( "Прайслист Каркам Электроникс " . date( 'Y-m-d G:i:s', time() ) );



/*
"IBLOCK_ORDER" => "Y"
"PRICE_REQUIRED" => "N",

и цену указать в настройках нормально

не забудь отключить кеширование
*/

?><?$APPLICATION->IncludeComponent(
	"zimin:pricelist", 
	".default", 
	array(
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID_IN" => array(
			0 => "153",
		),
		"IBLOCK_SECTION" => array(
			0 => "0",
		),
		"DO_NOT_INCLUDE_SUBSECTIONS" => "N",
		"SITE" => "carcam.ru",
		"COMPANY" => "\"официальный интернет магазин каркам\"",
		"SKU_NAME" => "PRODUCT_NAME",
		"FILTER_NAME" => "arrFilter",
		"MORE_PHOTO" => "MORE_PHOTO",
		"PRICE_CODE" => array(
			4 => "ТОР МРЦ",
			5 => "ТОР Опт1",
			6 => "ТОР Опт2",
			8 => "ТОР Опт3",
		),
		"PRICE_REQUIRED" => "N",
		"IBLOCK_ORDER" => "Y",
		"CURRENCIES_CONVERT" => "NOT_CONVERT",
		"LOCAL_DELIVERY_COST" => "",
		"NAME_PROP" => "0",
		"DETAIL_TEXT_PRIORITET" => "N",
		//"DISCOUNTS" => "DISCOUNT_CUSTOM",
		"DISCOUNTS" => "PRICE_ONLY",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "0",
		"CACHE_FILTER" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE_LIST" => array(
			0 => "catalog",
		),
		"IBLOCK_CATALOG" => "N",
		"IBLOCK_AS_CATEGORY" => "Y",
		"CACHE_NON_MANAGED" => "Y",
		"SKU_PROPERTY" => "PROPERTY_CML2_LINK",
		"OLD_PRICE_LIST" => "FROM_DISCOUNT",
		"PARAMS" => array(
		),
		"COND_PARAMS" => array(
		),
		"SELF_SALES_NOTES" => "N",
		"SALES_NOTES_NAMES" => "0"
	),
	false
);?>