<?
require_once( $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php' );

$lastPdfGenerationTime = COption::GetOptionInt( 'zimin.logic', 'pdfTimestamp' );

$fileName = 'Прайс-лист КАРКАМ® Электроникс™.pdf';
$priceFilePath = dirname( __FILE__ ) . '/' . $fileName;

//если pdf старее дня? или его нету, или принудительно 
if( time() - $lastPdfGenerationTime > 24*60*60 || ! file_exists( $priceFilePath ) || isset( $_GET['new'] ) )
{
	if( CModule::IncludeModule('zimin.logic') )
	{
	    $urlToPrint = dirname( 
	        ( ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' ) ? 'https://' : 'http://' ) 
	        . $_SERVER['HTTP_HOST'] . dirname( $_SERVER['PHP_SELF'] ) 
	    );

	    $result = CZiminLogic::createAndSavePdf( $urlToPrint, $priceFilePath );

	    if( $result == 'success' ) 
	    {
	    	COption::SetOptionInt( 'zimin.logic', 'pdfTimestamp', time() );
	    }
	    else
	    {
	    	header( 'Content-Type: text/html; charset=UTF-8' );
	    	echo $result;
	    	die;
	    }
	}
	else
	{
	    echo 'Для создания pdf подключите модуль zimin.logic';
	    die;
	}
}

//give file
header( 'Content-type: application/pdf' );
header( 'Content-Disposition: attachment; filename="' . $fileName . '"' );
header( 'Content-Transfer-Encoding: binary' );
readfile( $priceFilePath );

?>