<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CHTTP::SetStatus("404 Not Found");
define("ERROR_404","Y");

$APPLICATION->SetPageProperty("title", "Страница не найдена");
// $APPLICATION->AddChainItem('404');

?>
<main class="container not-found-page">
	<div class="row">
		<div class="col-xs-12">
			<div class="big404">404</div>
			<h1><?$APPLICATION->ShowTitle()?></h1>
			<p>К сожалению, страница, которую Вы запросили, не была найдена. Вы можете перейти на главную страницу или воспользоваться каталогом товаров. Если эта ошибка будет повторяться, обратитесь, пожалуйста, в службу поддержки.</p>
			<p>Вернуться на <a href="<?=SITE_DIR?>" class="link"><span class="text">главную страницу</span></a>.</p>
		</div><!-- /.col-xs-12 -->
	</div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>