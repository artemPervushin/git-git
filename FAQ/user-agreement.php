<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пользовательское соглашение");
?>
<main class="container about-page">
	<div class="row">
		<div class="col-xs-12">
<h1><?$APPLICATION->ShowTitle(false)?></h1>
<style>
  .ys_article {
      margin-left: -210px;
  }
  </style>
 Информация на сайте <u>carcam.ru</u> носит исключительно справочный характер и не является публичной офертой. 
<br />
 Если вы нашли неточность в карточке товара, просим сообщить об этом на адрес <a rel="nofollow" target="_blank" href="mailto:order@carcam.ru" >order@carcam.ru</a>. Специалисты КАРКАМ  устранят недочет в самые короткие сроки. 
<br />
 Для получения дополнительной информации о  товаре, пожалуйста, свяжитесь с менеджерами КАРКАМ по номеру 8(800)555-6-808. 
<br />
 Обращаем ваше внимание, что при оплате товара 

 через электронную систему оплаты возможны задержки поступления средств и может 

 возникнуть ситуация, когда на момент поступления средств товар отсутствует на 

 складе. Данные ситуации возникают крайне редко и мы прикладываем все усилия для 

 их решения.
<br>
<h5 onClick="history.back();" style="cursor:pointer;text-decoration:underline;">Вернуться в каталог</h5>
		</div>
	</div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>