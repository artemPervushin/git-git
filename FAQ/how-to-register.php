<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как зарегистрироваться");
?> 
<main class="container about-page">
	<div class="row">
		<div class="col-xs-12">
<h1><?$APPLICATION->ShowTitle(false)?></h1>
<style>
  .ys_article {
      margin-left: -210px;
  }
  </style>
 Для того, чтобы зарегистрироваться на сайте carcam.ru, выполните следующие условия:
<br />
 
<ol> 	
  <li>Откройте главную страницу carcam.ru.</li>
 	
  <li>Нажмите на кнопку «Войти на сайт» в верхнем левом углу.</li>
 	
  <li>Заполните форму регистрации – введите личные данные.</li>
 	
  <li>Введите проверочный код и нажмите на кнопку «Зарегистрироваться».</li>
 </ol>
 
<p>Регистрация успешно пройдена! Теперь вы можете пользоваться всеми преимуществами авторизованного пользователя на сайте <b>carcam.ru.</b></p>

<img src="/upload/for_reg.jpg"> 
<br />


 
<h5 style="cursor: pointer; text-decoration: underline;" onclick="history.back();">Вернуться в каталог</h5>
		</div>
	</div>
</main>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>