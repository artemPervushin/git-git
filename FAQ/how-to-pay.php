<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как оплатить");
?>
<main class="container about-page">
	<div class="row">
		<div class="col-xs-12">
<h1><?$APPLICATION->ShowTitle(false)?></h1>
<style>
.ys_article {
      margin-left: -210px;
  }
</style>



<p><b>Наличными курьеру</b></p>
Данный способ доступен только для жителей Москвы и Санкт - Петербурга. Вы оплачиваете товар курьеру, доставившему заказ.<br>
<img src="/upload/delivery-man2.png" width="380px">
<br><br>

<p><b>ROBOKASSA</b></p>
Самый быстрый и удобный вариант оплаты. После заполнения контактной информации, нажмите кнопку "Оплатить". Откроется окно платежной системы, где вы можете выбрать наиболее удобный для вас вариант оплаты.<br>
<img src="/upload/roboex.jpg" width="850px">
<br><br>

<p>Сбербанк России</p>
Данный вариант оплаты вы можете использовать, если по каким-то причинам вам не подходит оплата банковской картой. Сформированный счет необходимо распечатать и оплатить в любом отделении банка. Перевод занимает 1-3 дня.<br>
<br><img src="/upload/sber.png" width="350px"><br>

<h5 onClick="history.back();" style="cursor:pointer;text-decoration:underline;">Вернуться в каталог</h5>
		</div>
	</div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>