<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизованные сервисные центры КАРКАМ");
?><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизованные сервисные центры КАРКАМ");
?>    
<main class="container about-page">
	<div class="row">
		<div class="col-xs-12"> 
  <style type="text/css">
	.stfilters + .info-bubble {
		display: none;
	}

	.stfilters{
		display: block;
		max-width: 360px;
		height: 20px;
		line-height: 20px;
		cursor: pointer;
		background: #e8e8e8;
		padding: 5px;
		text-decoration: none;
		text-align:center;
		font-weight:bold;
		margin-left:0px;
		color: #333333;
		font-size:13pt;
	}
	
	.info-bubble{
		width:100%;
	}
	
	.sf-field{
		z-index:1;
	}
	.info-bubble p{
		font-size:12pt;
	}
    
	.garant_s{

		float:right;
	}
	
	.garant_s p{
		font-size:12pt;
	}

	.garant_s b{
		font-size:14pt; 
		margin-left:24%;
	}
	

	
	#serv_map{
		position: relative;
		top: 0px;
		left: 0px;
		width: 100%;
		/*height: 709px;*/
	}
	
	#map_points{
		position: absolute;
		top: 0px;
		left: 0px;
		width: 1219px
		height: 709px;
	}
	
	.map_point{
		background: url(white_k.png) 0 0 no-repeat;
		width: 20px;
		height: 22px;
		position: absolute;
		cursor: pointer;
		z-index: 1;
	}
	
	.map_point:hover{
		background: url(orange_k.png) 0 0 no-repeat;	
	}
	
	#map_points .msk{
		top: 318px;
		left: 188px;
	}
	#map_points .pit{
		top: 240px;
		left: 228px;
	}
	#map_points .bel{
		top: 436px;
		left: 277px;
	}
	#map_points .eka{
		top: 430px;
		left: 356px;
	}
	#map_points .kal{
		top: 209px;
		left: 100px;
	}
	#map_points .kem{
		top: 533px;
		left: 560px;
	}
	#map_points .kras{
		top: 431px;
		left: 67px;
	}
	#map_points .mur{
		top: 155px;
		left: 333px;
	}
	#map_points .nn{
		top: 350px;
		left: 228px;
	}
	#map_points .omsk{
		top: 515px;
		left: 450px;
	}
	#map_points .orbg{
		top: 475px;
		left: 270px;
	}
	#map_points .penz{
		top: 390px;
		left: 203px;
	}
	#map_points .perm{
		top: 390px;
		left: 336px;
	}
	#map_points .rnd{
		top: 413px;
		left: 103px;
	}	
	#map_points .sterl{
		top: 450px;
		left: 304px;
	}
	#map_points .tula{
		top: 355px;
		left: 168px;
	}
	#map_points .tum{
		top: 460px;
		left: 407px;
	}
	#map_points .sam{
		top: 431px;
		left: 250px;
	}
	#map_points .ufa{
		top: 433px;
		left: 304px;
	}
	
	#map_points .volg{
		top: 436px;
		left: 167px;
	}
	#map_points .vol{
		top: 294px;
		left: 249px;
	}
	#map_points .raz{
		top: 349px;
		left: 192px;
	}
	#map_points .almt{
		top: 589px;
		left: 419px;
		background: url(orange_k.png) 0 0 no-repeat;
	}
	
	@font-face { font-family: 'FranckerCYR-Medium';  src: local('FranckerCYR-Medium'), url('http://nomail.com.ua/files/woff/c081dbc04130ba39f418625cf0b7971d.woff') format('woff'); }
	
	.hidden_city{
		position: relative;
		top: 1px;
		z-index: 999999;
		height: 15px;
		width: auto;
		float: left;
		left: 22px;
		color: #ff8800;
		background-color: white;
		padding: 2px 5px 0 5px;
		word-wrap: normal;
		display: none;
		border-radius: 4px;
		font-size: 11px;
		text-transform: uppercase; 
		font-family: FranckerCYR-Medium;
		-webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3);
       	-moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3);
       	box-shadow:0 1px 4px rgba(0, 0, 0, 0.3);
        -moz-border-radius:4px;
	}
	
    .rnd .hidden_city{
		width: 120px;	
    }
    
    .nn .hidden_city{
		width: 123px;	
    }
    
    .pit .hidden_city{
		width: 134px;	
    }
    
    .cityes iframe{
		border:1px dashed #ff8b00;
    }
    
    .hidden_city2{
		position: relative;
		top: 1px;
		z-index: 999999;
		height: 15px;
		width: auto;
		float: left;
		left: 22px;
		color: #ff8800;
		background-color: white;
		padding: 2px 5px 0 5px;
		word-wrap: normal;
		display: block;
		border-radius: 4px;
		font-size: 11px;
		text-transform: uppercase; 
		font-family: FranckerCYR-Medium;
		-webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3);
       	-moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3);
       	box-shadow:0 1px 4px rgba(0, 0, 0, 0.3);
        -moz-border-radius:4px;
	}
	@media (max-width: 1199px){
		.map_point{
			display:none;
		}
	}
</style> <script>
	$(document).ready(function() {
	   $(".stfilters").click(function(){
		   $(this).parent().find(".info-bubble").toggle(500);
	   })
	   
	   //map points toggle function
	   $(".map_point").mouseover(function(){
	   		$(this).find(".hidden_city").show();   
	   });
	   $(".map_point").mouseleave(function(){
	   		$(".hidden_city").hide();   
	   });

	   $(".map_point").mousedown(function(){
	   		$(this).find(".hidden_city").css("color", "white");
            $(this).find(".hidden_city").css("background-color", "#ff8800");
            $(this).find(".hidden_city2").css("color", "white");
            $(this).find(".hidden_city2").css("background-color", "#ff8800");
	   });
	   
	   $(".map_point").mouseover(function(){
	   		$(this).find(".hidden_city").show();   
	   });
	   
	   $(".map_point").click(function(){
	   		$(this).find(".hidden_city").css("color", "#ff8800");
            $(this).find(".hidden_city").css("background-color", "white");
	   		$(this).find(".hidden_city2").css("color", "#ff8800");
            $(this).find(".hidden_city2").css("background-color", "white");
	   		
	   		var tab_arrow;
	   		var target;
	   		
	   		tab_arrow = $(this).attr("name");
            
	   		//$(".stfilters + .info-bubble").slideUp();
	   		$("div[name='"+tab_arrow+"_block']:hidden").toggle();
	   		
			target = $("div[name='"+tab_arrow+"_block']").offset().top;
			target -= 40;
			$('html, body').animate({scrollTop:target}, 'slow');
	   });
	   
/*	   $(".stfilters").on("click", function (event) {
	       $info = $("+ .info-bubble", this);
	       $(".stfilters + .info-bubble").not($info).slideUp(500);
		   $info.slideToggle(800);
	   });

  
	   $(document).on("click", function (event) {
	   		if ($(".sf-field").has(event.target).length === 0) $(".stfilters + .info-bubble").slideUp(500);
	   });*/
	
	});
</script>
<div id="serv_map">
 <img width="100%" src="1250x709.jpg">
	<div id="map_points">
		<div class="map_point msk" name="msk">
			<div class="hidden_city">
				Москва
			</div>
		</div>
		<div class="map_point pit" name="pit">
			<div class="hidden_city">
				Санкт-Петербург
			</div>
		</div>
		<div class="map_point eka" name="eka">
			<div class="hidden_city">
				Екатиренбург
			</div>
		</div>
		<div class="map_point kal" name="kal">
			<div class="hidden_city">
				Калининград
			</div>
		</div>
		<div class="map_point kem" name="kem">
			<div class="hidden_city">
				Кемерово
			</div>
		</div>
		<div class="map_point mur" name="mur">
			<div class="hidden_city">
				Мурманск
			</div>
		</div>
		<div class="map_point nn" name="nn">
			<div class="hidden_city">
				Нижний&nbsp;Новгород
			</div>
		</div>
		<div class="map_point omsk" name="omsk">
			<div class="hidden_city">
				Омск
			</div>
		</div>
		<div class="map_point orbg" name="orbg">
			<div class="hidden_city">
				Оренбург
			</div>
		</div>
		<div class="map_point penz" name="penz">
			<div class="hidden_city">
				Пенза
			</div>
		</div>
		<div class="map_point perm" name="perm">
			<div class="hidden_city">
				Пермь
			</div>
		</div>
		<div class="map_point rnd" name="rnd">
			<div class="hidden_city">
				Ростов-на-Дону
			</div>
		</div>
		<div class="map_point kras" name="kras">
			<div class="hidden_city">
				Краснодар
			</div>
		</div>
		<div class="map_point tum" name="tum">
			<div class="hidden_city">
				Тюмень
			</div>
		</div>
		<div class="map_point sterl" name="sterl">
			<div class="hidden_city">
				Стерлитамак
			</div>
		</div>
		<div class="map_point ufa" name="ufa">
			<div class="hidden_city">
				Уфа
			</div>
		</div>
		<div class="map_point bel" name="bel">
			<div class="hidden_city">
				Белебей
			</div>
		</div>
		<div class="map_point sam" name="sam">
			<div class="hidden_city">
				Самара
			</div>
		</div>
		<div class="map_point volg" name="volg">
			<div class="hidden_city">
				Волгоград
			</div>
		</div>
		<div class="map_point vol" name="vol">
			<div class="hidden_city">
				Вологда
			</div>
		</div>
		<div class="map_point raz" name="raz">
			<div class="hidden_city">
				Рязань
			</div>
		</div>
		<div class="map_point almt" name="almt">
			<div class="hidden_city2">
				Алматы
			</div>
		</div>
		<div class="map_point tula" name="tula">
			<div class="hidden_city">
				Тула
			</div>
		</div>
	</div>
</div>
 <!----Условия гарантийного обслуживания----------->
<div class="col-xs-12 col-sm-12  col-md-8 col-lg-8 garant_s">
 <b>Условия гарантийного обслуживания</b><br>
	<br>
	<p>
		1. Производитель гарантирует отсутствие производственных дефектов и неисправностей Оборудования, и несет ответственность по гарантийным обязательствам в соответствии с законодательством Российской Федерации.<br>
		 2. Гарантийный период исчисляется с момента приобретения Устройства у официального дилера на территории России и составляет 12 месяцев на все Устройства.<br>
		 3. В течении гарантийного срока Производитель обязуется бесплатно устранить дефекты Оборудования путем его ремонта или замены на аналогичное при условии, что дефект возник по вине Производителя. Устройство, предоставляемое для замены, может быть как новым, так и восстановленным, но в любом случае Производитель гарантирует, что его характеристики будут не хуже, чем у заменяемого Устройства.<br>
		 4. Производитель не несет ответственность за совместимость своего Программного Обеспечения с любым аппаратными и программными средствами, поставляемыми другими производителями.<br>
		 5. Ни при каких обстоятельствах Производитель не несет ответственность за любые убытки, включая потерю данных, потерю прибыли и другие случайные, последовательные или косвенные убытки, возникшие вследствие некорректных действий по инсталляции, сопровождению, эксплуатации либо связанных с производительностью, выходом из строя или временной неработоспособностью Оборудования. <br>
		6. Производитель не несет ответственность по гарантии в случае, если произведенные им тестирование и/или анализ показали, что заявленный дефект в устройстве отсутствует, либо он возник вследствие нарушения правил инсталляции или условий эксплуатации, а также любых действий, связанных с попытками добиться от Устройства выполнения функций, не заявленных Производителем.<br>
		 7. Условия гарантии не предусматривают чистку и профилактику Оборудования силами и за счет Производителя.
	</p>
 <b>ГАРАНТИЯ НЕ РАСПРОСТРАНЯЕТСЯ:</b><br>
	<br>
	<p>
		- на контрафактные устройства, приобретенные под маркой Производителя;<br>
		 - на неисправности, возникшие в результате воздействия окружающей среды (дождь, снег, гроза и т.п.);<br>
		 - наступления форс-мажорных обстоятельств (пожар, стихийные бедствия и др.) вызванные несоответствием государственным стандартам параметров питающих, телекоммуникационных, кабельных сетей;<br>
		 - на неисправности, вызванные нарушением правил транспортировки, хранения, эксплуатации или неправильной установкой;<br>
		 - на неисправности, вызванные ремонтом или модификацией Оборудования лицами, не уполномоченными на это производителем;<br>
		 - на повреждения, вызванные попаданием внутрь Устройства посторонних предметов, веществ, жидкостей, насекомых т.дом;<br>
		 - на Устройства, имеющие внешние дефекты (явные механические повреждения сколы - на корпусе и внутри устройства, сломанные разъемы);<br>
		 - гарантийные обязательства не распространяются на аксессуары (соединительные кабели, зарядные устройства, адаптеры питания, кронштейны и т.дом);
	</p>
</div>
 <!----Условия гарантийного обслуживания-----------> <!------Города------>
<div  class="col-xs-12 col-sm-12  col-md-4 col-lg-4 cityes">
	<div class="sf-field">
 <span class="stfilters">Алматы</span>
		<div class="info-bubble almt" name="almt_block">
			<br>
			<p>
				Проспект Алтынсарина, дом 37
			</p>
			<p>
				Телефон: +7 (727) 328-36-91
			</p>
			<p>
				Телефон: 317-46-37
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2907.573008123138!2d76.87253825114274!3d43.21844448834967!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x388368f8de20b4cd%3A0x28daa90b4535aaf0!2z0L_RgNC-0YHQvy4g0JDQu9GC0YvQvdGB0LDRgNC40L3QsCAzNywg0JDQu9C80LDRgtGLLCDQmtCw0LfQsNGF0YHRgtCw0L0!5e0!3m2!1sru!2sru!4v1448978244024" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Белебей</span>
		<div class="info-bubble bel" name="bel_block">
			<br>
			<p>
				Улица Ленина, дом 38
			</p>
			<p>
				Телефон: 8 (34786) 5-00-25
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2339.183904350185!2d54.10483845138648!3d54.10594342514581!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x416217d19094ac79%3A0xe1f69c191eff3016!2z0YPQuy4g0JvQtdC90LjQvdCwLCAzOCwg0JHQtdC70LXQsdC10LksINCg0LXRgdC_LiDQkdCw0YjQutC-0YDRgtC-0YHRgtCw0L0sIDQ1MjAwMA!5e0!3m2!1sru!2sru!4v1448256775244" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Волгоград</span>
		<div class="info-bubble volg" name="volg_block">
			<br>
			<p>
				Улица Калинина, дом 23
			</p>
			<p>
				Телефон: 8 (927) 511-53-20
			</p>
			<p>
				Телефон: 8 (8442) 515-320
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2633.1996158543534!2d44.5011272417286!3d48.70166548676167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x411acb4c2486302f%3A0x63ab2023263fc501!2z0YPQuy4gSE0uINCa0LDQu9C40L3QuNC90LAsIDIzLCDQktC-0LvQs9C-0LPRgNCw0LQsINCS0L7Qu9Cz0L7Qs9GA0LDQtNGB0LrQsNGPINC-0LHQuy4sIDQwMDAwMQ!5e0!3m2!1sru!2sru!4v1448977671300" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Вологда</span>
		<div class="info-bubble vol" name="vol_block">
			<br>
			<p>
				Улица Мира, дом 80
			</p>
			<p>
				Телефон: 8 (8172) 72-73-70
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2042.2041930171254!2d39.878333951626!3d59.21260172732818!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46ae982b59f8027b%3A0x42b99e89f0bff0b8!2z0YPQuy4g0JzQuNGA0LAsIDgwLCDQktC-0LvQvtCz0LTQsCwg0JLQvtC70L7Qs9C-0LTRgdC60LDRjyDQvtCx0LsuLCAxNjAwMDk!5e0!3m2!1sru!2sru!4v1448977936993" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Екатеринбург</span>
		<div class="info-bubble eka" name="eka_block">
			<br>
			<p>
				 Улица Данилы Зверева, дом 31, литер А, офис 2
			</p>
			<p>
				Телефон: 8 (343) 385-12-89
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2180.6599056409573!2d60.65110137302034!3d56.86892741273087!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43c16db860707099%3A0xf785fb5956b4daa7!2z0YPQuy4g0JTQsNC90LjQu9GLINCX0LLQtdGA0LXQstCwLCAzMdCwLCDQldC60LDRgtC10YDQuNC90LHRg9GA0LMsINCh0LLQtdGA0LTQu9C-0LLRgdC60LDRjyDQvtCx0LsuLCA2MjAxMzc!5e0!3m2!1sru!2sru!4v1447244026064" width="480" height="330" frameborder="0"   allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Калининград</span>
		<div class="info-bubble kal" name="kal_block">
			<br>
			<p>
				Улица Космонавта Пацаева, дом 5а
			</p>
			<p>
				Телефон: 8 (4012) 93-45-44
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2304.129888880433!2d20.478157951477673!3d54.7249300781611!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e33de323145265%3A0xd42508d421cafce4!2z0YPQuy4g0JrQvtGB0LzQvtC90LDQstGC0LAg0J_QsNGG0LDQtdCy0LAsIDXQkCwg0JrQsNC70LjQvdC40L3Qs9GA0LDQtCwg0JrQsNC70LjQvdC40L3Qs9GA0LDQtNGB0LrQsNGPINC-0LHQuy4sIDIzNjAyMg!5e0!3m2!1sru!2sru!4v1447245183577" width="480" height="330" frameborder="0"   allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Кемерово</span>
		<div class="info-bubble kem" name="kem_block">
			<br>
			<p>
				Улица Веры Волошиной, дом 4
			</p>
			<p>
				Телефон: 8 (3842) 900-396
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2270.230154288967!2d86.06946631662385!3d55.319066980439565!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x42d80c15e827296b%3A0xa6200ca529fdc29!2z0JLQtdGA0Ysg0JLQvtC70L7RiNC40L3QvtC5INGD0LsuLCA0LCDQmtC10LzQtdGA0L7QstC-LCDQmtC10LzQtdGA0L7QstGB0LrQsNGPINC-0LHQuy4sIDY1MDAyNA!5e0!3m2!1sru!2sru!4v1448543045985" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Краснодар</span>
		<div class="info-bubble kras" name="kras_block">
			<br>
			<p>
				Улица Березанская, дом 88
			</p>
			<p>
				Телефон: 8 (861) 274-53-78
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2819.2164475349705!2d38.997451251105005!3d45.04082936951139!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40f04f895f204845%3A0x5f2a37aad8e24a!2z0YPQuy4g0JHQtdGA0LXQt9Cw0L3RgdC60LDRjywgODgsINCa0YDQsNGB0L3QvtC00LDRgCwg0JrRgNCw0YHQvdC-0LTQsNGA0YHQutC40Lkg0LrRgNCw0LksIDM1MDAzOA!5e0!3m2!1sru!2sru!4v1448280770896" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Москва</span>
		<div class="info-bubble msk" name="msk_block">
			<br>
			<p>
				Улица Складочная 1, строение 1, вход 3, офис 1922
			</p>
			<p>
				Режим работы: с 10.00 до 18.00 (будние дни)
			</p>
			<p>
				Телефон: 8 (800) 555-6-808
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2242.56275539214!2d37.59238495151241!3d55.80082999565886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b549fde0444075%3A0xb377df7f5b5b41fb!2z0YPQuy4g0KHQutC70LDQtNC-0YfQvdCw0Y8sIDHRgTEsINCc0L7RgdC60LLQsCwgMTI3MDE4!5e0!3m2!1sru!2sru!4v1447768606244" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Мурманск</span>
		<div class="info-bubble mur" name="mur_block">
			<br>
			<p>
				Улица Гвардейская, дом 3
			</p>
			<p>
				Телефон: 8 (8152) 255-263, 255-262
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1432.7377702650374!2d33.08447295197625!3d68.95515791227632!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x44340fdcf2e32951%3A0x2b0f7f0f127baecb!2z0JPQstCw0YDQtNC10LnRgdC60LDRjyDRg9C7LiwgMywg0JzRg9GA0LzQsNC90YHQuiwg0JzRg9GA0LzQsNC90YHQutCw0Y8g0L7QsdC7LiwgMTgzMDMy!5e0!3m2!1sru!2sru!4v1447245793248" width="480" height="330" frameborder="0"   allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Нижний Новгород</span>
		<div class="info-bubble nn" name="nn_block">
			<br>
			<p>
				Площадь Максима Горького, дом 6 (вход с Улица Новая)
			</p>
			<p>
				Телефон: 8 (831) 2-780-777
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2212.9674956147446!2d43.99062450152922!3d56.31313415600858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4151d5b21647729d%3A0x9ed5c2981fd841ef!2z0L_Quy4g0JzQsNC60YHQuNC80LAg0JPQvtGA0YzQutC-0LPQviwgNiwg0J3QuNC20L3QuNC5INCd0L7QstCz0L7RgNC-0LQsINCd0LjQttC10LPQvtGA0L7QtNGB0LrQsNGPINC-0LHQuy4sIDYwMzAwMA!5e0!3m2!1sru!2sru!4v1447246018875" width="480" height="330" frameborder="0"   allowfullscreen></iframe> <br>
 <br>
			<p>
				Улица Родионова, дом 189/24
			</p>
			<p>
				Телефон: 8 (831) 2-207-077
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2213.4234232053172!2d44.07625115152884!3d56.30526515661938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4151d4c11b62eae7%3A0x2d9a421196cbe53a!2z0YPQuy4g0KDQvtC00LjQvtC90L7QstCwLCAxODkvMjQsINCd0LjQttC90LjQuSDQndC-0LLQs9C-0YDQvtC0LCDQndC40LbQtdCz0L7RgNC-0LTRgdC60LDRjyDQvtCx0LsuLCA2MDMxNjM!5e0!3m2!1sru!2sru!4v1447335245942" width="480" height="330" frameborder="0"   allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Омск</span>
		<div class="info-bubble omsk" name="omsk_block">
			<br>
			<p>
				Улица Декабристов, дом 45, офис 201
			</p>
			<p>
				Телефон: 8 (3812) 53-45-61
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2289.528576457926!2d73.39098675141602!3d54.98136595859268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43aafe0c768b6019%3A0x9ad6822a66160951!2z0YPQuy4g0JTQtdC60LDQsdGA0LjRgdGC0L7QsiwgNDUsINCe0LzRgdC6LCDQntC80YHQutCw0Y8g0L7QsdC7LiwgNjQ0MDI0!5e0!3m2!1sru!2sru!4v1448257515132" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Оренбург</span>
		<div class="info-bubble orbg" name="orbg_block">
			<br>
			<p>
				Улица Салмышская, дом 45
			</p>
			<p>
				Телефон: 8 (3532) 50-70-30
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2466.0602852808734!2d55.16374795131142!3d51.823329195275484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x417bf659561d9745%3A0xd7675c4be470953a!2z0YPQuy4g0KHQsNC70LzRi9GI0YHQutCw0Y8sIDQ1LCDQntGA0LXQvdCx0YPRgNCzLCDQntGA0LXQvdCx0YPRgNCz0YHQutCw0Y8g0L7QsdC7LiwgNDYwMDUy!5e0!3m2!1sru!2sru!4v1448257170756" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Пенза</span>
		<div class="info-bubble penz" name="penz_block">
			<br>
			<p>
				Улица Володарского, дом 32
			</p>
			<p>
				Телефон: 8 (8412) 20-60-90
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2390.516033167314!2d45.010037316580195!3d53.19066097994669!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41410059ca9d28fd%3A0x2f4e329c28796670!2z0YPQuy4g0JLQvtC70L7QtNCw0YDRgdC60L7Qs9C-LCAzMiwg0J_QtdC90LfQsCwg0J_QtdC90LfQtdC90YHQutCw0Y8g0L7QsdC7LiwgNDQwMDAw!5e0!3m2!1sru!2sru!4v1448541344099" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Пермь</span>
		<div class="info-bubble perm" name="perm_block">
			<br>
			<p>
				Улица Маршала Рыбалко, дом 84а
			</p>
			<p>
				Телефон: 8 (342) 282-73-49
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2113.8125018286023!2d55.96032031601932!3d58.00794298121614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43e8b9fb448164ab%3A0x633680fc575e7be6!2z0YPQuy4g0JzQsNGA0YjQsNC70LAg0KDRi9Cx0LDQu9C60L4sIDg00JAsINCf0LXRgNC80YwsINCf0LXRgNC80YHQutC40Lkg0LrRgNCw0LksIDYxNDExMw!5e0!3m2!1sru!2sru!4v1448541079340" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Ростов-на-Дону</span>
		<div class="info-bubble rnd" name="rnd_block">
			<br>
			<p>
				Проспект Шолохова, дом 7
			</p>
			<p>
				Телефон: 8 (863) 2-907-900
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2708.8990940287426!2d39.74293195125162!3d47.23811962125033!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40e3b9bfb27a04c1%3A0x6d2c7b86e58e6960!2z0L_RgC4g0KjQvtC70L7RhdC-0LLQsCwgNywg0KDQvtGB0YLQvtCyLdC90LAt0JTQvtC90YMsINCg0L7RgdGC0L7QstGB0LrQsNGPINC-0LHQuy4sIDM0NDAxOQ!5e0!3m2!1sru!2sru!4v1447246061129" width="480" height="330" frameborder="0"   allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Рязань</span>
		<div class="info-bubble raz" name="raz_block">
			<br>
			<p>
				Улица Большая, дом 94, офис 1
			</p>
			<p>
				Телефон: 8 (4912) 99-60-27
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2310.3011933118014!2d39.80503635147419!3d54.616302286432166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4149e172e60c6221%3A0x728876bfdf74e695!2z0YPQuy4g0JHQvtC70YzRiNCw0Y8sIDk0LCDQoNGP0LfQsNC90YwsINCg0Y_Qt9Cw0L3RgdC60LDRjyDQvtCx0LsuLCAzOTAwMzc!5e0!3m2!1sru!2sru!4v1448978078286" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Самара</span>
		<div class="info-bubble sam" name="sam_block">
			<br>
			<p>
				Улица Советской Армии, дом 180/1, офис 809а
			</p>
			<p>
				Телефон: 8 (846) 273-49-00, +7 (927) 206-76-37
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2388.8303463286343!2d50.19910695143007!3d53.22089019170356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41661eb282fd782d%3A0x9653a8491779b6b7!2z0YPQuy4g0KHQvtCy0LXRgtGB0LrQvtC5INCQ0YDQvNC40LgsIDE4MNC6MSwg0KHQsNC80LDRgNCwLCDQodCw0LzQsNGA0YHQutCw0Y8g0L7QsdC7LiwgNDQzMDkw!5e0!3m2!1sru!2sru!4v1447246166697" width="480" height="330" frameborder="0"   allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Санкт-Петербург</span>
		<div class="info-bubble pit" name="pit_block">
			<br>
			<p>
				Улица Марата, дом 43
			</p>
			<p>
				Телефон: 8 (812) 404-67-88
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1999.4406701259866!2d30.348565951650357!3d59.92482967008558!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x469631ae30062b5b%3A0x7cd918a8697562d6!2z0YPQuy4g0JzQsNGA0LDRgtCwLCA0Mywg0KHQsNC90LrRgi3Qn9C10YLQtdGA0LHRg9GA0LMsIDE5MTAwMg!5e0!3m2!1sru!2sru!4v1447246108989" width="480" height="330" frameborder="0"   allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Стерлитамак</span>
		<div class="info-bubble sterl" name="sterl_block">
			<br>
			<p>
				Проспект Октября, дом 59
			</p>
			<p>
				Телефон: 8 (3473) 24-38-02
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2365.9993298792706!2d55.91186635144283!3d53.629151361093385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43d811df688d4fbb%3A0xdc9a78d2410112b7!2z0L_RgC4g0J7QutGC0Y_QsdGA0Y8sIDU5LCDQodGC0LXRgNC70LjRgtCw0LzQsNC6LCDQoNC10YHQvy4g0JHQsNGI0LrQvtGA0YLQvtGB0YLQsNC9LCA0NTMxMjA!5e0!3m2!1sru!2sru!4v1448978274136" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Тула</span>
		<div class="info-bubble tula" name="tula_block">
			<br>
			<p>
				Проспект Ленина, дом 97
			</p>
			<p>
				Телефон: 8 (4872) 33-54-88
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2335.4756249118814!2d37.59274131660018!3d54.17165198016027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41343ff17cc70533%3A0x230c075012cabaa8!2z0L_RgC4g0JvQtdC90LjQvdCwLCA5Nywg0KLRg9C70LAsINCi0YPQu9GM0YHQutCw0Y8g0L7QsdC7LiwgMzAwMDEy!5e0!3m2!1sru!2sru!4v1448543523545" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Тюмень</span>
		<div class="info-bubble tum" name="tum_block">
			<br>
			<p>
				Улица Червишевский тракт, дом 64/2
			</p>
			<p>
				Телефон: 8 (3452) 50-06-26
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2165.3880871842066!2d65.52012095155605!3d57.13042689227511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43bbe1a1ccc5a5cf%3A0x10c55fef7e47fec8!2z0YPQuy4g0KfQtdGA0LLQuNGI0LXQstGB0LrQuNC5INGC0YDQsNC60YIsIDY00LoyLCDQotGO0LzQtdC90YwsINCi0Y7QvNC10L3RgdC60LDRjyDQvtCx0LsuLCA2MjUwMzI!5e0!3m2!1sru!2sru!4v1447246219577" width="480" height="330" frameborder="0"   allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="sf-field">
 <span class="stfilters">Уфа</span>
		<div class="info-bubble ufa" name="ufa_block">
			<br>
			<p>
				Улица Мубарякова, дом 3
			</p>
			<p>
				Телефон: 8 (347) 246-09-09
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1152.7968233836687!2d55.996963087027915!3d54.69917809351553!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43d93bbdf56f8103%3A0xb0c375a92075cb5d!2z0YPQuy4g0JzRg9Cx0LDRgNGP0LrQvtCy0LAsIDMsINCj0YTQsCwg0KDQtdGB0L8uINCR0LDRiNC60L7RgNGC0L7RgdGC0LDQvSwgNDUwMDky!5e0!3m2!1sru!2sru!4v1448255902182" width="480" height="330" frameborder="0" allowfullscreen></iframe> <br>
			<br>
			<p>
				Улица Революционная, дом 97
			</p>
			<p>
				Телефон: 8 (347) 294-74-64
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1628.8952968444567!2d55.96102930300123!3d54.734148055959416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43d93a4081c73805%3A0xf3be0418fcf59083!2z0KDQtdCy0L7Qu9GO0YbQuNC-0L3QvdCw0Y8g0YPQuy4sIDk3LCDQo9GE0LAsINCg0LXRgdC_LiDQkdCw0YjQutC-0YDRgtC-0YHRgtCw0L0sIDQ1MDAwNQ!5e0!3m2!1sru!2sru!4v1448279854923" width="480" height="330" frameborder="0" allowfullscreen></iframe> <br>
			<br>
			<p>
				Улица Айская, дом 75
			</p>
			<p>
				Телефон: 8 (347) 29-28-111
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2303.6672063718443!2d55.97729645140756!3d54.733068377541116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43d93a3a4fa30ef7%3A0x2dca802dee41b1a0!2z0JDQudGB0LrQsNGPINGD0LsuLCA3NSwg0KPRhNCwLCDQoNC10YHQvy4g0JHQsNGI0LrQvtGA0YLQvtGB0YLQsNC9!5e0!3m2!1sru!2sru!4v1448256014670" width="480" height="330" frameborder="0" allowfullscreen></iframe> <br>
			<br>
			<p>
				Улица Сагита Агиша, дом 1/3
			</p>
			<p>
				Телефон: 8 (800) 700-29-28
			</p>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2303.859231253204!2d55.99223935140748!3d54.72969087779844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43d93a2e0c237e31%3A0xf291e2ffa67acc44!2z0YPQuy4g0KHQsNCz0LjRgtCwINCQ0LPQuNGI0LAsIDHQujMsINCj0YTQsCwg0KDQtdGB0L8uINCR0LDRiNC60L7RgNGC0L7RgdGC0LDQvSwgNDUwMDk3!5e0!3m2!1sru!2sru!4v1448256274278" width="480" height="330" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<br>
</div>
 <!------Города------>
		</div>
	</div>
</main>


















  <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>