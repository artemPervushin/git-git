<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сервис");
?><main class="container about-page">
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1><?$APPLICATION->ShowTitle(false)?></h1>
		 <style>
.ys_article {
      margin-left: -210px;}

    hr {
        display: block;
        width: 100%;
        -webkit-margin-before: 0.5em;
        -webkit-margin-after: 0.5em;
        -webkit-margin-start: auto;
        -webkit-margin-end: auto;
        border-style: inset;
        border-width: 1px;
        margin-left:0px;
    }
    ul {
        margin: 0px;
        padding: 0px;
    }
    .spanh{
        display:none;
    }
    .spanTitle{
        display: block;
        width: 100%;
        height: 20px;
        line-height: 20px;
        cursor: pointer;
        background: #ACACAC;
        padding: 5px;
        text-decoration: none;
        text-align:center;
        font-weight:bold;
        margin-left:0px;
        color: #ffffff;
    }
    

#page {

	height:auto;
	margin:0 auto;
	}

@font-face {
  font-family: 'fontello';
  src: url('../font/fontello.eot?65453789');
  src: url('../font/fontello.eot?65453789#iefix') format('embedded-opentype'),
       url('../font/fontello.woff?65453789') format('woff'),
       url('../font/fontello.ttf?65453789') format('truetype'),
       url('../font/fontello.svg?65453789#fontello') format('svg');
  font-weight: normal;
  font-style: normal;
}
/* Chrome hack: SVG is rendered more smooth in Windozze. 100% magic, uncomment if you need it. */
/* Note, that will break hinting! In other OS-es font will be not as sharp as it could be */
/*
@media screen and (-webkit-min-device-pixel-ratio:0) {
@font-face {
  font-family: 'fontello';
  src: url('../font/fontello.eot?65453789');
  src: url('../font/fontello.eot?65453789#iefix') format('embedded-opentype'),
       url('../font/fontello.woff?65453789') format('woff'),
       url('../font/fontello.ttf?65453789') format('truetype'),
       url('../font/fontello.svg?65453789#fontello') format('svg');
  font-weight: normal;
  font-style: normal;
}
/* Chrome hack: SVG is rendered more smooth in Windozze. 100% magic, uncomment if you need it. */
/* Note, that will break hinting! In other OS-es font will be not as sharp as it could be */
/*
@media screen and (-webkit-min-device-pixel-ratio:0) {
  @font-face {
    font-family: 'fontello';
    src: url('../font/fontello.svg?65453789#fontello') format('svg');
  }
}
*/
 
 [class^="icon-"]:before, [class*=" icon-"]:before {
  font-family: "fontello";
  font-style: normal;
  font-weight: normal;
  speak: none;
 
  display: inline-block;
  text-decoration: inherit;
  width: 1em;
  margin-right: .2em;
  text-align: center;
  /* opacity: .8; */
 
  /* For safety - reset parent styles, that can break glyph codes*/
  font-variant: normal;
  text-transform: none;
 
  /* fix buttons height, for twitter bootstrap */
  line-height: 1em;
 
  /* Animation center compensation - margins should be symmetric */
  /* remove if not needed */
  margin-left: .2em;
 
  /* you can be more comfortable with increased icons size */
  /* font-size: 120%; */
 
  /* Font smoothing. That was taken from TWBS */
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
 
  /* Uncomment for 3D effect */
  /* text-shadow: 1px 1px 1px rgba(127, 127, 127, 0.3); */
}
 
.icon-phone-squared:before { content: '\e800'; } /* 'о Ђ' */
.icon-docs:before { content: '\e801'; } /* 'о Ѓ' */
.icon-doc-text:before { content: '\e802'; } /* 'о ‚' */
.icon-angle-double-right:before { content: '\e803'; } /* 'о ѓ' */
.icon-angle-double-up:before { content: '\e804'; } /* 'о „' */
.icon-angle-double-down:before { content: '\e805'; } /* 'о …' */
.icon-angle-double-left:before { content: '\e806'; } /* 'о †' */
.icon-chat-empty:before { content: '\e807'; } /* 'о ‡' */
.icon-chat:before { content: '\e808'; } /* 'о €' */
.icon-attach:before { content: '\e809'; } /* 'о ‰' */
.icon-phone:before { content: '\e80a'; } /* 'о Љ' */
.icon-help:before { content: '\e80b'; } /* 'о ‹' */
.icon-floppy:before { content: '\e80c'; } /* 'о Њ' */
.icon-mail:before { content: '\e80d'; } /* 'о Ќ' */
.icon-mail-alt:before { content: '\e80e'; } /* 'о Ћ' */
.icon-basket:before { content: '\e80f'; } /* 'о Џ' */
.icon-cloud:before { content: '\e810'; } /* 'о ђ' */
.icon-icq:before { content: '\e811'; } /* 'о ‘' */
</style> <script type="text/javascript">// <![CDATA[

// ]]></script> <!--  BODY{font: normal 10pt Arial,sans-serif;COLOR: #CC6600;text-align:left; background:#fff; } #tabbed_sub li{float:left;margin:8px;} A{COLOR: #CC6600; TEXT-DECORATION: none;} A:hover{COLOR: #000;TEXT-DECORATION: none;}  --> <script type="text/javascript">// <![CDATA[
function setLink(i){a[i].onclick=function(){div[i].style.display="block";for(var j=0;j<div.length;j++){if(i!=j){div[j].style.display="none";}}
    return false;}}
function initTabs(){var tabs=document.getElementById("tabbed_sub");var links=tabs.getElementsByTagName("a");div=[];a=[];for(var i=0;i<links.length;i++){div[i]=document.getElementById(links[i].href.substr(links[i].href.indexOf("#")+1));if(i!=0){div[i].style.display="none";}else{div[i].style.display="block";}
    a[i]=links[i];setLink(i)}}
function addLoadEvent(func){var oldonload=BX.ajax.onload_113535;if(typeof BX.ajax.onload_113535!='function'){BX.ajax.onload_113535=func;}else{BX.ajax.onload_113535=function(){if(oldonload){oldonload();}
    func();}}}
addLoadEvent(function(){initTabs();});
//


function span_cl(th){
    var id=th.getAttribute("id");
    var el=document.getElementById("city"+id);
    if(el.className=="spans")el.className="spanh"; else el.className="spans";


}
// ]]></script>
		<p style="color: #a5a5a5; font-size: 20px; line-height: 18px; text-align: justify;">
			 В случае необходимости ремонта или гарантийного обслуживания Вы можете:
		</p>
		<div id="page">
			 <!-------------------------------------->
			<div id="left-side" class="col-xs-12 col-sm-7 col-md-8 col-lg-8" style="float: left; margin-bottom: 30px;">
				 <!--------------------------------------> <style type="text/css">

#steps {padding: 0; width:100%; min-height:250px; font-size: 18px; margin-top:-3px; }
#steps ul {list-style: none; margin: 0; padding: 0; line-height:60px;}
#steps li {display:block; background-color:#e8e8e8; width: 100%;
   min-height:60px; margin:3px 0; text-align:center;    font-size: 100%;}
	
#steps a:first-child{
   float: left;
   width:100%;
   min-height: 60px;
   text-decoration: none;
   text-align:center;
   background-color: #ff8000;

  /* border:1px solid #ed1600;*/
   text-shadow: 1px 1px 2px red; 
   color:#FFFFFF;
}
#steps li p {
    font-size: 1em;
    margin: 0;
    line-height: 25px;
    padding:15px 0;
}
#steps a:hover {background-color:#ff6600; text-shadow: 1px 1px 2px #D40306;}
</style>
				<div id="steps">
					<ul>
						<li><a href="/support/tickets/">
						<p>
							 1. Обратиться в on-line службу тех поддержки.
						</p>
 </a></li>
						<li>
						<p>
							 2. Обратиться по месту покупки.
						</p>
 </li>
						<li><a href="/service/location.php" target="_blank">
						<p>
							 3. Обратиться в региональный авторизированный сервисный центр КАРКАМ
						</p>
 </a></li>
						<li style="line-height: 18px !important;">
						<p>
							 4. Воспользоваться нашей Европейской моделью гарантии, <br>
							 заполнив форму заявки на гарантийное обслуживание <i class=" icon-angle-double-right"></i>
						</p>
 </li>
					</ul>
				</div>
				 <!--------------------------> <script>
 function collapsElement(id) {
 if ( document.getElementById(id).style.display != "none" ) {
 document.getElementById(id).style.display = 'none';
 }
 else {
 document.getElementById(id).style.display = '';
 }
 }
 </script>
				<div style="width: 100%; min-height: 40px; line-height: 40px; text-align: center; font-size: 18px;background-color: #f4f4f4;">
 <a href="javascript:collapsElement('identifikator')" rel="nofollow">Условия Европейской модели гарантии <i class="icon-angle-double-down"></i></a>
					<div id="identifikator" style="display: none; width: 96%; height: auto; border: 1px solid #ff8000; z-index: 1000; position: absolute; padding: 20px; font-size: 12px; line-height: 14px; list-style-position: inside; text-align: left !important; background-color: #ffffff;">
						<p>
						</p>
						<div style="text-align: center;">
 <strong>Правила и условия обслуживания в регионах.</strong>
						</div>
						<p>
						</p>
						<p>
							 Компания Каркам ввела европейскую модель гарантийного обслуживания. Это значит, что в регионах, в которых нет сервисного центра, пользователь может отправить неисправное устройство в центральный сервисный центр, по адресу: <br>
 <strong>Москва, ул. Складочная, д.1, с.1, офис 1922</strong>
						</p>
						<p>
							 Вы можете абсолютно бесплатно отправить в гарантийный ремонт продукцию производства компании Каркам.
						</p>
						<p>
							 Обращаем ваше внимание, что в случае не гарантийного ремонта, вследствие нарушения клиентом условий эксплуатации устройства, потребитель обязан оплатить стоимость пересылки в обе стороны и предварительно согласованную стоимость ремонта. До оплаты полной стоимости оказанных услуг отправка клиенту не производится. <br>
 <strong>Для оказания сервисных услуг клиентом должна быть предоставлена следующая информация:</strong>
						</p>
						<ul>
							<li>Полностью заполненный<strong> гарантийный талон</strong> в котором указана: модель устройства, серийный номер, дата покупки, указание фирмы-продавца, адрес и телефон;</li>
							<li>Разборчиво заполненный <strong>адресный бланк</strong> с контактной информацией;</li>
						</ul>
						<p>
						</p>
						<p>
							 При отправке устройства в сервисный центр потребительдолжен заполнить форму-заявку, справа <i class="fa fa-arrow-right"></i> <br>
							 Пожалуйста, не забудьте приложить к ней <strong>фотографию или скан-копию гарантийного талона</strong>. После ее заполнения, в течение 3 рабочих дней с вами свяжется сотрудник местного отделения Почты России &nbsp;(EMS ПОЧТА РОССИИ) для согласования деталей (дата/время приезда курьера)&nbsp;по отправке вашего устройства. <br>
							 Отправка осуществляется за счёт отправителя. <br>
							 Обращаем ваше внимание на то, что указан примерный срок обработки заявки, который может отличаться в зависимости от вашего региона.
						</p>
						<p>
 <strong>ВНИМАНИЕ!</strong> После покупки необходимо сохранять оригинальную упаковку устройства, в течение всего срока гарантии. Необходимо использовать ее для отправки устройства в сервисный центр.
						</p>
						<p>
 <strong>ФОРМА ОБРАЩЕНИЯ:</strong>
						</p>
						<p>
							 Ф.И.О. (обязательно) <br>
							 Ваш E-Mail (обязательно) <br>
							 Город <br>
							 Название организации <br>
							 Почтовый индекс: <br>
							 Улица, дом, квартира <br>
							 Телефон: <br>
							 Модель <br>
							 Серийный номер <br>
							 Описание проблемы:
						</p>
						<p>
							 Если у вас возникли вопросы, вы можете связаться с нами по телефону 8 (800) 555-6-808 (бесплатный звонок по России) или электронной почте: <a href="mailto:service@carcam.ru">service@carcam.ru</a>; <a name="_GoBack"></a><a href="mailto:service-msk@carcam.ru">service-msk@carcam.ru</a>
						</p>
						<p>
 <strong>Получатель:</strong> <br>
							 ООО "НИБИРУ ТЕХНОЛОДЖИ". <br>
							 127018, Москва <br>
							 Ул. Складочная, дом 1, строение 1, вход 3, офис 1922 <br>
							 Режим работы: 10.00 до 18.00 (будние дни) <br>
							 8 (800) 555-6-808 <br>
							 № договора - 7803482Ф
						</p>
					</div>
				</div>
				 <!------------------------------>
				<div class="service" style="margin: 0px 22px 10px 0px; float: left;">
					<p style="color: #333333; font-size: 14px; line-height: 18px; margin-top: 25px;">
 <strong>Сервис Центр КАРКАМ</strong> <br>
						 127018,<strong>Москва</strong> ул. Складочная 1 строение 1 Вход 3 офис 1922 <br>
 <strong>Режим работы: с 10.00 до 18.00 </strong>будние дни <br>
 <i class=" icon-phone-squared"></i> 8(800) 555-6-808
					</p>
				</div>
				 <!--service---->
				<div id="map" style="clear: both;">
					 <?$APPLICATION->IncludeComponent(
	"bitrix:map.google.view",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(),
		"INIT_MAP_TYPE" => "ROADMAP",
		"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.801722499993;s:10:\"google_lon\";d:37.590845365051;s:12:\"google_scale\";i:14;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:17:\"СЦ КАРКАМ\";s:3:\"LON\";d:37.594785690308;s:3:\"LAT\";d:55.800557328213;}}}",
		"MAP_HEIGHT" => "465",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array()
	)
);?>
				</div>
				 <!---entry---->
			</div>
			 <!---left-side----> <!----------------------------------------------------------------------------------------->
			<div id="warranty" class="col-xs-12 col-sm-5 col-md-4 col-lg-3" style=" height: auto; float: right; font-size: 8pt; font-weight: normal; padding: 15px; background-color: #f8f8f8;">
				<div style="text-align: center;">
					<p style="color: #a5a5a5; font-size: 16px; line-height: 14pt;">
						 Форма заявки на гарантийное обслуживание
					</p>
				</div>
				 <?$APPLICATION->IncludeComponent(
	"api:main.feedback",
	".default",
	Array(
		"ADMIN_EVENT_MESSAGE_ID" => array(0=>"81",),
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BRANCH_ACTIVE" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"COUNT_INPUT_FILE" => "1",
		"CSS_MODAL_CONTENT" => "<p>Модуль <b>расширенная форма обратной связи битрикс с вложением</b> предназначен для отправки сообщений с сайта, включая код CAPTCHA и скрытую защиту от спама, и отличается от стандартной формы Битрикс:
<br> - <b>отправкой файлов вложениями или ссылками на файл</b>,
<br> - <b>встроенным конструктором форм,</b>
<br> - скрытой защитой от спама,
<br> - работой нескольких форм на одной странице,
<br> - встроенным фирменным валидатором форм,
<br> - 4 встроенными WEB 2.0 шаблонами,
<br> - дополнительными полями со своим именованием,
<br> - и многими другими функциями...
<br>подробнее читайте на странице модуля <a id=\"bxid_472347\" >Расширенная форма обратной связи</a>
</p>",
		"CSS_MODAL_FOOTER" => "<a id=\"bxid_508061\" >Разработка модуля</a> - Тюнинг Софт",
		"CSS_MODAL_HEADER" => "Информация",
		"CUSTOM_FIELDS" => array(0=>"Модель@input@text@required",1=>"Серийный номер@input@text@required",2=>"Заявленная неисправность@input@text@required",3=>"ФИО@input@text@required",4=>"Контактный телефон@input@text@required",5=>"Дата продажи@input@date@required",6=>"Адрес отправителя (место приезда курьера)@input@text@required",7=>"Место продажи@input@text@required",8=>"Тип устройства@input@text@required",9=>"Комплектность@input@text@required",10=>"Адрес электронной почты@input@email@required",11=>"Примечания@textarea",12=>"",),
		"DELETE_FILES_AFTER_UPLOAD" => "N",
		"DISPLAY_FIELDS" => array(),
		"EMAIL_TO" => "check@carcam.ru, service@carcam.ru, service-msk@carcam.ru",
		"FILE_DESCRIPTION" => array(0=>"Загрузить сканкопию гарантийного талона   ",1=>"",),
		"FILE_EXTENSIONS" => "txt, rtf, doc, docx, xls, xlsx, ods, odt, jpg, jpeg, bmp, png",
		"FORM_STYLE" => "text-align:left;",
		"FORM_STYLE_DIV" => "overflow:hidden;padding:5px;",
		"FORM_STYLE_INPUT" => "width:250px;padding:3px 5px;",
		"FORM_STYLE_LABEL" => "font-weight:bold;",
		"FORM_STYLE_SELECT" => "min-width:200px;padding:3px 5px;",
		"FORM_STYLE_SUBMIT" => "",
		"FORM_STYLE_TEXTAREA" => "padding:3px 5px;width:98%;height:20px;",
		"FORM_STYLE_TITLE" => "",
		"FORM_SUBMIT_CLASS" => "",
		"FORM_SUBMIT_ID" => "",
		"FORM_SUBMIT_VALUE" => "Отправить",
		"FORM_TITLE" => "",
		"FORM_TITLE_LEVEL" => "1",
		"HIDE_FIELD_NAME" => "N",
		"HIDE_FORM_AFTER_SEND" => "N",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_FORM_STYLER" => "N",
		"INCLUDE_JQUERY" => "N",
		"INCLUDE_PLACEHOLDER" => "Y",
		"INCLUDE_PRETTY_COMMENTS" => "N",
		"INSTALL_IBLOCK" => "N",
		"MAX_FILE_SIZE" => "10000",
		"OK_TEXT" => "Спасибо, ваше обращение #TICKET_ID# принято.",
		"PHP_ANTISPAM_LEVEL" => "1",
		"REPLACE_FIELD_FROM" => "Y",
		"REQUIRED_FIELDS" => array(),
		"SCROLL_TO_FORM_IF_MESSAGES" => "N",
		"SCROLL_TO_FORM_SPEED" => "1000",
		"SEND_ATTACHMENT" => "Y",
		"SET_ATTACHMENT_REQUIRED" => "N",
		"SHOW_ATTACHMENT_EXTENSIONS" => "N",
		"SHOW_CSS_MODAL_AFTER_SEND" => "N",
		"SHOW_FILES" => "Y",
		"TITLE_DISPLAY" => "N",
		"UNIQUE_FORM_ID" => "5450c534c60af",
		"UPLOAD_FOLDER" => "/upload/feedback",
		"USER_AUTHOR_ADRESS" => "",
		"USER_AUTHOR_CITY" => "",
		"USER_AUTHOR_EMAIL" => "",
		"USER_AUTHOR_FAX" => "",
		"USER_AUTHOR_FIO" => "Модель",
		"USER_AUTHOR_ICQ" => "",
		"USER_AUTHOR_LAST_NAME" => "",
		"USER_AUTHOR_MAILBOX" => "",
		"USER_AUTHOR_MESSAGE" => "",
		"USER_AUTHOR_MESSAGE_THEME" => "",
		"USER_AUTHOR_NAME" => "",
		"USER_AUTHOR_NOTES" => "",
		"USER_AUTHOR_PERSONAL_MOBILE" => "",
		"USER_AUTHOR_PERSONAL_PHONE" => "",
		"USER_AUTHOR_POSITION" => "",
		"USER_AUTHOR_PROFESSION" => "",
		"USER_AUTHOR_SECOND_NAME" => "",
		"USER_AUTHOR_SKYPE" => "",
		"USER_AUTHOR_STATE" => "",
		"USER_AUTHOR_STREET" => "",
		"USER_AUTHOR_WORK_CITY" => "",
		"USER_AUTHOR_WORK_COMPANY" => "",
		"USER_AUTHOR_WORK_MAILBOX" => "",
		"USER_AUTHOR_WORK_PHONE" => "",
		"USER_AUTHOR_WORK_WWW" => "",
		"USER_AUTHOR_WWW" => "",
		"USER_EVENT_MESSAGE_ID" => array(),
		"USE_CAPTCHA" => "Y",
		"USE_HIDDEN_PROTECTION" => "Y",
		"USE_PHP_ANTISPAM" => "N",
		"UUID_LENGTH" => "10",
		"UUID_PREFIX" => "",
		"VALIDTE_REQUIRED_FIELDS" => "N"
	)
);?>
			</div>
		</div>
	</div>
</div>
 </main>
<!------- page -----------><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>