<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контактная информация");
$APPLICATION->SetTitle("Контакты");
?><main class="container about-page">
<h1><?$APPLICATION->ShowTitle()?></h1>
 <script type="text/javascript" src="/upload/jquery.lightbox-0.5.js"></script> <script type="text/javascript">$(document).ready(function() { jQuery(".li a").lightBox(); });</script> <style>
		/**
		 * jQuery lightBox plugin
		 * This jQuery plugin was inspired and based on Lightbox 2 by Lokesh Dhakar (http://www.huddletogether.com/projects/lightbox2/)
		 * and adapted to me for use like a plugin from jQuery.
		 * @name jquery-lightbox-0.5.css
		 * @author Leandro Vieira Pinho - http://leandrovieira.com
		 * @version 0.5
		 * @date April 11, 2008
		 * @category jQuery plugin
		 * @copyright (c) 2008 Leandro Vieira Pinho (leandrovieira.com)
		 * @license CCAttribution-ShareAlike 2.5 Brazil - http://creativecommons.org/licenses/by-sa/2.5/br/deed.en_US
		 * @example Visit http://leandrovieira.com/projects/jquery/lightbox/ for more informations about this jQuery plugin
		 */
		#jquery-overlay {
			position: absolute;
			top: 0;
			left: 0;
			z-index: 90;
			width: 100%;
			height: 500px;
		}
		#jquery-lightbox {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			z-index: 100;
			text-align: center;
			line-height: 0;
		}
		#jquery-lightbox a img { border: none; }
		#lightbox-container-image-box {
			position: relative;
			background-color: #fff;
			width: 250px;
			height: 250px;
			margin: 0 auto;
		}
		#lightbox-container-image { padding: 10px; }
		#lightbox-loading {
			position: absolute;
			top: 40%;
			left: 0%;
			height: 25%;
			width: 100%;
			text-align: center;
			line-height: 0;
		}
		#lightbox-nav {
			position: absolute;
			top: 0;
			left: 0;
			height: 100%;
			width: 100%;
			z-index: 10;
		}
		#lightbox-container-image-box > #lightbox-nav { left: 0; }
		#lightbox-nav a { outline: none;}
		#lightbox-nav-btnPrev, #lightbox-nav-btnNext {
			width: 49%;
			height: 100%;
			zoom: 1;
			display: block;
		}
		#lightbox-nav-btnPrev { 
			left: 0; 
			float: left;
		}
		#lightbox-nav-btnNext { 
			right: 0; 
			float: right;
		}
		#lightbox-container-image-data-box {
			font: 10px Verdana, Helvetica, sans-serif;
			background-color: #fff;
			margin: 0 auto;
			line-height: 1.4em;
			overflow: auto;
			width: 100%;
			padding: 0 10px 0;
		}
		#lightbox-container-image-data {
			padding: 0 10px; 
			color: #666; 
		}
		#lightbox-container-image-data #lightbox-image-details { 
			width: 70%; 
			float: left; 
			text-align: left; 
		}	
		#lightbox-image-details-caption { font-weight: bold; }
		#lightbox-image-details-currentNumber {
			display: block; 
			clear: left; 
			padding-bottom: 1.0em;	
		}			
		#lightbox-secNav-btnClose {
			width: 66px; 
			float: right;
			padding-bottom: 0.7em;	
		}

			ul {
				list-style:none;
			}

		.li {
		    float: left;
			margin:0 7px 7px 0;
		}

			.li a {
				text-decoration: none;
				border:none;
			}
			.li a:hover {
				text-decoration:none;
				border: none;
			}


		@font-face {
		  font-family: 'fontello';
		  src: url('../font/fontello.eot?65453789');
		  src: url('../font/fontello.eot?65453789#iefix') format('embedded-opentype'),
		       url('../font/fontello.woff?65453789') format('woff'),
		       url('../font/fontello.ttf?65453789') format('truetype'),
		       url('../font/fontello.svg?65453789#fontello') format('svg');
		  font-weight: normal;
		  font-style: normal;
		}
		/* Chrome hack: SVG is rendered more smooth in Windozze. 100% magic, uncomment if you need it. */
		/* Note, that will break hinting! In other OS-es font will be not as sharp as it could be */
		/*
		@media screen and (-webkit-min-device-pixel-ratio:0) {
		  @font-face {
		    font-family: 'fontello';
		    src: url('../font/fontello.svg?65453789#fontello') format('svg');
		  }
		}
		*/
		 
		 [class^="icon-"]:before, [class*=" icon-"]:before {
		  font-family: "fontello";
		  font-style: normal;
		  font-weight: normal;
		  speak: none;
		 
		  display: inline-block;
		  text-decoration: inherit;
		  width: 1em;
		  margin-right: .2em;
		  text-align: center;
		  /* opacity: .8; */
		 
		  /* For safety - reset parent styles, that can break glyph codes*/
		  font-variant: normal;
		  text-transform: none;
		 
		  /* fix buttons height, for twitter bootstrap */
		  line-height: 1em;
		 
		  /* Animation center compensation - margins should be symmetric */
		  /* remove if not needed */
		  margin-left: .2em;
		 
		  /* you can be more comfortable with increased icons size */
		  /* font-size: 120%; */
		 
		  /* Font smoothing. That was taken from TWBS */
		  -webkit-font-smoothing: antialiased;
		  -moz-osx-font-smoothing: grayscale;
		 
		  /* Uncomment for 3D effect */
		  /* text-shadow: 1px 1px 1px rgba(127, 127, 127, 0.3); */
		}
		 
		.icon-phone-squared:before { content: '\e800'; } /* '' */
		.icon-docs:before { content: '\e801'; } /* '' */
		.icon-doc-text:before { content: '\e802'; } /* '' */
		.icon-angle-double-right:before { content: '\e803'; } /* '' */
		.icon-angle-double-up:before { content: '\e804'; } /* '' */
		.icon-angle-double-down:before { content: '\e805'; } /* '' */
		.icon-angle-double-left:before { content: '\e806'; } /* '' */
		.icon-chat-empty:before { content: '\e807'; } /* '' */
		.icon-chat:before { content: '\e808'; } /* '' */
		.icon-attach:before { content: '\e809'; } /* '' */
		.icon-phone:before { content: '\e80a'; } /* '' */
		.icon-help:before { content: '\e80b'; } /* '' */
		.icon-floppy:before { content: '\e80c'; } /* '' */
		.icon-mail:before { content: '\e80d'; } /* '' */
		.icon-mail-alt:before { content: '\e80e'; } /* '' */
		.icon-basket:before { content: '\e80f'; } /* '' */
		.icon-cloud:before { content: '\e810'; } /* '' */
		.icon-icq:before { content: '\e811'; } /* '' */
		.icon-cloud-1:before { content: '\e812'; } /* '' */

		#middle {
			width:1266px;
			height:auto;
			margin:0 auto;
		}

		.ys_article {
			margin-left: -210px;
		}

		#coop_requets .form-carcam {
			margin-bottom: 0;
		}
	</style>
<div class="row">
	<div class="col-md-4">
		<h4 style="padding:10px;width:100%;text-align:center;cursor:pointer;" class="coop_h1 btn-main">Запрос на сотрудничество <i class="icon-angle-double-down"></i></h4>
		 <script type="text/javascript">
	        $(document).ready(function() {
	            $(".coop_h1").click(function() {
	                $("#coop_requets").fadeToggle("slow");
	            })
	            if ($('#coop_requets .form-note,#coop_requets .errortext').length) {
	                $("#coop_requets").show();
	            }
	        })
	        </script>
		<div id="coop_requets" style="position:absolute;z-index:1;left:0;right:0;display:none;box-shadow:0 0 5px 5px #bbb;">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"carcam",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => Array(),
		"WEB_FORM_ID" => 1
	)
);?>
		</div>
 <img src="/images/build_carcam.jpg" style="width:100%;">
	</div>
	 <!-- Отдел оптовых продаж -->
	<div class="col-md-8">
		<h3 style="color: #ff8b00;font-size: 24px;text-align:center;line-height:24px;font-weight:normal;">Отдел оптовых продаж</h3>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p>
 <b>Руководитель оптового отдела<br>
					 Косенков Дмитрий</b><br>
 <i class="icon-phone-squared"></i>8 (800) 555-68-08 доб.601<br>
 <i class="icon-mail"></i> <img width="87" src="/images/emails/opt.png" height="12" style="position: relative;">
				</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p>
 <b>Божко Андрей </b><br>
					 Региональный менеджер<br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08, доб. 607<br>
 <i class="icon-mail"></i> <img width="95" src="/images/emails/opt5.png" height="12" style="position: relative;">
				</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p>
 <b>Павел Букин</b><br>
					 Региональный менеджер<br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08, доб. 605<br>
 <i class="icon-mail"></i> <img width="95" src="/images/emails/opt1.png" height="12" style="position: relative;"><br>
 <i class="icon-icq"></i> 653-453-134<br>
				</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p>
 <b>Павел Трофимов</b><br>
					 Региональный менеджер<br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08, доб. 603<br>
 <i class="icon-mail"></i> <img width="95" src="/images/emails/opt4.png" height="12" style="position: relative;"><br>
 <i class="icon-icq"></i> 660-910-970<br>
				</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p>
 <b>Станислав Астафьев</b><br>
					 Региональный менеджер<br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08, доб. 504<br>
 <i class="icon-mail"></i> <img width="95" src="/images/emails/opt7.png" height="12" style="position: relative;"> <br>
 <i class="icon-icq"></i> 408-058-681 <br>
				</p>
			</div>
		</div>
	</div>
</div>
 <!-- /Отдел оптовых продаж --> <!-- Контакты -->
<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-3">
		<h3 style="color: #ff8b00; font-size: 24px; margin-bottom: 10px; line-height: 24px; font-weight: normal;">Сервисный Центр <br>
		 КАРКАМ</h3>
		<p>
			 127018, Москва <br>
			 ул. Складочная дом 1, строение 1, <br>
			 вход 3, второй этаж, офис 1922 <br>
			 с 9.00 до 21.00 будние дни <br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08
		</p>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-3">
		<h3 style="color: #ff8b00; font-size: 24px; margin-bottom: 10px; line-height: 24px; font-weight: normal;">Головной офис <br>
		 в Санкт-Петербурге</h3>
		<p>
			 192012, г. Санкт-Петербург <br>
			 пр-т Обуховской обороны 120 лит. Б <br>
			 БЦ "Троицкое поле-2" офис 245 <br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08 <br>
 <i class="icon-mail"></i> <img width="90" src="/images/emails/info.png" height="11" style="position: relative;">
		</p>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-3">
		<h3 style="color: #ff8b00; font-size: 24px; margin-bottom: 10px; line-height: 24px; font-weight: normal;">Дополнительный офис <br>
		 в Москве</h3>
		<p>
			 127018, Москва <br>
			 ул. Складочная дом 1, строение 1, <br>
			 вход 3, второй этаж, офис 1922 <br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08 <br>
 <i class="icon-mail"></i> <img width="90" src="/images/emails/info.png" height="11" style="position: relative; ">
		</p>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-3">
		<h3 style="color: #ff8b00; font-size: 24px; margin-bottom: 10px; line-height: 24px; font-weight: normal;">Интернет-магазин</h3>
		<p>
 <br>
			  Заказ и доставка on-line заказов <br>
			  8 (800) 555-68-08 (доб. 1) <br>
 <i class="icon-mail"></i> <img width="97" src="/images/emails/sales.png" height="11" style="position: relative;">
		</p>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-4">
		<h3 style="color: #ff8b00; font-size: 24px; margin-bottom: 10px; line-height: 24px; font-weight: normal;">Purchasing department</h3>
		<p>
 <i class="icon-mail"></i> <img width="97" src="/images/emails/offer.png" height="11" style="position: relative; ">
		</p>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<h3 style="color: #ff8b00; font-size: 24px; margin-bottom: 10px; line-height: 24px; font-weight: normal;">PR-отдел</h3>
		<p>
 <i class="icon-mail"></i> <img width="88" src="/images/emails/pr3.png" height="13" opacity="50%" style="position: relative; ">
		</p>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<h3 style="color: #ff8b00; font-size: 24px; margin-bottom: 10px; line-height: 24px; font-weight: normal;">Отдел маркетинга</h3>
		<p>
 <i class="icon-mail"></i> <img width="126" src="/images/emails/mark.png" height="12" style="position: relative; ">
		</p>
	</div>
</div>
 <!-- /Контакты --> <!-- Наши фирменные магазины --> <?php
        if (CModule::IncludeModule("iblock")) {
            
            // Поиск раздела
            $sections = CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => 157, "?NAME" => 'Санкт-Петербург||Москва||Нижний Новгород'), false, Array("ID", "NAME", "UF_ZOOM", "UF_LAT", "UF_LON"));
            while ($section = $sections->GetNext()) {
        		
            	// Устновка центра и масштаба карты
                $map_data[$section['NAME']]['google_lat']	= $section["UF_LAT"]?	floatval($section["UF_LAT"]):55.751480318208;
                $map_data[$section['NAME']]['google_lon']	= $section["UF_LON"]?	floatval($section["UF_LON"]):37.59977671393;
                $map_data[$section['NAME']]['google_scale']	= $section["UF_ZOOM"]?	intval($section["UF_ZOOM"])	:9;

                // Поиск точек самовывоза
                $elements = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 157, "SECTION_ID" => $section["ID"], 'PROPERTY_GEO_TYPE_VALUE' => 'Фирменный магазин'), false, false, Array("ID", "PROPERTY_GEO_TYPE", "PROPERTY_GEO_PHONE", "PROPERTY_GEO_TIME", "PROPERTY_GEO_MAP_POINT", "PROPERTY_ADDRESS", "PROPERTY_PHOTOS"));
                
                // Формирование массива данных для отображения точек на карте
                while($element = $elements->GetNext()) {
                    $coordinates = explode(',', $element['PROPERTY_GEO_MAP_POINT_VALUE']);
                    $map_data[$section['NAME']]['PLACEMARKS'][] = Array(
                    	"TEXT"	=> $element['PROPERTY_ADDRESS_VALUE'],
                    	"LON"	=> floatval($coordinates[0]),
                    	"LAT"	=> floatval($coordinates[1])
                   	);

                    $data[$section['NAME']][$element["ID"]]["PHOTOS"][]	= $element['PROPERTY_PHOTOS_VALUE'];
                    $data[$section['NAME']][$element["ID"]]["TEXT"]		= $element['PROPERTY_ADDRESS_VALUE'];
                    $data[$section['NAME']][$element["ID"]]["PHONE"]	= $element['PROPERTY_GEO_PHONE_VALUE'];
                    $data[$section['NAME']][$element["ID"]]["TIME"]		= $element['PROPERTY_GEO_TIME_VALUE'];
                }
            }
        }
    ?> <?php if (!empty($map_data)): ?>
<h2 style="font-size:28px;text-align:center;">Наши фирменные магазины</h2>
<div class="row">

	 <?php foreach ($map_data as $region => $region_data): ?>

	<div class="col-xs-12 col-sm-6 col-md-4" style="border: 2px solid #d5d5d5; box-shadow: #e4e4e4 0px 0px 1px 1px; float: left; ">
		<h4 style="color:#A5A5A5;text-align:center;"><?= $region ?></h4>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:map.google.view",
	"bit_map_google",
	Array(
		"COMPONENT_TEMPLATE" => "bit_map_google",
		"CONTROLS" => array(0=>"SMALL_ZOOM_CONTROL",),
		"INIT_MAP_TYPE" => "ROADMAP",
		"MAP_DATA" => serialize($region_data),
		"MAP_HEIGHT" => "260",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(0=>"ENABLE_DBLCLICK_ZOOM",1=>"ENABLE_DRAGGING",2=>"ENABLE_KEYBOARD",)
	)
);?> <?php foreach ($data[$region] as $shop): ?>
		<div style="text-align: justify; color: #5c5c5c; margin-top: 10px; position: relative; ">
			<div style="position: absolute; top: 18px; left: 7px">
 <img width="22px" src="/images/icon-carcam.png" height="32px">
			</div>
			<p style="margin-left: 40px; margin-bottom: 0px;">
				 <?= $shop['TEXT'] ?><br>
				 <?= $shop['TIME'] ?><br>
 <i class="icon-phone-squared"></i><?= $shop['PHONE'] ?>
			</p>
			 <?php if ($shop['PHOTOS']): ?>
			<div class="main">
				<ul>
					 <?php foreach ($shop['PHOTOS'] as $photo): ?> <?php 
			        			$url1 = CFile::ResizeImageGet($photo, array("width" => 800, "height" => 800), BX_RESIZE_IMAGE_EXACT);
			        			$url2 = CFile::ResizeImageGet($photo, array("width" => 90, "height" => 60), BX_RESIZE_IMAGE_EXACT);
			        		?>
					<li class="li"><a href="<?= $url1['src'] ?>"><img src="<?= $url2['src'] ?>"></a></li>
					 <?php endforeach; ?>
				</ul>
			</div>
			 <?php endif; ?>
		</div>
		 <?php endforeach; ?>
	</div>
	 <?php endforeach; ?> <?php endif; ?> <!-- /Наши фирменные магазины -->
</div>

 </main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>