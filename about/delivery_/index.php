<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Доставка и оплата – интернет-магазин «КаркамЭлектроникс»");
$APPLICATION->SetTitle("Доставка");
?>
<main class="container about-page">
	<div class="row">
		<div class="col-xs-12">
<h1><?$APPLICATION->ShowTitle(false)?></h1>
<style>
  .ys_article {
      margin-left: -210px;
  }
  </style>
 
<p>Доставка осуществляется с 11 до 20.00 в будние дни, в другое время и выходные – условия доставки необходимо согласовать с менеджером.</p>
 
<p>МЫ БЕСПЛАТНО ДОСТАВЛЯЕМ ПО РОССИИ ВСЕ ТОВАРЫ ОТ 2999 РУБЛЕЙ, ЗА ИСКЛЮЧЕНИЕМ КОМПЛЕКТУЮЩИХ (КАРТЫ ПАМЯТИ, МОДУЛИ GPS И Т.Д.)</p>
 
<p>Доставка в любые страны мира оплачивается отдельно. Узнать информацию по тарифам Вы можете у наших партнёров:</p>
 
<ol> 
  <li><a href="http://www.emspost.ru/" target="_blank" >EMS Почта России</a></li>
 
  <li><a href="http://pecom.ru/ru/" target="_blank" >Транспортная компания ПЭК</a></li>
 
  <li><a href="http://www.dpd.ru/" target="_blank" >Международная служба экспресс-доставки DPD</a></li>
 </ol>
 
<p>Просим Вас помнить, что все технические параметры и потребительские свойства приобретаемого товара Вам следует уточнять у нашего менеджера до момента покупки товара. В обязанности работников Службы доставки не входит осуществление консультаций и комментариев относительно потребительских свойств товара, а также относительно правил возврата товара или денег за него.</p>
 
<p>При доставке заказанного товара нашей курьерской службой проверяйте комплектацию доставляемого товара, проверьте товар на наличие механических повреждений. 
  <br />
 Если при получении товара Вы не заявили претензий о механических повреждениях, то в дальнейшем подобные претензии не рассматриваются.</p>
 
<br />
 <b>Вы можете оплатить заказ любым удобным для Вас способом:</b> 
<ol> 
  <li>наличными в магазине 
    <br />
   </li>
 
  <li>наличными курьеру при доставке заказа (только для жителей Санкт-Петербурга и Москвы) 
    <br />
   </li>
 
  <li>банковским переводом (реквизиты ниже) 
    <br />
   </li>
 
  <li>пластиковой картой в магазине 
    <br />
   </li>
 
  <li>пластиковой картой при доставке заказа (только для жителей Санкт-Петербурга и Ленинградской обл., Москвы и Московской обл.)  </li>
 
  <li>PayPal 
    <br />
   </li>
 
  <li>WebMoney 
    <br />
   </li>
 
  <li>Яндекс. Деньги 
    <br />
   </li>
 
  <li>QiWi кошелек 
    <br />
   </li>
 
  <li>Элекснет</li>
 </ol>
 
<ol> </ol>
 
<br />
 
<p><b>Реквизиты:</b></p>
 
<p>ООО «Каркам Электроникс»</p>
 
<p>ИНН 7811593682 / КПП 781101001</p>
 
<p>Юр. адрес: 192012, Санкт-Петербург, пр-т Обуховской обороны,120, литер Б, офис 222</p>
 
<p>Факт. адрес: 192012, Санкт-Петербург, пр-т Обуховской обороны,120, литер Б, офис 222</p>
 
<p>Р/С 40702810432250000489 в филиале Санкт-Петербургский ОАО «АЛЬФА-БАНК</p>
 
<p>К/С 30101810600000000786</p>
 
<p>БИК № 044030786</p>
 
<p>ОГРН 1147847371550</p>
 
<br />
 
<p style="color: silver;">Информация на сайте carcam.ru носит исключительно справочный характер и не является публичной офертой.</p>
 
<p style="color: silver;">Все найденные разногласия в информации о товаре с фактическими его характеристиками - являются опечатками, о которых мы просим Вас сообщать по адресу order@carcam.ru для их быстрейшего исправления.</p>
 
<p style="color: silver;">Для получения более точной информации о товаре - пожалуйста, свяжитесь с нашими менеджерами по телефонам 8(800)555-6-808</p>
 
<p style="color: silver;">Обращаем ваше внимание, что при оплате через электронную систему оплаты возможны задержки поступления средств, в результате этого мы не можем гарантировать наличие товара на складе.</p>
 
<p style="color: silver;">Данные ситуации крайне редки и в случае их возникновения мы приложим все силы для решения проблемы.</p>
		</div>
	</div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>