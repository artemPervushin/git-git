<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><main class="container about-page">
<h1><?$APPLICATION->ShowTitle()?></h1>
 <style>
  .ys_article {
      margin-left: -210px;
  }
  </style>
<p>«КАРКАМ Электроникс» - одна из крупнейших компаний-производителей электроники в России. Приоритетными направлениями являются разработка автомобильной электроники (автомобильные видеорегистраторы, радар-детекторы, аксессуары) и систем безопасности (видеонаблюдение, GSM-сигнализации). В 2010 году компания первая в мире выпустила автомобильный видеорегистратор с качеством записи Full HD 1920х1080.</p>
<p>КАРКАМ Q2 стал прорывом на рынке автомобильной электроники и продолжает пользоваться популярностью среди автомобилистов и по сей день.</p>
<p>Преимуществами «КАРКАМ Электроникс» являются: сугубо профессиональный подход и полное понимание тенденций рынка. Продукция КАРКАМ производится на заводах Юго-Восточной Азии. Компания осознает свою ответственность перед потребителями, поэтому все оборудование, выпускающееся под торговой маркой КАРКАМ, изготавливается из высококачественных компонентов (Ambarella, OmniVision, HiSilicon, Aptina, Texas Instruments, Altek, Sony, NextChip) и проходит многоступенчатый контроль качества.</p>
<p>Головной офис компании находится в Санкт-Петербурге, также есть офис в Москве. «КАРКАМ Электроникс» имеет: развитую дилерскую сеть в России, странах СНГ и ЕС; фирменные магазины; авторизованные сервисные центры. Компания ведет активную деятельность по продвижению бренда: участвует в различных выставках, устраивает множество акций, создает и воплощает креативные концепции.</p>
<p>В штате компании «КАРКАМ Электроникс» работают высококлассные инженеры, имеющие большой опыт в сфере разработки электроники. Они постоянно отслеживают актуальные тренды и инновации, прислушиваясь к пожеланиям пользователей. Благодаря этому, все устройства торговой марки КАРКАМ обладают не только конкурентной ценой, но и отличным функционалом.</p>
<p>За годы работы на рынке компания «КАРКАМ Электроникс» завоевала репутацию ответственного и надёжного партнёра, обеспечивая непревзойдённое качество выпускаемой электроники.</p>
<p>КАРКАМ – это качество, проверенное временем!</p>
<script type="text/javascript" src="/upload/jquery.lightbox-0.5.js"></script>
<script type="text/javascript">
	jQuery(function(){
	    jQuery(".about-certificate a").lightBox();
	});
</script>
<style>
	#jquery-overlay {
		position: absolute;
		top: 0;
		left: 0;
		z-index: 90;
		width: 100%;
		height: 500px;
	}
	#jquery-lightbox {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		z-index: 100;
		text-align: center;
		line-height: 0;
	}
	#jquery-lightbox a img { border: none; }
	#lightbox-container-image-box {
		position: relative;
		background-color: #fff;
		width: 250px;
		height: 250px;
		margin: 0 auto;
	}
	#lightbox-container-image { padding: 10px; }
	#lightbox-loading {
		position: absolute;
		top: 40%;
		left: 0%;
		height: 25%;
		width: 100%;
		text-align: center;
		line-height: 0;
	}
	#lightbox-nav {
		position: absolute;
		top: 0;
		left: 0;
		height: 100%;
		width: 100%;
		z-index: 10;
	}
	#lightbox-container-image-box > #lightbox-nav { left: 0; }
	#lightbox-nav a { outline: none;}
	#lightbox-nav-btnPrev, #lightbox-nav-btnNext {
		width: 49%;
		height: 100%;
		zoom: 1;
		display: block;
	}
	#lightbox-nav-btnPrev { 
		left: 0; 
		float: left;
	}
	#lightbox-nav-btnNext { 
		right: 0; 
		float: right;
	}
	#lightbox-container-image-data-box {
		font: 10px Verdana, Helvetica, sans-serif;
		background-color: #fff;
		margin: 0 auto;
		line-height: 1.4em;
		overflow: auto;
		width: 100%;
		padding: 0 10px 0;
	}
	#lightbox-container-image-data {
		padding: 0 10px; 
		color: #666; 
	}
	#lightbox-container-image-data #lightbox-image-details { 
		width: 70%; 
		float: left; 
		text-align: left; 
	}	
	#lightbox-image-details-caption { font-weight: bold; }
	#lightbox-image-details-currentNumber {
		display: block; 
		clear: left; 
		padding-bottom: 3.0em;	
	}			
	#lightbox-secNav-btnClose {
		width: 66px; 
		float: right;
		padding-bottom: 0.7em;	
	}

	ul {
		list-style-type:none;
	}

	.li {
	    float: left;
		margin:0 7px 7px 7px;
	}

	.li a {
		text-decoration: none;
		border:none;
	}
	.li a:hover {
		text-decoration:none;
		border: none;
	}

	@font-face {
		font-family: 'fontello';
		src: url('../font/fontello.eot?65453789');
		src: url('../font/fontello.eot?65453789#iefix') format('embedded-opentype'),
		   url('../font/fontello.woff?65453789') format('woff'),
		   url('../font/fontello.ttf?65453789') format('truetype'),
		   url('../font/fontello.svg?65453789#fontello') format('svg');
		font-weight: normal;
		font-style: normal;
	}
 
	[class^="icon-"]:before, [class*=" icon-"]:before {
		font-family: "fontello";
		font-style: normal;
		font-weight: normal;
		speak: none;

		display: inline-block;
		text-decoration: inherit;
		width: 1em;
		margin-right: .2em;
		text-align: center;

		font-variant: normal;
		text-transform: none;

		line-height: 1em;

		margin-left: .2em;

		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}
	 
	.icon-phone-squared:before { content: '\e800'; } /* '&#59392;' */
	.icon-docs:before { content: '\e801'; } /* '&#59393;' */
	.icon-doc-text:before { content: '\e802'; } /* '&#59394;' */
	.icon-angle-double-right:before { content: '\e803'; } /* '&#59395;' */
	.icon-angle-double-up:before { content: '\e804'; } /* '&#59396;' */
	.icon-angle-double-down:before { content: '\e805'; } /* '&#59397;' */
	.icon-angle-double-left:before { content: '\e806'; } /* '&#59398;' */
	.icon-chat-empty:before { content: '\e807'; } /* '&#59399;' */
	.icon-chat:before { content: '\e808'; } /* '&#59400;' */
	.icon-attach:before { content: '\e809'; } /* '&#59401;' */
	.icon-phone:before { content: '\e80a'; } /* '&#59402;' */
	.icon-help:before { content: '\e80b'; } /* '&#59403;' */
	.icon-floppy:before { content: '\e80c'; } /* '&#59404;' */
	.icon-mail:before { content: '\e80d'; } /* '&#59405;' */
	.icon-mail-alt:before { content: '\e80e'; } /* '&#59406;' */
	.icon-basket:before { content: '\e80f'; } /* '&#59407;' */
	.icon-cloud:before { content: '\e810'; } /* '&#59408;' */
	.icon-icq:before { content: '\e811'; } /* '&#59409;' */
	.icon-cloud-1:before { content: '\e812'; } /* '&#59410;' */

	#middle {
		width:1000px;
		height:auto;
		margin:0 auto;
	}

	.ys_article {
		margin-left: -210px;
	}

	h3{
		font-size:20px;

	
		text-align:left;

	}
 	 .api-submit{
		 text-align: center !important;
	 }
</style>
	<div class="about-certificates">
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/236/1.jpg"><img src="/upload/medialibrary/236/1.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/4c7/2.jpg"><img src="/upload/medialibrary/4c7/2.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/c76/3.jpg"><img src="/upload/medialibrary/c76/3.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/5da/4.jpg"><img src="/upload/medialibrary/5da/4.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/506/5.jpg"><img src="/upload/medialibrary/506/5.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/f08/6.jpg"><img src="/upload/medialibrary/f08/6.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/7ed/7.jpg"><img src="/upload/medialibrary/7ed/7.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/a95/8.jpg"><img src="/upload/medialibrary/a95/8.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/8e5/9.jpg"><img src="/upload/medialibrary/8e5/9.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/84c/10.jpg"><img src="/upload/medialibrary/84c/10.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/51c/11.jpg"><img src="/upload/medialibrary/51c/11.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/7b9/12.jpg"><img src="/upload/medialibrary/7b9/12.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/11b/13.jpg"><img src="/upload/medialibrary/11b/13.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/b02/14.jpg"><img src="/upload/medialibrary/b02/14.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/2ed/15.jpg"><img src="/upload/medialibrary/2ed/15.jpg"></a></div>
		<div class="about-certificate col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="/upload/medialibrary/bfa/16.jpg"><img src="/upload/medialibrary/bfa/16.jpg"></a></div>
	</div>
</main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>