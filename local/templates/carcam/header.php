<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Yenisite\Core\Tools;

// @var $moduleId
// @var $moduleCode
// @var $settingsClass
include 'include/module_code.php';

if ($_POST['rz_ajax_no_header'] === 'y') {
	$APPLICATION->IncludeComponent("yenisite:settings.panel", "empty", array(
			"SOLUTION" => $moduleId,
			"SETTINGS_CLASS" => $settingsClass,
			"GLOBAL_VAR" => "rz_b2_options"
		),
		false
	);
	return;
}


//2017-02-13 12:20 zimin
//Admitad
if( $_GET['admitad_uid'] && $_GET['admitad_uid'] !== '' && empty( $_COOKIE['ADMITAD'] ) )
{
	SetCookie( 
		'ADMITAD', 
		$_GET['admitad_uid'], 
		time() + 60 * 60 * 24 * 45, 
		'/' 
	);
	//SetCookie( 'admitad', $_GET['admitad_uid'], time()+60*60*24*45, '/', 'carcam.ru' );
}


?><!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?$APPLICATION->ShowTitle()?></title>
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<?
	\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
	
	global $rz_b2_options;
	global $USER;
	
	$bMainPage = $APPLICATION->GetCurPage(false) == SITE_DIR;
	$arDefIncludeParams = array(
		"AREA_FILE_SHOW" => "file",	
		"EDIT_TEMPLATE" => "include_areas_template.php"
	);
	
	if(!Loader::IncludeModule($moduleId)) die('Module ' . $moduleId . ' not installed!');
	if(!Loader::IncludeModule("yenisite.core")) die('Module yenisite.core not installed!');
	
	use Bitronic2\Mobile;
	mobile::Init();
	
	$frame = new \Bitrix\Main\Page\FrameBuffered("rz_dynamic_full_mode_meta", false);
	?>
	<script type="text/javascript" data-skip-moving="true" id="rz_dynamic_full_mode_meta">/*
	<?
	$frame->begin('');
		if(mobile::isMobile(false) && mobile::isFullMode()):
		?>
		 	*/
			var viewPortTag = document.createElement('meta');
			viewPortTag.name = "viewport";
			viewPortTag.content = "";
			document.getElementsByTagName('head')[0].appendChild(viewPortTag);
		  /*
		<?endif;
	$frame->end();?>
	*/
	</script>
	<!-- fonts -->
	<?
	$APPLICATION->AddHeadString('<link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&amp;subset=cyrillic-ext,latin" rel="stylesheet" type="text/css">');
	?>

	<!-- styles -->
	<?
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/s.min.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/templates_addon.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/template_styles.css");
	$APPLICATION->SetAdditionalCSS("/bitrix/js/socialservices/css/ss.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/intlTelInput.css");
	?>
	
	<!-- Respond.js - IE8 support of media queries -->
	<!-- WARNING: Respond.js doesn t work if you view the page via file:// -->
	<!-- selectivizr - IE8- support for css3 classes like :checked -->
	<!--[if lt IE 9]>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/3rd-party-libs/selectivizr-min.js"></script>
    	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?ob_start();
	$APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/settings.php")), false, array("HIDE_ICONS"=>"Y"));
	$panelSettings = ob_get_clean();
	?>

	<?
	// #### PROCESS transformers settings
	if($rz_b2_options['header-version'] == 'v3' || ($bMainPage && mobile::isMobile()))
	{
		$bVerticalTopMenu = $rz_b2_options['header-version'] == 'v3';
		$rz_b2_options['catalog-placement'] = 'top';
	}

	?>
	<script type="text/javascript" data-skip-moving="true">
		<?
		$arSettings = array();
		foreach($rz_b2_options as $key => $value){
			$key = preg_replace("/[^a-z]+/i", " ", strtolower($key));
			$key = str_replace(' ', '',substr_replace($key, substr(ucwords($key), 1), 1));
			$arSettings[$key] = $value;
		}
		//correct settings names
		$arSettings['photoViewType'] = $arSettings['detailGalleryType'];
		$arSettings['productInfoMode'] = $arSettings['detailInfoMode'];
		$arSettings['productInfoModeDefExpanded'] = ($arSettings['detailInfoFullExpanded'] == 'Y');
		?>
		serverSettings = <?= CUtil::PhpToJSObject($arSettings)?><?unset($arSettings)?>;
		SITE_DIR = '<?=SITE_DIR?>';
		SITE_ID = '<?=SITE_ID?>';
		SITE_TEMPLATE_PATH = '<?=SITE_TEMPLATE_PATH?>';
		COOKIE_PREFIX = '<?=COption::GetOptionString("main", "cookie_name", "BITRIX_SM")?>';
	</script>

	<?
		$APPLICATION->ShowHead();/*
	?>
	<script>require([
			'libs/spin.min'
		], function(){
		});</script>
	<?*/
	// for instant switch from settings panel we use link, delete link and uncomment this on production
	//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/themes/theme_{$rz_b2_options['theme-demo']}.css");
	?>
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH . "/css/themes/theme_{$rz_b2_options['theme-demo']}.css" ?>" class="current-theme"
		  data-path="<?= SITE_TEMPLATE_PATH . '/css/themes/' ?>"/>
	<?
	//set main theme color to use in JS
	include_once 'include/js_colors.php';

	CJSCore::Init(array('jquery', 'window'));
	if (Loader::IncludeModule('currency')) {
		CJSCore::Init(array('currency'));
	}
	$asset = Asset::getInstance();
	//$asset->addJs(SITE_TEMPLATE_PATH."/js/js.js");

	// bitrix
	$asset->addJs("/bitrix/js/socialservices/ss.js");

	// basic js libraries
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/spin.min.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/modernizr.custom.min.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/bootstrap/transition.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/bootstrap/modal.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/requestAnimationFrame.min.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/velocity.min.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/velocity.ui.min.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/sly.min.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/wNumb.min.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.maskedinput.min.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/require.min.js");

	//custom js scripts
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/utils/themeSwitch.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/utils/makeSwitch.js");
	$asset->addJs(SITE_TEMPLATE_PATH . "/js/custom-scripts/utils/posPopup.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmMainMenu.js");

	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initGlobals.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/settingsInitial.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/settingsHelpers.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/settingsRelated.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initSettings.js");

	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/toggles/initToggles.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/popups/initModals.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/popups/initPopups.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/popups/initSearchPopup.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/forms/initSearch.js");
	$asset->addJs(SITE_TEMPLATE_PATH.'/js/custom-scripts/inits/sliders/initHorizontalCarousels.js');

	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initCommons.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/ready.js");

	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/intlTelInput.js");

	CJSCore::RegisterExt('rz_b2_um_countdown', array(
		'js' => SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.countdown.2.0.2/jquery.countdown-ru.js",
		'lang' => SITE_TEMPLATE_PATH.'/lang/'.LANGUAGE_ID.'/countdown.php'
	));

	if ($bMainPage) {
		if ('Y' == $rz_b2_options['block_home-feedback']) {
			$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.mobile.just-touch.min.js");
		}
		if ('Y' == $rz_b2_options['wow-effect']) {
			$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/wow.min.js");
		}
		$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.countdown.2.0.2/jquery.plugin.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.countdown.2.0.2/jquery.countdown.min.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initTimers.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmAccordeon.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmTabs.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmComboBlocks.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/sliders/initPhotoThumbs.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initCatalogHover.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/sliders/initSpecialBlocks.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmSlider.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/sliders/initBigSlider.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/pages/initHomePage.js");
		CJSCore::Init(array('rz_b2_um_countdown'));
	}

	if (!$USER->IsAuthorized()) {
		$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/progression.js");
		$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/modals/initModalRegistration.js");
	}

	// back-end stuff
	$asset->addJs(SITE_TEMPLATE_PATH."/js/back-end/utils.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/back-end/visual/hits.js");
	$asset->addJs(SITE_TEMPLATE_PATH."/js/back-end/visual/commons.js");

	CJSCore::RegisterExt('rz_b2_um_validation', array(
		//'js' => SITE_TEMPLATE_PATH."/js/custom-scripts/utils/formValidation.js", LAZY LOAD
		'lang' => SITE_TEMPLATE_PATH.'/lang/'.LANGUAGE_ID.'/validation.php'
	));
	// AJAX
	CJSCore::RegisterExt('rz_b2_ajax_core', array(
		'js' => SITE_TEMPLATE_PATH."/js/back-end/ajax/core.js",
		'lang' => SITE_TEMPLATE_PATH.'/lang/'.LANGUAGE_ID.'/ajax.php',
		'rel' => array('core', 'currency')
	));
	CJSCore::RegisterExt('rz_b2_bx_catalog_item', array(
		'js' => SITE_TEMPLATE_PATH."/js/back-end/bx_catalog_item.js",
		'lang' => SITE_TEMPLATE_PATH.'/lang/'.LANGUAGE_ID.'/ajax.php',
	));

	CJSCore::Init(array('rz_b2_um_validation', 'rz_b2_ajax_core'));

	if ($rz_b2_options['block_main-menu-elem'] != 'N') {
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/back-end/visual/hits.js");
	}
	?>
	
<link rel="stylesheet" href="/font/fontello.css" type="text/css"></link>
<script type="text/javascript" src="<? echo SITE_TEMPLATE_PATH; ?>/static/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript"> 
$(document).ready(function() { 

          $(".fancybox")
    .attr('rel', 'gallery')
    .fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        nextEffect  : 'none',
        prevEffect  : 'none',
		"frameWidth" : 630,	 // ������ ����, px (425px - �� ���������)
		"frameHeight" : 630, // ������ ����, px(355px - �� ���������)
        padding     : 0,
        margin      : [20, 60, 20, 60] // Increase left/right margin
    });
        }); 
</script>


<link rel="stylesheet" href="//api.direct-credit.ru/style.css" type="text/css"></link>
<script src="//api.direct-credit.ru/JsHttpRequest.js" type="text/javascript"></script>
<script src="//api.direct-credit.ru/dc.js" charset="windows-1251" type="text/javascript"></script>
<script type="text/javascript">
    var partnerID = "7033261";
</script>


<!--СтопЯндексСоветник-->
<script>
!function(){var e=function(){function e(e,t){r&&"#ssdebug"===r&&console.log(e,t||"")}function t(){}function n(t){var n=!1,o={display:"table",opacity:"1",position:"fixed","min-width":"800px","border-collapse":"collapse"};if(t&&"STYLE"===t.tagName&&/(market_context_headcrab_container)|(PD94bWwgdmVyc2lvbj0iMS4wIiB)/.test(t.innerHTML)&&(t.innerText=t.innerText.replace(/transform/g,""),e("Yandex style cracked")),t&&"DIV"===t.tagName)for(var r in o){if(window.getComputedStyle(t).getPropertyValue(r)!==o[r]){e("YandexDesktopFound-"+r+" false"),n=!1;break}e("YandexDesktopFound-"+r+" true"),n=!0}t&&n&&(t&&t.style&&(t.style.transform="translate(-10000px, -10000px)"),a.yaD===!1&&(a.yaD=!0))}function o(e){}var r=window.location.hash||!1;if(!window.MutationObserver)return e("MutationObserver not supported!"),!1;if(navigator.userAgent.indexOf("MSIE")!=-1||navigator.userAgent.indexOf("Trident")!=-1||navigator.userAgent.indexOf("Edge")!=-1)return e("IE or Edge"),!1;var a={yaD:!1,yaM:!1,smB:!1},i=new MutationObserver(function(e){e.map(function(e){var r=e.addedNodes[0];n(r),o(r),t()})}),d=new MutationObserver(function(){var e=document.documentElement.style.marginTop;e&&parseInt(e)>0&&(document.documentElement.style.marginTop="")}),s=new MutationObserver(function(){var e=document.body.style.marginTop;e&&parseInt(e)>0&&(document.body.style.marginTop="")}),u=function(){return document.body?(i&&i.observe(document.body,{childList:!0,subtree:!0}),d&&d.observe(document.documentElement,{attributes:!0,attributeFilter:["style"]}),void(s&&s.observe(document.body,{attributes:!0,attributeFilter:["style"]}))):void setTimeout(u,100)};r&&"#ssoff"===r||u()};e()}();
</script>
<!--СтопЯндексСоветник-->




</head>


<body itemscope itemtype="http://schema.org/Store"
	  data-top-line-position="<?= $rz_b2_options['top-line-position'] ?>"
	  data-additional-prices-enabled="<?= $rz_b2_options['additional-prices-enabled'] ?>"
	  data-catalog-placement="<?= $rz_b2_options['catalog-placement'] ?>"
	  data-container-width="<?= $rz_b2_options['work_area'] ?>" 
	  style="background: url('/background_site.jpg') no-repeat fixed center top / cover !important;"
	>
	<script>
	//PHP Magic starts here
	b2.s.hoverEffect = "<?=$rz_b2_options['product-hover-effect']?>";
	BX.message({'tooltip-last-price': "<?=GetMessage('BITRONIC2_TOOLTIP_LAST_PRICE')?>"});
	</script>
	<!-- Yandex.Metrika Val-counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31460098 = new Ya.Metrika({
                    id:31460098,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31460098" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika Val-counter -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WP5J5X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WP5J5X');</script>
<!-- End Google Tag Manager -->

	<div class="bitrix-admin-panel">
		<div class="b_panel"><?$APPLICATION->ShowPanel();?></div>
	</div>
	
	<button class="btn-main to-top">
		<i class="flaticon-key22"></i>
		<span class="text"><?=GetMessage('BITRONIC2_BUTTON_UP');?></span>
	</button>
	
	<div class="top-line">
		<div class="container">
			<div class="top-line-content row clearfix">
				<?=$panelSettings;?>
				<button type="button" class="btn-sitenav-toggle"><i class="flaticon-menu6"></i> Меню</button>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/user_auth.php")), false, array("HIDE_ICONS"=>"Y"));?>
				<?php
					ob_start();
					$APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/geoip.php")), false, array("HIDE_ICONS"=>"Y"));
					$_includeGeoip = ob_get_clean();
				?>
				<div class="top-line-item city-and-time hidden-xs" id="city-and-time"><?= $_includeGeoip; ?></div>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/basket.php")), false, array("HIDE_ICONS"=>"Y"));?>
				<?if(Loader::includeModule('yenisite.favorite')) $APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/favorites.php")), false, array("HIDE_ICONS"=>"Y"));?>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/compare.php")), false, array("HIDE_ICONS"=>"Y"));?>
			</div><!-- /top-line-content -->
		</div><!-- container -->
	</div><!-- top-line -->

	<header class="page-header" data-header-version="<?=$rz_b2_options['header-version']?>" style="background: <?=$rz_b2_options['color-header']?>"><?//COLOR?>
		<div class="container">
			<div class="header-main-content clearfix">
				<nav class="sitenav horizontal" id="sitenav">
					<button class="btn-sitenav-toggle"><i class="flaticon-close47"></i></button>
					<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/menu_top.php")), false, array("HIDE_ICONS"=>"Y")); ?>
					<?php $APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/menu_catalog_mobile.php")), false, array("HIDE_ICONS"=>"Y")); ?>
					<?php Tools::includePostfixArea($pf, SITE_DIR . "include_areas/header/phones.php", true); ?>
					<!-- <a href="#" data-toggle="modal" data-target="#modal_callme" class="modal_callme">Заказать звонок</a> -->
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/user_auth.php")), false, array("HIDE_ICONS"=>"Y"));?>
				</nav>
				<a href="<?=SITE_DIR?>" class="brand">
					<img width="255" alt="logo_carcam_offc.png" src="/upload/medialibrary/831/83184ba0dca7caaafe338ad4302b24b9.png" height="101" title="logo_carcam_offc.png">
				</a>
				<div class="header-contacts">
					<?php $pf = ('Y' == $rz_b2_options['change_contacts'])? $rz_b2_options['GEOIP']['INCLUDE_POSTFIX']:''; ?>
					<div class="contacts-content">
						<div class="phones"><i class="flaticon-phone12 phone"></i><?php Tools::includePostfixArea($pf, SITE_DIR . "include_areas/header/phones.php", true); ?></div>
						<span class="free-call-text"><?php Tools::includePostfixArea($pf, SITE_DIR . "include_areas/header/phones_text.php", true); ?></span>
					</div>	
				</div>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/search.php")), false, array("HIDE_ICONS"=>"Y"));?>
			</div>
		</div>
		<div class="<?=($bVerticalTopMenu) ? 'catalog-at-side minified container' : 'catalog-at-top'?>" id="catalog-at-top">
		<?php
			if ($rz_b2_options['catalog-placement'] == 'top' || $bVerticalTopMenu) {
				$APPLICATION->IncludeComponent("bitrix:main.include", "", array_merge($arDefIncludeParams,array("PATH" => SITE_DIR."include_areas/header/menu_catalog.php")), false, array("HIDE_ICONS"=>"Y"));
			}
		?>
		</div>
	</header>
	
	<?if(!$bMainPage):?>
	<div class="container bcrumbs-container">
		<nav class="breadcrumbs" data-backnav-enabled="<?= ($rz_b2_options['backnav_enabled'] == 'Y') ? 'true' : 'false' ?>">
			<?Tools::showViewContent('left_menu');?>
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
				"START_FROM" => "1"
				),
				false,
				array(
				"ACTIVE_COMPONENT" => "Y"
				)
			);?>
		</nav>
	</div>
	<?endif?>