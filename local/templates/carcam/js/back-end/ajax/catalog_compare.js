//Compare Page
RZB2.ajax.ComparePage = {
	SendRequest: function(sender, params, callback){
		if (typeof params == "function" && typeof callback == "undefined") {
			callback = params;
			params = undefined;
		}
		var data = params || {};
		for (var key in RZB2.ajax.params) {
			data[key] = RZB2.ajax.params[key];
		}
		if (!!sender) {
			var href = $(sender).attr('href');
			if (!!href) {
				data['REQUEST_URI'] = href;
				var uriParams = RZB2.utils.getQueryVariable(null, href);
				for (var key in uriParams) {
					data[key] = uriParams[key];
				}
			}
		}
		data['rz_ajax'] = 'y';
		if (typeof callback != "function") {
			callback = null;
		}
		//alert( SITE_DIR );
		//console.log( callback );
		//console.log( data );
		$.ajax({
			type: "POST",
			url: SITE_DIR + 'ajax/catalog.php',
			//url: data['REQUEST_URI'],
			data: data,
			dataType: "html",
			error: function(){
				window.location.assign(data['REQUEST_URI']);
			},
			//success: callback,
			success: function(res)
			{
				callback(res);
				//Надо обновить весь файл целиком! 
				//Удаление из списка сравнения товаров для страницы
				LenColEl = $("#popup_compare tbody tr.table-item").length - 1;
				$('.btn-compare .items-inside').html(LenColEl);
				$('#popup_compare .popup-header').load("/ #popup_compare .popup-header *");
				console.log("Удаление из списка сравнения товаров страницы");
				if(LenColEl <= 0){
					$("#popup_compare .popup-footer").remove();
					$("#popup_compare .table-wrap").remove();
					$(".container.compare-page").empty().html('<p><font class="notetext">Список сравниваемых элементов пуст.</font></p>');
				}else{
					$('#popup_compare .items-table .popup-table-item .name input[data-id='+ data['ID'] +']').closest('.popup-table-item').remove();
				};
				//END Удаление из списка сравнения товаров для страницы
			},
		});
	}
};