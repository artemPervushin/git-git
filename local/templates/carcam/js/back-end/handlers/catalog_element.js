function RZB2_initDetailHandlers($){

	// CATALOG ELEMENT - EDOST
	$('.calc-delivery').on('click', function(e){
		var _ = $(this);
		e.preventDefault();
		edost_catalogdelivery_show(_.data('id'), _.data('name'));
	});

	// REVIEWS
	$('.combo-target.comments').on('click', '.form-wrap>header', function(){
		var $captcha = $(this).closest('.form-wrap').find('div.captcha');
		var $captchaImg = $captcha.find('img');
		if ($captchaImg.length < 1) {
			$captcha.html('<img src="/bitrix/tools/captcha.php?captcha_code=' + $captcha.data('code') + '" alt="CAPTCHA">');
		}
	});
	//Blog
	RZB2.ajax.Review.Blog.Refresh();
	$('#comments').on('submit', '#form_comment_blog', function (e) {
		e.preventDefault();
		var $this = $(this),
			data = $this.serializeArray();
		RZB2.ajax.Review.Blog.SendRequest($this, data, false);
	});
	$('#comments').on('click', '#blog_comments .pagination a', function (e) {
		e.preventDefault();
		var $this = $(this);
		if(!$this.hasClass('active')) 
		{
			RZB2.ajax.Review.Blog.ChangePage($this, $this.data('page'));
		}
	});
	
	//Forum
	RZB2.ajax.Review.Forum.Refresh();
	$('#comments').on('submit', '#form_comment_forum', function (e) {
		e.preventDefault();
		var $this = $(this),
			data = $this.serializeArray();
		RZB2.ajax.Review.Forum.SendRequest(data);
	});
	$('#comments').on('click', '#forum_comments .pagination a', function (e) {
		e.preventDefault();
		var $this = $(this);
		if(!$this.hasClass('active') || !$this.hasClass('disabled')) 
		{
			RZB2.ajax.Review.Forum.ChangePage($this.data('page'), $this.data('pagen-key'));
		}
	});
	
	// CATALOG SET CONSTRUCTOR
	$('.product-page').on('click', '.collection-wrap .btn-main', function(e){
		e.preventDefault();
		RZB2.ajax.CatalogSetConstructor.AddToBasket(this);
	});
	
	$('.product-page').on('click', '.custom-collection', function(e){
		e.preventDefault();
		if(typeof RZB2.ajax.SetConstructor !== 'undefined')
		{
			RZB2.ajax.SetConstructor.Load();
		}
	});

	// DETAIL MODALS - CRY FOR PRICE, PRICE_DROPS, EXIST_PRODUCT
	var $doc = $(document);

	var $mcfp = $('#modal_cry-for-price');
	if ($mcfp.length > 0) {
		$mcfp.on('shown.bs.modal', function (e) {
			var $btn = $(e.relatedTarget);
			var data = [
				{'name': 'PRICE', 'value': $btn.data('price')},
				{'name': 'CURRENCY', 'value': $btn.data('currency')},
				{'name': 'PRODUCT', 'value': $btn.data('product')},
				{'name': 'PRICE_TYPE', 'value': $btn.data('price_type')}
			];
			$(this).find('form').rise_modal(data);
		});
	}
	$doc.on('submit', '.form_cry-for-price', function (e) {
		var $this = $(this);
		if (!formCheck($this)) {
			return false;
		} else {
			e.preventDefault();
			$this.send_modal();
			return false;
		}
	});
	var $miwpd = $('#modal_inform-when-price-drops');
	if ($miwpd.length > 0) {
		$miwpd.on('shown.bs.modal', function () {
			var $form = $(this).find('form');
			var $btn = $('#button_price_drops');
			var data = [
				{'name': 'PRICE', 'value': $btn.data('price')},
				{'name': 'CURRENCY', 'value': $btn.data('currency')},
				{'name': 'PRODUCT', 'value': $btn.data('product')},
				{'name': 'PRICE_TYPE', 'value': $btn.data('price_type')}
			];
			$.when($form.rise_modal(data)).then(function () {
				var moneyFormat = wNumb({
					mark: '.',
					thousand: ' ',
					decimals: 2
				});
				var thisModal = $('#modal_inform-when-price-drops');
				var currentPriceField = $('#price-current').children('.value');
				var currentPrice = moneyFormat.from(currentPriceField.html());
				thisModal.on('show.bs.modal', function () {
					currentPrice = moneyFormat.from(currentPriceField.html());
				});

				var npSlider = $('.desired-price-slider').noUiSlider({
					start: currentPrice * 0.9,
					connect: "lower",
					step: 1,
					range: {
						'min': 1,
						'max': currentPrice
					},
					format: moneyFormat
				});

				npSlider.Link('lower').to($('#desired-price>.value'));
				npSlider.Link('lower').to($('#modal_inform-when-price-drops_price'));

				var desiredPriceField = $('#desired-price').children('.value');
				var priceDifferenceField = $('#price-difference').children('.value');
				var priceDifferencePercentField = $('#price-difference').children('.percent-value');

				var desiredPrice = moneyFormat.from(desiredPriceField.html());
				var priceDifference = currentPrice - desiredPrice;
				var priceDifferencePercent = Number((priceDifference / currentPrice) * 100).toFixed(2);

				function setDifference() {
					priceDifferenceField.html(moneyFormat.to(priceDifference));
					priceDifferencePercentField.html('(' + priceDifferencePercent + '%)');
				}

				setDifference();

				npSlider.on('slide set', function () {
					desiredPrice = moneyFormat.from($(this).val());
					priceDifference = currentPrice - desiredPrice;
					priceDifferencePercent = Number((priceDifference / currentPrice) * 100).toFixed(2);
					setDifference();
				});
			});
		});
	}

	$doc.on('keypress', '#modal_inform-when-price-drops_price', function (e) {
		if (e.which !== 13) return true;
		$(this).change();
		return false;
	});
	$doc.on('keypress', '#modal_inform-when-price-drops_email', function (e) {
		if (e.which !== 13) return true;
		$('#modal_inform-when-price-drops_price').focus();
		return false;
	});
	$doc.on('submit', '.form_inform-when-price-drops', function (e) {
		var $this = $(this);
		if (!formCheck($this)) {
			return false;
		} else {
			e.preventDefault();
			$this.send_modal();
			return false;
		}
	});
}

if (typeof domReady != "undefined" && domReady == true) {
	RZB2_initDetailHandlers(jQuery);
} else {
	jQuery(document).ready( RZB2_initDetailHandlers );
}
