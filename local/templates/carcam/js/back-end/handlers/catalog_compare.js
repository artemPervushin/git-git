function RZB2_initCompareHandlers($){
	// COMPARE PAGE
	$('main.compare-page')
	.on('click', '.remove-property', function(e){
		e.preventDefault();
		var _ = $(this);

		var spinner = RZB2.ajax.spinner(_.closest('th, td'));
		spinner.Start({width:2, radius:5, color:RZB2.themeColor});

		RZB2.ajax.ComparePage.SendRequest(_, function(res){
			//stop spinner
			spinner.Stop();
			delete spinner;
			//remove deleted table row
			var tr = _.closest('tr');
			var tbody = tr.parent('tbody');
			var trClass = tr.attr('class');
			var scroller = $('.compare-outer-wrapper .scroller');
			scroller.height(scroller.height() - tr.height());
			$('.compare-table tr.'+trClass).remove();
			if (tbody.length > 0 && tbody.children().not('.section-header').length < 1) {
				scroller.height(scroller.height() - tbody.height());
				$('.compare-table tbody.'+tbody.attr('class')).remove();
			}
			//update list of deleted properties
			//console.log( res );
			var $res = $(res);
			var $deletedRes = $res.find('.deleted-properties');
			var $tBody = $res.find('tbody.group-common');
			//console.log( $tBody );
			var $deletedDiv = $('.deleted-properties');
			//if ($deletedDiv.length > 0) {
			if ($tBody.length > 0) {
				$deletedDiv.html( $deletedRes.html() );
				$( 'tbody.group-common' ).html( $tBody.html() );
                widthCorrecter();
                //b2.init.comparePage();
                //b2.init.scrollbars();
            } else {
				$('main.compare-page').append($deletedRes);
			}
		});
	})
	.on('mousedown', '.compare-switch, .compare-item .btn-close, .deleted-property a', function(e){
		e.preventDefault();
		var spinner = RZB2.ajax.spinner($(this));
		spinner.Start({width:2, radius:5});
		var $container = $('main.compare-page');
		RZB2.ajax.loader.Start($container);

		RZB2.ajax.ComparePage.SendRequest($(this), function(res){
			$body.off('.b2comparepage');
			$(window).off('.b2comparepage');

			var $res = $('<div>'+res+'</div>');
			var $main = $res.find('main.compare-page');//.andSelf().filter('main.compare-page');
			$container.html($main.html());
            widthCorrecter();
            b2.init.comparePage();
            b2.init.scrollbars();//Скролл после нажатия кнопки
			$(window)
				.trigger('scroll.b2comparepage')
				.trigger('b2ready');
			RZB2.ajax.BasketSmall.RefreshButtons();
			RZB2.ajax.loader.Stop($container);
			$(".compare-outer-wrapper .fixed-column-wrap").width("270px");
            $(".compare-outer-wrapper .compare-table td").not(".property").width((100/($(".compare-items-width").eq(0).find("th").length - 1)-22) + "%");
            console.log("Ячейка " + 100/($(".compare-items-width").eq(0).find("th").length - 1));
		});
	});
}

if (typeof domReady != "undefined" && domReady == true) {
	RZB2_initCompareHandlers(jQuery);
} else {
	jQuery(document).ready( RZB2_initCompareHandlers );
}
