function RZB2_initCommonHandlers($){
	//COMPARE
	$('#popup_compare').on('click', '.table-wrap button.btn-delete', function(e){
		e.preventDefault(); console.log("#popup_compare");
		RZB2.ajax.Compare.Delete($(this).data('id'));
	});
	$('#popup_compare').on('click', '.popup-footer button.btn-delete', function(e){
		e.preventDefault();
		RZB2.ajax.Compare.DeleteAll();
	});
	
	//FAVORITES
	$('#popup_favorites').on('click', '.table-wrap button.btn-delete', function(e){
		e.preventDefault();
		RZB2.ajax.Favorite.Delete($(this).data('id'));
		// RZB2.ajax.Favorite.Refresh();
	});
	$('#popup_favorites').on('click', '.popup-footer button.btn-delete', function(e){
		e.preventDefault();
		RZB2.ajax.Favorite.DeleteAll();
	});
	$('#popup_favorites').on('click', '.popup-footer button.btn-main', function(e){
		e.preventDefault();
		RZB2.ajax.Favorite.ToBasket.AddAll();
	});

	// SMALL BASKET
	var timerQuantity, timerTimeout;
	$('#popup_basket .basket-content').on({
			'mousedown': function(e){
				e = e || window.event;
				var _ = $(this);
				var quanInput = _.siblings('.quantity-input');
				if ( quanInput.hasClass('disabled') || quanInput.is(':disabled') || _.hasClass('disabled') ) return;
				var curValue = Number(quanInput.val());
				var ratio = parseFloat(quanInput.data('ratio'));
				
				var changeStep = ratio;
				if ( _.hasClass('decrease') ) changeStep = -ratio;
				if (isNaN(curValue) || curValue<1) curValue = ratio;
				

				curValue += changeStep;
				if ( curValue < ratio ) curValue = ratio;
				quanInput.val(curValue);
				timerTimeout = setTimeout(function(){
					timerQuantity = setInterval(function(){
						curValue += changeStep;
						if ( curValue < ratio ) curValue = ratio;
						quanInput.val(curValue);
					}, 100);
				}, 300);
				_.data('changed', true);
			},
			'mouseup mouseleave': function(e){
				clearTimeout(timerTimeout);
				clearInterval(timerQuantity);
				if (!$(this).data('changed')) return;
				
				RZB2.ajax.BasketSmall.ChangeQuantity(e);
			}
		}, 'button.quantity-change'
	).on('change','input[name=quantity]', function (e) {
			RZB2.ajax.BasketSmall.ChangeQuantity(e);
	}).on('click','button.btn-delete', function (e) {
			e = e || window.event;
			e.preventDefault();
			
			RZB2.ajax.BasketSmall.Delete(e);
	});
	$('#popup_basket .popup-footer').on('click', 'button.btn-delete', function(e) {
		e = e || window.event;
		e.preventDefault();
		
		RZB2.ajax.BasketSmall.DeleteAll(e);
	});
	$('#popup_basket').on('shown.bs.modal', function(e){
		initTooltips(this);
	});

	// REGISTRATION
	$('#modal_registration').on('show.bs.modal', function(){
		if (typeof RZB2.ajax.registration != "undefined") return;

		RZB2.ajax.registration = new RZB2.ajax.FormUnified({
			ID: 'modal_registration',
			AJAX_FILE: SITE_DIR + "ajax/registration.php",
		});
		$('#modal_registration').on('shown.bs.modal', function(){
			$(this).find('.textinput').first().focus();
		});
		RZB2.ajax.registration.Load(false, function(){
			b2.init.formValidation('#modal_registration');
			initModalRegistration();
			$('#modal_registration')
			.find('.btn-form-switch').click(function(e){
				e.preventDefault();
				$(this).closest('.modal').modal('hide');
			})
			.end()
			.find('form').on('submit', function(e){
				e.preventDefault();

				$(this).find('input[name="USER_LOGIN"]').val($(this).find('input[name="USER_EMAIL"]').val());
				$(this).find('input[name="NEW_LOGIN"]').val($(this).find('input[name="NEW_EMAIL"]').val());
				$(this).find('input[name="USER_CONFIRM_PASSWORD"]').val($(this).find('input[name="USER_PASSWORD"]').val());	

				RZB2.ajax.registration.Post($(this), function(res){
					$("<div></div>").html(res).find('.anti-robot').replaceAll($('#modal_registration').find('.anti-robot'));
				}, false);
			});
		});
	});
	
	// CALL ME
	$('.contacts-content').on('click', '.phone-link', function(e){
		if ( !Modernizr.mq('(max-width: 767px)') ){
			e.preventDefault();
			$('#modal_callme').modal('show');
			if(typeof RZB2.ajax.callMe === 'undefined')
			{
				RZB2.ajax.callMe = new RZB2.ajax.FormUnified({
					ID: 'modal_callme',
					AJAX_FILE: SITE_DIR + "ajax/callme.php",
				});
			}
			RZB2.ajax.callMe.Load();
		}
	});
	$('#modal_callme').on('submit', 'form', function(e){
		e.preventDefault();
		RZB2.ajax.callMe.Post($(this));
	});

	// FEEDBACK
	$(document).on('click', '.feedback-link', function (e) {
		e.preventDefault();
		$('#modal_feedback').modal('show');
		if (typeof RZB2.ajax.feedBack === 'undefined') {
			RZB2.ajax.feedBack = new RZB2.ajax.FormUnified({
				ID: 'modal_feedback',
				AJAX_FILE: SITE_DIR + "ajax/feedback.php",
				URL: location.pathname
			});
		}
		RZB2.ajax.feedBack.Load();
	});
	$('#modal_feedback').on('submit', 'form', function (e) {
		e.preventDefault();
		RZB2.ajax.feedBack.Post($(this));
	});
	// FEEDBACK - CONTACT FOR PRODUCT
	$('#modal_contact_product').on('show.bs.modal', function(e){
		var $button = $(e.relatedTarget);
		if (typeof RZB2.ajax.feedbackContact === 'undefined') {
			RZB2.ajax.feedbackContact = new RZB2.ajax.FormUnified({
				ID: 'modal_contact_product',
				AJAX_FILE: SITE_DIR + 'ajax/feedback_contact.php'
			});
			RZB2.ajax.feedbackContact.UpdateJS = function(){
				$('#modal_contact_product [name="romza_feedback[PRODUCT]"]').val(RZB2.ajax.feedbackContact.productId);
				$('#modal_contact_product [name="romza_feedback[QUANTITY]"]').closest('.textinput-wrapper').after(RZB2.ajax.feedbackContact.measureHTML);
			}
		}
		RZB2.ajax.feedbackContact.Load([], function(){
			var productId = $button.data('product-id');
			var offerId;
			if (offerId = $button.data('offer-id')) {
				productId = offerId;
			}
			RZB2.ajax.feedbackContact.productId = productId;
			RZB2.ajax.feedbackContact.measureHTML = ' - <span>' + $button.data('measure-name') + '</span>';
			RZB2.ajax.feedbackContact.UpdateJS();
		});
	}).on('submit', 'form', function (e) {
		e.preventDefault();
		RZB2.ajax.feedbackContact.Post($(this), function(res){
			if (res.indexOf("'fail'") == -1) {
				$('#modal_contact_product').modal('hide');
			} else {
				$('#modal_contact_product .content').html(res);
				RZB2.ajax.feedbackContact.UpdateJS();
			}
		}, false);
	});
	// FEEDBACK - SUBSCRIBE PRODUCT
	var $msp = $('#modal_subscribe_product');
	if ($msp.length > 0) {
		$msp.on('shown.bs.modal', function (e) {
			var $btn = $(e.relatedTarget);
			var data = [
				{'name': 'PRODUCT', 'value': $btn.data('product')}
			];
			$(this).find('form').rise_modal(data);
		});
	}
	$body.on('submit', '#modal_subscribe_product_form', function (e) {
		var $this = $(this);
		if (!formCheck($this)) {
			return false;
		} else {
			e.preventDefault();
			$this.send_modal();
			return false;
		}
	});

	// MAP
	$('#modal_address-on-map').on('show.bs.modal', function(){
		if (typeof RZB2.ajax.map != "undefined") return;

		RZB2.ajax.map = new RZB2.ajax.FormUnified({
			ID: 'modal_address-on-map',
			AJAX_FILE: SITE_DIR + "ajax/map.php",
		});
		RZB2.ajax.map.Load();
	});

	// CATALOG STORES
	$(document).on('open', '.store-info.notification-popup', function(e){
		var _ = $(this);
		if (_.data('loaded') == true) return;
		_.data('loaded', true);

		var params = [
			{name: "ITEM_ID",       value: _.data('id')},
			{name: "STORE_POSTFIX", value: _.data('postfix')}
		];

		var $content = _.find('.content');
		RZB2.ajax.Stores.Load($content, params, function(res){
			_.data('spinner').Stop();
		});
	});
	$('.store-info.notification-popup[data-state="shown"]').trigger('open');

	// SEARCH AJAX
	$('#search').on('click', 'button.btn-buy', function(e){
		e.preventDefault();
		var $_ = $(this);
		if ($_.hasClass('main-clicked') && $_.hasClass('forced')) {
			location.href = $("#bxdinamic_bitronic2_basket_string").attr("href");
			return;
		}
		if ($_.hasClass('buy')) {
			var spinner = RZB2.ajax.spinner($_);
			spinner.Start(smallSpinnerParams);
			RZB2.ajax.CatalogSection.AddToBasketSimple($_.data('product-id'), 1, spinner);
		}
	})
	.on('change', '#search-area', function(e){
		var _ = $(this);
		var $form = _.closest('form');
		_.find('option').each(function(){
			var category = 'category_' + $(this).data('category');
			if ($(this).val() == _.val()) {
				$form.addClass(category);
			} else {
				$form.removeClass(category);
			}
		});
		setTimeout(function(){
			$('#popup_ajax-search').velocity("finish");
			$('#search-field').focus();
		}, 50);
	});
	
	// QUICK VIEW
	if (typeof RZB2.ajax.quickView == "object") {
		$('#modal_quick-view')
			.on('shown.bs.modal',  RZB2.ajax.quickView.onModalShown)
			.on('hidden.bs.modal', RZB2.ajax.quickView.onModalHidden);
	}
	
	// BUY IN ONE CLICK
	$('.product-page, .catalog-page, .special-blocks, .search-results-page, .basket-big-page, .small-basket-buy-wrap').on('click', '.one-click-buy', oneClickBuyHandler);
	$('.viewed-products').find('[data-toggle="modal"]').off('click').on('click', function(e){
		var $this = $(this);
		$($this.data('target')).modal('show', this);
		if ($this.hasClass('one-click-buy')) {
			oneClickBuyHandler.call(this, e);
		}
	});
	$('#modal_quick-buy').on('submit', 'form', function(e){
		e.preventDefault();
		RZB2.ajax.oneClick.Post($(this));
	});

	// SUBSCRIBE IN FOOTER
	if (typeof window.frameCacheVars !== "undefined" && window.isFrameDataReceived == false) {
		BX.addCustomEvent("onFrameDataReceived", function (json){
			b2.init.formValidation('.form_footer-subscribe, .subscribe-edit', true);
		});
	} else {
		b2.init.formValidation('.form_footer-subscribe, .subscribe-edit', true);
	}

	// CLOSE POPUPS AND SAVE STATE TO COOKIE
	var $ysPopup = $('.yourcity-popup');
	if ($ysPopup.length > 0) {
		$ysPopup.on('click','a, button', function(){
			RZB2.utils.setCookie('YNS_IS_YOUR_CITY', 1);
		});
	}
	var $settings = $('#settings-toggle');
	if ($settings.length > 0) {
		$settings.on('click',function(e){
			RZB2.utils.setCookie('YNS_FIRST_HIT', 1);
		});
	}

	// RATING
	$(document).on('click', '.rating-stars i', function(e){
		var $div = $(this).closest('.rating-stars');
		if ($div.data('disabled') === true) return;

		var arParams = RZB2.ajax.Vote.arParams[$div.data('params')];
		RZB2.ajax.Vote.do_vote(this, arParams, e);
	});
}

var oneClickBuyHandler = function (e) {
	e.preventDefault();
	var $this = $(this);
	if (typeof RZB2.ajax.oneClick === 'undefined') {
		RZB2.ajax.oneClick = new RZB2.ajax.FormUnified({
			ID: 'modal_quick-buy',
			AJAX_FILE: SITE_DIR + "ajax/one_click.php",
		});
	}
	RZB2.ajax.oneClick.Load([
		{name: "id", value: $this.data('id')},
		{name: "RZ_BASKET", value: $this.data('basket')},
		{name: 'PROPS', value: $this.data('props')}
	]);
};

if (typeof domReady != "undefined" && domReady == true) {
	RZB2_initCommonHandlers(jQuery);
} else {
	jQuery(document).ready( RZB2_initCommonHandlers );
}

//@ sourceURL=js/back-end/handlers/commons.js
