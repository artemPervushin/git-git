b2.init.homePage = function(){
	b2.el.$coolSlider = $('#cool-slider');
	b2.el.$specialBlocks = $('#special-blocks');
	b2.el.specialSliders = [];
	if ( b2.s.coolsliderEnabled && b2.el.$coolSlider.length && typeof UmCoolSlider === 'function'){
		b2.el.coolSlider = new UmCoolSlider(b2.el.$coolSlider);
	}
	if(typeof b2.el.$specialBlocks !='undefined' && b2.el.$specialBlocks.length > 0) {
		b2.el.specialBlocks = new UmComboBlocks(b2.el.$specialBlocks, {
			bp: 767,
			mode: b2.el.$specialBlocks.data('sb-mode'),
			defaultExpanded: ( b2.el.$specialBlocks.data('sb-mode-def-expanded') ) ? 'all' : (serverSettings.sbModeDefExpanded || 0),
			onOpen: function(target){
				setTimeout(function(){
					initPhotoThumbs(target);
				}, 10);
				target.trigger('b2SblockTabOpen');
				var slider = target.find('.special-blocks-carousel').data('UmSlider');
				if (typeof slider !== 'undefined' && !!slider){
					slider.updatePages();
				}
			}
		});
	}
	if (b2.s.wowEffect == 'Y') {
		new WOW({
			offset: 100,
		}).init();
	}
	$(document).on('click', '.special-blocks-showall', function() {
		event.preventDefault();
		$(this).closest('.special-blocks-carousel').addClass('all-visible');
	});
	$('#comments-carousel').on('swipeleft', function() {
		$(this).carousel('next');
	}).on('swiperight', function() {
		$(this).carousel('prev');
	});
	$('#comments-carousel').each(function(){
		var number = $(this).find('.comment.item').length;
		var active = $(this).find('.comment.active').index();
		var dots = $();
		for (var i = 0; i < number; i++){
			dots = dots.add($('<i class="dot" data-target="#comments-carousel" data-slide-to="' + i + '"></i>'))
		}
		dots.eq(active).addClass('active');
		$(this).find('.carousel-indicators').append(dots);
	});

	initBigSlider();
	initSpecialBlocks(document);
	initCatalogHover(document);
	initPhotoThumbs(document);
	initTimers(document);

	function loadHomePageChunks(){
		require([
			'libs/bootstrap/carousel',
			'init/sliders/initBrandsCarousel',
			'util/basket',
//			'init/initBuyClick',  BACK_END not need in ready project
			'init/initVideo'
		], function(){
//			b2.init.buyClick();   BACK_END not need in ready project
			b2.init.brandsCarousel();
		})
	}

	if (windowLoaded) loadHomePageChunks();
	else $(window).load(loadHomePageChunks);
}