b2.init.floatingBuyBlock = function(){
	var buyBlock = $('.product-page .product-main .buy-block-wrap');
	// .product-page to filter out modal
	// .product-main to filter out collection buy-block
	var origin = $('.product-page').find('.buy-block-origin');
	var collectionMainItem = $('#main-item');
	var bbScrollFix;
	var bbScrollCheck;

	function floatBlockReset(){
		if ( bbScrollFix ){
			bbScrollFix.destroy();
			bbScrollFix = null;
		}
	}
	function colMainItemReset(){
		if ( bbScrollCheck ){
			bbScrollCheck.destroy();
			bbScrollCheck = null;
		}
		buyBlock.css({display: '', opacity: ''});
	}
	function floatBlockInit(){
		if ( !bbScrollFix){
			bbScrollFix = new UmScrollFix(buyBlock, origin, 0, 39);
		}
	}
	function colMainItemInit(){
		if ( !bbScrollCheck && collectionMainItem.length ){
			function initCheck(){
				if (typeof UmScrollCheck === 'function'){
					bbScrollCheck = new UmScrollCheck(collectionMainItem, {
						check: 'bottom',
						offset: 200,
						onCross: function(state){
							if ( state === 'before' ){
								buyBlock.velocity('fadeIn', 300);
							} else {
								buyBlock.velocity('fadeOut', 300);
							}
						}
					});
				} else setTimeout(initCheck, 1000);
			}
			initCheck();
		}
	}
	function buyBlockUpdate(){
		if ( Modernizr.mq('(max-width: 767px)') ) floatBlockReset();
		else floatBlockInit();

		if ( Modernizr.mq('(max-width: 991px)') ) colMainItemReset();
		else colMainItemInit();
	}
	buyBlockUpdate();

	// function to launch after window resize
	function onResizeComplete(){
		buyBlockUpdate();
	}
	// timer for window resize
	var resizeTimeout;
	$(window).resize(function(){
		clearTimeout(resizeTimeout);
		resizeTimeout = setTimeout(onResizeComplete, 300);
	});
}