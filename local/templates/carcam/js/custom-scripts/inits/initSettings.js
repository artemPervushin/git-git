function initSettings(){
//	SETTINGS SET HANDLER
	$(b2.s).on('set', function(e, name, value, byInput){
		// console.log(e, name, value);
		var nameCC = $.camelCase(name);
		//if ( b2.s[nameCC] === value ) return false;
	
		// saving previous, setting new value
		var prevValue = b2.s[nameCC];
		b2.s[nameCC] = value;

		// ensuring that b2.changedS contains only changed values
		if ( value === b2.initialS[nameCC]) delete b2.changedS[name];
		else if (b2.initialS[nameCC]) b2.changedS[name] = value;

		// looking for associated DOM element and updating data-<name>
		var $elDOM = $('[data-' + name + ']');
		if ( $elDOM.length > 0 ) $elDOM.attr('data-' + name, value);
		// setting associated input if set was triggered not by input
		if ( !byInput ){
			var $input = $('[name=' + name + ']');
			var inputType = ( $input.is('select') ) ? 'select' : $input.attr('type');
			switch (inputType){
				case 'select':
					$input.val(value).change(); // .change for styling plugins
					break;
				case 'text':
					$input.val(value);
					break;
				case 'radio':
					$input.filter('[value=' + value +']').prop('checked', true);
			break;
				case 'checkbox':
					$input.prop('checked', value);
			break;
				default:
					console.log('unknown input type in settings set: ', inputType);
			}
		}

		// property-specific actions and DOM manipulations via helper functions
		if (typeof b2.set[nameCC] === 'function'){
			b2.set[nameCC](value, {prevValue: prevValue, $elDOM: $elDOM});
		};

		// switching states of related elements/inputs
		b2.rel[nameCC] && b2.rel[nameCC].forEach(function(element){
			setRelated(value, element);
		});
	});
//	END OF SETTINGS SET HANDLER

//	SETTINGS CHANGES DISPATCHER FROM FORM
	b2.el.$settingsForm = $('#settings-panel-cblocks').on('change', function(e){
		var $t = $(e.target),
			name = $t.data('name'),
			value = $t.val();
		if (value === undefined || name === undefined) return;
		if (value === "false") value = false;
		else if (value === "true") value = true;
		else if (value === "checkbox" ) value = $t.is(':checked');

		//console.log('form changed by', name, 'with value ', value);

		$(b2.s).trigger('set', [name, value, true]);
	});
//	END OF CHANGES DISPATCHER FROM FORM

//	MODAL HANDLING
	var saving = false;
	b2.el.$settingsModal.on('show.bs.modal', function(){
		// saving copy of settings on moment of opening settings panel
		b2.initialS = $.extend({}, b2.s);
		b2.changedS = {};
		saving = false;
	}).on('hide.bs.modal', function(){
		if (false && !saving ) {
			for (var name in b2.changedS){
				// we know for sure that b2.changedS contains _changed_
				// values, so just reset them
				$(b2.s).trigger('set', [name, b2.initialS[$.camelCase(name)]]);
			}
		}
		saving = false;
	});
	b2.el.$settingsForm.on('submit', function(){
		saving = true;
		b2.el.$settingsModal.modal('hide');

		// SERVER SAVING GOES HERE, preferably via AJAX
		// because all DOM changes are already made, you only need to
		// update settings array on server.
		// - TIPS -
		// all changed settings are stored in b2.changedS object
		// access individual props via b2.changedS[name],
		// NOT b2.changedS.name, and name is NOT camel-cased

		//return false; // prevent default submitting
	});
	$('#submit-settings').on('click', function(){
		b2.el.$settingsForm.submit();
	});
//	END OF MODAL HANDLING
}