b2.init.selects = function(target){
	var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
	//if ( iOS ) return;
	// SELECT STYLING

	$.ikSelect && $(target).find('select').ikSelect({
		syntax: '<div class="ik_select_link"> \
					<span class="ik_select_link_text"></span> \
					<div class="trigger"></div> \
				</div> \
				<div class="ik_select_dropdown"> \
					<div class="ik_select_list"> \
					</div> \
				</div>',
		dynamicWidth: true,
		ddMaxHeight: 120,
		//equalWidths: true,
		onShow: function(inst){
			//console.log(inst);
			
			if ( inst.$dropdown.outerWidth() < inst.$link.outerWidth() ){
				inst.$dropdown.css('width', inst.$link.outerWidth());
			}
			inst.$link.addClass('opened');
			if ( inst.$listInner.hasClass('baron') ){
				return;
			}
			$().baron && inst.$listInner.addClass('scroller')
			.append('<div class="scroller__track scroller__track_v"> \
						<div class="scroller__bar scroller__bar_v"></div> \
					</div>')
			.baron({
			 	bar: inst.$list.find('.scroller__bar_v'),
	 		 	barOnCls: 'baron',
	 		 	direction: 'v'
			});

			
		},
		onHide: function(inst){
			inst.$link.removeClass('opened');
		}
	});
	// END OF SELECT STYLING
}