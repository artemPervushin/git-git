function initModalSettings(){
	// SETTINGS MODAL PROCESSING
	b2.el.inputSliderWidth = $('input[name="big-slider-width"]');
	b2.el.inputCatalogPlacement = $('input[name="catalog-placement"]');
	
	bs.dummy.$el = $('.big-slider.dummy');
	if ( bs.dummy.$el.length > 0 ){
		bs.dummy.media = bs.dummy.$el.find('.media');
		bs.dummy.text = bs.dummy.$el.find('.text');
	}
	bs.curSettingsFor = 'all';
	bs.curSettingsForInput = $('[name="bs_cur-settings-for"]');
	bs.curBlockInputs = {};
	bs.curBlockInputs.text = $('[name="bs_cur-block"][value="text"]');
	bs.curBlockInputs.media = $('[name="bs_cur-block"][value="media"]');
	bs.hAlignInputs = $('[name="bs_h-align"]');
	bs.vAlignInputs = $('[name="bs_v-align"]');
	bs.textAlignInput = $('[name="bs_text-align"]');
	bs.textAlignWrap = $('#bs_text-align-wrap');
	bs.animInput = $('[name="bs_anim"]');

	var settingsComboBlocks = $('#settings-panel-cblocks'),
		settingsView = (settingsComboBlocks.length) ? 
			new UmComboBlocks(settingsComboBlocks, {
				bp: 767,
			}) : null;
	
	$('.settings-view-link').click(function(){
		var _ = $(this);
		if ( _.hasClass('active')) return;

		_.addClass('active').siblings('.active').removeClass('active');
		var mode = _.attr('data-mode');
		settingsView.switchMode(mode);
	})

	$('.settings-to-defaults').click(function(){
		var form = $(this).closest('.modal-settings').children('.form_settings');
		form.find('input[type="radio"][data-default]').attr('checked', true).change();
		form.find('option[data-default]').attr('selected', true).change();
		form.find('input[type="text"], input[type="hidden"]').each(function(){
			var _ = $(this);
			var defVal = _.attr('data-default');
			if ( defVal ) _.val(defVal).change();
			if ( _.hasClass('minicolors') ) _.keyup(); // to trigger color change
		});
		//from backend with love ^_^
		form.find('input[type="checkbox"]').each(function (e) {
			var $this = $(this);
			var defVal = $this.attr('data-default') | 0;
			if (defVal == 1) {
				$this.prop('checked', true);
			} else {
				$this.prop('checked', false);
			}
		});
		//form.find('.theme-demo[data-default]').trigger('click');
		//form.submit();
	})
// ATTENTION BACK-END HAS BEEN THERE
	$('.simple-slider').each(function(){
		var $t = $(this),
			dataName = $t.data('name'),
			postfix = $t.data('postfix');
		if (typeof postfix == 'undefined') {
			postfix = '';
		}
/*	var bigHeightSlider = $('#big-height-slider').noUiSlider({*/ $t.noUiSlider({
/*		start: b2.s.bs_height,*/   start: $t.data('start'),
		connect: "lower",
/*		step: 0.01,*/              step: $t.data('step'),
		range: {
/*			'min': 20,*/             'min': $t.data('min'),
/*			'max': 50*/              'max': $t.data('max')
		},
		format: wNumb({
			decimals: 2,
/*			postfix: '%'*/           postfix: postfix
		})
	});
/*	bigHeightSlider.Link('lower').to($('#bs_height-input'));*/ $t.Link('lower').to($('#settings_' + dataName)); if($t.data('set')) {
/*	bigHeightSlider.on('slide set', function(){*/  $t.on('slide set', function(){
/*		$('[data-bs_height]').css('padding-bottom', bigHeightSlider.val());*/ $('[data-' + dataName + ']').css('padding-bottom', $t.val());
/*	});*/
			});
		}
	});
// END OF BACK-END INTRUSION
	$('.range-slider.percents').each(function(){
		var $t = $(this),
			$inputLower = $t.siblings('.textinput.limit-start'),
			$inputUpper = $t.siblings('.textinput.limit-end');
		var orient =  ($t.parent().hasClass('vertical')) ? 'vertical' : 'horizontal';
		$t.noUiSlider({
			start: [0, 100],
			connect: true,
			behaviour: 'snap',
			step: 1,
			orientation: orient,
			range: {
				'min': 0,
				'max': 100
			},
			format: wNumb({
				postfix: '%'
			})
		});

		$t.Link('lower').to('-inline-<div class="handle-inner"></div>', function ( value ) {
			// The tooltip HTML is 'this', so additional
			// markup can be inserted here.
			$(this).html(
				'<div class="arrow"></div><div class="stripes"></div> \
				<span class="text">' + value +'</span>'
			);
		});
		$t.Link('lower').to($inputLower);
		$t.Link('upper').to('-inline-<div class="handle-inner"></div>', function ( value ) {
			// The tooltip HTML is 'this', so additional
			// markup can be inserted here.
			var newVal = 100 - parseInt(value);
			$(this).html(
				'<div class="arrow"></div><div class="stripes"></div> \
				<span class="text">' + newVal +'%</span>'
			);
		});

		if ( $t.hasClass('h-limits') ){
			bs.hLimits = $t;
			$t.on('slide set', function(){
				var val = parseInt($t.val()[1]),
					newVal = (100 - val) + '%';
				$inputUpper.val(newVal);
				bs.dummy[b2.s.bs_curBlock] && bs.dummy[b2.s.bs_curBlock].css({
					'left': $t.val()[0],
					'right': newVal
				});
			});
		} else if ( $t.hasClass('v-limits') ){
			bs.vLimits = $t;
			$t.on('slide set', function(){
				var val = parseInt($t.val()[1]),
					newVal = (100 - val) + '%';
				$inputUpper.val(newVal);
				bs.dummy[b2.s.bs_curBlock] && bs.dummy[b2.s.bs_curBlock].css({
					'top': $t.val()[0],
					'bottom': newVal
				});
			});
		}
		
		$inputUpper.val((100 - parseInt($t.val()[1])) + '%');

		$inputUpper.on('change', function(){
			var val = parseInt($(this).val());
			if ( isNaN(val) || val < 0 ) val = 0;
			if ( val > 100 ) val = 100;
			var newVal = 100 - val;
			$t.val([null, newVal]);
		});
	});

	$('input.minicolors').each(function(){
		var defValue = $(this).attr('data-default');
		$(this).minicolors({
			position: 'bottom right',
			dataUris: false,
			control: 'wheel',
			defaultValue: defValue,
		});
	});
}