function initGenInfoToggle(){
	var genInfo = $('.general-info');
	if (!genInfo || !genInfo.length) return;
	var genDesc = genInfo.find('.desc');
	var genInfoToggle = genInfo.find('.link');
	var infoHeightLimit, genInfoScrollHeight;
	this.update = function(){
		if (!genDesc || !genDesc.length) return;
		infoHeightLimit = parseInt(genDesc.css('max-height'));
		genInfoScrollHeight = genDesc.get(0).scrollHeight;

		// button is added only if content is higher than limit by more than 20px
		// otherwise, open content fully
		if ( infoHeightLimit - genInfoScrollHeight > -20 ){
			genInfo.addClass('toggled');
			genInfoToggle.hide();
		} else {
			genInfo.removeClass('toggled');
			genInfoToggle.show();
		}
	}

	this.update();
	genInfoToggle.on('click.genInfoToggle', function(e){
		if (!genInfo || !genInfo.length) return;
		if ( genInfo.hasClass('toggled') ){
			genDesc.velocity({
				'max-height': infoHeightLimit
			}, 250, function(){
				genInfo.removeClass('toggled');
			});
		} else {
			genDesc.velocity({
				'max-height': genInfoScrollHeight
			}, 250, function(){
				genInfo.addClass('toggled');
			})
		}
	});

	b2.quickViewGenInfoInited = true;
}