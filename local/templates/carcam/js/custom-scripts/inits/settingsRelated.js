function setRelated(value, related){
	var target = $(related.selector);
	var state = related.states[value];

	state && state.forEach(function(cur){
		target[cur.action].apply(target, cur.arguments || []);
	})
}

b2.rel.menuHitsEnabled = [
	{
		selector: '#menu-hits-pos-wrap',
		states: {
			'true': [
				{
					action: 'removeClass',
					arguments: ['hidden']
				}
			],
			'false': [
				{
					action: 'addClass',
					arguments: ['hidden']
				},
			]
		},
	},
];

b2.rel.bs_curBlock = [
	{
		selector: '#bs_text-align-wrap',
		states: {
			'text':  [{ action: 'show'}],
			'media': [{ action: 'hide'}]
		}
	},
]