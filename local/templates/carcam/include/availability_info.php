<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
  * @var $availableClass (string) - add text inside class attribute
  * @var $availableFrame (bool) - do we need to create dynamic frame
  * @var $availableID (string) - add attribute "id" with variable's content
  * @var $availableItemID (int) - iblock element id
  * @var $availableOnRequest (bool) - is our product available on request only
  * @var $availableParamsName (string) - name of an array with catalog params
  * @var $availableStoresPostfix (string) - postfix to add to stores container id attribute
  * @var $availableSubscribe (string) - Y if can subscribe
  * @var $bShowEveryStatus (bool) - do we need to put every status into html (for SKU switch)
  * @var $bShowStore (bool) - do we need to show stores popup
  **/

if ($headerLangIncluded !== true) {
	\Bitrix\Main\Localization\Loc::loadMessages(SITE_TEMPLATE_PATH . '/header.php');
	$headerLangIncluded = true;
}

$availableFrameID = 'bxdinamic_availability_' . $this->randString();
?>

<div class="availability-info"<?if($availableFrame === true):?> id="<?=$availableFrameID?>"<?endif?>>
<?
if ($availableFrame === true) {
	$frame = $this->createFrame($availableFrameID, false)->begin(CRZBitronic2Composite::insertCompositLoader());
}
?>
<div class="availability-status <?=empty($availableClass)?'':$availableClass?>"<?if(!empty($availableID)):?> id="<?=$availableID?>"<?endif?>>
<? if(!empty($bShowEveryStatus) || $availableClass == 'in-stock' || empty($availableClass)): ?>
	<div class="when-in-stock">
		<div class="info-tag"
			<?if($bShowStore):?>
			data-tooltip title="<?=GetMessage('BITRONIC2_PRODUCT_AVAILABLE_STORES')?>"
			data-placement="right"
			data-popup=">.store-info" data-position="centered bottom"
			<?endif?>
			>
			<span class="text"><?=GetMessage('BITRONIC2_PRODUCT_AVAILABLE_TRUE')?></span>
			<?if($bShowStore):?>
			<div class="store-info notification-popup" data-id="<?=$availableItemID?>" data-postfix="<?=$availableStoresPostfix?>">
				<div class="content" id="catalog_store_amount_div_<?=$availableStoresPostfix?>_<?=$availableItemID?>">
				</div>
			</div>
			<?endif?>
		</div><!-- .info-tag -->
	</div><!-- /.when-in-stock -->
<? endif ?>
<? if(!empty($bShowEveryStatus) || $availableClass == 'available-for-order' || $availableClass == 'available-on-request'): ?>
	<div class="when-available-for-order<?if($availableOnRequest):?> when-available-on-request<?endif?>">
		<div class="info-tag" <? /* TODO 
			title="title" 
			data-tooltip
			data-placement="bottom"
			data-toggle="modal"
			data-target="#modal_place-order"*/?>
			>
			<span class="text on-request"><?=GetMessage('BITRONIC2_PRODUCT_AVAILABLE_ON_REQUEST')?></span>
			<span class="text  for-order"><?=GetMessage('BITRONIC2_PRODUCT_AVAILABLE_FOR_ORDER') ?></span>
		</div>
		<div class="info-info">
			<?$APPLICATION->IncludeComponent('bitrix:main.include', '', array("PATH" => SITE_DIR."include_areas/catalog/for_order_text.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => "include_areas_template.php"), $component, array("HIDE_ICONS"=>"Y"))?>
		</div>
	</div>
<? endif ?>
<? if(!empty($bShowEveryStatus) || $availableClass == 'out-of-stock'): ?>
	<div class="when-out-of-stock">
		<div class="info-tag"
			<? if ($availableSubscribe == 'Y'): ?>
			title="<?=GetMessage('BITRONIC2_PRODUCT_SUBSCRIBE')?>"
			data-tooltip
			data-product="<?= $availableItemID ?>"
			data-placement="bottom"
			data-toggle="modal"
			data-target="#modal_subscribe_product"
			<? endif ?>
			>
			<span class="text"><?=GetMessage('BITRONIC2_PRODUCT_AVAILABLE_FALSE')?></span>
		</div><!-- .info-tag -->
	</div>
<? endif ?>
</div><!-- .availability-status -->
<? if ($availableFrame === true) $frame->end() ?>
</div><!-- .availability-info -->
