<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?echo '<?xml version="1.0" encoding="'. LANG_CHARSET. '"?>';?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?=$arResult["DATE"]?>">
    <shop>
        <name><?=$arResult["SITE"]?></name>
        <company><?=$arResult["COMPANY"]?></company>
        <url><?="http://".$_SERVER["SERVER_NAME"]?></url>
        
        <currencies>
            <?if ( !empty($arResult["CURRENCIES"]) ):?>
                <?foreach($arResult["CURRENCIES"] as $k=>$cur):?>
                    <?if(!empty($cur) && $cur != 'RUR'):?><currency id="<?=$cur?>"<?if ( $k == 0 ):?> rate="1"<?endif;?>/><?endif;?>
                <?endforeach;?>
            <?else:?>
                <currency id="<?=$arParams["CURRENCY"]?>" rate="1"/>
            <?endif;?>
        </currencies>
        
    <categories>
<?foreach($arResult["CATEGORIES"] as $arCategory):?>
        <category id="<?=$arCategory["ID"]?>"<?
if($arCategory["PARENT"])
    echo ' parentId="'. $arCategory['PARENT']. '"';
?>><?=$arCategory["NAME"]?></category>
<?endforeach;?>
    </categories>   
    <delivery-options><option cost="0" days="0"/></delivery-options>
        <offers>
        <?foreach($arResult["OFFER"] as $arOffer):?>
            <offer id="<?=$arOffer["ID"]?>" available="<?=$arOffer["AVAIBLE"]?>">
				<?if($arOffer["ID"] == 18193 || $arOffer["ID"] == 19252 || $arOffer["ID"] == 16529 || $arOffer["ID"] == 12549 || $arOffer["ID"] == 12543 || $arOffer["ID"] == 9242 || $arOffer["ID"] == 8945 || $arOffer["ID"] == 6309 || $arOffer["ID"] == 6308|| $arOffer["ID"] == 5720 || $arOffer["ID"] == 5137 || $arOffer["ID"] == 4498 || $arOffer["ID"] == 4497 || $arOffer["ID"] == 4491 || $arOffer["ID"] == 4489 || $arOffer["ID"] == 4480 || $arOffer["ID"] == 4477 || $arOffer["ID"] == 4475 || $arOffer["ID"] == 4474 || $arOffer["ID"] == 4382 || $arOffer["ID"] == 2388 || $arOffer["ID"] == 2105 || $arOffer["ID"] == 1980 || $arOffer["ID"] == 1416 || $arOffer["ID"] == 1413 || $arOffer["ID"] == 1406 || $arOffer["ID"] == 560 || $arOffer["ID"] == 529 || $arOffer["ID"] == 500){?>
					<url><?=$arOffer["URL"]?>?region=ek&amp;roistat=yamarket2_21319133_<?=$arOffer["ID"]?>&amp;utm_source=YMarket&amp;utm_medium=cpc&amp;utm_content=offer_<?=$arOffer["ID"]?>&amp;utm_campaign=ek</url>
				<?}else{?>	
					<url><?=$arOffer["URL"]?>&amp;region=ek&amp;roistat=yamarket2_21319133_<?=$arOffer["ID"]?>&amp;utm_source=YMarket&amp;utm_medium=cpc&amp;utm_content=offer_<?=$arOffer["ID"]?>&amp;utm_campaign=ek</url>
                <?}?>
                <price><?=$arOffer["PRICE"]?></price>
                <vendor>������</vendor>
                <currencyId>
                    <?if ( !empty($arOffer["CURRENCY"]) ):?>
                        <?=$arOffer["CURRENCY"]?>
                    <?else:?>
                        <?=$arParams["CURRENCY"]?>
                    <?endif;?>
                </currencyId>
                
                <categoryId><?=$arOffer["CATEGORY"]?></categoryId>
                
                <?if ( !empty( $arOffer["PICTURE"] ) ):?>
                    <picture><?=$arOffer["PICTURE"]?></picture>
                <?endif;?>
                
                <?foreach ($arOffer["MORE_PHOTO"] as $pic):?>
                    <picture><?=$pic?></picture>
                <?endforeach;?>                 
               <delivery-options><option cost="0" days="0"/></delivery-options>
                <name><?=$arOffer["MODEL"]?></name>
                <?if($arOffer["DESCRIPTION"]):?><description><?=$arOffer["DESCRIPTION"]?></description><?endif?>              
                <?foreach($arParams["PARAMS"] as $k=>$v): if($arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_VALUE"]):?>
                 <param name="<?=$arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_NAME"]?>"><?=$arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_VALUE"]?></param>
                <?endif; endforeach;?>
                <sales_notes>�� ����������</sales_notes>
            </offer>
        <?endforeach;?>
        </offers>
    </shop>
</yml_catalog>