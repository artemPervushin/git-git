<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?echo '<?xml version="1.0" encoding="'. LANG_CHARSET. '"?>';?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?=$arResult["DATE"]?>">
    <shop>
        <name><?=$arResult["SITE"]?></name>
        <company><?=$arResult["COMPANY"]?></company>
        <url><?="http://".$_SERVER["SERVER_NAME"]?></url>
        
        <currencies>
            <?if ( !empty($arResult["CURRENCIES"]) ):?>
                <?foreach($arResult["CURRENCIES"] as $k=>$cur):?>
                    <?if(!empty($cur) && $cur != 'RUR'):?><currency id="<?=$cur?>"<?if ( $k == 0 ):?> rate="1"<?endif;?>/><?endif;?>
                <?endforeach;?>
            <?else:?>
                <currency id="<?=$arParams["CURRENCY"]?>" rate="1"/>
            <?endif;?>
        </currencies>
        
    <categories>
<?foreach($arResult["CATEGORIES"] as $arCategory):?>
        <category id="<?=$arCategory["ID"]?>"<?
if($arCategory["PARENT"])
    echo ' parentId="'. $arCategory['PARENT']. '"';
?>><?=$arCategory["NAME"]?></category>
<?endforeach;?>
    </categories>   
    <delivery-options><option cost="0" days="0"/></delivery-options>
        <offers>
        <?foreach($arResult["OFFER"] as $arOffer):?>
            <offer id="<?=$arOffer["ID"]?>" available="<?=$arOffer["AVAIBLE"]?>">
			
<?if($arOffer["ID"] == 18193 
|| $arOffer["ID"] == 17910 
|| $arOffer["ID"] == 13382 
|| $arOffer["ID"] == 12991 
|| $arOffer["ID"] == 9082 
|| $arOffer["ID"] == 8448 
|| $arOffer["ID"] == 8442 
|| $arOffer["ID"] == 8438 
|| $arOffer["ID"] == 8433 
|| $arOffer["ID"] == 8429 
|| $arOffer["ID"] == 8396 
|| $arOffer["ID"] == 8377 
|| $arOffer["ID"] == 8368 
|| $arOffer["ID"] == 8354 
|| $arOffer["ID"] == 7847 
|| $arOffer["ID"] == 7813 
|| $arOffer["ID"] == 7811 
|| $arOffer["ID"] == 7500 
|| $arOffer["ID"] == 7494 
|| $arOffer["ID"] == 7492 
|| $arOffer["ID"] == 5687 
|| $arOffer["ID"] == 5685 
|| $arOffer["ID"] == 5682 
|| $arOffer["ID"] == 5676 
|| $arOffer["ID"] == 4692  
|| $arOffer["ID"] == 19252 
|| $arOffer["ID"] == 16529 
|| $arOffer["ID"] == 12549 
|| $arOffer["ID"] == 12543 
|| $arOffer["ID"] == 9242 
|| $arOffer["ID"] == 8945 
|| $arOffer["ID"] == 6309 
|| $arOffer["ID"] == 6308 
|| $arOffer["ID"] == 5720
|| $arOffer["ID"] == 5137 
|| $arOffer["ID"] == 4498 
|| $arOffer["ID"] == 4497 
|| $arOffer["ID"] == 4491 
|| $arOffer["ID"] == 4489 
|| $arOffer["ID"] == 4480 
|| $arOffer["ID"] == 4477 
|| $arOffer["ID"] == 4475 
|| $arOffer["ID"] == 4474 
|| $arOffer["ID"] == 4382 
|| $arOffer["ID"] == 2388 
|| $arOffer["ID"] == 2105 
|| $arOffer["ID"] == 1980 
|| $arOffer["ID"] == 1416 
|| $arOffer["ID"] == 1413 
|| $arOffer["ID"] == 1406 
|| $arOffer["ID"] == 560 
|| $arOffer["ID"] == 529 
|| $arOffer["ID"] == 8809 
|| $arOffer["ID"] == 500 
|| $arOffer["ID"] == 60889 
|| $arOffer["ID"] == 60810 
|| $arOffer["ID"] == 60747 
|| $arOffer["ID"] == 60889 
|| $arOffer["ID"] == 60888 
|| $arOffer["ID"] == 60857 
|| $arOffer["ID"] == 60858 
|| $arOffer["ID"] == 60859 
|| $arOffer["ID"] == 60990 
|| $arOffer["ID"] == 60992 
|| $arOffer["ID"] == 22037 
|| $arOffer["ID"] == 21985 
|| $arOffer["ID"] == 22042 
|| $arOffer["ID"] == 22036 
|| $arOffer["ID"] == 21997 
|| $arOffer["ID"] == 21996 
|| $arOffer["ID"] == 21994 
|| $arOffer["ID"] == 21992 
|| $arOffer["ID"] == 21991 
|| $arOffer["ID"] == 21990 
|| $arOffer["ID"] == 21988 
|| $arOffer["ID"] == 21986 
|| $arOffer["ID"] == 21984 
|| $arOffer["ID"] == 60812 
|| $arOffer["ID"] == 60815 
|| $arOffer["ID"] == 60816 
|| $arOffer["ID"] == 60817 
|| $arOffer["ID"] == 3484 
|| $arOffer["ID"] == 4152 
|| $arOffer["ID"] == 60830 
|| $arOffer["ID"] == 60746 
|| $arOffer["ID"] == 60745 
|| $arOffer["ID"] == 60744 
|| $arOffer["ID"] == 60743 
|| $arOffer["ID"] == 60740 
|| $arOffer["ID"] == 60733 
|| $arOffer["ID"] == 60735 
|| $arOffer["ID"] == 61078 
|| $arOffer["ID"] == 61114 
|| $arOffer["ID"] == 61115 
|| $arOffer["ID"] == 61162 
|| $arOffer["ID"] == 61189 
|| $arOffer["ID"] == 61191 
|| $arOffer["ID"] == 61193 
|| $arOffer["ID"] == 61194 
|| $arOffer["ID"] == 61199 
|| $arOffer["ID"] == 61200 
|| $arOffer["ID"] == 61201 
|| $arOffer["ID"] == 61204 
|| $arOffer["ID"] == 61361 
|| $arOffer["ID"] == 61478 
|| $arOffer["ID"] == 61479 
|| $arOffer["ID"] == 61480 
|| $arOffer["ID"] == 61481 
|| $arOffer["ID"] == 61483 
|| $arOffer["ID"] == 61484 
|| $arOffer["ID"] == 61485 
|| $arOffer["ID"] == 61486 
|| $arOffer["ID"] == 61103 
|| $arOffer["ID"] == 61102 
|| $arOffer["ID"] == 60747 
|| $arOffer["ID"] == 66823 
|| $arOffer["ID"] == 66828 
|| $arOffer["ID"] == 66830 
|| $arOffer["ID"] == 66831 
|| $arOffer["ID"] == 66757 
|| $arOffer["ID"] == 61376 
|| $arOffer["ID"] == 66742 
|| $arOffer["ID"] == 61078 
|| $arOffer["ID"] == 61659 
|| $arOffer["ID"] == 61658 
|| $arOffer["ID"] == 61657 
|| $arOffer["ID"] == 61656 
|| $arOffer["ID"] == 61649 
|| $arOffer["ID"] == 61446 
|| $arOffer["ID"] == 61064 
|| $arOffer["ID"] == 61063 
|| $arOffer["ID"] == 61711 
|| $arOffer["ID"] == 61401 
|| $arOffer["ID"] == 61057 
|| $arOffer["ID"] == 61333 
|| $arOffer["ID"] == 61324 
|| $arOffer["ID"] == 61314 
|| $arOffer["ID"] == 22116
|| $arOffer["ID"] == 17915
|| $arOffer["ID"] == 66741
|| $arOffer["ID"] == 66740
|| $arOffer["ID"] == 66738
|| $arOffer["ID"] == 60593
|| $arOffer["ID"] == 60592
|| $arOffer["ID"] == 60591
|| $arOffer["ID"] == 60590
|| $arOffer["ID"] == 60537
|| $arOffer["ID"] == 60536
|| $arOffer["ID"] == 60111
|| $arOffer["ID"] == 60109
|| $arOffer["ID"] == 60108
|| $arOffer["ID"] == 60105
|| $arOffer["ID"] == 13199
|| $arOffer["ID"] == 12629
|| $arOffer["ID"] == 9397
|| $arOffer["ID"] == 9392
|| $arOffer["ID"] == 9389
|| $arOffer["ID"] == 9382
|| $arOffer["ID"] == 9380
|| $arOffer["ID"] == 9378
|| $arOffer["ID"] == 9375   
|| $arOffer["ID"] == 9373
|| $arOffer["ID"] == 9078
|| $arOffer["ID"] == 9400
|| $arOffer["ID"] == 9403
|| $arOffer["ID"] == 9387
|| $arOffer["ID"] == 9410
|| $arOffer["ID"] == 9406 
|| $arOffer["ID"] == 60037 
|| $arOffer["ID"] == 60036 
|| $arOffer["ID"] == 60035 
|| $arOffer["ID"] == 12436 
|| $arOffer["ID"] == 61648 
|| $arOffer["ID"] == 19469 
|| $arOffer["ID"] == 8159 
|| $arOffer["ID"] == 13282 
|| $arOffer["ID"] == 19482 
|| $arOffer["ID"] == 8147 
|| $arOffer["ID"] == 19481 
|| $arOffer["ID"] == 19473
|| $arOffer["ID"] == 8141 
|| $arOffer["ID"] == 8143 
|| $arOffer["ID"] == 8145 
|| $arOffer["ID"] == 8147 
|| $arOffer["ID"] == 8149 
|| $arOffer["ID"] == 8151 
|| $arOffer["ID"] == 8153 
|| $arOffer["ID"] == 8155 
|| $arOffer["ID"] == 8157 
|| $arOffer["ID"] == 8159 
|| $arOffer["ID"] == 8161 
|| $arOffer["ID"] == 8454 
|| $arOffer["ID"] == 8463 
|| $arOffer["ID"] == 8477 
|| $arOffer["ID"] == 8480 
|| $arOffer["ID"] == 8484 
|| $arOffer["ID"] == 8488 
|| $arOffer["ID"] == 8495 
|| $arOffer["ID"] == 13280 
|| $arOffer["ID"] == 13282 
|| $arOffer["ID"] == 13284 
|| $arOffer["ID"] == 13286 
|| $arOffer["ID"] == 19464 
|| $arOffer["ID"] == 19465 
|| $arOffer["ID"] == 19467 
|| $arOffer["ID"] == 19468 
|| $arOffer["ID"] == 19469 
|| $arOffer["ID"] == 19470 
|| $arOffer["ID"] == 19471 
|| $arOffer["ID"] == 19472 
|| $arOffer["ID"] == 19473 
|| $arOffer["ID"] == 19475 
|| $arOffer["ID"] == 19478 
|| $arOffer["ID"] == 19480 
|| $arOffer["ID"] == 19481 
|| $arOffer["ID"] == 19482 
|| $arOffer["ID"] == 19483 
|| $arOffer["ID"] == 60597 
|| $arOffer["ID"] == 60598 
|| $arOffer["ID"] == 61642 
|| $arOffer["ID"] == 61644 
|| $arOffer["ID"] == 61645 
|| $arOffer["ID"] == 61647 
|| $arOffer["ID"] == 61648 
|| $arOffer["ID"] == 12430  
|| $arOffer["ID"] == 12436  
|| $arOffer["ID"] == 12432   
|| $arOffer["ID"] == 12433  
|| $arOffer["ID"] == 12429  
|| $arOffer["ID"] == 12434  
|| $arOffer["ID"] == 12431  
|| $arOffer["ID"] == 61648  
|| $arOffer["ID"] == 61648   
|| $arOffer["ID"] == 8809 
|| $arOffer["ID"] == 8813 
|| $arOffer["ID"] == 8818 
|| $arOffer["ID"] == 8821 
|| $arOffer["ID"] == 8824 
|| $arOffer["ID"] == 8826 
|| $arOffer["ID"] == 8935 
|| $arOffer["ID"] == 61048 
|| $arOffer["ID"] == 61047 
|| $arOffer["ID"] == 61046  
|| $arOffer["ID"] == 19050 
|| $arOffer["ID"] == 17849 
|| $arOffer["ID"] == 17848 
|| $arOffer["ID"] == 8852 
|| $arOffer["ID"] == 8851 
|| $arOffer["ID"] == 8850 
|| $arOffer["ID"] == 7491 
|| $arOffer["ID"] == 7490 
|| $arOffer["ID"] == 7489 
|| $arOffer["ID"] == 7488 
|| $arOffer["ID"] == 7487 
|| $arOffer["ID"] == 7486 
|| $arOffer["ID"] == 7485 
|| $arOffer["ID"] == 7484 
|| $arOffer["ID"] == 7483 
|| $arOffer["ID"] == 7482 
|| $arOffer["ID"] == 7481 
|| $arOffer["ID"] == 11578 
|| $arOffer["ID"] == 11577 
|| $arOffer["ID"] == 11576 
|| $arOffer["ID"] == 11561  
|| $arOffer["ID"] == 11560  
|| $arOffer["ID"] == 11559 
|| $arOffer["ID"] == 11558 
|| $arOffer["ID"] == 11557 
|| $arOffer["ID"] == 13352 
|| $arOffer["ID"] == 9312 
|| $arOffer["ID"] == 9311 
|| $arOffer["ID"] == 9310  
|| $arOffer["ID"] == 9109 
|| $arOffer["ID"] == 6561  
|| $arOffer["ID"] == 6560 
|| $arOffer["ID"] == 60838 
|| $arOffer["ID"] == 60837 
|| $arOffer["ID"] == 12962 
|| $arOffer["ID"] == 12954 
|| $arOffer["ID"] == 12936 
|| $arOffer["ID"] == 10396 
|| $arOffer["ID"] == 10394 
|| $arOffer["ID"] == 10384 
|| $arOffer["ID"] == 10337 
|| $arOffer["ID"] == 10332 
|| $arOffer["ID"] == 7566 
|| $arOffer["ID"] == 7564 
|| $arOffer["ID"] == 7562 
|| $arOffer["ID"] == 7560 
|| $arOffer["ID"] == 7558 
|| $arOffer["ID"] == 7556 
|| $arOffer["ID"] == 7554 
|| $arOffer["ID"] == 7552 
|| $arOffer["ID"] == 7550 
|| $arOffer["ID"] == 7548 
|| $arOffer["ID"] == 67025 
|| $arOffer["ID"] == 66968 
|| $arOffer["ID"] == 66972 
|| $arOffer["ID"] == 67031 
|| $arOffer["ID"] == 66999 ){?>

            
            <url><?=$arOffer["URL"]?>?region=perm&amp;roistat=yamarket2_21351973_<?=$arOffer["ID"]?>&amp;utm_source=market.yandex.ru&amp;utm_medium=cpc&amp;utm_content=<?=$arOffer["ID"]?>&amp;utm_campaign=permcarcam</url>
				<?}else{?>	
					<url><?=$arOffer["URL"]?>&amp;region=perm&amp;roistat=yamarket2_21351973_<?=$arOffer["ID"]?>&amp;utm_source=market.yandex.ru&amp;utm_medium=cpc&amp;utm_content=<?=$arOffer["ID"]?>&amp;utm_campaign=permcarcam</url>
                <?}?>
                <price><?=$arOffer["PRICE"]?></price>
                 <vendor>������</vendor>
                <currencyId>
                    <?if ( !empty($arOffer["CURRENCY"]) ):?>
                        <?=$arOffer["CURRENCY"]?>
                    <?else:?>
                        <?=$arParams["CURRENCY"]?>
                    <?endif;?>
                </currencyId>
                
                <categoryId><?=$arOffer["CATEGORY"]?></categoryId>
                
                <?if ( !empty( $arOffer["PICTURE"] ) ):?>
                    <picture><?=$arOffer["PICTURE"]?></picture>
                <?endif;?>
                
                <?foreach ($arOffer["MORE_PHOTO"] as $pic):?>
                    <picture><?=$pic?></picture>
                <?endforeach;?>                 
                <?if ($arOffer["PRICE"]>2990){?>
                    <delivery-options><option cost="0" days="0"/></delivery-options>
                <?}else{?>
                    <delivery-options><option cost="400" days="0"/></delivery-options>
                <?}?> 
                <name><?=$arOffer["MODEL"]?></name>
                <?if($arOffer["DESCRIPTION"]):?><description><?=$arOffer["DESCRIPTION"]?></description><?endif?>              
                <?foreach($arParams["PARAMS"] as $k=>$v): if($arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_VALUE"]):?>
                 <param name="<?=$arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_NAME"]?>"><?=$arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_VALUE"]?></param>
                <?endif; endforeach;?>
                <sales_notes>�/� ��������, ��������,����.�����,������, ���� ��</sales_notes>
                <manufacturer_warranty>true</manufacturer_warranty>
            </offer>
        <?endforeach;?>
        </offers>
    </shop>
</yml_catalog>