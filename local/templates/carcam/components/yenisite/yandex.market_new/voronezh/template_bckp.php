<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?echo '<?xml version="1.0" encoding="'. LANG_CHARSET. '"?>';?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?=$arResult["DATE"]?>">
    <shop>
        <name><?=$arResult["SITE"]?></name>
        <company><?=$arResult["COMPANY"]?></company>
        <url><?="http://".$_SERVER["SERVER_NAME"]?></url>
        
        <currencies>
            <?if ( !empty($arResult["CURRENCIES"]) ):?>
                <?foreach($arResult["CURRENCIES"] as $k=>$cur):?>
                    <?if(!empty($cur) && $cur != 'RUR'):?><currency id="<?=$cur?>"<?if ( $k == 0 ):?> rate="1"<?endif;?>/><?endif;?>
                <?endforeach;?>
            <?else:?>
                <currency id="<?=$arParams["CURRENCY"]?>" rate="1"/>
            <?endif;?>
        </currencies>
        
    <categories>
<?foreach($arResult["CATEGORIES"] as $arCategory):?>
        <category id="<?=$arCategory["ID"]?>"<?
if($arCategory["PARENT"])
    echo ' parentId="'. $arCategory['PARENT']. '"';
?>><?=$arCategory["NAME"]?></category>
<?endforeach;?>
    </categories>   
    <delivery-options><option cost="0" days="0"/></delivery-options>
        <offers>
        <?foreach($arResult["OFFER"] as $arOffer):?>
            <offer id="<?=$arOffer["ID"]?>" available="<?=$arOffer["AVAIBLE"]?>">
				<?if($arOffer["ID"] == 18193 || $arOffer["ID"] == 17910 || $arOffer["ID"] == 13382 || $arOffer["ID"] == 12991 || $arOffer["ID"] == 9082 || $arOffer["ID"] == 8448 || $arOffer["ID"] == 8442 || $arOffer["ID"] == 8438 || $arOffer["ID"] == 8433 || $arOffer["ID"] == 8429 || $arOffer["ID"] == 8396 || $arOffer["ID"] == 8377 || $arOffer["ID"] == 8368 || $arOffer["ID"] == 8354 || $arOffer["ID"] == 7847 || $arOffer["ID"] == 7813 || $arOffer["ID"] == 7811 || $arOffer["ID"] == 7500 || $arOffer["ID"] == 7494 || $arOffer["ID"] == 7492 || $arOffer["ID"] == 5687 || $arOffer["ID"] == 5685 || $arOffer["ID"] == 5682 || $arOffer["ID"] == 5676 || $arOffer["ID"] == 4692  || $arOffer["ID"] == 19252 || $arOffer["ID"] == 16529 || $arOffer["ID"] == 12549 || $arOffer["ID"] == 12543 || $arOffer["ID"] == 9242 || $arOffer["ID"] == 8945 || $arOffer["ID"] == 6309 || $arOffer["ID"] == 6308|| $arOffer["ID"] == 5720 || $arOffer["ID"] == 5137 || $arOffer["ID"] == 4498 || $arOffer["ID"] == 4497 || $arOffer["ID"] == 4491 || $arOffer["ID"] == 4489 || $arOffer["ID"] == 4480 || $arOffer["ID"] == 4477 || $arOffer["ID"] == 4475 || $arOffer["ID"] == 4474 || $arOffer["ID"] == 4382 || $arOffer["ID"] == 2388 || $arOffer["ID"] == 2105 || $arOffer["ID"] == 1980 || $arOffer["ID"] == 1416 || $arOffer["ID"] == 1413 || $arOffer["ID"] == 1406 || $arOffer["ID"] == 560 || $arOffer["ID"] == 529 || $arOffer["ID"] == 500 || $arOffer["ID"] == 60889 || $arOffer["ID"] == 60810 || $arOffer["ID"] == 60747 || $arOffer["ID"] == 60889 || $arOffer["ID"] == 60888 || $arOffer["ID"] == 60857 || $arOffer["ID"] == 60858 || $arOffer["ID"] == 60859 || $arOffer["ID"] == 60990 || $arOffer["ID"] == 60992 || $arOffer["ID"] == 22037 || $arOffer["ID"] == 21985 || $arOffer["ID"] == 22042 || $arOffer["ID"] == 22036 || $arOffer["ID"] == 21997 || $arOffer["ID"] == 21996 || $arOffer["ID"] == 21994 || $arOffer["ID"] == 21992 || $arOffer["ID"] == 21991 || $arOffer["ID"] == 21990 || $arOffer["ID"] == 21988 || $arOffer["ID"] == 21986 || $arOffer["ID"] == 21984 || $arOffer["ID"] == 60812 || $arOffer["ID"] == 60815 || $arOffer["ID"] == 60816 || $arOffer["ID"] == 60817 || $arOffer["ID"] == 3484 || $arOffer["ID"] == 4152 || $arOffer["ID"] == 60830 || $arOffer["ID"] == 60746 || $arOffer["ID"] == 60745 || $arOffer["ID"] == 60744 || $arOffer["ID"] == 60743 || $arOffer["ID"] == 60740 || $arOffer["ID"] == 60733 || $arOffer["ID"] == 60735 || $arOffer["ID"] == 61078 || $arOffer["ID"] == 61114 || $arOffer["ID"] == 61115){?>
					<url><?=$arOffer["URL"]?>?region=voronezh&amp;roistat=yamarket2_21351978_<?=$arOffer["ID"]?>&amp;utm_source=market.yandex.ru&amp;utm_medium=cpc&amp;utm_content=carcam&amp;utm_campaign=<?=$arOffer["ID"]?></url>
				<?}else{?>	
					<url><?=$arOffer["URL"]?>&amp;region=voronezh&amp;roistat=yamarket2_21351978_<?=$arOffer["ID"]?>&amp;utm_source=market.yandex.ru&amp;utm_medium=cpc&amp;utm_content=carcam&amp;utm_campaign=<?=$arOffer["ID"]?></url>
                <?}?>
                <price><?=$arOffer["PRICE"]?></price>
                 <vendor>КАРКАМ</vendor>
                <currencyId>
                    <?if ( !empty($arOffer["CURRENCY"]) ):?>
                        <?=$arOffer["CURRENCY"]?>
                    <?else:?>
                        <?=$arParams["CURRENCY"]?>
                    <?endif;?>
                </currencyId>
                
                <categoryId><?=$arOffer["CATEGORY"]?></categoryId>
                
                <?if ( !empty( $arOffer["PICTURE"] ) ):?>
                    <picture><?=$arOffer["PICTURE"]?></picture>
                <?endif;?>
                
                <?foreach ($arOffer["MORE_PHOTO"] as $pic):?>
                    <picture><?=$pic?></picture>
                <?endforeach;?>                 
                <?if ($arOffer["PRICE"]>2990){?>
                    <delivery-options><option cost="0" days="0"/></delivery-options>
                <?}else{?>
                    <delivery-options><option cost="400" days="0"/></delivery-options>
                <?}?> 
                <name><?=$arOffer["MODEL"]?></name>
                <?if($arOffer["DESCRIPTION"]):?><description><?=$arOffer["DESCRIPTION"]?></description><?endif?>              
                <?foreach($arParams["PARAMS"] as $k=>$v): if($arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_VALUE"]):?>
                 <param name="<?=$arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_NAME"]?>"><?=$arOffer["DISPLAY_PROPERTIES"][$v]["DISPLAY_VALUE"]?></param>
                <?endif; endforeach;?>
                <sales_notes>Б/п доставка, наличные,банк.карты,кредит, свои СЦ</sales_notes>
                <manufacturer_warranty>true</manufacturer_warranty>
            </offer>
        <?endforeach;?>
        </offers>
    </shop>
</yml_catalog>