<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(method_exists($this, 'setFrameMode')) $this->setFrameMode(true);?>
<div class="stickers">
	<?if($arResult["NEW"]):?>
		<div class="sticker new">
			<i class="flaticon-new292"></i>
			<span class="text"><?=GetMessage('STICKER_SECT_NEW')?></span>
		</div>
	<?endif?>
	<?if($arResult["HIT"]):?>
		<div class="sticker hit">
			<i class="flaticon-first43"></i>
			<span class="text"><?=GetMessage('STICKER_SECT_HIT')?></span>
		</div>
	<?endif?>
	<?if($arResult["SALE"]):?>
		<div class="sticker discount" <?if(!empty($arParams['CONT_ID_DSC_PERC'])):?> id="<?=$arParams['CONT_ID_DSC_PERC']?>" <?endif?>>
			<i class="flaticon-sale"></i>
			<span class="text"><?=GetMessage('STICKER_SECT_SALE')?></span>
			<?/*if($arResult["SALE_DISC"]>0):?>
				-<?=Round($arResult["SALE_DISC"])?>
			<?endif*/?>
		</div>
	<?endif;?>

	<?if($arResult["BESTSELLER"]):?>
		<div class="sticker best-choice">
			<i class="flaticon-like"></i>
			<span class="text"><?=GetMessage('STICKER_SECT_BESTSELLER')?></span>
		</div>
	<?endif?>

	<?if($arResult["CATCHBUY"]):?>
		<div class="sticker circle hurry-buy">
			<i class="flaticon-stopwatch6"></i>
			<span class="text"><?=GetMessage('STICKER_SECT_CATCHBUY')?></span>
		</div>
	<?endif?>
</div><!-- /.stickers -->
