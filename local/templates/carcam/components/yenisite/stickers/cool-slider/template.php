<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(method_exists($this, 'setFrameMode')) $this->setFrameMode(true);?>
<div class="stickers">
<?if($arResult["NEW"]):?>
	<div class="cool-sticker new">
		<i class="flaticon-new292"></i>
		<span class="text"><?=GetMessage('STICKER_COOL_NEW')?></span>
	</div>
<?endif?>
<?if($arResult["HIT"]):?>
	<div class="cool-sticker like">
		<i class="flaticon-first43"></i>
		<span class="text"><?=GetMessage('STICKER_COOL_HIT')?></span>
	</div>
<?endif?>
<?if($arResult["SALE"]):?>
	<div class="cool-sticker discount" <?if(!empty($arParams['CONT_ID_DSC_PERC'])):?> id="<?=$arParams['CONT_ID_DSC_PERC']?>" <?endif?>>
		<i class="flaticon-sale"></i>
		<span class="text"><?=GetMessage('STICKER_COOL_SALE')?></span>
		<?/*if($arResult["SALE_DISC"]>0):?>
			-<?=Round($arResult["SALE_DISC"])?>
		<?endif*/?>
	</div>
<?endif;?>

<?if($arResult["BESTSELLER"]):?>
	<div class="cool-sticker like">
		<i class="flaticon-like"></i>
		<span class="text"><?=GetMessage('STICKER_COOL_BESTSELLER')?></span>
	</div>
<?endif?>

<?if($arResult["CATCHBUY"]):?>
	<div class="cool-sticker hurry-buy">
		<i class="flaticon-stopwatch6"></i>
		<span class="text"><?=GetMessage('STICKER_COOL_CATCHBUY')?></span>
	</div>
<?endif?>
	
</div>