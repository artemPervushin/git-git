<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitronic2\Mobile;
use Bitrix\Main\Loader;
CJSCore::Init(array('rz_b2_bx_catalog_item'));

$arContainer = array(
	'NEW' 			=> array('CLASS' => 'new', 'ICON' => 'flaticon-new2'),
	'HIT' 			=> array('CLASS' => 'hits', 'ICON' => 'flaticon-first43'),
	'SALE' 			=> array('CLASS' => 'recommended', 'ICON' => 'flaticon-sale'),
	'BESTSELLER' 	=> array('CLASS' => 'superprice', 'ICON' => 'flaticon-like'),
);
foreach($arResult['TABS'] as $codeTab => &$arTab)
{
	$arTab['CONTAINER_HEADER_CLASS'] = $arContainer[$codeTab]['CLASS'];
	$arTab['CONTAINER_HEADER_ICON'] = $arContainer[$codeTab]['ICON'];
	$arTab['HEADER'] = strlen($arParams['TAB_TEXT_'.$codeTab]) > 0 
	                 ? $arParams['TAB_TEXT_'.$codeTab]
	                 : GetMessage('BITRONIC2_MAINSPEC_TAB_'.$codeTab);
}
if (isset($arTab)) {
	unset($arTab);
}

// hack for correct work with SKU
if(is_array($arParams["PROPERTY_CODE"]))
	$arParams["PROPERTY_CODE"] = array_diff($arParams["PROPERTY_CODE"], array(0, null));
if(empty($arParams["PROPERTY_CODE"])) 
	$arParams["PROPERTY_CODE"] = array('1');

if(!is_array($arParams['OFFERS_PROPERTY_CODE'])) $arParams['OFFERS_PROPERTY_CODE'] = array();
if(is_array($arParams['OFFER_TREE_PROPS']))
{
	foreach($arParams['OFFER_TREE_PROPS'] as $tree_prop)
	{
		if(!in_array($tree_prop, $arParams['OFFERS_PROPERTY_CODE']))
			$arParams['OFFERS_PROPERTY_CODE'][] = $tree_prop;
	}
	$arParams['LIST_OFFERS_LIMIT'] = 0;
}

//resizer
$arParams['RESIZER_SECTION_ICON'] = intval($arParams['RESIZER_SECTION_ICON']) ? $arParams['RESIZER_SECTION_ICON'] : 5;

//mobile
$arParams["IS_MOBILE"] = mobile::isMobile();

$arParams['DISPLAY_FAVORITE'] = Loader::includeModule('yenisite.favorite') && $arParams['DISPLAY_FAVORITE'] != 'N';

// ##### Set params from setting.panel
global $rz_b2_options;
$arResult['SB-MODE'] = $rz_b2_options['sb-mode'];
$arResult['HOVER-MODE'] = $rz_b2_options['product-hover-effect'];

if ($rz_b2_options['currency-switcher'] == 'Y') {
	$arParams['CONVERT_CURRENCY'] = 'Y';
	$arParams['CURRENCY_ID'] = $rz_b2_options['active-currency'];
}

?>