<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(method_exists($this, 'setFrameMode')) $this->setFrameMode(true);?>

<?
global $rz_main_spec_filter;
global $rz_b2_options;
// echo "<pre style='text-align:left;'>";print_r($arParams);echo "</pre>";
?>

<?if (strtolower($_REQUEST['rz_ajax']) === 'y' || strtolower($_REQUEST['ajax_basket']) === 'y'):
	$APPLICATION->RestartBuffer();
	$tabU = strtoupper($_REQUEST['tab']);
	$tabL = strtolower($_REQUEST['tab']);

	if (!array_key_exists($tabU, $arResult['TABS'])) {
		reset($arResult['TABS']);
		$tabU = key($arResult['TABS']);
	}

	$arTab = $arResult['TABS'][$tabU];
	$rz_main_spec_filter = $arTab['FILTER'];

	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"spec",
		array_merge($arParams, array(
			"IS_YS_MS" => "Y",
			"TAB_BLOCK" => $tabL,
			"TAB_LINK" => $arTab['LINK'],
			"FILTER_NAME" => "rz_main_spec_filter",
			"OFFERS_SORT_FIELD" => (!empty($arParams["LIST_PRICE_SORT"]))? $arParams["LIST_PRICE_SORT"] : $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => (!empty($arParams["LIST_PRICE_SORT"]))? "asc" : $arParams["OFFERS_SORT_ORDER"],
			"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
			'STORE_DISPLAY_TYPE' => $arParams['STORE_DISPLAY_TYPE'],
		)),
		$component
	);
?>
<?else:?>
	<?
	$uniqID = 'special-blocks-' . $this->randString();
	?>
<div class="special-blocks" id="<?=$uniqID?>">
<?
if(strtolower($_REQUEST['ajax_basket']) != 'y')
{
	$frame = $this->createFrame(false,$uniqID)->begin(CRZBitronic2Composite::insertCompositLoader());
	//include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info.php';
}
//	<!-- combo-blocks can have "tabs" or "full" class
//	data-mode contains name of radiogroup for switching mode
//	same value is used for local storage checks -->
?>
	<div class="container combo-blocks" id="special-blocks" data-sb-mode="<?= $arResult['SB-MODE'] ?>"
		 data-sb-mode-def-expanded="<?= ($arParams['SB_FULL_DEFAULT'] == 'open') ? 'true' : 'false' ?>">
		<div class="combo-links">
			<?foreach($arResult['TABS'] as $codeTab => $arTab):
				$bActive = ($codeTab == $arParams['DEFAULT_TAB']);
				$codeTab = strtolower($codeTab);
				?>
				<a href="#tab_<?=$codeTab?>" class="combo-link <?=$arTab['CONTAINER_HEADER_CLASS']?> <?=$bActive ? 'active' : ''?>">
					<!-- <i class="<?=$arTab['CONTAINER_HEADER_ICON']?>"></i> -->
					<span class="text"><?=$arTab['HEADER']?></span>
					<!-- <span class="i-number"><?=$arTab['COUNT']?></span> -->
				</a>
			<?endforeach?>
		</div>
		<?
		$catalogClass  = 'catalog blocks special-blocks-carousel';
		if ($arParams['HIDE_ICON_SLIDER'] === 'Y') {
			$catalogClass .= ' thumbs-disabled';
		}
		$isTab = $arResult['SB-MODE'] == 'tabs';
		$i = 0;
		foreach($arResult['TABS'] as $codeTab => $arTab):
			if ($arResult['SB-MODE'] != 'tabs' && $arParams['SB_FULL_DEFAULT'] == 'open') {
				$bActive = true;
			} else {
				$bActive = ($codeTab == $arParams['DEFAULT_TAB']);
			}
			$codeTab = strtolower($codeTab);?>
			<div class="combo-target <?= $bActive ? 'shown' : '' ?><?= (!$isTab ? ' wow fadeIn' : '') ?>" id="tab_<?= $codeTab ?>">
				<h3 class="<?=$arTab['CONTAINER_HEADER_CLASS']?>"><?=$arTab['HEADER']?></h3>
				<div class="combo-target-content">
					<div class="<?= $catalogClass ?>" id="special-<?= $codeTab ?>" data-hover-effect="<?= $arResult['HOVER-MODE'] ?>"
						 data-quick-view-enabled="<?= ($rz_b2_options['quick-view'] === 'Y' ? 'true' : 'false') ?>">
						<?
						$arJSParams = array(
							'tab' => $codeTab,
							'tabId' => 'tab_'.$codeTab,
							'contentId' => 'special-'.$codeTab
							);
						$jsName = 'obTab' . ucfirst($codeTab);
						?>
						<script type="text/javascript">
							jQuery(window).load(function(){
								require([
									'back-end/ajax/main_spec',
									'libs/bootstrap/tooltip',
									'init/popups/initTooltips',
									'init/forms/initRatingStars',
									], function(){
										RZB2.ajax.<?=$jsName?> = new RZB2.ajax.MainSpecTab(<?=CUtil::PhpToJSObject($arJSParams, false, true)?>);
										RZB2.ajax.<?=$jsName?>.Init();
									}
								);
							});
							<?if ($bActive && $arParams['SB_FULL_DEFAULT'] !== 'open'):?>

							serverSettings.sbModeDefExpanded = <?=$i?>;
							<?endif?>

						</script>
					</div>
				</div>
			</div><!-- .tab-target -->
		<?$i++; endforeach?>
	</div><!-- /.container -->
<?
if(strtolower($_REQUEST['ajax_basket']) != 'y') {
	$frame->end();
}?>
</div><!-- /.special-blocks -->

<?
endif;
