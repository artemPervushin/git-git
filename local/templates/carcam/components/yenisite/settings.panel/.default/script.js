$(document).ready(function () {
	$('[name=styling-type]').on('change', function () {
		var val = $(this).val();
		$('.theme-demos').each(function () {
			var _ = $(this);
			if (_.hasClass(val)) {
				_.show();
			} else {
				_.hide();
			}
		});

		var activeThemeName = $('.theme-demo.active').data('theme');
		activeThemeName = activeThemeName.replace(/-\w+/i, '-' + val);
		if ($('.' + activeThemeName).length) {
			$('.' + activeThemeName).click();
		}
		else {
			$('.theme-demos.' + val).find('li.theme-demo:first').click();
		}
	});

	// color-theme switch
	$('.theme-demo').on('click', function () {
		if (!$(this).hasClass('active')) {
			$('.theme-demo.active').removeClass('active');
			$(this).addClass('active');

			var newTheme = $(this).attr('data-theme');

			$('input#theme-demo').val(newTheme);
		} else return;
	});

	var $setPanel = $('#settings-panel-cblocks');
	var $setPanelSubmit = $('#settings-panel-submit');

	$.fn.serializeAssoc = function () {
		var obj = {
			result: {},
			add: function (name, value) {
				var tmp = name.match(/^(.*)\[([^\]]*)\]$/);
				if (tmp) {
					var v = {};
					if (tmp[2])
						v[tmp[2]] = value;
					else
						v[$.count(v)] = value;
					this.add(tmp[1], v);
				} else if (typeof value == 'object') {
					if (typeof this.result[name] != 'object') {
						this.result[name] = {};
					}
					this.result[name] = $.extend(this.result[name], value);
				} else {
					this.result[name] = value;
				}
			}
		};
		var ar = this.serializeArray();
		for (var i = 0; i < ar.length; i++) {
			obj.add(ar[i].name, ar[i].value);
		}
		return obj.result;
	};
	var setCookie = function(name, value, prefix) {
		var date = new Date();
		date.setFullYear(date.getFullYear() + 1);
		document.cookie = prefix + name + '=' + value + ';domain=.'+ window.location.hostname + '; path=/; expires=' + date.toUTCString();
	};
	var fillBSSettings = function(object, prefix) {
		for (var key in object) {
			switch (typeof object[key]) {
				case "object":
					fillBSSettings(object[key], prefix + key + '_');
					break;
				case "function":
					break;
				default:
					$(prefix + key).val(object[key]);
					break;
			}
		}
	}
	$setPanelSubmit.on('click', function (e) {
		fillBSSettings(bs.defaults, '#settings_bs_');
		if ("DEMO" in RZB2 && RZB2.DEMO) {
			var arSettings = $setPanel.serializeAssoc();
			if ('SETTINGS' in arSettings && typeof arSettings.SETTINGS == 'object') {
				for (var param in arSettings.SETTINGS) {
					setCookie(param, arSettings.SETTINGS[param], COOKIE_PREFIX + '_' + SITE_ID + '_');
				}
			}
			$.ajax({
				type: 'GET',
				url: SITE_DIR + 'ajax/composite.php',
				dataType: 'json',
				success: function(obj) {
					$setPanel.submit();
				}
			});
		} else {
			$setPanel.submit();
		}
	});
	$setPanel.on('click', '.set-color li', function (e) {
		var $this = $(this),
			$parent = $this.closest('.set-color'),
			$tar = $parent.data('target');
		if (!$tar) {
			$tar = $('#settings_'+ $parent.data('name'));
			$parent.data('target', $tar);
		}
		$tar.val($this.data('value')).trigger('change');
	});
	$setPanel.on('change', '.minicolors', function (e) {
		var $this = $(this),
			$tar = $this.data('target');
		if (!$tar) {
			$tar = $('#settings_'+ $this.data('name'));
			$this.data('target', $tar);
		}
		$tar.val($this.val()).trigger('change');
	});
	$setPanel.on('change', '.color-setting', function (e) {
		var $this = $(this),
			obj = $this.data('obj');
		if (!obj) {
			obj = $($this.data('selector'));
			$this.data('obj', obj);
		}
		if ($this.data('selector')) {
			obj.css($this.data('property'), $this.val());
		}
	});
	$setPanel.on('change', '.type-choose', function (e) {
		var $this = $(this),
			$parent = $this.closest('.setting-content');
		$parent.find('.data-type').hide();
		var $curElem = $parent.find('.data-type.type-' + $this.val());
		$curElem.show();
		$curElem.find('.set-color').trigger('change');
	});
});

$(window).on('modalSettingsInited', function(){
	b2.el.inputSliderWidth = $('input[data-name="big-slider-width"]');
	b2.set.work_area = function(value){
		switch (value){
			case 'full_width':
				b2.el.inputSliderWidth.filter('[value="normal"]').attr('disabled', true);
				if (b2.s.bigSliderWidth === "normal"){ b2.el.inputSliderWidth.filter('[value="full"]').attr('checked', true).change();}
			break;
			case 'container':
				b2.el.inputSliderWidth.filter('[value="normal"]').attr('disabled', false);
			break;
			default: ;
		}
	};
	$('input[data-name="catalog-placement"]:checked').change();
	$('input[data-name="work_area"]:checked').change();
});
