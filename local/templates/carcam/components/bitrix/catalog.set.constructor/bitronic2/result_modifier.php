<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult["SET_ITEMS"]["PRICE_NOT_FORMATED"] =
$arResult["SET_ITEMS"]["OLD_PRICE_NOT_FORMATED"] =
$arResult["SET_ITEMS"]["PRICE_DISCOUNT_DIFFERENCE_NOT_FORMATED"] = 0;

$arResult["SET_ITEMS"]["PRICE_NOT_FORMATED"] += $arResult["ELEMENT"]["PRICE_DISCOUNT_VALUE"];
$arResult["SET_ITEMS"]["OLD_PRICE_NOT_FORMATED"] += $arResult["ELEMENT"]["PRICE_VALUE"];
$arResult["SET_ITEMS"]["PRICE_DISCOUNT_DIFFERENCE_NOT_FORMATED"] += $arResult["ELEMENT"]["PRICE_DISCOUNT_DIFFERENCE_VALUE"];
		
$arResult["ELEMENT"]["PICTURE_PRINT"]['SRC'] = CRZBitronic2CatalogUtils::getElementPictureById($arResult["ELEMENT"]['ID'], $arParams['RESIZER_SET_CONTRUCTOR']);

$arDefaultSetIDs = array($arResult["ELEMENT"]["ID"]);


foreach (array("DEFAULT", "OTHER") as $type)
{
	foreach ($arResult["SET_ITEMS"][$type] as $key=>$arItem)
	{
		$arItem["PICTURE_PRINT"]['SRC'] = CRZBitronic2CatalogUtils::getElementPictureById($arItem['ID'], $arParams['RESIZER_SET_CONTRUCTOR']);
	
		if ($type == "DEFAULT")
		{
			$arDefaultSetIDs[] = $arItem["ID"];
			$arResult["SET_ITEMS"]["PRICE_NOT_FORMATED"] += $arItem["PRICE_DISCOUNT_VALUE"];
			$arResult["SET_ITEMS"]["OLD_PRICE_NOT_FORMATED"] += $arItem["PRICE_VALUE"];
			$arResult["SET_ITEMS"]["PRICE_DISCOUNT_DIFFERENCE_NOT_FORMATED"] += $arItem["PRICE_DISCOUNT_DIFFERENCE_VALUE"];
		}
		$arResult["SET_ITEMS"][$type][$key] = $arItem;		
	}
}

$arResult["DEFAULT_SET_IDS"] = $arDefaultSetIDs;