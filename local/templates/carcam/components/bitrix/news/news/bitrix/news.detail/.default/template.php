<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info_dynamic.php';
?>
<div class="row">
	<div class="col-xs-12">
		<h1><?=$arResult["NAME"]?></h1>
		
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<div class="row images-row">
			<div class="img-container">
				<img src="<?=CResizer2Resize::ResizeGD2($arResult["DETAIL_PICTURE"]["SRC"], $arParams["RESIZER_NEWS_DETAIL"])?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>" title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>">
			</div>
		</div>
		<?endif;?>
		
		<p>
		<?if(strlen($arResult["DETAIL_TEXT"])>0):?>
			<?echo $arResult["DETAIL_TEXT"];?>
		<?else:?>
			<?echo $arResult["PREVIEW_TEXT"];?>
		<?endif?>
		</p>
		<?$frame = $this->createFrame()->begin('')?>
		<?if (!empty($arResult["CATALOG_ID"])):?>
		<?$APPLICATION->IncludeComponent("bitrix:catalog.recommended.products", "bitronic2", array(
			"HEADER_TEXT" => $arParams["RELATED_HEADER_TEXT"],
			"DISPLAY_COMPARE" => $arResult['CATALOG_PARAMS']['DISPLAY_COMPARE'],
			"DISPLAY_FAVORITE" => $arResult['CATALOG_PARAMS']["DISPLAY_FAVORITE"],
			"DISPLAY_ONECLICK" => $arResult['CATALOG_PARAMS']["DISPLAY_ONECLICK"],
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ID" => $arResult['ID'],
			"PROPERTY_LINK" => "RELATED_ITEMS",
			"SHOW_PRODUCTS_".$arResult["CATALOG_ID"] => "Y",///////////
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"BASKET_URL" => $arResult['CATALOG_PARAMS']["BASKET_URL"],
			"ACTION_VARIABLE" => $arResult['CATALOG_PARAMS']["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arResult['CATALOG_PARAMS']["PRODUCT_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arResult['CATALOG_PARAMS']["PRODUCT_QUANTITY_VARIABLE"],
			"ADD_PROPERTIES_TO_BASKET" => (isset($arResult['CATALOG_PARAMS']["ADD_PROPERTIES_TO_BASKET"]) ? $arResult['CATALOG_PARAMS']["ADD_PROPERTIES_TO_BASKET"] : ''),
			"PRODUCT_PROPS_VARIABLE" => $arResult['CATALOG_PARAMS']["PRODUCT_PROPS_VARIABLE"],
			"PARTIAL_PRODUCT_PROPERTIES" => (isset($arResult['CATALOG_PARAMS']["PARTIAL_PRODUCT_PROPERTIES"]) ? $arResult['CATALOG_PARAMS']["PARTIAL_PRODUCT_PROPERTIES"] : ''),
			"PAGE_ELEMENT_COUNT" => $arResult['CATALOG_PARAMS']["PAGE_ELEMENT_COUNT"],
			"SHOW_OLD_PRICE" => $arResult['CATALOG_PARAMS']['SHOW_OLD_PRICE'],
			"SHOW_DISCOUNT_PERCENT" => $arResult['CATALOG_PARAMS']['SHOW_DISCOUNT_PERCENT'],
			"PRICE_CODE" => $arResult['CATALOG_PARAMS']["PRICE_CODE"],
			"SHOW_PRICE_COUNT" => $arResult['CATALOG_PARAMS']["SHOW_PRICE_COUNT"],
			"PRODUCT_SUBSCRIPTION" => 'N',
			"PRICE_VAT_INCLUDE" => $arResult['CATALOG_PARAMS']["PRICE_VAT_INCLUDE"],
			"USE_PRODUCT_QUANTITY" => $arResult['CATALOG_PARAMS']['USE_PRODUCT_QUANTITY'],
			"SHOW_NAME" => "Y",
			"SHOW_IMAGE" => "Y",
			"MESS_BTN_BUY" => $arResult['CATALOG_PARAMS']['MESS_BTN_BUY'],
			"MESS_BTN_DETAIL" => $arResult['CATALOG_PARAMS']["MESS_BTN_DETAIL"],
			"MESS_NOT_AVAILABLE" => $arResult['CATALOG_PARAMS']['MESS_NOT_AVAILABLE'],
			"MESS_BTN_SUBSCRIBE" => $arResult['CATALOG_PARAMS']['MESS_BTN_SUBSCRIBE'],
			"HIDE_NOT_AVAILABLE" => $arResult['CATALOG_PARAMS']["HIDE_NOT_AVAILABLE"],
			//"OFFER_TREE_PROPS_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
			//"OFFER_TREE_PROPS_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
			"ADDITIONAL_PICT_PROP_".$arResult["CATALOG_ID"] => $arResult['CATALOG_PARAMS']['ADD_PICT_PROP'],
			//"ADDITIONAL_PICT_PROP_".$arRecomData['OFFER_IBLOCK_ID'] => $arResult['CATALOG_PARAMS']['OFFER_ADD_PICT_PROP'],
			//"PROPERTY_CODE_".$arRecomData['OFFER_IBLOCK_ID'] => array(),
			"CONVERT_CURRENCY" => $arResult['CATALOG_PARAMS']["CONVERT_CURRENCY"],
			"CURRENCY_ID" => $arResult['CATALOG_PARAMS']["CURRENCY_ID"],
			"RESIZER_SECTION" => $arResult['CATALOG_PARAMS']['RESIZER_SECTION'],
		))?>
		<?endif?>
		<?$frame->end()?>
	</div><!-- /.col-xs-12 -->
</div><!-- /.row -->
<?// echo '<pre>', htmlspecialcharsBx(var_export($arResult, 1)), '</pre>'; ?>
