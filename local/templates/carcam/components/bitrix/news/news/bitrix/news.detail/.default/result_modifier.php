<?
use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult["DISPLAY_PROPERTIES"]["RELATED_ITEMS"])) {
	$arResult['CATALOG_ID'] = $arResult["DISPLAY_PROPERTIES"]["RELATED_ITEMS"]["LINK_IBLOCK_ID"];
	$arResult['CATALOG_PARAMS'] = array();
	$arMainParams = $arParams;

	$arParams = array();
	if (Loader::IncludeModule('yenisite.core')) {
		$arParams = \Yenisite\Core\Ajax::getParams('bitrix:catalog', false, SITE_DIR . 'catalog/?rz_update_catalog_parameters_cache=Y');
	}
	if(!is_array($arParams) || empty($arParams)) {
		$this->__component->AbortResultCache();
	}

	// @var $arPrepareParams
	include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/components/bitrix/catalog/.default/include/prepare_params_element.php';

	$arResult['CATALOG_PARAMS'] = $arPrepareParams;
	$arParams = $arMainParams;
	unset($arMainParams);
}

?>
