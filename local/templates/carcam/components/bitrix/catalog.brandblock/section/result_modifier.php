<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arProp = CIBlockProperty::GetList(
	array('SORT' => 'ASC', 'ID' => 'ASC'),
	array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'CODE' => $arParams['PROP_CODE'][0],
		'ACTIVE' => 'Y'
	)
)->Fetch();
CBitrixComponent::includeComponentClass("bitrix:catalog.smart.filter");
$arResult['FILTER_PROP'] = $arProp;


$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
	array(
		"filter" => array(
			'TABLE_NAME' => $arProp['USER_TYPE_SETTINGS']['TABLE_NAME']
		)
	)
)->fetch();



$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
$entityDataClass = $entity->getDataClass();

$arFilter = array();

if (!empty($arParams['FILTER_NAME'])) {
	global ${$arParams["FILTER_NAME"]};
	$arrFilter = ${$arParams["FILTER_NAME"]};

	if (is_array($arrFilter))
	foreach($arrFilter as $k=>$v)
	{
		if(strpos($k, 'NAME') !== false)
		{
			$arFilter['filter'][str_replace('NAME', 'UF_NAME', $k)] = $v;
		}
	}
}

$rsPropEnums = $entityDataClass::getList($arFilter);

$arResult['ITEMS'] = array();
while ($arEnum = $rsPropEnums->fetch())
{
	$arItem = $arResult['BRAND_BLOCKS'][$arEnum['ID']];
	$arItem['ID'] = $arEnum['ID'];
	
	$arItem['UF_XML_ID'] = $arEnum['UF_XML_ID'];
	
	CBitrixCatalogSmartFilter::fillItemValues($arResult['FILTER_PROP'], $arEnum['UF_XML_ID']);
	
	$arItem['LINK'] = (isset($arEnum['UF_LINK']) && '' != $arEnum['UF_LINK'])
	                ? $arParams["SEF_BASE_URL"].$arEnum['UF_LINK']
	                : (empty($arParams['CATALOG_FILTER_NAME'])
	                  ? ''
	                  : $arParams["PATH_FOLDER"]
	                    .'?'.$arParams['CATALOG_FILTER_NAME']
	                    .$arResult['FILTER_PROP']['VALUES'][$arEnum['UF_XML_ID']]['CONTROL_ID']
	                    .'=Y&amp;set_filter=y&amp;rz_all_elements=y'
	                  );
	
	$arResult['ITEMS'][$arItem['ID']] = $arItem;
}

unset($arResult['BRAND_BLOCKS']);
