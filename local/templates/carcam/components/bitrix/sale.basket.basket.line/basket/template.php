<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Sale\DiscountCouponsManager;

$this->setFrameMode(true);

$id = 'bxdinamic_bitronic2_basket_string';
$id_header = 'bxdinamic_bitronic2_basket_header';
$id_footer = 'bxdinamic_bitronic2_basket_footer';
?>
<div class="top-line-item basket" id="basket">
	<a id="<?=$id?>" href="<?=$arParams['PATH_TO_BASKET']?>" class="btn-main btn-basket" data-popup="#popup_basket">
		<?$frame = $this->createFrame($id, false)->begin(CRZBitronic2Composite::insertCompositLoader());?>
			<i class="flaticon-shopping109">
				<span class="basket-items-number-sticker"><?=$arResult['TOTAL_COUNT']?></span>
			</i>
			<strong class="text-info"><?=GetMessage('BITRONIC2_BASKET_IN_BASKET')?></strong>
			<span class="text-content">
				<span class="basket-items-number"><?=$arResult['TOTAL_COUNT']?></span>
				<span class="basket-items-text hidden-xs"><?=$arResult['PRODUCT(S)']?></span>
				<span class="basket-simple-text hidden-xs"><?=GetMessage('BITRONIC2_BASKET_ON_SUMM')?></span>
				<strong class="basket-total-price hidden-xs"><?=$arResult["TOTAL_SUM_FORMATTED"]?></strong>
			</span>
		<?$frame->end();?>
	</a>
	<div class="top-line-popup popup_basket" id="popup_basket" data-darken >
		<button class="btn-close" data-popup="#popup_basket">
			<span class="btn-text"><?=GetMessage('BITRONIC2_MODAL_CLOSE')?></span>
			<i class="flaticon-close47"></i>
		</button>
		<div class="popup-header">
			<div id="<?=$id_header?>" class="header-text">
				<?$frame = $this->createFrame($id_header, false)->begin(CRZBitronic2Composite::insertCompositLoader());?>
				<div class="basket-content">
					<div class="text"><?=GetMessage('BITRONIC2_BASKET_YOUR_CHOOSE', array('#COUNT#'=>$arResult['TOTAL_COUNT'], '#PRODUCT#'=>$arResult['PRODUCT(S)']))?></div>
					<span class="total-price"><?=$arResult["TOTAL_SUM_FORMATTED"]?></span>
					<a class="delivery-and-payment" href="/about/delivery/" target="_blank" title="<?=GetMessage('BITRONIC2_BASKET_DELIVERY_AND_PAYMENT')?>"><span><?=GetMessage('BITRONIC2_BASKET_DELIVERY_AND_PAYMENT')?></span></a>
				</div>
				<?$frame->end();?>
			</div>
		</div>
		<div class="table-wrap basket-small">
			<div class="scroller scroller_v">
				<div class="basket-content">
					<?$frame = $this->createFrame()->begin('');?>
					<table class="items-table">
						<? $arProductId = array();
						if(!empty($arResult["CATEGORIES"]["READY"])):?>
							<thead>
								<tr>
									<th colspan="2"><?=GetMessage('BITRONIC2_BASKET_GOOD')?></th>
									<th class="availability"><?=GetMessage('BITRONIC2_BASKET_QUANTITY')?></th>
									<th class="price"><?=GetMessage('BITRONIC2_BASKET_PRICE_FOR_1')?></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$recomendations = array();
								foreach($arResult["CATEGORIES"]["READY"] as $arItem):
									$arProductId[$arItem['PRODUCT_ID']] = $arItem['PRODUCT_ID'];
								
									if (!empty($arItem['CATALOG']['PROPERTIES']['RECOMMEND']['VALUE']))
										$recomendations = array_merge($recomendations, $arItem['CATALOG']['PROPERTIES']['RECOMMEND']['VALUE']);
									
									?>
									<tr class="table-item" data-id="<?=$arItem['ID']?>" data-product-id="<?=$arItem['PRODUCT_ID']?>">
										<td class="photo">
											<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
												<img src="<?=$arItem['MAIN_PHOTO']?>" alt="<?=$arItem['NAME']?>">
											</a>
										</td>
										<td class="name">
											<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link"><span class="text"><?=$arItem['NAME']?></span></a>
											<div>
											<? if($arParams['SHOW_ARTICUL'] !== 'N' && !empty($arItem['ARTICUL'])): ?>

												<span class="art"><?=GetMessage('BITRONIC2_BASKET_ARTICUL')?>: <strong><?=htmlspecialcharsBx($arItem['ARTICUL'])?></strong></span>
											<? endif ?>
												<?foreach($arItem['PROPS'] as $arProp):?>
													<span class="art"><?=$arProp['NAME']?>: <strong><?=$arProp['VALUE']?></strong></span>
												<?endforeach?>
											</div>
										</td>
										<td class="availability">
											<form action="#" method="post" class="quantity-counter"
												data-tooltip
												data-placement="bottom"
												title="<?=$arItem['MEASURE_NAME']?>">
												<!-- parent must have class .quantity-counter! -->
												<button type="button" class="btn-silver quantity-change decrease"><span class="minus">&ndash;</span></button>
												<input type="text" class="quantity-input textinput" name="quantity" value="<?=$arItem['QUANTITY']?>" data-ratio="<?=$arItem['MEASURE_RATIO']?>">
												<button type="button" class="btn-silver quantity-change increase"><span class="plus">+</span></button>
											</form>
										</td>
										<td class="price">
											<span class="price-new"><?=$arItem['PRICE_FMT']?></span>
											<?if($arItem['DISCOUNT_PRICE'] > 0):?>
											<div>
												<span class="price-old"><?=$arItem['FULL_PRICE']?></span>
											</div>
											<?endif?>
										</td>
										<td class="actions">
											<button class="btn-delete pseudolink with-icon" data-tooltip title="<?=GetMessage('BITRONIC2_BASKET_DELETE')?>" data-placement="bottom">
												<i class="flaticon-trash29"></i>
												<span class="btn-text"><?=GetMessage('BITRONIC2_BASKET_DELETE')?></span>
											</button>
										</td>
									</tr>
								<?endforeach;?>
							</tbody>
						<?endif?>
					</table>
					<script type="text/javascript">
						RZB2.ajax.BasketSmall.ElementsList = <?=CUtil::PhpToJSObject($arProductId, false, true, true)?>;
					</script>
					<?$frame->end(); unset($arProductId);?>
				</div>
				<div class="scroller__track scroller__track_v">
					<div class="scroller__bar scroller__bar_v"></div>
				</div>
			</div>
		</div>
		<div class="popup-footer">
			<div class="coupon-outer-wrap">
				<?php if (empty($arResult['COUPON_LIST'])): ?>
				<label><input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();" class="textinput" placeholder="<?=GetMessage('BITRONIC2_BASKET_PROMOCODE')?>"></label>
				<button type="button" class="apply-coupon"><i class="flaticon-right10"></i></button>
				<?php else: 
					$cp = $arResult['COUPON_LIST'][0]; 
					$couponClass = 'disabled';
					switch ($cp['STATUS'])
					{
						case DiscountCouponsManager::STATUS_NOT_FOUND:
						case DiscountCouponsManager::STATUS_FREEZE:
							$couponClass = 'not-valid';
							break;
						case DiscountCouponsManager::STATUS_APPLYED:
							$couponClass = 'valid';
							break;
					}
				?>
				<label><input class="textinput <? echo $couponClass; ?>" readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($cp['COUPON']);?>" placeholder="<?=GetMessage('BITRONIC2_BASKET_PROMOCODE')?>"></label>
				<button type="button" class="apply-coupon <?=$couponClass?>" data-coupon="<? echo htmlspecialcharsbx($cp['COUPON']); ?>"><i class="icon-<?=$couponClass?> flaticon-<?=($couponClass=='valid')?'check33':'x5'?>"></i></button>
				<?php endif; ?>
			</div>
			<span id="<?=$id_footer?>" class="total">
				<?$frame = $this->createFrame($id_footer, false)->begin(CRZBitronic2Composite::insertCompositLoader());?>
				<span class="text"><?=GetMessage('BITRONIC2_BASKET_ITOGO')?></span>
				<span class="price" data-total-price="<?=$arResult["TOTAL_SUM"]?>"><?=$arResult["TOTAL_SUM_FORMATTED"]?></span>
				<?$frame->end();?>
			</span>
			<div class="small-basket-buy-wrap">
				<a href="<?=$arParams['PATH_TO_ORDER']?>" class="btn-main"><span class="text"><?=GetMessage('BITRONIC2_BASKET_MAKE_ORDER')?></span></a>
				<?if($arParams['DISPLAY_ONECLICK']):?>
				<div>
					<button type="button" class="action one-click-buy" data-toggle="modal" data-target="#modal_quick-buy" data-basket="Y">
						<i class="flaticon-shopping220"></i>
						<span class="text"><?=GetMessage('BITRONIC2_BASKET_ONE_CLICK')?></span>
					</button>
				</div>
				<?endif?>
			</div>
		</div>
		<?php if (!empty($recomendations)): ?>
		<div class="popup-recomendations">
			<h2>Рекомендованные товары</h2>
			<div class="scroll-slider-wrap shown">
				<div class="ctrl-arrow-wrap prev">
					<button type="button" class="ctrl-arrow prev">
						<i class="flaticon-arrow133"></i>
					</button>
				</div>
				<div class="ctrl-arrow-wrap next">
					<button type="button" class="ctrl-arrow next">
						<i class="flaticon-right20"></i>
					</button>
				</div>
				<div class="sly-scroll horizontal">
					<div class="sly-bar"></div>
				</div>
				<div class="scroll-slider frame">
					<div class="slider-content slidee">
					<?php 
						$arSelect = Array("ID", "NAME", "PICTURE_PRINT", "DETAIL_PAGE_URL");
						$arFilter = Array("IBLOCK_ID" => 153, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $recomendations);
						$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
						while($ob = $res->GetNextElement()) {
							$arFields = $ob->GetFields();
							$arFields['PICTURE_PRINT']['ALT'] = $arFields['NAME'];
							$arFields['PICTURE_PRINT']['SRC'] = CRZBitronic2CatalogUtils::getElementPictureById($arFields['ID'], 1);

					?>
						<div class="slider-item">
							<div class="photo">
								<span data-picture data-alt="<?=$arFields['PICTURE_PRINT']['ALT']?>">
									<span data-src="<?=$arFields['PICTURE_PRINT']['SRC']?>"></span>
									<span data-src="" data-media="(max-width: 767px)"></span>
									<noscript><img src="<?=$arFields['PICTURE_PRINT']['SRC']?>" alt="<?=$arFields['PICTURE_PRINT']['ALT']?>"></noscript>
								</span>	
							</div>
							<a href="<?=$arFields['DETAIL_PAGE_URL']?>" class="name link bx_rcm_view_link" data-product-id="<?=$arFields['ID']?>"><span class="text"><?=$arFields['NAME']?></span></a>
						</div>
					<?
						}
					?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div><!-- /.top-line-popup.popup_basket#popup_basket -->
</div>