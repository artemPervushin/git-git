function enterCoupon()
{
	var newCoupon = BX('coupon');
	if (!!newCoupon && !!newCoupon.value)
		recalcBasketAjax({'coupon' : newCoupon.value});
}

function recalcBasketAjax(params, simpleUpdate)
{
	var postData = {
		'sessid': BX.bitrix_sessid(),
		'site_id': BX.message('SITE_ID'),
		'props': {},
		'action_var': 'action',
		'select_props': 'NAME,DISCOUNT,DELETE,DELAY,PRICE,QUANTITY,SUM',
		'offers_props': '',
		'quantity_float': 'N',
		'count_discount_4_all_quantity': 'N',
		'price_vat_show_value': 'Y',
		'hide_coupon': 'N',
		'use_prepayment': 'N',
		'rz_ajax': 'y',
		'rz_ajax_no_header': 'y',
		'BasketRefresh': 'y',
		'self_url': location.href,
		'tab': ''
	};
	
	if (!!params && typeof params === 'object')
	{
		for (i in params)
		{
			if (params.hasOwnProperty(i))
				postData[i] = params[i];
		}
	}

	RZB2.ajax.loader.Start($('#popup_basket .items-table'));

	BX.ajax({
		url: SITE_DIR + 'ajax/big_basket.php',
		method: 'POST',
		data: postData,
		dataType: 'html',
		onsuccess: function(result) {
			RZB2.ajax.BasketSmall.Refresh(true);
			BX.onCustomEvent('OnBasketChange');
		}
	});
}

$(document).ready(function(){
	$('#popup_basket')
		.on('click', 'button[data-coupon]', function(e){
			var value = $(this).attr('data-coupon');
			if (!!value && value.length > 0)
			{
				recalcBasketAjax({'delete_coupon': value});
			}
		})
		.on('click', '.coupon-link', function(e){
			$(this).remove();
			e.preventDefault();
		})
		.on('mouseenter', 'button.apply-coupon.valid', function(){
			$(this).find('i').removeClass('flaticon-check33').addClass('flaticon-x5');
		})
		.on('mouseleave', 'button.apply-coupon.valid', function(){
			$(this).find('i').removeClass('flaticon-x5').addClass('flaticon-check33');
		});

	initHorizontalCarousels('.popup-recomendations');
});
