<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
?>
<div class="subcategory-section">
	<? foreach ($arResult['SECTIONS'] as $arSection): ?>
		<a href="<?= $arSection['SECTION_PAGE_URL'] ?>" class="subcategory-link col-xs-12 col-sm-4 col-md-3 col-lg-2">
			<img src="<?=$arSection['PICTURE']? $arSection['PICTURE']:'http://placehold.it/400x400' ?>" alt="<?=$arSection['NAME']?>" title="<?=$arSection['NAME']?>" class="subcategory-image">
			<div class="subcategory-name"><?= $arSection['NAME'] ?></div>
		</a>
	<? endforeach ?>
</div>
<?if (!empty($arResult['SECTION']['DESCRIPTION'])): ?>
<div class="category-description col-xs-12">
	<?= $arResult['SECTION']['DESCRIPTION'] ?>
</div>
<? endif ?>