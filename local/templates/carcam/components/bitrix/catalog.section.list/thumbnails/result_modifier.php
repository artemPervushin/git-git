<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

foreach ($arResult['SECTIONS'] as &$arSection) {
	if ($arParams['VIEW_MODE'] == 'BOTH' && empty($arSection['PICTURE'])) continue;
	$arSection['PICTURE'] = CRZBitronic2CatalogUtils::getSectionPictureById($arSection['ID'], 4);
}
unset($arSection);
