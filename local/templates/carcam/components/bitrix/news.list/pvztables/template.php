<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>

<div class="region_points">
    <table>
        <tbody>
        <tr>
            <td style="font-size: 12pt; font-weight: bold;">Адрес:</td>
            <td style="font-size: 12pt; font-weight: bold;">Тип магазина:</td>
            <td style="font-size: 12pt; font-weight: bold;">Телефон:</td>
            <td style="font-size: 12pt; font-weight: bold;">Режим работы магазина:</td>
        </tr>
        <? foreach($arResult["ITEMS"] as $arItem): ?>
        <? //_::d($arItem); ?>
        <tr>
            <td><?=$arItem["NAME"]?></td>
            <? foreach($arItem["PROPERTIES"] as $key => $code): ?>
                <td style="text-transform: lowercase; <? ($key == "GEO_PHONE") ? "font-weight: bold;" : '' ?>">
                    <? $healthy = array("Воскресенье", "Понедельник", "Суббота", "Пятница", " - ");
                    $yummy = array("вс", "пн", "сб", "пт", "-");

                    $newphrase = str_replace($healthy, $yummy, $arItem["PROPERTIES"][$key]["VALUE"]); ?>
                    <?=$newphrase;?>
                </td>
            <?endforeach;?>
        </tr>
        <? endforeach ?>
        </tbody>
    </table>
</div>