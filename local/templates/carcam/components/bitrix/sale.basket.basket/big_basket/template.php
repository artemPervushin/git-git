<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Page\Asset;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
$asset = Asset::getInstance();
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmTabs.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/pages/initBigBasketPage.js");

$curPage = $APPLICATION->GetCurPage().'?'.$arParams["ACTION_VARIABLE"].'=';
$arUrls = array(
	"delete" => $curPage."delete&id=#ID#",
	"delay" => $curPage."delay&id=#ID#",
	"add" => $curPage."add&id=#ID#",
);
unset($curPage);

$arBasketJSParams = array(
	'BASKET_CURRENCY' => CSaleLang::GetLangCurrency(SITE_ID),
	'SALE_DELETE' => GetMessage("BITRONIC2_SALE_DELETE"),
	'SALE_DELAY' => GetMessage("BITRONIC2_SALE_DELAY"),
	'SALE_TYPE' => GetMessage("BITRONIC2_SALE_TYPE"),
	'TEMPLATE_FOLDER' => $templateFolder,
	'DELETE_URL' => $arUrls["delete"],
	'DELAY_URL' => $arUrls["delay"],
	'ADD_URL' => $arUrls["add"],
	'SELF_URL' => $APPLICATION->GetCurPage(true)
);
?>
<script type="text/javascript">
	var basketJSParams = <?=CUtil::PhpToJSObject($arBasketJSParams)?>;
	BX.message({basket_sku_not_available: "<?=GetMessage('BITRONIC2_SKU_NOT_AVAILABLE')?>"});
</script>
<main class="container basket-big-page" data-page="big-basket-page">
	<h2><?$APPLICATION->ShowTitle()?></h2>
	<a href="<?=$arParams['PATH_TO_ORDER']?>" class="btn-main btn-continue king-btn" onclick="checkOut();"
		><span class="text"><?=GetMessage('BITRONIC2_SALE_ORDER')?></span></a>
	<div class="clearfix"></div>
	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">

<?
$APPLICATION->AddHeadScript($templateFolder."/script.js");

if ($_POST['rz_ajax_no_header'] === 'y'){
	$APPLICATION->RestartBuffer();
}

if (strlen($arResult["ERROR_MESSAGE"]) <= 0)
{
	if (!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"]))
	{
		CRZBitronic2CatalogUtils::ShowMessage(array("MESSAGE"=>$arResult["WARNING_MESSAGE"], "TYPE"=>"ERROR"));
	}
	
	$normalCount    = count($arResult["ITEMS"]["AnDelCanBuy"]);
	$delayCount     = count($arResult["ITEMS"]["DelDelCanBuy"]);
	$subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
	$naCount        = count($arResult["ITEMS"]["nAnCanBuy"]);

	if ($naCount > 0) {
		foreach ($arResult["ITEMS"]["nAnCanBuy"] as $key => $arItem) {
			if ($arItem['DELAY'] == "Y") {
				$delayCount++;
				continue;
			}
			$normalCount++;
		}
		$naCount = 0;
	}

	$normalShow    = ($normalCount    > 0 || true);
	$delayShow     = ($delayCount     > 0);
	$subscribeShow = ($subscribeCount > 0);
	$naShow        = ($naCount        > 0);

	$delayActive     = ($_SESSION['RZ_BASKET_TAB'] == 'delay'     && $delayShow)     ? ' active' : '';
	$subscribeActive = ($_SESSION['RZ_BASKET_TAB'] == 'subscribe' && $subscribeShow) ? ' active' : '';
	$naActive        = ($_SESSION['RZ_BASKET_TAB'] == 'na'        && $naShow)        ? ' active' : '';
	$normalActive    = ($_SESSION['RZ_BASKET_TAB'] == 'items'
	                    || !($delayActive || $subscribeActive || $naActive))        ? ' active' : '';

	$noJSpage = CRZBitronic2CatalogUtils::noJsPage();
	$bShowStore = $arResult['USE_STORE'];
	$catalogParams = $arResult['CATALOG_PARAMS']; 
	?>
		<div id="basket_form_container">
			<a href="<?=htmlspecialcharsbx(str_replace('#ID#', 'all', $arUrls['delete']))?>" class="btn-delete pseudolink with-icon" id="basket-delete-all" data-action="delete" data-id="all">
				<i class="flaticon-trash29"></i>
				<span class="btn-text"><?=GetMessage("BITRONIC2_SALE_CLEAR")?></span>
			</a>
			
			<div class="um_tabs"><?

			if ($normalShow):
			?>

				<a href="#basket-big" class="um_tab<?=$normalActive?> basket-btn" id="basket_toolbar_button" data-tab="items">
					<i class="flaticon-shopping109"></i>
					<span class="btn-text"><?=GetMessage("BITRONIC2_SALE_BASKET_ITEMS_READY")?> <span class="hidden-xs"><?=GetMessage("BITRONIC2_SALE_BASKET_ITEMS")?> </span>(<span class="items-in-basket"><?=$normalCount?></span>)</span>
				</a><?

			endif;
			?><?
			if ($delayShow):
			?>

				<a href="#waitlist-big" class="um_tab<?=$delayActive?> waitlist-btn" id="basket_toolbar_button_delayed" data-tab="delay">
					<i class="flaticon-verified18"></i>
					<span class="btn-text"><?=GetMessage("BITRONIC2_SALE_BASKET_ITEMS_DELAYED")?> <span class="hidden-xs"><?=GetMessage("BITRONIC2_SALE_BASKET_ITEMS")?> </span>(<span class="items-in-waitlist"><?=$delayCount?></span>)</span>
				</a><?

			endif;
			?><?
			if ($naShow):
			?>

				<a href="#nalist-big" class="um_tab<?=$naActive?> waitlist-btn" id="basket_toolbar_button_not_available" data-tab="na">
					<i class="flaticon-251"></i>
					<span class="btn-text"><?=GetMessage("BITRONIC2_SALE_BASKET_ITEMS_NOT_AVAILABLE")?> <span class="hidden-xs"><?=GetMessage("BITRONIC2_SALE_BASKET_ITEMS")?> </span>(<span class="items-in-nalist"><?=$naCount?></span>)</span>
				</a><?

			endif;
			?>

			</div>
	
			<?
			if ($normalShow)     include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");
			if ($delayShow)      include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_delayed.php");
			/* TODO
			if ($subscribedShow) include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_subscribed.php");*/
			if ($naShow)         include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_not_available.php");
			?>
			<footer>
			<?if ($normalCount > 0):?>
				<div class="text-justify justify-fix">
					<a href="<?=SITE_DIR?>" class="btn-return" onclick="history.back();">
						<span class="text"><?=GetMessage('BITRONIC2_SALE_RETURN')?></span>
					</a>
					<?if($arParams['DISPLAY_ONECLICK']):?>
					<div class="one-click-buy-wrap">
						<button type="button" class="action one-click-buy" data-toggle="modal" data-target="#modal_quick-buy" data-basket="Y">
							<i class="flaticon-shopping220"></i>
							<span class="text"><?=GetMessage('BITRONIC2_ONECLICK_BUTTON')?></span>
						</button>
						<span class="helper"><?=GetMessage('BITRONIC2_ONECLICK_HELPER')?></span>
					</div>
					<?endif?>
					<!-- if authorized, the link below leads to order-step2
					if not, then to order-step1, which contains authorization/registration -->
					<a href="<?=$arParams['PATH_TO_ORDER']?>" class="btn-main btn-continue" onclick="checkOut();"><span class="text"><?=GetMessage('BITRONIC2_SALE_ORDER')?></span></a>
				</div>
				<?
				/** @noinspection PhpDynamicAsStaticMethodCallInspection */
				$infoContent = CMain::GetFileContent($_SERVER['DOCUMENT_ROOT'].SITE_DIR."include_areas/basket/info.php");
				$isExitstInfo = !empty($infoContent);?>
				<div class="basket-footer-info">
					<?$APPLICATION->IncludeComponent('bitrix:main.include', '', array("PATH" => SITE_DIR."include_areas/basket/info.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => "include_areas_template.php"))?>
				</div>
			<?endif?>
				<? if (!$isExitstInfo):?>
					<span class="shopping-bg">
						<i class="flaticon-shopping109"></i>
					</span>
				<? endif ?>
			</footer>
		</div>
		<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode(array('NAME','DISCOUNT','PROPS','DELETE','DELAY','PRICE','QUANTITY','SUM'), ","))?>" />
		<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
		<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
		<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
		<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
		<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
		<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
		<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />

			<?//<input type="hidden" name="BasketOrder" value="BasketOrder" />?>
			<!-- <input type="hidden" name="ajax_post" id="ajax_post" value="Y"> -->
	<?
}
else
{
	if (!empty($arResult["GRID"]["ROWS"])) {
		CRZBitronic2CatalogUtils::ShowMessage(array("MESSAGE"=>$arResult["ERROR_MESSAGE"], "TYPE"=>"ERROR"));
	} else {
		echo '<p>', $arResult["ERROR_MESSAGE"], '</p>';
	}
	echo '
		<footer>
			<span class="shopping-bg">
				<i class="flaticon-shopping109"></i>
			</span>
		</footer>
	';
}

if ($_POST['rz_ajax_no_header'] === 'y') die();

?>
	</form>
</main>

<?
// echo "<pre style='text-align:left;'>";var_export($arResult);echo "</pre>";
