<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Yenisite\Core\Ajax;
use Bitrix\Main\Loader;

if (strlen($arParams['DELIVERY_URL']) < 1) {
	$arParams['DELIVERY_URL'] = SITE_DIR.'about/delivery/';
}
$arParams['DISPLAY_ONECLICK'] = CModule::IncludeModule("yenisite.oneclick");
$arResult['FULL_PRICE_WITHOUT_DISCOUNT'] = 0;
$arResult['CATALOG_PARAMS'] = array();
$arResult['USE_STORE'] = false;

if(is_array($arResult["GRID"]["ROWS"]))
{
	if (Loader::IncludeModule('yenisite.core')) {
		$catalogParams = \Yenisite\Core\Ajax::getParams('bitrix:catalog', false, SITE_DIR . 'catalog/?rz_update_catalog_parameters_cache=Y');
		if (is_array($catalogParams) && !empty($catalogParams)) {
			global $rz_b2_options;
			$catalogParams['STORE_DISPLAY_TYPE'] = $rz_b2_options['store_amount_type'];
			$arResult['USE_STORE'] = $catalogParams["USE_STORE"] == "Y" && Bitrix\Main\ModuleManager::isModuleInstalled("catalog");
			$arResult['CATALOG_PARAMS'] = $catalogParams;
		}
	}
	foreach($arResult["GRID"]["ROWS"] as &$arItem)
	{
		$arItem['PICTURE_PRINT']['SRC'] = CRZBitronic2CatalogUtils::getElementPictureById($arItem['PRODUCT_ID'], $arParams['RESIZER_BASKET_PHOTO']);
		$arItem['SUM_NOT_FORMATED'] = $arItem['QUANTITY'] * $arItem['PRICE'];
		$arItem['FULL_SUM_NOT_FORMATED'] = $arItem['QUANTITY'] * $arItem['FULL_PRICE'];
		$arItem['FOR_ORDER'] = false;


		$arProduct = CCatalogProduct::GetByID($arItem['PRODUCT_ID']);
		$arItem['SUBSCRIBE'] = $arProduct['SUBSCRIBE'];
		if ($arItem['CAN_BUY'] == 'Y') {
			$arResult['FULL_PRICE_WITHOUT_DISCOUNT'] += $arItem['FULL_SUM_NOT_FORMATED'];
			$arItem['FOR_ORDER'] = ('Y' == $arProduct['QUANTITY_TRACE'] && 'Y' == $arProduct['CAN_BUY_ZERO'] && 0 >= $arProduct['QUANTITY']);
		}
	}
	unset($arItem);
}


$arResult['CURRENCIES'] = CRZBitronic2CatalogUtils::getCurrencyArray();

if(!\Bitrix\Main\Loader::includeModule('yenisite.core')) {
	die('Module yenisite.core not installed!');
}
Ajax::saveParams($this, $arParams, 'main_basket');