<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);
$compositeLoader = CRZBitronic2Composite::insertCompositLoader();

$productTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
	: $arResult["NAME"]
);
$bDiscountShow = (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF']);
$bStores = $arParams["USE_STORE"] == "Y" && Bitrix\Main\ModuleManager::isModuleInstalled("catalog");
$bShowStore = $bStores;
$canBuy = $arResult['CAN_BUY'];

$availableClass = !$canBuy ? 'out-of-stock' : ($arResult['FOR_ORDER'] ? 'available-for-order' : '');

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PRICE_WRAP' => $strMainID.'_price_wrap',
);
?>
<h2><?=GetMessage('BITRONIC2_POPUP_ITEM_TITLE')?></h2>
	<div class="basket-content">
		<?include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info.php';?>
		<div class="img-wrap">
			<img src="<?=$arResult['PICTURE_PRINT']['SRC']?>" alt="<?=$arResult['PICTURE_PRINT']['ALT']?>">
		</div>
		<div class="main-data">
			<a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="link"><span class="text"><?=$productTitle?></span></a>
			<div>
				<?if(!empty($arResult['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE'])):?>
					<span class="info art"><?=$arResult['PROPERTIES'][$arParams['ARTICUL_PROP']]['NAME']?>: <strong itemprop="productID"><?=is_array($arResult['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE']) ? implode(' / ', $arResult['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE']) : $arResult['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE']?></strong></span>
				<?endif?>
				<?$id = 'bxdinamic_BITRONIC2_popup_item_vote_'.$arResult['ID'];
				?><div id="<?=$id?>" class="inline"><?
				$frame = $this->createFrame($id, false)->begin($compositeLoader);?>
					<?/*$APPLICATION->IncludeComponent("bitrix:iblock.vote", "stars", array(
					"IBLOCK_TYPE" => $arResult['IBLOCK_TYPE_ID'],
					"IBLOCK_ID" => $arResult['IBLOCK_ID'],
					"ELEMENT_ID" => $arResult['ID'],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"MAX_VOTE" => "5",
					"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
					"SET_STATUS_404" => "N",
					),
					$component, array("HIDE_ICONS"=>"Y")
				);*/?>
				<?$frame->end();?>
				</div>
			</div>
			<div class="prices-wrap">
				<?if($bDiscountShow):?><span class="price-old"><?=CRZBitronic2CatalogUtils::getElementPriceFormat($arResult['MIN_PRICE']['CURRENCY'],$arResult['MIN_PRICE']['VALUE'],$arResult['MIN_PRICE']['PRINT_VALUE'])?></span><?endif?>
				<span class="price-now"><?=CRZBitronic2CatalogUtils::getElementPriceFormat($arResult['MIN_PRICE']['CURRENCY'],$arResult['MIN_PRICE']['DISCOUNT_VALUE'],$arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE'])?></span>
			</div>
			<div class="quantity-counter">
				
			</div>
			<?
			$availableID = false;
			$availableFrame = false;
			$availableItemID = &$arResult['ID'];
			$availableParamsName = 'arParams';
			$availableStoresPostfix = 'popup_item';
			$availableSubscribe = $arResult['CATALOG_SUBSCRIBE'];
			include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include/availability_info.php';
			?>
		</div>
	</div>
	<span class="catalog-hits">
		<?include 'similar_sell.php';?>
	</span>
	<div class="actions">
		<a href="#" class="btn-return" data-toggle="modal"
			data-target="#modal_basket">
			<span class="text"><?=GetMessage('BITRONIC2_POPUP_ITEM_CONTINUE')?></span>
		</a>
		<a href="<?=$arParams['BASKET_URL']?>" class="btn-continue">
			<span class="text"><?=GetMessage('BITRONIC2_POPUP_ITEM_MAKE_ORDER')?></span>
		</a>
	</div>
<?
// echo "<pre style='text-align:left;'>";print_r($arParams);echo "</pre>";