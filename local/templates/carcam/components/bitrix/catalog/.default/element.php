<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $rz_b2_options, $rz_current_sectionID;

$asset = Asset::getInstance();
$asset->addJs(SITE_TEMPLATE_PATH."/js/back-end/ajax/catalog_element.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/sliders/initHorizontalCarousels.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.mobile.just-touch.min.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.countdown.2.0.2/jquery.plugin.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.countdown.2.0.2/jquery.countdown.min.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initTimers.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmAccordeon.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmTabs.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmComboBlocks.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.magnify.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initMainGallery.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/sliders/initThumbs.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/toggles/initGenInfoToggle.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmScrollSpyMenu.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/libs/UmScrollFix.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/jquery.nouislider.all.min.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initCollectionHandle.js");
$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/pages/initProductPage.js");
CJSCore::Init(array('rz_b2_um_countdown', 'rz_b2_bx_catalog_item'));
if ('Y' == $rz_b2_options['wow-effect']) {
	$asset->addJs(SITE_TEMPLATE_PATH."/js/3rd-party-libs/wow.min.js");
}
if ('modal' == $rz_b2_options['detail_gallery_type']) {
	$asset->addJs(SITE_TEMPLATE_PATH."/js/custom-scripts/inits/initModalGallery.js");
}

include 'include/prepare_params_element.php'; // @var $arPrepareParams
ob_start();
$ElementID = $APPLICATION->IncludeComponent(
	"bitrix:catalog.element",
	'catalog',
	$arPrepareParams,
	$component
);

$component = ob_get_clean();
$arResult['ELEMENT_ID'] = $ElementID;

$arReplace = array();

ob_start();
include 'include/get_cur_section.php'; // @var $arCurSection
$rz_current_sectionID = $arCurSection['ID'];

	if($arParams['DETAIL_YM_API_USE'] == 'Y')
	{
		$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
			"AREA_FILE_SHOW" => "file",
			"CACHE_TYPE" => "N",
			"EDIT_TEMPLATE" => "include_areas_template.php",
			"PATH" => SITE_DIR."include_areas/catalog/rw_ymapi.php",
			"ELEMENT_ID" => $arResult["PROPERTIES"]["TURBO_YANDEX_LINK"]["VALUE"],
			), 
			false, 
			array("HIDE_ICONS"=>"Y")
		);
	}
$arReplace['#DETAIL_RW_YM_API#'] = ob_get_clean();

ob_start();
if ($arPrepareParams['HIDE_VIEWED'] != 'Y') {
	include 'include/viewed_products.php';
}
$arReplace['#DETAIL_RW_VIEWED_PRODUCTS#'] = ob_get_clean();

ob_start();
include 'include/local_shops_map.php';
$arReplace['#LOCAL_SHOPS_MAP#'] = ob_get_clean();

//replace macros in template.php
echo str_replace(array_keys($arReplace), $arReplace, $component);
?>

<? /* =================== EDOST START =================== */ ?>
<?
if (CModule::IncludeModule('sale') && CModule::IncludeModule('edost.catalogdelivery')) {
	$location = intval($_COOKIE['YS_GEO_IP_LOC_ID']);
	if ($location < 1) {
		$location = COption::GetOptionString('sale', 'location', '', SITE_ID);
		$location = CSaleLocation::getLocationIDbyCODE($location);
	}

	$arEdostParams = array('minimize' => '|full');
	if ($arParams['EDOST_SORT']      === 'Y') $arEdostParams['sort'] = 'ASC';
	if ($arParams['EDOST_ECONOMIZE'] === 'Y') $arEdostParams['economize'] = 'Y';
	if ($arParams['EDOST_MINIMIZE']  === 'Y') $arEdostParams['minimize'] = 'normal|full';
	if ($location > 0)                        $arEdostParams['location_id_default'] = $location;

	$frame = new \Bitrix\Main\Page\FrameBuffered("edost_catalogdelivery");
	$frame->begin('');

	$APPLICATION->IncludeComponent('edost:catalogdelivery', '', array(
		'PARAM' => $arEdostParams,
		'FRAME_X' => '650',
		'SHOW_QTY' => ($arParams['EDOST_SHOW_QTY'] === 'N' ? 'N' : 'Y'),
		'SHOW_ADD_CART' => ($arParams['EDOST_SHOW_ADD_CART'] === 'N' ? 'N' : 'Y'),
		'COLOR' => 'clear_white',
		'RADIUS' => '8',
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
	), null, array('HIDE_ICONS' => 'Y'));

	$frame->end();
}
?>
<? /* ==================== EDOST END ==================== */ ?>
