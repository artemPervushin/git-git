<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Page\Asset;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
define("IS_CATALOG_LIST", true);

$this->setFrameMode(true);

if(defined("ERROR_404"))
    return;

global $rz_b2_options, $rz_current_sectionID;
// $view, $sort, $by, $pagen, $pagen_key, $page_count
include 'include/service_var.php';

//global $rz_b2_options, $rz_current_sectionID;

include 'include/get_cur_section.php'; // @var $arCurSection
/*ini_set("display_errors",1);
error_reporting(E_ALL);
$rz_current_sectionID = $arCurSection['ID'];
_::dd($arCurSection['ID']);*/

//var_dump($rz_current_sectionID);


//unset($rz_current_sectionID);

$rz_current_sectionID = $arCurSection['ID'];

//die ('ccc');

//_::dd(memory_get_usage(true));

// THIS EXPRESSION NEEDS TO BE CHANGED IF REVIEWS OR RELATED-CATEGORIES SHOULD BE ADDED
$noAside = ($arResult['MENU_CATALOG'] !== 'side' && ($arParams['USE_FILTER'] !== 'Y' || $arResult['FILTER_PLACE'] !== 'side')) ? ' no-aside' : '';

$asset = Asset::getInstance();
$asset->addJs(SITE_TEMPLATE_PATH . "/js/3rd-party-libs/jquery.countdown.2.0.2/jquery.plugin.js");
$asset->addJs(SITE_TEMPLATE_PATH . "/js/3rd-party-libs/jquery.countdown.2.0.2/jquery.countdown.min.js");
$asset->addJs(SITE_TEMPLATE_PATH . "/js/custom-scripts/inits/initTimers.js");
$asset->addJs(SITE_TEMPLATE_PATH . "/js/custom-scripts/libs/UmTabs.js");
$asset->addJs(SITE_TEMPLATE_PATH . "/js/custom-scripts/inits/sliders/initPhotoThumbs.js");
$asset->addJs(SITE_TEMPLATE_PATH . "/js/custom-scripts/inits/initCatalogHover.js");
$asset->addJs(SITE_TEMPLATE_PATH . "/js/3rd-party-libs/jquery.nouislider.all.min.js");
CJSCore::Init(array('rz_b2_um_countdown', 'rz_b2_bx_catalog_item'));
if($rz_b2_options['quick-view'] === 'Y')
{
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/3rd-party-libs/jquery.mobile.just-touch.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/custom-scripts/inits/initMainGallery.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/custom-scripts/inits/toggles/initGenInfoToggle.js");
}
if('Y' == $rz_b2_options['wow-effect'])
{
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/3rd-party-libs/wow.min.js");
}
$asset->addJs(SITE_TEMPLATE_PATH . "/js/custom-scripts/inits/pages/initCatalogPage.js");

$is_first_level_category = (strpos($arResult['VARIABLES']['SECTION_CODE_PATH'], '/') === false);
?>

<?php if ($is_first_level_category): ?>
<main class="container catalog-page" id="catalog-page" data-page="catalog-page">
    <div class="row">
	    <?php
            $section = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], intval($arResult["VARIABLES"]["SECTION_ID"]));
            if (!empty($sectionValues = $section->getValues())) {
                $APPLICATION->SetPageProperty("title", $sectionValues['SECTION_META_TITLE']);
                $APPLICATION->SetPageProperty("description", $sectionValues['SECTION_META_DESCRIPTION']);
            }
        ?>
        <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "thumbnails", array(
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "SECTION_ID" => intval($arResult["VARIABLES"]["SECTION_ID"]),
            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
            "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
            "TOP_DEPTH" => '1',
            "SECTION_URL" => "",
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "ADD_SECTIONS_CHAIN" => "N",
            "VIEW_MODE" => strtoupper($rz_b2_options['catalog_subsection_view']),
            "SHOW_PARENT_NAME" => "N",
            "SHOW_SUBSECTIONS" => $bDescBottom ? 'N' : $rz_b2_options['block_list-sub-sections'],
            "SHOW_DESCRIPTION" => $showDescription,
            "RESIZER_SET" => $arParams['RESIZER_SUBSECTION'],
            ),
            $component
        );?>
    </div>
</main>
<?php else: ?>
<main class="container catalog-page<?=$noAside?>" id="catalog-page" data-page="catalog-page">
    <div class="row">
        <aside class="catalog-aside col-sm-12 col-md-3 col-xxl-2" id="catalog-aside">
            <div id="catalog-at-side" class="catalog-at-side minified">
                <? if($arResult['MENU_CATALOG'] == 'side' && $_REQUEST['rz_ajax'] !== 'y'): ?>
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                        "AREA_FILE_SHOW" => "file",
                        "EDIT_TEMPLATE"  => "include_areas_template.php",
                        "PATH"           => SITE_DIR . "include_areas/header/menu_catalog.php"
                    ), false, array("HIDE_ICONS" => "Y")); ?>
                <? endif ?>
            </div>
            <?php if ($arParams['USE_FILTER'] == 'Y'): ?>
            <div id="filter-at-side">
                <?php
                    if ($arResult['FILTER_PLACE'] == 'side' || ($_GET['ajax'] == 'y' && isset($_SERVER['HTTP_BX_AJAX'])))
                        include 'include/filter.php';
                ?>
            </div>
            <?php endif; ?>
        </aside>
        <div class="catalog-main-content col-sm-12 col-md-9 col-xxl-10">
            <h1><? $APPLICATION->ShowTitle(false) ?></h1>
            <?php
                $showDescription = $rz_b2_options['block_list-section-desc'];
                if($arParams['SHOW_DESCRIPTION_TOP'] == 'N') {
                    $showDescription = 'N';
                }
                if($rz_b2_options['block_list-sub-sections'] == "Y" || ($rz_b2_options['block_list-section-desc'] == "Y" && $arParams['SHOW_DESCRIPTION_TOP'] != 'N'))
                {
                    include 'include/section_list.php';
                }
                $brandsId = 'bx_dynamic_' . $this->randString(20);
            ?>
            <div class="brands-catalog hidden-xs" id="<?=$brandsId?>">
                <?php
                    $dynamicArea = new \Bitrix\Main\Page\FrameStatic("catalog_brands_dynamic");
                    $dynamicArea->setAnimation(true);
                    $dynamicArea->setContainerID($brandsId);
                    $dynamicArea->startDynamicArea();
                    $APPLICATION->ShowViewContent('catalog_brands');
                    $dynamicArea->finishDynamicArea();
                ?>
            </div>

            <?php if($arParams['USE_FILTER'] == 'Y' && $arResult['FILTER_PLACE'] == 'top'): ?>
            <div id="filter-at-top"><?php include 'include/filter.php'; ?></div>
            <?php endif; ?>

            <div class="sort-n-view for-catalog">
                <? include 'include/sort.php'; ?>
                <? include 'include/view.php'; ?>
            </div>
            <?
            $catalogClass = 'catalog ';
            $catalogClass .= ($view == 'table' ? 'catalog-table' : $view);
            $catalogClass .= ' active';
            $catalogClass .= ($arParams['HIDE_ICON_SLIDER'] === 'Y' ? ' thumbs-disabled' : '');
            $quickView = ($rz_b2_options['quick-view'] === 'Y' ? 'true' : 'false');

            global ${$arParams["FILTER_NAME"]};
            if(!empty(${$arParams["FILTER_NAME"]}))
            {
                $arSkipFilters = array('FACET_OPTIONS');

                $arDiff = array_diff(array_keys(${$arParams["FILTER_NAME"]}), $arSkipFilters);

                $arParams["CACHE_FILTER"] = count($arDiff) > 0 ? 'N' : 'Y';

                unset($arSkipFilters, $arDiff);
            }

            include 'include/prepare_params_section.php';
            ?>

            <div class="<?=$catalogClass?>" id="catalog_section" data-hover-effect="<?=$arResult['HOVER-MODE']?>" data-quick-view-enabled="<?=$quickView?>"><!--
				-->
                <? $APPLICATION->IncludeComponent("bitrix:catalog.section", $view, $arSectionParams, //prepare_params_section.php
                    $component); ?><!--
			--></div>

            <?php include 'include/pagination.php'; ?>

            <?
            $dynamicArea = new \Bitrix\Main\Page\FrameStatic("catalog_hits_dynamic");
            $dynamicArea->setAnimation(true);
            $dynamicArea->startDynamicArea();
            if($rz_b2_options['block_list-hits'] == 'Y' && $_REQUEST['rz_ajax'] !== 'y')
            {
                include 'include/catalog_hits.php';
                Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/back-end/visual/hits.js");
            }
            $dynamicArea->finishDynamicArea();
            ?>

            <?
            $showDescription = $rz_b2_options['block_list-section-desc'];
            if($arParams['SHOW_DESCRIPTION_BOTTOM'] == 'N')
            {
                $showDescription = 'N';
            }
            if($rz_b2_options['block_list-section-desc'] == "Y" && $arParams['SHOW_DESCRIPTION_BOTTOM'] == 'Y')
            {
                $bDescBottom = true;
                include 'include/section_list.php';
            }
            ?>
            <div class="banners">
                <? if(\Bitrix\Main\Loader::includeModule("advertising")): ?>
                    <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", "catalog_bottom", Array(
                            "TYPE"       => "b2_catalog_bottom",
                            "NOINDEX"    => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "1000"
                        ), $component, array("HIDE_ICONS" => "Y")); ?>
                <? endif ?>
            </div>
        </div>
        <!-- /.catalog-main-content.col-sm-12.col-md-9 -->
    </div>
    <!-- /.row -->
</main>
<script type="text/javascript">
    window.ad_category = "<?=$arCurSection['ID']?>";   // required

    window._retag = window._retag || [];
    window._retag.push({code: "9ce88869eb", level: 1});
    (function () {
        var id = "admitad-retag";
        if (document.getElementById(id)) {return;}
        var s=document.createElement("script");
        s.async = true; s.id = id;
        var r = (new Date).getDate();
        s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static/js/retag.js?r="+r;
        var a = document.getElementsByTagName("script")[0]
        a.parentNode.insertBefore(s, a);
    })()
</script>
<?php endif; ?>