<?
foreach ($arItemPrices as $priceCode => $arPrice): ?>
	<? if ($arPrice['PRICE_ID'] != $arItem['MIN_PRICE']['PRICE_ID']): ?>
		<div class="additional-price-type">
			<span class="price-desc"><?= $arResult['PRICES'][$priceCode]['TITLE'] ?>:</span>
			<span class="price"><?if(!empty($arItem['OFFERS'])) echo GetMessage('RZ_OT')?><?
				echo CRZBitronic2CatalogUtils::getElementPriceFormat(
					$arPrice['CURRENCY'],
					$arPrice['DISCOUNT_VALUE'],
					$arPrice['PRINT_DISCOUNT_VALUE']
				);
			?></span>
		</div>
	<? endif ?>
<? endforeach ?>