<?
$MESS["BITRONIC2_BLOCKS_ADD_TO_BASKET"] = "В корзину";
$MESS["BITRONIC2_BLOCKS_ADD_TO_COMPARE"] = "Добавить в <strong>список сравнения</strong>";
$MESS["BITRONIC2_BLOCKS_DEL_TO_COMPARE"] = "Убрать из списка сравнения";
$MESS["BITRONIC2_BLOCKS_ADD_TO_FAVORITE"] = "Добавить в избранное";
$MESS["BITRONIC2_BLOCKS_TO_DETAIL"] = "Подробнее";
$MESS["BITRONIC2_BLOCKS_NOTAVAIL"] = "Нет в наличии";
$MESS["BITRONIC2_BLOCKS_NEW_PRICE"] = "Новая цена:";
$MESS["BITRONIC2_BLOCKS_OLD_PRICE"] = "Цена без скидки:";
$MESS["BITRONIC2_BLOCKS_FROM"] = "от";
$MESS["BITRONIC2_BLOCKS_QUICK_VIEW"] = "Быстрый просмотр";
$MESS["BITRONIC2_BLOCKS_ADMIN_INFO"] = "Информация для администратора интернет-магазина (пользователям данный блок не отображается)";
$MESS["BITRONIC2_BLOCKS_ADMIN_INFO_INDEX_SORT"] = "Индекс сортировки";
$MESS["BITRONIC2_BLOCKS_ADMIN_INFO_SALE_EXT"] = "Продаж (выгружено)";
$MESS["BITRONIC2_BLOCKS_ADMIN_INFO_SALE_INT"] = "Продаж (подсчитано на сайте)";
$MESS["BITRONIC2_BLOCKS_ADMIN_INFO_SHOW_WEEK"] = "Просмотров за неделю";
$MESS["BITRONIC2_BLOCKS_ADMIN_INFO_SHOW_DAYS"] = "Просмотры по дням";

$MESS["BITRONIC2_BLOCKS_ONECLICK"] = "В 1 клик";

$MESS["BITRONIC2_BLOCKS_PAGEN_NEXT"] = "След.";
$MESS["BITRONIC2_BLOCKS_PAGEN_PREV"] = "Пред.";
$MESS["CT_BCS_TPL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";

$MESS["BITRONIC2_PRODUCT_IN_CART"] = "В корзине";
$MESS["RZ_OT"] = "от ";
