<div style="text-align:center;margin:10px 0;">
	<?php
		$geo = explode('/', $_COOKIE["YS_GEO_IP_CITY"]);
		$map_data = '';
		$city = isset($geo[2])? $geo[2]:'Санкт-Петербург';

		if (CModule::IncludeModule("iblock")) {
			
			// Поиск раздела
			$sections = CIBlockSection::GetList(Array("SORT" => "­­ASC"), Array("IBLOCK_ID" => 157, "NAME" => $city), false, Array("UF_ZOOM", "UF_LAT", "UF_LON"));
			if ($section = $sections->GetNext()) {

				// Устновка центра и масштаба карты
                $map_data['google_lat']		= $section["UF_LAT"]?	floatval($section["UF_LAT"]):55.751480318208;
                $map_data['google_lon']		= $section["UF_LON"]?	floatval($section["UF_LON"]):37.59977671393;
                $map_data['google_scale']	= $section["UF_ZOOM"]?	intval($section["UF_ZOOM"])	:9;

				// Поиск точек самовывоза
				$elements = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 157, "SECTION_ID" => $section["ID"]), false, false, Array("PROPERTY_GEO_TYPE", "PROPERTY_GEO_PHONE", "PROPERTY_GEO_TIME", "PROPERTY_GEO_MAP_POINT", "PROPERTY_ADDRESS"));
				while($element = $elements->GetNext()) {
					$coordinates = explode(',', $element['PROPERTY_GEO_MAP_POINT_VALUE']);
					$map_data['PLACEMARKS'][] = Array(
						'TEXT' => $element['PROPERTY_ADDRESS_VALUE'],
						'LON' => floatval($coordinates[0]),
						'LAT' => floatval($coordinates[1]),
					);
				}
			}
		}
	?>
	<div data-location-id="<?= $_COOKIE["YS_GEO_IP_LOC_ID"]; ?>">Доставка по городу <?= $city; ?> - бесплатно</div>
	<?php
		if (!empty($map_data))
			$APPLICATION->IncludeComponent(
				"bitrix:map.google.view", 
				"bit_map_google", 
				array(
					"COMPONENT_TEMPLATE" => ".default",
					"INIT_MAP_TYPE" => "ROADMAP",
					"MAP_DATA" => serialize($map_data),
					"MAP_HEIGHT" => "250",
					"MAP_ID" => "",
					"MAP_WIDTH" => "100%",
					"CONTROLS" => array(
						0 => "SMALL_ZOOM_CONTROL",
					),
					"OPTIONS" => array(
						0 => "ENABLE_DBLCLICK_ZOOM",
						1 => "ENABLE_DRAGGING",
						2 => "ENABLE_KEYBOARD",
					),
				),
				false
			);
	?>
	<a href="/buy/" target="_blank">смотреть все пункты самовывоза</a>
</div>