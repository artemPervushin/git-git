$(document).ready(function() {
	$('.product-3d-view-link').fancybox({
		width		: 600,
		height		: 600,
		autoSize	: false,
		type		: 'iframe',
		beforeShow: function(){
			var self = this;
			setTimeout(function() {
				console
				this.width = $('.fancybox-iframe').contents().find('img:first').width();
				this.height = $('.fancybox-iframe').contents().find('img:first').height();
				$.fancybox.update()
			}, 1000);
		}
	});
});