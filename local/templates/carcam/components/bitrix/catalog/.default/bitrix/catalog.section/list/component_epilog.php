<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

//Needs for iblock.vote to not break composite
IncludeAJAX();

global $arPagination;
$arPagination = $arResult['NAV_PAGINATION'];

if (!empty($templateData['jsFile'])) {
	if (\Yenisite\Core\Tools::isAjax() || $_SERVER['HTTP_BX_AJAX'] !== null) {
		$jsString = file_get_contents(rtrim($_SERVER['DOCUMENT_ROOT'], '/\\') . $templateData['jsFile']);
		echo '<script>', $jsString, '</script>';
	} else {
		echo '<script src="', $templateData['jsFile'], '?', time(), '"></script>';
	}
}
