<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\ModuleManager;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

//no whitespace in this file!!!!!!
$this->setFrameMode(true);

include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info_dynamic.php';

$this->SetViewTarget('catalog_paginator');
	echo $arResult["NAV_STRING"];
$this->EndViewTarget();

if(empty($arResult['ITEMS']))
	return;

$arJsCache = CRZBitronic2CatalogUtils::getJSCache($component);

$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$bStores = $arParams["USE_STORE"] == "Y" && Bitrix\Main\ModuleManager::isModuleInstalled("catalog");

foreach($arResult['ITEMS'] as $arItem):
	$this->AddEditAction($templateName.'-'.$arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($templateName.'-'.$arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($templateName.'-'.$arItem['ID']);
	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'STICKER_ID' => $strMainID.'_sticker',
		'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BUY_ONECLICK' => $strMainID.'_buy_oneclick',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',
		'FAVORITE_LINK' => $strMainID.'_favorite_link',

		'OLD_PRICE' => $strMainID.'_old_price',
		'PRICE' => $strMainID.'_price',
		'PRICE_ADDITIONAL' => $strMainID.'_price_additional',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
		'AVAILABILITY' => $strMainID . '_availability',
	);
	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);

	$bShowStore = $bStores && !$arItem['bOffers'];
	$bShowOneClick = $arParams['DISPLAY_ONECLICK'] && !$arItem['bOffers'];
	
	$availableOnRequest = $arItem['ON_REQUEST'];
	$availableClass = (
		!$arItem['CAN_BUY'] && !$availableOnRequest
		? 'out-of-stock'
		: (
			$arItem['FOR_ORDER'] || $availableOnRequest
			? 'available-for-order'
			: 'in-stock'
		)
	);
	if ($availableOnRequest) $arItem['CAN_BUY'] = false;

	?><div class="catalog-item-wrap active" id="<?=$arItemIDs['ID']?>">
		<div class="catalog-item blocks-item wow fadeIn">
			<div class="photo-wrap <?=!$arItem['SHOW_SLIDER'] ? ' no-thumbs' : ''?>">
				<div class="photo">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<img src="<?=$arItem['PICTURE_PRINT']['SRC']?>" alt="<?=$arItem['PICTURE_PRINT']['ALT']?>">
					</a>
					<?$APPLICATION->IncludeComponent("yenisite:stickers", "section", array(
						"ELEMENT" => $arItem,
						"STICKER_NEW" => $arParams['STICKER_NEW'],
						"STICKER_HIT" => $arParams['STICKER_HIT'],
						),
						$component, array("HIDE_ICONS"=>"Y")
					);?>
				</div>
			</div>
			<div class="main-data">
				<div class="name-and-description">
					<div class="name">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link"><span class="text"><?=$productTitle?></span></a>
					</div>
					<?php if (isset($arItem['PROPERTIES']['SHORT_TEXT']['VALUE']) && $arItem['PROPERTIES']['SHORT_TEXT']['VALUE'] != ''): ?>
					<div class="short-description"><?= $arItem['PROPERTIES']['SHORT_TEXT']['VALUE'] ?></div>
					<?php endif; ?>
				</div>
				<div class="prices<?=(empty($availableOnRequest)?'':' invisible')?>">
					<div>
						<span class="price-old" id="<?=$arItemIDs['OLD_PRICE']?>">
							<? $frame = $this->createFrame($arItemIDs['OLD_PRICE'], false)->begin() ?>
							<? if($arItem['MIN_PRICE']['DISCOUNT_DIFF'] > 0 && $arParams['SHOW_OLD_PRICE'] == 'Y'): ?>
								<?=CRZBitronic2CatalogUtils::getElementPriceFormat($arItem['MIN_PRICE']['CURRENCY'], $arItem['MIN_PRICE']['VALUE'], $arItem['MIN_PRICE']['PRINT_VALUE']);?>
							<? endif ?>
							<? $frame->end() ?>
						</span>
						<span class="price" id="<?=$arItemIDs['PRICE']?>">
							<? $frame = $this->createFrame($arItemIDs['PRICE'], false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
								<?=($arItem['bOffers']) ? GetMessage('BITRONIC2_BLOCKS_FROM') : ''?>
								<?=CRZBitronic2CatalogUtils::getElementPriceFormat($arItem['MIN_PRICE']['CURRENCY'], $arItem['MIN_PRICE']['DISCOUNT_VALUE'], $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']);?>
							<? $frame->end() ?>
						</span>
					</div>
					<div id="<?= $arItemIDs['PRICE_ADDITIONAL'] ?>" class="additional-price-container">
						<? $frame = $this->createFrame($arItemIDs['PRICE_ADDITIONAL'], false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
						<? foreach ($arItem['PRICES'] as $priceCode => $arPrice):?>
							<? if ($arPrice['PRICE_ID'] != $arItem['MIN_PRICE']['PRICE_ID']): ?>
								<div class="additional-price-type">
									<span class="price-desc"><?=$arResult['PRICES'][$priceCode]['TITLE']?>:</span>
									<span class="price"><?if(!empty($arItem['OFFERS'])) echo GetMessage('RZ_OT')?><?
										echo CRZBitronic2CatalogUtils::getElementPriceFormat(
											$arPrice['CURRENCY'],
											$arPrice['DISCOUNT_VALUE'],
											$arPrice['PRINT_DISCOUNT_VALUE']
										);
									?></span>
								</div>
							<? endif ?>
						<? endforeach ?>
						<? $frame->end() ?>
					</div>
				</div>
				<div class="action-buttons" id="<?=$arItemIDs['BASKET_ACTIONS']?>">
					<? $frame = $this->createFrame($arItemIDs['BASKET_ACTIONS'], false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
					<div class="xs-switch">
						<i class="flaticon-arrow128 when-closed"></i>
						<i class="flaticon-key22 when-opened"></i>
					</div>
					<?if ($arParams['DISPLAY_FAVORITE'] && !$arItem['bOffers']):?>
						<button 
							type="button" 
							class="btn-action favorite" 
							data-favorite-id="<?=$arItem['ID']?>" 
							data-tooltip title="<?=GetMessage('BITRONIC2_BLOCKS_ADD_TO_FAVORITE')?>"
							id="<?=$arItemIDs['FAVORITE_LINK']?>">
							<i class="flaticon-heart3"></i>
						</button>
					<?endif?>
					<div class="btn-buy-wrap text-only">
						<?if($arItem['bOffers']):?>
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn-action buy when-in-stock">
								<i class="flaticon-shopping109"></i>
								<span class="text"><?=GetMessage('BITRONIC2_BLOCKS_TO_DETAIL')?></span>
							</a>
						<?else:?>
							<?if($arItem['CAN_BUY']):?>
								<button type="button" class="btn-action buy when-in-stock" id="<?=$arItemIDs['BUY_LINK']?>" data-product-id="<?=$arItem['ID']?>">
									<i class="flaticon-shopping109"></i>
									<span class="text"><?=GetMessage('BITRONIC2_BLOCKS_ADD_TO_BASKET')?></span>
									<span class="text in-cart"><?=GetMessage('BITRONIC2_PRODUCT_IN_CART')?></span>
								</button>
								<?if($bShowOneClick):?> 
									<div class="one-click-wrap1">
										<button id="<?= $arItemIDs['BUY_ONECLICK'] ?>" type="button"
												class="action one-click-buy <?= ($arItem['CAN_BUY'] || !empty($arItem['OFFERS']) ? '' : ' disabled') ?>"
												data-toggle="modal" data-target="#modal_quick-buy" data-id="<?= $arItem['ID'] ?>"
												data-props="<?= \Yenisite\Core\Tools::GetEncodedArParams($arParams['OFFER_TREE_PROPS']) ?>">
											<i class="flaticon-shopping220"></i>
											<span class="text"><?=GetMessage('BITRONIC2_BLOCKS_ONECLICK')?></span>
										</button>
									</div>
								<?endif?>
							<?elseif($availableOnRequest):?>
								<button type="button" class="btn-action buy when-in-stock" data-product-id="<?= $arItem['ID'] ?>" data-measure-name="<?=$arItem['CATALOG_MEASURE_NAME']?>"
									data-toggle="modal" data-target="#modal_contact_product">
									<i class="flaticon-speech90"></i>
									<span class="text"><?=GetMessage('BITRONIC2_PRODUCT_REQUEST')?></span>
								</button>
							<?else:?>
								<span class="when-out-of-stock"><?=GetMessage('BITRONIC2_BLOCKS_NOTAVAIL')?></span>
							<?endif?>
						<?endif?>
					</div>
					<?if ($arParams['DISPLAY_COMPARE']):?>
						<button 
							type="button" 
							class="btn-action compare" 
							data-compare-id="<?=$arItem['ID']?>" 
							data-tooltip title="<?=GetMessage('BITRONIC2_BLOCKS_ADD_TO_COMPARE')?>" 
							id="<?=$arItemIDs['COMPARE_LINK']?>">
							<i class="flaticon-balance3"></i>
						</button>
					<?endif?>
					<? $frame->end() ?>
				</div>
			</div>
			<!--<div class="description full-view">
				<dl class="techdata">
					<?/*foreach($arItem['DISPLAY_PROPERTIES'] as $arProp):?>
						<dt><?=$arProp['NAME']?></dt>
						<dd><?=strip_tags(is_array($arProp['DISPLAY_VALUE']) ? implode(' / ',$arProp['DISPLAY_VALUE']) : $arProp['DISPLAY_VALUE'])?></dd>
					<?endforeach?>
				</dl>
				<?=$arItem['PREVIEW_TEXT']*/?>
			</div>-->
				
			<? // ADMIN INFO
			include 'admin_info.php'; ?>
		</div><!-- /.catalog-item.blocks-item -->
		<?
		$arJSParams = array(
			'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
			'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
			'SHOW_ADD_BASKET_BTN' => false,
			'SHOW_BUY_BTN' => false,
			'SHOW_ABSENT' => false,
			'SHOW_SKU_PROPS' => false,
			'SECOND_PICT' => $arItem['SECOND_PICT'],
			'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
			'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'DISPLAY_FAVORITE' => $arParams['DISPLAY_FAVORITE'],
			'REQUEST_URI' => $arItem['DETAIL_PAGE_URL'],//$_SERVER["REQUEST_URI"], need for AJAX to work on search page
			'SCRIPT_NAME' => $_SERVER["SCRIPT_NAME"],
			'DEFAULT_PICTURE' => array(
				'PICTURE' => $arItem['PRODUCT_PREVIEW'],
				'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
			),
			'VISUAL' => array(
				'ID' => $arItemIDs['ID'],
				'PICT_ID' => $arItemIDs['PICT'],
				'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
				'QUANTITY_ID' => $arItemIDs['QUANTITY'],
				'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
				'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
				'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
				'PRICE_ID' => $arItemIDs['PRICE'],
				'TREE_ID' => $arItemIDs['PROP_DIV'],
				'TREE_ITEM_ID' => $arItemIDs['PROP'],
				'BUY_ID' => $arItemIDs['BUY_LINK'],
				'BUY_ONECLICK' => $arItemIDs['BUY_ONECLICK'],
				'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
				'DSC_PERC' => $arItemIDs['DSC_PERC'],
				'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
				'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
				'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
				'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
				'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
				'FAVORITE_ID' => $arItemIDs['FAVORITE_LINK']
			),
			'BASKET' => array(
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
				'BASKET_URL' => $arParams['BASKET_URL'],
				'ADD_URL_TEMPLATE' => $arResult['ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['BUY_URL_TEMPLATE']
			),
			'PRODUCT' => array(
				'ID' => $arItem['ID'],
				'IBLOCK_ID' => $arItem['IBLOCK_ID'],
				'NAME' => $productTitle,
				'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
				'CAN_BUY' => $arItem["CAN_BUY"],
				'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
				'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
				'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
				'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
				'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
				'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
				'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
			),
			'OFFERS' => array(),
			'OFFER_SELECTED' => 0,
			'TREE_PROPS' => array(),
			'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
		);
		if ($arParams['DISPLAY_COMPARE'])
		{
			$arJSParams['COMPARE'] = array(
				'COMPARE_URL_TEMPLATE' => $arResult['COMPARE_URL_TEMPLATE'],
				'COMPARE_URL_TEMPLATE_DEL' => $arResult['COMPARE_URL_TEMPLATE_DEL'],
				'COMPARE_PATH' => $arParams['COMPARE_PATH']
			);
		}
		if ($arParams['DISPLAY_FAVORITE'])
		{
			$arJSParams['FAVORITE'] = array(
				'FAVORITE_URL_TEMPLATE' => $arResult['FAVORITE_URL_TEMPLATE'],
				'FAVORITE_URL_TEMPLATE_DEL' => $arResult['FAVORITE_URL_TEMPLATE_DEL'],
				'FAVORITE_PATH' => $arParams['FAVORITE_PATH']
			);
		}
		?>
<? $frame = $this->createFrame()->begin('') ?><?
		$jsString = 'var ' . $strObName . ' = new JCCatalogItem('. CUtil::PhpToJSObject($arJSParams, false, true) .');';
		if ($arJsCache['file']):
			fwrite($arJsCache['file'], $jsString);
		else:?>
		<script type="text/javascript">
			<?=$jsString?>
		</script>
		<?endif?>
<? $frame->end() ?>
	</div><!-- /.catalog-item-wrap --><?
endforeach;
// echo "<pre style='text-align:left;'>";print_r($arResult);echo "</pre>";

?><script>
	$("#catalog_section").toggleClass("availability-comments-enabled", <?=($arResult['AVAILABILITY_COMMENTS_ENABLED']?'true':'false')?>);
</script><?

if ($arJsCache['file']):?><script src="<?= $arJsCache['path'], '/', $arJsCache['idJS'], '?', time()?>"></script><?
fclose($arJsCache['file']);
endif;
