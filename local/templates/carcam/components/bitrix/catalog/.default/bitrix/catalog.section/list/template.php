<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info_dynamic.php';

$this->SetViewTarget('catalog_paginator');
	echo $arResult["NAV_STRING"];
$this->EndViewTarget();

if(empty($arResult['ITEMS']))
	return;

$arJsCache = CRZBitronic2CatalogUtils::getJSCache($component);
	
$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$bStores = $arParams["USE_STORE"] == "Y" && Bitrix\Main\ModuleManager::isModuleInstalled("catalog");
?>

<?foreach($arResult['ITEMS'] as $arItem):
	$this->AddEditAction($templateName.'-'.$arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($templateName.'-'.$arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($templateName.'-'.$arItem['ID']);
	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'STICKER_ID' => $strMainID.'_sticker',
		'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BUY_ONECLICK' => $strMainID.'_buy_oneclick',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'AVAILABLE_INFO' => $strMainID.'_avail_info',
		'AVAILABLE_INFO_FULL' => $strMainID.'_avail_info_full',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',
		'FAVORITE_LINK' => $strMainID.'_favorite_link',
		'REQUEST_LINK' => $strMainID.'_request_link',

		'PRICE' => $strMainID.'_price',
		'PRICE_OLD' => $strMainID.'_price_old',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'ARTICUL' => $strMainID.'_articul',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
		'BASKET_BUTTON' => $strMainID.'_basket_button',
		'STORES' => $strMainID.'_stores',
		'PRICE_ADDITIONAL' => $strMainID.'_price_additional',
	);
	$arItemCLASSes = array(
		'LINK' => $strMainID.'_link',
	);
	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	
	$bSkuExt = $arItem['bSkuExt'];
	$bShowStore = $bStores && !$arItem['bSkuSimple'];
	$bShowOneClick = $arParams['DISPLAY_ONECLICK'] && (!$arItem['bOffers'] || $arItem['bSkuExt']);

	$arItem['ARTICUL'] = (
		$arItem['bOffers'] && $bSkuExt && !empty($arItem['JS_OFFERS'][$arItem['OFFERS_SELECTED']]['ARTICUL'])
		? $arItem['JS_OFFERS'][$arItem['OFFERS_SELECTED']]['ARTICUL']
		: (
			is_array($arItem['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE'])
			? implode(' / ', $arItem['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE'])
			: $arItem['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE']
		)
	);

	$availableOnRequest = (
		$arItem['bOffers'] && $bSkuExt
		? empty($arItem['JS_OFFERS'][$arItem['OFFERS_SELECTED']]['MIN_PRICE']) || $arItem['JS_OFFERS'][$arItem['OFFERS_SELECTED']]['MIN_PRICE']['VALUE'] <= 0 
		: empty($arItem['MIN_PRICE']) || $arItem['MIN_PRICE']['VALUE'] <= 0
	);
	$arItem['CAN_BUY'] = (
		$arItem['bOffers'] && $bSkuExt 
		? $arItem['JS_OFFERS'][$arItem['OFFERS_SELECTED']]['CAN_BUY'] 
		: $arItem['CAN_BUY'] && !$availableOnRequest
	);
	
	$availableClass = (
		!$arItem['CAN_BUY'] && !$availableOnRequest
		? 'out-of-stock'
		: (
			$arItem['FOR_ORDER'] || $availableOnRequest
			? 'available-for-order'
			: 'in-stock'
		)
	);
	?><div class="catalog-item-wrap" id="<?=$arItemIDs['ID']?>">
		<div class="catalog-item list-item wow fadeIn">
			<div class="photo-wrap <?=!$arItem['SHOW_SLIDER'] ? ' no-thumbs' : ''?>">
				<div class="photo">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="<?=$arItemCLASSes['LINK']?>">
						<img src="<?=$arItem['PICTURE_PRINT']['SRC']?>" alt="<?=$arItem['PICTURE_PRINT']['ALT']?>" id="<?=$arItemIDs['PICT']?>">
					</a>
					<?$APPLICATION->IncludeComponent("yenisite:stickers", "section", array(
						"ELEMENT" => $arItem,
						"STICKER_NEW" => $arParams['STICKER_NEW'],
						"STICKER_HIT" => $arParams['STICKER_HIT'],
						),
						$component
					);?>
				</div>
			</div>
			<div class="main-data">
				<div class="name-and-description">
					<div class="name">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link"><span class="text"><?=$productTitle?></span></a>
					</div>
					<?php if (isset($arItem['PROPERTIES']['SHORT_TEXT']['VALUE']) && $arItem['PROPERTIES']['SHORT_TEXT']['VALUE'] != ''): ?>
					<div class="short-description"><?= $arItem['PROPERTIES']['SHORT_TEXT']['VALUE'] ?></div>
					<?php endif; ?>
				</div>
				<div class="availability clearfix" id="<?= $arItemIDs['AVAILABLE_INFO_FULL'] ?>">
					<? $frame = $this->createFrame($arItemIDs['AVAILABLE_INFO_FULL'],false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
					<?if($bShowStore && $arParams['HIDE_STORE_LIST'] !== 'Y'):?>
						<?$APPLICATION->IncludeComponent("bitrix:catalog.store.amount", "store", array(
							"PER_PAGE" => "10",
							"USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
							"SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
							"USE_MIN_AMOUNT" => 'N',
							"MIN_AMOUNT" => $arParams["MIN_AMOUNT"],
							"ELEMENT_ID" => $arItem['ID'],
							"STORE_PATH"  =>  $arParams["STORE_PATH"],
							"MAIN_TITLE"  =>  $arParams["MAIN_TITLE"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							'STORE_CODE' => $arParams["STORE_CODE"],
							'FIELDS' => array('DESCRIPTION'),
							'STORE_DISPLAY_TYPE' => $arParams['STORE_DISPLAY_TYPE'],
							'STORES' => $arParams['STORES']
						),
						$component, 
						array("HIDE_ICONS"=>"Y")
						);?>
					<?endif?>
					<? $frame->end() ?>
				</div>
				
				
				<?// DISPLAY_PROPERTIES?>
				<dl class="techdata">
					<?foreach($arItem['DISPLAY_PROPERTIES'] as $arProp):
							$arProp["DISPLAY_VALUE"] = (substr_count($arProp["DISPLAY_VALUE"], "a href") > 0)
								? strip_tags($arProp["DISPLAY_VALUE"])
								: $arProp["DISPLAY_VALUE"];
							?>
						<dt><?=$arProp['NAME']?>:</dt>
						<dd><?=(is_array($arProp['DISPLAY_VALUE']) ? implode(' / ',$arProp['DISPLAY_VALUE']) : $arProp['DISPLAY_VALUE'])?></dd>
					<?endforeach?>
				</dl>
				<?
				if ($bSkuExt && $arItem['OFFERS_PROPS_DISPLAY'])
				{
					foreach ($arItem['JS_OFFERS'] as $keyOffer => $arJSOffer)
					{
						$strProps = '';
						if (!empty($arJSOffer['DISPLAY_PROPERTIES']))
						{
							foreach ($arJSOffer['DISPLAY_PROPERTIES'] as $arOneProp)
							{
								$strProps .= '<dt>'.$arOneProp['NAME'].':</dt><dd>'.(
									is_array($arOneProp['VALUE'])
									? implode(' / ', $arOneProp['VALUE'])
									: $arOneProp['VALUE']
								).'</dd>';
							}
						}
						$arItem['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
					}
					?>
					<dl class="techdata clearfix" id="<? echo $arItemIDs['DISPLAY_PROP_DIV']; ?>" style="display: none;"></dl>
					<?
				}
				?>

				<div class="action-buttons">
					<?if ($arParams['DISPLAY_FAVORITE'] && !$arItem['bOffers']):?>
						<button 
							type="button" 
							class="btn-action favorite" 
							data-favorite-id="<?=$arItem['ID']?>" 
							data-tooltip title="<?=GetMessage('BITRONIC2_BLOCKS_ADD_TO_FAVORITE')?>"
							id="<?=$arItemIDs['FAVORITE_LINK']?>">
							<i class="flaticon-heart3"></i>
						</button>
					<?endif?>
					<?if ($arParams['DISPLAY_COMPARE']):?>
						<button 
							type="button" 
							class="btn-action compare" 
							data-compare-id="<?=$arItem['ID']?>" 
							data-tooltip title="<?=GetMessage('BITRONIC2_LIST_ADD_TO_COMPARE')?>" 
							id="<?=$arItemIDs['COMPARE_LINK']?>">
							<i class="flaticon-balance3"></i>
						</button>
					<?endif?>
				</div>
			</div><!-- main-data -->
			<div class="buy-block">
				<div class="prices<?=(empty($availableOnRequest)?'':' hide')?>">
					<div>
						<span class="price-old" id="<?=$arItemIDs['PRICE_OLD']?>">
						<? $frame = $this->createFrame($arItemIDs['PRICE_OLD'], false)->begin('') ?>
						<? if($arItem['MIN_PRICE']['DISCOUNT_DIFF'] > 0 && $arParams['SHOW_OLD_PRICE'] == 'Y'): ?>
							<?=CRZBitronic2CatalogUtils::getElementPriceFormat($arItem['MIN_PRICE']['CURRENCY'], $arItem['MIN_PRICE']['VALUE'], $arItem['MIN_PRICE']['PRINT_VALUE']);?>
						<? endif ?>
						<? $frame->end() ?>
						</span>
						<span class="price" id="<?=$arItemIDs['PRICE']?>">
						<? $frame = $this->createFrame($arItemIDs['PRICE'], false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
						<?=($arItem['bOffers'] && !$bSkuExt) ? GetMessage('BITRONIC2_LIST_FROM') : ''?>
						<?=CRZBitronic2CatalogUtils::getElementPriceFormat($arItem['MIN_PRICE']['CURRENCY'], $arItem['MIN_PRICE']['DISCOUNT_VALUE'], $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']);?>
						<? $frame->end() ?>
						</span>
					</div>
					<div id="<?= $arItemIDs['PRICE_ADDITIONAL'] ?>" class="additional-price-container">
						<? $frame = $this->createFrame($arItemIDs['PRICE_ADDITIONAL'], false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
						<?
						$arItemPrices = $arItem['PRICES'];
						include 'additional_prices.php'
						?>
						<? $frame->end() ?>
					</div>
				</div>
				<div id="<?= $arItemIDs['BASKET_BUTTON']; ?>">
				<? $frame = $this->createFrame($arItemIDs['BASKET_BUTTON'],false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
					<form action="#" method="post" class="form_buy" id="<?= $arItemIDs['PROP_DIV']; ?>">
	<?
	// ***************************************
	// ************ EXTENDED SKU *************
	// ***************************************
	if (isset($arItem['OFFERS']) && !empty($arItem['OFFERS']) && !empty($arItem['OFFERS_PROP']))
	{
		$arSkuProps = array();

		foreach ($arResult['SKU_PROPS'] as &$arProp)
		{
			if (!isset($arItem['OFFERS_PROP'][$arProp['CODE']]))
				continue;
			$arSkuProps[] = array(
				'ID' => $arProp['ID'],
				'SHOW_MODE' => $arProp['SHOW_MODE'],
				'VALUES_COUNT' => $arProp['VALUES_COUNT']
			);
			//if ('TEXT' == $arProp['SHOW_MODE'])
			//{
				?>
					<select name="sku" class="select-styled" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list">
						<?foreach ($arProp['VALUES'] as $arOneValue):
							$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);?>
							<option
								data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>"
								data-onevalue="<? echo $arOneValue['ID']; ?>"
								data-showmode="<? echo $arProp['SHOW_MODE']; ?>"
								id="<? echo $arItemIDs['PROP'] . $arProp['ID'] . '_' . $arOneValue['ID']; ?>"
								value="<? echo $arOneValue['ID']; ?>"
								><?=$arOneValue['NAME']; ?></option>
						<?endforeach?>
					</select>
				<?
			//}
			// elseif ('PICT' == $arProp['SHOW_MODE'])
			// {

			// }
		}
		unset($arProp);
	}
	?>
						<div class="xs-switch">
							<i class="flaticon-arrow128 when-closed"></i>
							<i class="flaticon-key22 when-opened"></i>
						</div>
						<div class="btn-buy-wrap text-only" id="<?=$arItemIDs['BASKET_ACTIONS']?>">
							<?if($arItem['bOffers'] && !$bSkuExt):?>
								<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn-action buy when-in-stock <?=$arItemCLASSes['LINK']?>">
									<i class="flaticon-shopping109"></i>
									<span class="text"><?=GetMessage('BITRONIC2_LIST_TO_DETAIL')?></span>
								</a>
							<?else:?>
								<?if($arItem['bOffers'] && $bSkuExt):?>
									<button type="button" class="btn-action buy when-in-stock<?=($arItem['CAN_BUY']) ? '' : ' hide'?>" id="<?=$arItemIDs['BUY_LINK']?>" data-product-id="<?=$arItem['ID']?>" data-offer-id="<?=$arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['ID']?>">
										<i class="flaticon-shopping109"></i>
										<span class="text"><?=GetMessage('BITRONIC2_LIST_ADD_TO_BASKET')?></span>
										<span class="text in-cart"><?=GetMessage('BITRONIC2_PRODUCT_IN_CART')?></span>
									</button>
									<button type="button" class="btn-action buy when-in-stock on-request" id="<?=$arItemIDs['REQUEST_LINK']?>" data-toggle="modal" data-target="#modal_contact_product"
										data-product-id="<?=$arItem['ID']?>" data-offer-id="<?=$arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['ID']?>" data-measure-name="<?=$arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['CATALOG_MEASURE_NAME']?>">
										<i class="flaticon-speech90"></i>
										<span class="text"><?=GetMessage('BITRONIC2_PRODUCT_REQUEST')?></span>
									</button>
								<?elseif($arItem['CAN_BUY']):?>
									<button type="button" class="btn-action buy when-in-stock" id="<?=$arItemIDs['BUY_LINK']?>" data-product-id="<?=$arItem['ID']?>">
										<i class="flaticon-shopping109"></i>
										<span class="text"><?=GetMessage('BITRONIC2_LIST_ADD_TO_BASKET')?></span>
										<span class="text in-cart"><?=GetMessage('BITRONIC2_PRODUCT_IN_CART')?></span>
									</button>
								<?elseif($availableOnRequest):?>
									<button type="button" class="btn-action buy when-in-stock" id="<?=$arItemIDs['REQUEST_LINK']?>" data-toggle="modal" data-target="#modal_contact_product"
										data-product-id="<?= $arItem['ID'] ?>" data-measure-name="<?=$arItem['CATALOG_MEASURE_NAME']?>">
										<i class="flaticon-speech90"></i>
										<span class="text"><?=GetMessage('BITRONIC2_PRODUCT_REQUEST')?></span>
									</button>
								<?else:?>
									<span class="when-out-of-stock"><?=GetMessage('BITRONIC2_LIST_NOTAVAIL')?></span>
								<?endif?>
							<?endif?>
						</div>
					</form>
					<?if($bShowOneClick):?>
						<button id="<?= $arItemIDs['BUY_ONECLICK'] ?>" type="button"
								class="action one-click-buy<?= ($arItem['CAN_BUY']) ? '' : ' hide' ?>" data-toggle="modal"
								data-target="#modal_quick-buy" data-id="<?= $arItem['ID'] ?>"
								data-props="<?= \Yenisite\Core\Tools::GetEncodedArParams($arParams['OFFER_TREE_PROPS']) ?>">
							<i class="flaticon-shopping220"></i>
							<span class="text"><?=GetMessage('BITRONIC2_LIST_ONECLICK')?></span>
						</button>
					<?endif?>
					<? $frame->end() ?>
				</div>
			</div><!-- /.buy-block -->
				
			<div class="description full-view">
				<?=$arItem['PREVIEW_TEXT']?>
			</div>
				
			<? // ADMIN INFO
			include 'admin_info.php';
			?>
			
		</div><!-- /.catalog-item.blocks-item -->
		<? // JS PARAMS
		include 'js_params.php';
		?>
	</div><!-- /.catalog-item-wrap --><?
endforeach;
// echo "<pre style='text-align:left;'>";print_r($arResult);echo "</pre>";

if ($arJsCache['file']) {
	$templateData['jsFile'] = $arJsCache['path'].'/'.$arJsCache['idJS'];
	fclose($arJsCache['file']);
}

