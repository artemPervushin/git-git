<div class="detailed-tech">
	<? if (!empty($arResult['DISPLAY_PROPERTIES'])): ?>
		<h4><?= GetMessage('BITRONIC2_CHARACTERISTICS_TECH') ?> <?= $productTitle ?></h4>
		<div class="tech-info-block expandable expanded allow-multiple-expanded">
			<dl class="expand-content clearfix">
				<? foreach ($arResult['DISPLAY_PROPERTIES'] as $arProp):?>
				<? if($arProp['CODE'] != "PODDERJKA_119" and $arProp['CODE'] != "VOLMODEL_119" and $arProp['CODE'] != "OBZORI_119" and $arProp['CODE'] != 'OPISANIE_119'):?>
					<dt><span class="property-name"><?= $arProp['NAME'] ?><?
							if (strlen($arProp['HINT']) > 0):
								?><sup data-tooltip title="<?= $arProp['HINT'] ?>" data-placement="right">?</sup><?
							endif ?></span></dt>
					<dd><?= (is_array($arProp['DISPLAY_VALUE']) ? implode(' / ', $arProp['DISPLAY_VALUE']) : $arProp['DISPLAY_VALUE']) ?></dd>
					<? endif?>
				<? endforeach ?>
			</dl>
		</div>
		<?php if ($arResult["CATALOG_TYPE"] == CCatalogProduct::TYPE_SET): ?>
			<?php foreach($arResult["SET"] as $arSet): ?>
			<h4><?= $arSet['NAME'] ?></h4>
			<div class="tech-info-block expandable expanded allow-multiple-expanded">
				<dl class="expand-content clearfix">
					<? foreach ($arSet['PROPERTIES'] as $code => $arProp): ?>
						<?php if (empty($arProp['VALUE']) || in_array($code, array('PODDERJKA_119', 'VOLMODEL_119', 'OBZORI_119', 'OPISANIE_119'))) continue; ?>
						<dt><span class="property-name"><?= $arProp['NAME'] ?></span></dt>
						<dd><?= (is_array($arProp['VALUE']) ? implode(' / ', $arProp['VALUE']) : $arProp['VALUE']) ?></dd>
					<? endforeach ?>
				</dl>
			</div>
			<?php endforeach; ?>
		<?php endif; ?>
	<? endif ?>
</div><!-- /.detailed-tech -->