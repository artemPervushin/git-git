<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info_dynamic.php';

$this->SetViewTarget('catalog_paginator');
	echo $arResult["NAV_STRING"];
$this->EndViewTarget();

if(empty($arResult['ITEMS']))
	return;

$arJsCache = CRZBitronic2CatalogUtils::getJSCache($component);

$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$commonBuyId = 'add_basket_table';
?>
<div class="table-header clearfix">
	<div class="actions">
		<button type="button" class="btn-main disabled" id="<?=$commonBuyId?>">
			<span class="text"><?=GetMessage('BITRONIC2_TABLE_ADD_TO_BASKET')?> (<span class="number">0</span>)</span>
		</button>
	</div>
	<div class="quan"><?=GetMessage('BITRONIC2_TABLE_QUANTITY')?></div>
	<div class="avail-n-price"><?=GetMessage('BITRONIC2_TABLE_AVAIL_AND_PRICE')?></div>
	<?if($arParams['ARTICUL_PROP'] && $arResult['ITEMS'][0]['PROPERTIES'][$arParams['ARTICUL_PROP']]['NAME']):?>
		<!-- use class .no-art on this div if there are no arts at all -->
		<div class="art-wrap"><?=$arResult['ITEMS'][0]['PROPERTIES'][$arParams['ARTICUL_PROP']]['NAME']?></div>
	<?endif?>
	<div class="name"><?=GetMessage('BITRONIC2_TABLE_NAME')?></div>
	
	
	
</div>

<table <?if($USER->IsAdmin()):?>class="has-admin-info"<?endif?>>
<?foreach($arResult['ITEMS'] as $arItem):
	$this->AddEditAction($templateName.'-'.$arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($templateName.'-'.$arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($templateName.'-'.$arItem['ID']);
	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'STICKER_ID' => $strMainID.'_sticker',
		'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'COMMON_BUY_ID' => $commonBuyId,
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',
		'FAVORITE_LINK' => $strMainID.'_favorite_link',

		'OLD_PRICE' => $strMainID.'_old_price',
		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	);
	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	$availableOnRequest = $arItem['ON_REQUEST'];
	$availableClass = (
		!$arItem['CAN_BUY'] && !$availableOnRequest
		? 'out-of-stock'
		: (
			$arItem['FOR_ORDER'] || $availableOnRequest
			? 'available-for-order'
			: 'in-stock'
		)
	);
	if ($availableOnRequest) {
		$arItem['CAN_BUY'] = false;
	}
	$bArticul = $arParams['ARTICUL_PROP'] && !empty($arItem['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE']);
	?>
	<tr class="catalog-item catalog-table-item wow fadeIn" id="<?=$arItemIDs['ID']?>">
		<td class="art-wrap <?=(!$bArticul) ? ' no-art' : ''?>">
			<div class="art">
				<div class="photo-wrap">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="photo">
						<img src="<?=$arItem['PICTURE_PRINT']['SRC']?>" alt="<?=$arItem['PICTURE_PRINT']['ALT']?>">
					</a>
				</div>
				<?if($bArticul):?>
					<strong><?=is_array($arItem['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE']) ? implode(' / ', $arItem['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE']) : $arItem['PROPERTIES'][$arParams['ARTICUL_PROP']]['VALUE']?></strong>
				<?endif?>
			</div>
		</td>
		<td class="name">
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link"><span class="text"><?=$productTitle?></span></a>
		</td>
		<td class="avail-n-price <?=$availableClass?>">
			<div class="xs-switch">
				<i class="flaticon-arrow128 when-closed"></i>
				<i class="flaticon-key22 when-opened"></i>
			</div>
			<span class="avail-dot when-in-stock"></span>
			<span class="avail-dot when-out-of-stock"></span>
			<span class="avail-dot when-available-for-order"></span>
			<span class="price" id="<?=$arItemIDs['PRICE']?>">
<? $frame = $this->createFrame($arItemIDs['PRICE'], false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
<? if(empty($availableOnRequest)): ?>
				<?=($arItem['bOffers']) ? GetMessage('BITRONIC2_TABLE_FROM') : ''?>
				<?=CRZBitronic2CatalogUtils::getElementPriceFormat($arItem['MIN_PRICE']['CURRENCY'], $arItem['MIN_PRICE']['DISCOUNT_VALUE'], $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']);?>

<? endif ?>
<? $frame->end() ?>
			</span>
		</td>
		<td class="quan">
			<?if ('Y' == $arParams['USE_PRODUCT_QUANTITY'] && !$arItem['bOffers'] && $arItem['CAN_BUY']):?>
				<div class="quantity-counter"
					data-tooltip
					data-placement="right"
					title="<?=$arItem['CATALOG_MEASURE_NAME']?>">
					<!-- parent must have class .quantity-counter! -->
					<button type="button" class="btn-silver quantity-change decrease" id="<?=$arItemIDs['QUANTITY_DOWN']?>"><span class="minus">&ndash;</span></button>
					<input type="text" name="quantity" class="quantity-input textinput" value="0" id="<?=$arItemIDs['QUANTITY']?>" data-item-id="<?=$arItem['ID']?>">
					<button type="button" class="btn-silver quantity-change increase" id="<?=$arItemIDs['QUANTITY_UP']?>"><span class="plus">+</span></button>
				</div>
			<?elseif($arItem['bOffers'] || $availableOnRequest):?>
				<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link"><span class="text"><?=GetMessage('BITRONIC2_TABLE_TO_DETAIL')?></span></a>
			<?endif?>
		</td>
		<td class="actions">
			<?if ($arParams['DISPLAY_FAVORITE'] && !$arItem['bOffers']):?>
				<button 
					type="button" 
					class="btn-action favorite" 
					data-favorite-id="<?=$arItem['ID']?>" 
					data-tooltip title="<?=GetMessage('BITRONIC2_TABLE_ADD_TO_FAVORITE')?>"
					id="<?=$arItemIDs['FAVORITE_LINK']?>">
					<i class="flaticon-heart3"></i>
				</button>
			<?endif?>
			<?if ($arParams['DISPLAY_COMPARE']):?>
				<button 
					type="button" 
					class="btn-action compare" 
					data-compare-id="<?=$arItem['ID']?>" 
					data-tooltip title="<?=GetMessage('BITRONIC2_TABLE_ADD_TO_COMPARE')?>" 
					id="<?=$arItemIDs['COMPARE_LINK']?>">
					<i class="flaticon-balance3"></i>
				</button>
			<?endif?>
		</td>
	</tr><!-- /.catalog-item.table-item -->
	<tr>
		<td colspan="5">
			<? // ADMIN INFO
			include 'admin_info.php';
			?>
		<?
		$arJSParams = array(
			'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
			'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
			'SHOW_ADD_BASKET_BTN' => false,
			'SHOW_BUY_BTN' => false,
			'SHOW_ABSENT' => false,
			'SHOW_SKU_PROPS' => false,
			'SECOND_PICT' => $arItem['SECOND_PICT'],
			'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
			'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'DISPLAY_FAVORITE' => $arParams['DISPLAY_FAVORITE'],
			'REQUEST_URI' => $_SERVER["REQUEST_URI"],
			'SCRIPT_NAME' => $_SERVER["SCRIPT_NAME"],
			'DEFAULT_PICTURE' => array(
				'PICTURE' => $arItem['PRODUCT_PREVIEW'],
				'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
			),
			'VISUAL' => array(
				'ID' => $arItemIDs['ID'],
				'PICT_ID' => $arItemIDs['PICT'],
				'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
				'QUANTITY_ID' => $arItemIDs['QUANTITY'],
				'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
				'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
				'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
				'PRICE_ID' => $arItemIDs['PRICE'],
				'TREE_ID' => $arItemIDs['PROP_DIV'],
				'TREE_ITEM_ID' => $arItemIDs['PROP'],
				'BUY_ID' => $arItemIDs['BUY_LINK'],
				'COMMON_BUY_ID' => $arItemIDs['COMMON_BUY_ID'],
				'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
				'DSC_PERC' => $arItemIDs['DSC_PERC'],
				'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
				'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
				'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
				'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
				'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
				'FAVORITE_ID' => $arItemIDs['FAVORITE_LINK']
			),
			'BASKET' => array(
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
				'BASKET_URL' => $arParams['BASKET_URL'],
				'ADD_URL_TEMPLATE' => $arResult['ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['BUY_URL_TEMPLATE']
			),
			'PRODUCT' => array(
				'ID' => $arItem['ID'],
				'IBLOCK_ID' => $arItem['IBLOCK_ID'],
				'NAME' => $productTitle,
				'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
				'CAN_BUY' => $arItem["CAN_BUY"],
				'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
				'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
				'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
				'MIN_QUANTITY' => 0,
				'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
				'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
				'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
				'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
			),
			'OFFERS' => array(),
			'OFFER_SELECTED' => 0,
			'TREE_PROPS' => array(),
			'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
		);
		if ($arParams['DISPLAY_COMPARE'])
		{
			$arJSParams['COMPARE'] = array(
				'COMPARE_URL_TEMPLATE' => $arResult['COMPARE_URL_TEMPLATE'],
				'COMPARE_URL_TEMPLATE_DEL' => $arResult['COMPARE_URL_TEMPLATE_DEL'],
				'COMPARE_PATH' => $arParams['COMPARE_PATH']
			);
		}
		if ($arParams['DISPLAY_FAVORITE'])
		{
			$arJSParams['FAVORITE'] = array(
				'FAVORITE_URL_TEMPLATE' => $arResult['FAVORITE_URL_TEMPLATE'],
				'FAVORITE_URL_TEMPLATE_DEL' => $arResult['FAVORITE_URL_TEMPLATE_DEL'],
				'FAVORITE_PATH' => $arParams['FAVORITE_PATH']
			);
		}
		?>
<? $frame = $this->createFrame()->begin('') ?><?
		$jsString = 'var ' . $strObName . ' = new JCCatalogItem('. CUtil::PhpToJSObject($arJSParams, false, true) .');';
		if ($arJsCache['file']):
			fwrite($arJsCache['file'], $jsString);
		else:?>
		<script type="text/javascript">
			<?=$jsString?>
		</script>
		<?endif?>
<? $frame->end() ?>
		</td>
	</tr>
		<?
endforeach;?>
</table>
<?
if ($arJsCache['file']):?><script src="<?= $arJsCache['path'], '/', $arJsCache['idJS'], '?', time()?>"></script><?
fclose($arJsCache['file']);
endif;
