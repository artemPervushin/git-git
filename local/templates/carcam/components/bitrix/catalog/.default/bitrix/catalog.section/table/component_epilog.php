<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

//Needs for iblock.vote to not break composite
IncludeAJAX();

global $arPagination;
$arPagination = $arResult['NAV_PAGINATION'];