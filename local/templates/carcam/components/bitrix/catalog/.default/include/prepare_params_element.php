<?
global $rz_b2_options;
//update parameters to set needed currency (only after CPHPCache check, changes through cookies)
if ($rz_b2_options['currency-switcher'] == 'Y') {
	$arParams['CONVERT_CURRENCY'] = 'Y';
	$arParams['CURRENCY_ID'] = $rz_b2_options['active-currency'];
}
	
// resizer
$arSets = array();
$arSets['RESIZER_DETAIL_SMALL'] = $arParams['RESIZER_DETAIL_SMALL'];
$arSets['RESIZER_DETAIL_BIG'] = $arParams['RESIZER_DETAIL_BIG'];
$arSets['RESIZER_DETAIL_ICON'] = $arParams['RESIZER_DETAIL_ICON'];
$arSets['RESIZER_DETAIL_FLY_BLOCK'] = $arParams['RESIZER_DETAIL_FLY_BLOCK'];
$arSets['RESIZER_SET_CONTRUCTOR'] = $arParams['RESIZER_SET_CONTRUCTOR'];
$arSets['RESIZER_SECTION'] = $arParams['RESIZER_SECTION'];
$arSets['RESIZER_COMMENT_AVATAR'] = $arParams['RESIZER_COMMENT_AVATAR'];
$arSets['RESIZER_RECOMENDED'] = $arParams['RESIZER_RECOMENDED'];

if (\Bitrix\Main\Loader::includeModule('yenisite.geoipstore')) {
	if(!isset($rz_b2_options['GEOIP'])) {
		$arRes = $APPLICATION->IncludeComponent('yenisite:geoip.store', 'empty');
		$rz_b2_options['GEOIP'] = $arRes;
	}
	if (!empty($rz_b2_options['GEOIP']['PRICES'])) {
		$arParams["PRICE_CODE"] = $rz_b2_options['GEOIP']['PRICES'];
	}
	$arParams['STORES'] = NULL;
	if (!empty($rz_b2_options['GEOIP']['STORES'])) {
		$arParams['STORES'] = $rz_b2_options['GEOIP']['STORES'];
	}
}

$arPrepareParams = array(
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
	"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
	"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
	"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
	"BASKET_URL" => $arParams["BASKET_URL"],
	"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
	"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
	"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
	"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
	"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	"SET_TITLE" => $arParams["SET_TITLE"],
	"SET_STATUS_404" => $arParams["SET_STATUS_404"],
	"PRICE_CODE" => $arParams["PRICE_CODE"],
	"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
	"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
	"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
	"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
	"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
	"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
	"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
	"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
	"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
	"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
	"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
	"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],
	"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
	"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
	"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
	"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
	"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
	'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
	'CURRENCY_ID' => $arParams['CURRENCY_ID'],
	'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
	'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],

	//compare:
	'DISPLAY_COMPARE' => ($arParams['USE_COMPARE'] == 'Y'),
	'COMPARE_PATH' => $arParams['COMPARE_PATH'],
	
	// favorites:
	'DISPLAY_FAVORITE' => $arParams['DISPLAY_FAVORITE'],

	// oneclick:
	"DISPLAY_ONECLICK" => $arParams["DISPLAY_ONECLICK"],
	
	'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
	'LABEL_PROP' => $arParams['LABEL_PROP'],
	'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
	'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
	'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
	'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
	'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
	'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
	//'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
	"BRAND_PROP_CODE" => $arParams['LIST_BRAND_PROP_CODE'],
	// 'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
	'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
	'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
	"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
	"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
	"DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
	"DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
	
	//sku:
	'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
	'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
	'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
	"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
	"OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
	"OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
	"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
	"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
	"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
	"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
	"OFFER_VAR_NAME" => $arParams["OFFER_VAR_NAME"],
	"ADD_PARENT_PHOTO" => $arParams["ADD_PARENT_PHOTO"],
	
	//store:
	'USE_STORE' => $arParams['USE_STORE'],
	'USE_STORE_PHONE' => $arParams['USE_STORE_PHONE'],
	'USE_MIN_AMOUNT' => $arParams['USE_MIN_AMOUNT'],
	
	//SEF:
	"SEF_MODE" => $arParams["SEF_MODE"],
	"SEF_FILTER_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
	"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
	'SEF_FOLDER' => $arParams['SEF_FOLDER'],
	"FILTER_NAME" => $arParams["FILTER_NAME"],
	"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
	
	//resizer :
	"RESIZER_SETS" => $arSets,
	
	//stickers :
	"STICKER_NEW" => $arParams['STICKER_NEW'],
	"STICKER_HIT" => $arParams['STICKER_HIT'],
	"STICKER_BESTSELLER" => $arParams['STICKER_BESTSELLER'],
	
	//articul:
	"ARTICUL_PROP" => $arParams['ARTICUL_PROP'],
	
	//tabs:
	"DETAIL_INFO_MODE" => $arResult['DETAIL_INFO_MODE'],
	
	//reviews
	'USE_REVIEW' => $arParams['USE_REVIEW'],
	'USE_OWN_REVIEW' => $arParams['USE_OWN_REVIEW'],
	'REVIEWS_MODE' => $arParams['REVIEWS_MODE'],
	"FORUM_ID" => $arParams['FORUM_ID'],
	"MESSAGES_PER_PAGE" => $arParams['MESSAGES_PER_PAGE'],
	'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
	'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
	'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
	'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
	'FEEDBACK_USE' => (isset($arParams['DETAIL_FEEDBACK_USE']) ? $arParams['DETAIL_FEEDBACK_USE'] : 'Y'),
	"FEEDBACK_IBLOCK_TYPE" => $arParams["FEEDBACK_IBLOCK_TYPE"],
	"FEEDBACK_IBLOCK_ID" => $arParams["FEEDBACK_IBLOCK_ID"],
	'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
	'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
	'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
	'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),		
	'DETAIL_YM_API_USE' => $arParams['DETAIL_YM_API_USE'],

	//quick view:
	'QUICK_VIEW' => (isset($_REQUEST['rz_quick_view']) ? 'Y' : 'N'),
	'QUICK_SHOW_CHARS' => (isset($_REQUEST['rz_quick_view']) ? $rz_b2_options['quick-view-chars'] : 'N'),
	//big data:
	'HIDE_ACCESSORIES' => ($rz_b2_options['block_detail-addtoorder'] == 'Y') ? 'N' : 'Y',
	'HIDE_SIMILAR' => ($rz_b2_options['block_detail-similar'] == 'Y') ? 'N' : 'Y',
	'HIDE_SIMILAR_VIEW' => ($rz_b2_options['block_detail-similar-view'] == 'Y') ? 'N' : 'Y',
	'HIDE_RECOMMENDED' => ($rz_b2_options['block_detail-recommended'] == 'Y') ? 'N' : 'Y',
	'HIDE_VIEWED' => ($rz_b2_options['block_detail-viewed'] == 'Y') ? 'N' : 'Y',
	'STORE_DISPLAY_TYPE' => $rz_b2_options['store_amount_type'],
	'STORES' => $arParams['STORES'],
	'DETAIL_GALLERY_DESCRIPTION' => $rz_b2_options['detail_gallery_description'],
	'DETAIL_GALLERY_TYPE' => $rz_b2_options['detail_gallery_type'],
	'DETAIL_TEXT_DEFAULT' => $rz_b2_options['detail_text_default'],
	'DETAIL_INFO_FULL_EXPANDED' => $rz_b2_options['detail_info_full_expanded'],
	'FOUND_CHEAP' => $arParams['DETAIL_FOUND_CHEAP'],
	'FOUND_CHEAP_TYPE' => $arParams['DETAIL_FOUND_CHEAP_TYPE'],
	'FOUND_CHEAP_IBLOCK' => $arParams['DETAIL_FOUND_CHEAP_IBLOCK'],
	'PRICE_LOWER' => $arParams['DETAIL_PRICE_LOWER'],
	'PRICE_LOWER_TYPE' => $arParams['DETAIL_PRICE_LOWER_TYPE'],
	'PRICE_LOWER_IBLOCK' => $arParams['DETAIL_PRICE_LOWER_IBLOCK'],
	'ELEMENT_EXIST' => $arParams['DETAIL_ELEMENT_EXIST'],
	'ELEMENT_EXIST_TYPE' => $arParams['DETAIL_ELEMENT_EXIST_TYPE'],
	'ELEMENT_EXIST_IBLOCK' => $arParams['DETAIL_ELEMENT_EXIST_IBLOCK'],
);
