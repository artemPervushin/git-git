<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.bigdata.products", 
	"catalog_hits", 
	array(
		// BITRONIC 2 RELATED
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"DISPLAY_FAVORITE" => $arParams["DISPLAY_FAVORITE"],
		"DISPLAY_ONECLICK" => $arParams["DISPLAY_ONECLICK"],
		"RESIZER_SECTION" => $arParams["RESIZER_SECTION"],
		"SHOW_HITS" => $arParams["SECTION_SHOW_HITS"],
		"SHOW_MINIFIED" => ($_COOKIE['RZ_show_hits_catalog'] === 'false'),
		//"COMPONENT_TEMPLATE" => "bitronic2",
		// MAIN PARAMETERS
		"RCM_TYPE" => $arParams["SECTION_HITS_RCM_TYPE"],//bestsell
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		// DATA SOURCE
		"SHOW_FROM_SECTION" => "Y",
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"DEPTH" => "2",
		"HIDE_NOT_AVAILABLE" => $arParams["SECTION_HITS_HIDE_NOT_AVAILABLE"],//"Y"
		// VISUAL
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_NAME" => "Y",
		"SHOW_IMAGE" => "Y",
		"MESS_BTN_BUY" => GetMessage('BITRONIC2_BUTTON_BUY'),
		"MESS_BTN_DETAIL" => GetMessage('BITRONIC2_BUTTON_DETAIL'),
		"MESS_BTN_SUBSCRIBE" => GetMessage('BITRONIC2_BUTTON_SUBSCRIBE'),
		"PAGE_ELEMENT_COUNT" => "4",
		// CACHE
		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
		"CACHE_TIME" => $arParams['CACHE_TIME'],
		"CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
		// PRICES
		"SHOW_OLD_PRICE" => "N",
		"PRICE_CODE" => $arParams['PRICE_CODE'],
		"SHOW_PRICE_COUNT" => $arParams['SHOW_PRICE_COUNT'],
		"PRICE_VAT_INCLUDE" => $arParams['PRICE_VAT_INCLUDE'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams['CURRENCY_ID'],
		// BASKET
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
		"USE_PRODUCT_QUANTITY" => "N",
		// CATALOG PRODUCTS
		"SHOW_PRODUCTS_{$arParams['IBLOCK_ID']}" => "Y",
		"PROPERTY_CODE_{$arParams['IBLOCK_ID']}" => $arParams["LIST_PROPERTY_CODE"],
	),
	$component
);?>