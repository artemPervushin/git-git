<?
$arFilterParams = array(
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"SECTION_ID" => $arCurSection["ID"],
	"FILTER_NAME" => $arParams["FILTER_NAME"],
	'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	"SAVE_IN_SESSION" => "N",
	"INSTANT_RELOAD" => "N",
	"PRICE_CODE" => $arParams["PRICE_CODE"],
	"XML_EXPORT" => "N",
	"SECTION_TITLE" => "-",
	"SECTION_DESCRIPTION" => "-",
	"VISIBLE_PROPS_COUNT" => $arParams["FILTER_VISIBLE_PROPS_COUNT"],
	"HIDE" => $arParams['FILTER_HIDE'],
	"FILTER_PLACE" => $arResult['FILTER_PLACE'],
	"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
	"CURRENCY_ID" => $arParams['CURRENCY_ID'],
	"RESIZER_FILTER" => $arParams['RESIZER_FILTER'],
	"BRAND_HIDE" => $arParams['LIST_BRAND_USE'],
	"BRAND_PROP_CODE" => $arParams['LIST_BRAND_PROP_CODE'],
	'SHOW_ALL_WO_SECTION' => $arParams['SHOW_ALL_WO_SECTION'],
	"SEF_MODE" => $arParams["SEF_MODE"],
	"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
	"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],

);

if (CModule::IncludeModule('catalog')) {

	$dbRes = CCatalogGroup::GetList();

	while ($arCatalogGroup = $dbRes->Fetch()) {
		if ($arCatalogGroup['CAN_BUY'] == 'Y') continue;

		$key = array_search($arCatalogGroup['NAME'], $arFilterParams['PRICE_CODE']);
		if ($key !== false) {
			unset($arFilterParams['PRICE_CODE'][$key]);
		}
	}
}

$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "filter", $arFilterParams,
$component
);