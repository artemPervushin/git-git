<?
use Bitronic2\Catalog\CookiesUtils;
use Bitronic2\Mobile;

CookiesUtils::setPrefix(SITE_ID.'_');

// for right work of composite
\CHTMLPagesCache::setUserPrivateKey(CacheProvider::getCachePrefix(), 0);

// view
$view = CookiesUtils::getView($rz_b2_options['catalog_view_default']);
$arSupViews = array();
if ($rz_b2_options['block_list-view-block'] == 'Y') {
	$arSupViews[] = 'blocks';
}
if ($rz_b2_options['block_list-view-list'] == 'Y') {
	$arSupViews[] = 'list';
}
if ($rz_b2_options['block_list-view-table'] == 'Y') {
	$arSupViews[] = 'table';
}
if (!in_array($view, $arSupViews, $strict = true)) {
	$view = empty($arSupViews)
	      ? (mobile::isMobile() ? 'list' : 'blocks')
	      : $arSupViews[array_rand($arSupViews)];
}

// Page count
$page_count = CookiesUtils::getPageCount();
$arPageCount = CookiesUtils::$_arPageCount;
// ---

// sort
$sort = CookiesUtils::getSort($arParams, NULL, $rz_b2_options['catalog_sort_default_field']);
$arSort = CookiesUtils::$_arSort;
// by
$by = CookiesUtils::getSortBy($rz_b2_options['catalog_sort_default_by']);

$arSupSort = array();
if ($rz_b2_options['block_list-sort-name'] == 'Y') {
	$arSupSort[] = 'name';
}
if ($rz_b2_options['block_list-sort-new'] == 'Y') {
	$arSupSort[] = 'created';
}
if ($rz_b2_options['block_list-sort-views'] == 'Y') {
	$arSupSort[] = 'shows';
}
if ($rz_b2_options['block_list-sort-rating'] == 'Y') {
	$arSupSort[] = 'property_rating';
}
if ($rz_b2_options['block_list-sort-price'] == 'Y') {
	$arSupSort[] = 'price';
}
foreach ($arSort as $key => $sSort) {
	if(!in_array($sSort, $arSupSort)) {
		unset($arSort[$key]);
	}
}
if (!in_array($sort['ACTIVE'], $arSupSort)) {
	$sort['ACTIVE'] = $arSupSort[array_rand($arSupSort)];
}
if (empty($sort)) {
	$sort['ACTIVE'] = 'name';
}
// pages
foreach ($_REQUEST as $k => $v)
{
	if (strpos($k, 'PAGEN_') === 0)
	{
		if ($_REQUEST[$k] > 0)
		{
			$pagen_key = $k;

			if (strpos($_REQUEST[$k], '?') !== false)
			{
				$tmp = explode('?', $_REQUEST[$k]);
				$pagen = htmlspecialchars($tmp[0]);
			}
			else
			{
				$pagen = htmlspecialchars($_REQUEST[$k]);
			}
			
			break;
		}
	}
}

$arAjaxParams = array(
	"view" => $view,
	"page_count" => $page_count,
	"sort" => $sort['ACTIVE'],
	"by" => $by,
);
if(isset($pagen_key, $pagen))
{
	$arAjaxParams[$pagen_key] = $pagen;
}
if (array_key_exists('rz_all_elements', $_REQUEST)) {
	$arAjaxParams['rz_all_elements'] = $_REQUEST['rz_all_elements'];
}
if($_REQUEST["rz_ajax"] !== "y")
{	//FOR AJAX
	?><script type="text/javascript">$.extend(RZB2.ajax.params, <?=CUtil::PhpToJSObject($arAjaxParams, false, true)?>);</script><?
}
?>