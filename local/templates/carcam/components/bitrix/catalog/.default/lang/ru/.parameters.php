<?
$MESS["SORT_PROPERTIES"] = "Свойства для сортировки";

// COMMON
$MESS["ARTICUL_PROP"] = "Свойство-артикул";
$MESS["DISPLAY_FAVORITE"] = "Отображать кнопки добавления в избранное";
$MESS["DISPLAY_ONECLICK"] = "Отображать кнопки для покупки в 1 клик в списках";
$MESS["DISPLAY_ONECLICK_TIP"] = "Кнопки появятся в разделе каталога вида блоки, а также на детальной странице в списках рекомендуемых, похожих и просматриваемых товаров";

$MESS["CP_BC_TPL_SHOW_OLD_PRICE"] = "Показывать старую цену";

// Resizer
$MESS["RESIZER_SETS"] = "Настройка наборов Ресайзера";
$MESS["RESIZER_SECTION_LVL0"] = "Набор для списка категорий";
$MESS["RESIZER_SECTION"] = "Набор для списка товаров";
$MESS["RESIZER_SECTION_ICON"] = "Набор для миниатюр в списке товаров";
$MESS["RESIZER_SUBSECTION"] = "Набор для иконок подразделов";
$MESS['RESIZER_DETAIL_SMALL'] = 'Среднее изображение для детальной страницы ';
$MESS['RESIZER_DETAIL_BIG'] = 'Увеличенное изображение для детальной страницы ';
$MESS['RESIZER_DETAIL_ICON'] = 'Изображения-иконки для детальной страницы';
$MESS['RESIZER_DETAIL_FLY_BLOCK'] = 'Изображения в летающем блоке справа для детальной страницы';
$MESS['RESIZER_COMMENT_AVATAR'] = 'Фото автора комментария для детальной страницы';
$MESS['RESIZER_SET_CONTRUCTOR'] = 'Для наборов на детальной странице';
$MESS['RESIZER_RECOMENDED'] = 'Рекомендуемые товары на детальной странице';
$MESS['RESIZER_FILTER'] = 'Для картинок в фильтре';

//  Section
$MESS['BITRONIC2_PRICE_SORT'] = 'Тип цены, по которому сортировать список товаров';
$MESS['BITRONIC2_DEFAULT_ELEMENT_SORT_BY'] = 'Сортировка списка товаров по умолчанию' ;
$MESS['BITRONIC2_DEFAULT_ELEMENT_SORT_ORDER'] = 'Порядок сортировки списка товаров по умолчанию' ;
$MESS['BITRONIC2_ELEMENT_SORT_ASC'] 	= 'по возрастанию' ;
$MESS['BITRONIC2_ELEMENT_SORT_DESC'] 	= 'по убыванию' ;
$MESS['BITRONIC2_ELEMENT_SORT_HIT']   = 'по популярности (количество просмотров)';
$MESS['BITRONIC2_ELEMENT_SORT_PRICE'] = 'по цене' ;
$MESS['BITRONIC2_ELEMENT_SORT_NAME']  = 'по названию' ;
$MESS['BITRONIC2_ELEMENT_SORT_SALE_INT'] = 'по автоматически подсчитанному количеству продаж [SALE_INT]';
$MESS['BITRONIC2_ELEMENT_SORT_SALE_EXT'] = 'по выгруженному количеству продаж [SALE_EXT]';
$MESS['BITRONIC2_ELEMENT_SORT_SORT'] = 'по сортировке';
$MESS['BITRONIC2_ELEMENT_SORT_RATING'] = 'по рейтингу';
$MESS['BITRONIC2_HIDE_SHOW_ALL_BUTTON'] = 'Скрыть кнопку "Все" в блоке "Показывать по"';
$MESS['BITRONIC2_HIDE_ICON_SLIDER'] = 'Скрыть слайдер с миниатюрными изображениями';
$MESS['BITRONIC2_HIDE_STORE_LIST'] = 'Скрыть список складов в виде "Список"';
$MESS['SHOW_DESCRIPTION_TOP'] = 'Выводить описание раздела над списком товаров';
$MESS['SHOW_DESCRIPTION_BOTTOM'] = 'Выводить описание раздела после списка товаров';

$MESS['SECTION_SHOW_HITS'] = 'Показывать блок хитовых товаров';
$MESS['SECTION_HITS_RCM_TYPE'] = 'Тип рекомендаций для блока хитовых товаров';
$MESS['SECTION_HITS_RCM_TYPE_BESTSELL'] = 'Самые продаваемые';
$MESS['SECTION_HITS_RCM_TYPE_PERSONAL'] = 'Персональные рекомендации';
$MESS['SECTION_HITS_HIDE_NOT_AVAILABLE'] = 'Скрыть отсутствующие товары в блоке хитовых товаров';

//	Detail:
$MESS['SETTINGS_HIDE'] = 'Не выводить указанные свойства';
$MESS['SETTINGS_HIDE_TIP'] = 'Скрывает указанные свойства на детальной странице товаров и в таблице сравнения';

$MESS["CP_BCE_TPL_USE_COMMENTS"] = "Включить отзывы о товаре";
$MESS["CP_BCE_TPL_BLOG_USE"] = "Использовать комментарии";
$MESS["CP_BCE_TPL_BLOG_URL"] = "Название блога латинскими буквами";
$MESS["CP_BCE_TPL_BLOG_EMAIL_NOTIFY"] = "Уведомление по электронной почте";
$MESS["CP_BCE_TPL_FEEDBACK_USE"] = "Использовать гостевую книгу";
$MESS["CP_BCE_TPL_FEEDBACK_IBLOCK_TYPE"] = "Тип инфоблока гостевой книги";
$MESS["CP_BCE_TPL_FEEDBACK_IBLOCK_ID"] = "Инфоблок гостевой книги";
$MESS["CP_BCE_TPL_VK_USE"] = "Использовать Вконтакте";
$MESS["CP_BCE_TPL_VK_API_ID"] = "Идентификатор приложения Вконтакте (API_ID)";
$MESS["CP_BCE_TPL_FB_USE"] = "Использовать Facebook";
$MESS["CP_BCE_TPL_FB_APP_ID"] = "Идентификатор приложения (APP_ID)";

$MESS["CP_BC_TPL_PROP_EMPTY"] = "не выбрано";
$MESS["CP_BC_TPL_LIST_BRAND_USE"] = "Отображать \"Бренды\" отдельным блоком вне фильтра";
$MESS["CP_BC_TPL_LIST_PROP_CODE"] = "Таблица с брендами";

$MESS["DETAIL_HIDE_ACCESSORIES"] = "Скрыть блок 'Не забудьте добавить к заказу'";
$MESS["DETAIL_HIDE_SIMILAR"] = "Скрыть блок 'Просматриваемые с этим товаром'";
$MESS["DETAIL_HIDE_SIMILAR_VIEW"] = "Скрыть блок 'Похожие товары'";

// EDOST:
$MESS["EDOST_SORT"] = "eDost: упорядочить тарифы по возрастанию";
$MESS["EDOST_SHOW_QTY"] = "eDost: ячейка для ввода количества товара";
$MESS["EDOST_SHOW_ADD_CART"] = 'eDost: галочка "Учитывать товары в корзине"';
$MESS["EDOST_MINIMIZE"] = "eDost: минимум информации";
$MESS["EDOST_MINIMIZE_TIP"] = "маленькие иконки + скрыть описание тарифа";
$MESS["EDOST_ECONOMIZE"] = "eDost: экономный расчет";
$MESS["EDOST_ECONOMIZE_TIP"] = "округление в большую сторону веса и стоимости товара и отключение учета габаритов - значительно снижает количество запросов на сервер расчетов";

//REVIEWS:
$MESS['USE_OWN_REVIEW'] = 'Использовать внутренние отзывы сайта';
$MESS['REVIEWS_MODE'] = 'Режим отзывов сайта';
$MESS['REVIEWS_FORUM'] = 'На модуле "Форум"';
$MESS['REVIEWS_BLOG'] = 'На модуле "Блог"';
$MESS['DETAIL_YM_API_USE'] = 'Отзывы с Я.М. API';

//FILTER:
$MESS["FILTER_VISIBLE_PROPS_COUNT"] = "Сколько свойств не скрывать в подробный фильтр";

//STICKERS:
$MESS['STICKER_GROUP'] = 'Настройка умных стикеров';
$MESS["STICKER_NEW"] = "Сколько дней считать товар новинкой";
$MESS["STICKER_HIT"] = "При каком количестве просмотров за неделю, считать товар хитом";
$MESS['STICKER_BESTSELLER'] = 'При каком количестве продаж считать товар лидером';

//SKU:
$MESS["CP_BCS_TPL_PROP_EMPTY"] = "не выбрано";
$MESS["CP_BCS_TPL_OFFER_ADD_PICT_PROP"] = "Дополнительные картинки предложения";
$MESS["CP_BCS_TPL_OFFER_TREE_PROPS"] = "Свойства для отбора предложений";
$MESS["OFFER_VAR_NAME"] = "Название параметра для идентификации SKU на детальной";
$MESS["MANUAL_PROP_NAME"] = "Символьный код свойства для отображения документов";
$MESS["OFFER_VAR_NAME_TIP"] = "Название GET параметра, в котором передается номер торгового предложения для переключения торговых предложений(SKU) на детальной странице товара";
$MESS["ADD_PARENT_PHOTO"] = "Показывать фото родителького товара";
$MESS["ADD_PARENT_PHOTO_TIP"] = "При включенной опции в галлерее картинок товара помимо собственных фотографий товара будут отображаться фотографии родительского товара (в конце списка)";

//STORES:
$MESS["SHOW_AMOUNT_STORE"] = "Показывать наличие на складах";
$MESS["SHOW_AMOUNT_STORE_TIP"] = "Для товаров с торговыми предложениями блок с наличием на складах не отображается в списке товаров";

//META
$MESS ['BITRONIC2_META_H1'] = "Шаблон для генерации заголовка страницы (H1)";
$MESS ['BITRONIC2_META_H1_FORCE'] = "Установить заголовок страницы (H1) из свойства (если заполнено)";
$MESS ['BITRONIC2_META_TITLE'] = "Шаблон для генерации заголовка браузера (title)";
$MESS ['BITRONIC2_META_TITLE_FORCE'] = "Установить заголовок браузера (title) из свойства (если заполнено)";
$MESS ['BITRONIC2_META_KEYWORDS'] = "Шаблон для генерации Keywords";
$MESS ['BITRONIC2_META_KEYWORDS_FORCE'] = "Установить Keywords из свойства (если заполнено)";
$MESS ['BITRONIC2_META_DESCRIPTION'] = "Шаблон для генерации Description";
$MESS ['BITRONIC2_META_DESCRIPTION_FORCE'] = "Установить Description из свойства (если заполнено)";
$MESS ['BITRONIC2_META_SPLITTER'] = "Разделитель для свойств имеющих множественное значение";
$MESS ['BITRONIC2_META_TITLE_PROP_BUY'] = 'Купить ' ;
$MESS ['BITRONIC2_META_COMPARE_SPLITTER'] = "Разделитель для перечисления товаров";
$MESS ['BITRONIC2_META_COMPARE_COMPARE'] = "Сравнение #TEXT#";
$MESS ['BITRONIC2_META_COMPARE_THAT_BETTER'] = "Что лучше: #TEXT# ?";
$MESS ['BITRONIC2_META_COMPARE_THAT_BETTER_BUY'] = "Что лучше купить: #TEXT# ?";

$MESS["BITRONIC2_FOUND_CHEAP_USE"] = "Использовать \"Нашли дешевле\"";
$MESS["BITRONIC2_FOUND_CHEAP_TYPE"] = "Тип инфоблока \"Нашли дешевле\"";
$MESS["BITRONIC2_FOUND_CHEAP_IBLOCK"] = "Инфоблок \"Нашли дешевле\"";

$MESS["BITRONIC2_PRICE_LOWER_USE"] = "Использовать \"Сообщить о снижении цены\"";
$MESS["BITRONIC2_PRICE_LOWER_TYPE"] = "Тип инфоблока \"Сообщить о снижении цены\"";
$MESS["BITRONIC2_PRICE_LOWER_IBLOCK"] = "Инфоблок \"Сообщить о снижении цены\"";

$MESS["BITRONIC2_ELEMENT_EXIST_USE"] = "Использовать \"Сообщить о поступлении\"";
$MESS["BITRONIC2_ELEMENT_EXIST_TYPE"] = "Тип инфоблока \"Сообщить о поступлении\"";
$MESS["BITRONIC2_ELEMENT_EXIST_IBLOCK"] = "Инфоблок \"Сообщить о поступлении\"";
