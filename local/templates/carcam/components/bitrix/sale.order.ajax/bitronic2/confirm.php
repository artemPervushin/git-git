<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
// TODO stylized confirm order
if (!empty($arResult["ORDER"]))
{
	// ADMITAD START
	CModule::IncludeModule('sale');
	$res = CSaleBasket::GetList(array(), array("ORDER_ID" => $arResult["ORDER"]["ID"])); // ID заказа
    $key2 = 0;
	while ($arItem = $res->Fetch()) {
		$order_list[$key2]["PRICE"] = $arItem["PRICE"];
		$order_list[$key2]["QUANTITY"] = intval($arItem["QUANTITY"]);
		$order_list[$key2]["ID"] = $arItem["PRODUCT_ID"];
		$order_list[$key2]["NAME"] = $arItem["NAME"];
		$key2++;
	}

	if(!empty($_COOKIE['ADMITAD'])){?>
		<script type="text/javascript">
			(function (d, w) {
				w._admitadPixel = {
					response_type: 'img', // 'script' or 'img'. Default: 'img'
					action_code: '1',
					campaign_code: 'ee6fec0cac'
				};
				w._admitadPositions = w._admitadPositions || [];
				
				<?foreach($order_list as $key => $tagitemad){
					$key++;?>
					w._admitadPositions.push({
						uid: '<?=$_COOKIE['ADMITAD']?>',
						tariff_code: '1',
						order_id: '<?=$arResult["ORDER"]["ID"]?>',
						position_id: '<?=$key?>',
						currency_code: 'RUB',
						position_count: '<?=count($order_list)?>',
						price: '<?=$tagitemad["PRICE"]?>',
						quantity: '<?=$tagitemad["QUANTITY"]?>',
						product_id: '<?=$tagitemad["ID"]?>',
						payment_type: 'sale'
					});
				<?}?>
				
				var id = '_admitad-pixel';
				if (d.getElementById(id)) { return; }
				var s = d.createElement('script');
				s.id = id;
				var r = (new Date).getTime();
				var protocol = (d.location.protocol === 'https:' ? 'https:' : 'http:');
				s.src = protocol + '//cdn.asbmit.com/static/js/pixel.js?r=' + r;
				var head = d.getElementsByTagName('head')[0];
				head.appendChild(s);
			})(document, window)
		</script>   
		<noscript>
			<?$key = 0;
			foreach($order_list as $key => $tagitemad){
				$key++;?>
				<img src="//ad.admitad.com/r?campaign_code=ee6fec0cac&action_code=1&payment_type=sale&response_type=img&uid=<?=$_COOKIE['ADMITAD']?>&tariff_code=1&order_id=<?=$arResult["ORDER"]["ID"]?>&position_id=<?=$key?>&currency_code=RUB&position_count=<?=count($order_list)?>&price=<?=$tagitemad["PRICE"]?>&quantity=<?=$tagitemad["QUANTITY"]?>&product_id=<?=$tagitemad["ID"]?>" width="1" height="1" alt="">
			<?}?>
		</noscript>		
	<?}

	?>
	<script type="text/javascript">
	    window.ad_order = "<?=$arResult["ORDER"]["ID"]?>";    // required
	    window.ad_amount = "<?=ceil($arResult["ORDER"]["PRICE"])?>";
	    window.ad_products = [
		<?foreach($order_list as $key => $tagitemad){?>    
		    {
		        "id": "<?=$tagitemad["ID"]?>",
		        "number": "<?=$tagitemad["QUANTITY"]?>"
		    },
	    <?}?>
	    ];

	    window._retag = window._retag || [];
	    window._retag.push({code: "9ce88869ec", level: 4});
	    (function () {
	        var id = "admitad-retag";
	        if (document.getElementById(id)) {return;}
	        var s = document.createElement("script");
	        s.async = true; s.id = id;
	        var r = (new Date).getDate();
	        s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static/js/retag.min.js?r="+r;
	        var a = document.getElementsByTagName("script")[0]
	       	a.parentNode.insertBefore(s, a);
	    })()
	</script>
	<?
	//ADMITAD END


	$arUser = CUser::GetByID($USER->GetID())->Fetch();
	?>
	<b><?=GetMessage("BITRONIC2_SOA_TEMPL_ORDER_COMPLETE")?></b><br /><br />
	<table class="sale_order_full_table">
		<tr>
			<td>
				<?= GetMessage("BITRONIC2_SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => '<a href="'.$arParams["PATH_TO_PERSONAL"].'?ID='.$arResult["ORDER"]["ID"].'" class="link"><span class="text">'.$arResult["ORDER"]["ACCOUNT_NUMBER"].'</span></a>'))?>
				<br /><br />
				<?= GetMessage("BITRONIC2_SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>
				<?if (time()-300 < MakeTimeStamp($arUser['DATE_REGISTER'])):?>
				<br /><br />
				<?= GetMessage("BITRONIC2_SOA_TEMPL_ORDER_SUC2", Array("#LINK#" => $arParams["PATH_TO_SETTINGS"]))?>
				<?endif?>
			</td>
		</tr>
	</table>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
		?>
		<br /><br />

		<table class="sale_order_full_table">
			<tr>
				<td class="ps_logo">
					<div class="pay_name"><?=GetMessage("BITRONIC2_SOA_TEMPL_PAY")?></div>
					<?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>
					<div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div><br>
				</td>
			</tr>
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
				?>
				<tr>
					<td>
						<?
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
							</script>
							<?= GetMessage("BITRONIC2_SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
							<?
							if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
							{
								?><br />
								<?= GetMessage("BITRONIC2_SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
								<?
							}
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{
								try
								{
									include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
								}
								catch(\Bitrix\Main\SystemException $e)
								{
									if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
										$message = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
									else
										$message = $e->getMessage();

									echo '<span style="color:red;">'.$message.'</span>';
								}
							}
						}
						?>
					</td>
				</tr>
				<?
			}
			?>
		</table>
		<?
	}
}
else
{
	?>
	<b><?=GetMessage("BITRONIC2_SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("BITRONIC2_SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("BITRONIC2_SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
