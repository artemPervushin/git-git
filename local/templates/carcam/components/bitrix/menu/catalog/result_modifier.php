<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;

$arMenuItemsIDs = array();
$arAllItems = array();
foreach($arResult as $key=>$arItem)
{
	if($arItem["DEPTH_LEVEL"] > $arParams["MAX_LEVEL"])
	{
		unset($arResult[$key]);
		continue;
	}

	if ($arItem["DEPTH_LEVEL"] == "1")
	{
		$arMenuItemsIDs[$arItem["PARAMS"]["ITEM_IBLOCK_ID"]] = array();
		if ($arItem["IS_PARENT"])
		{
			$curItemLevel_1 = $arItem["PARAMS"]["ITEM_IBLOCK_ID"];
		}
		$arAllItems[$arItem["PARAMS"]["ITEM_IBLOCK_ID"]] = $arItem;
	}
	elseif($arItem["DEPTH_LEVEL"] == "2")
	{
		$arMenuItemsIDs[$curItemLevel_1][$arItem["PARAMS"]["ITEM_IBLOCK_ID"]] = array();
		if ($arItem["IS_PARENT"])
		{
			$curItemLevel_2 = $arItem["PARAMS"]["ITEM_IBLOCK_ID"];
		}
		$arAllItems[$arItem["PARAMS"]["ITEM_IBLOCK_ID"]] = $arItem;
	}
	elseif($arItem["DEPTH_LEVEL"] == "3")
	{
		$arMenuItemsIDs[$curItemLevel_1][$curItemLevel_2][] = $arItem["PARAMS"]["ITEM_IBLOCK_ID"];
		$arAllItems[$arItem["PARAMS"]["ITEM_IBLOCK_ID"]] = $arItem;
	}
}
// echo "<pre style='text-align:left;'>";print_r($arAllItems);echo "</pre>";
$arResult = array();
$arResult["ALL_ITEMS"] = $arAllItems;
$arResult["ALL_ITEMS_ID"] = $arMenuItemsIDs;