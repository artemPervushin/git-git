<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
?>
<h3><?=(!empty($arParams['TITLE'])) ? $arParams['TITLE'] : GetMessage('BITRONIC2_CAT_BOT_MENU_TITLE')?></h3>
<div class="catalog-menu-footer">
	<?php foreach($arResult as $key=>$arItem):
		if($arItem['DEPTH_LEVEL'] > 1) continue;
		if($arItem['SELECTED']) $class .= ' active';?>
		<div class="footer-menu-item">
			<a href="<?=$arItem['LINK']?>" class="link <?=$class?>">
				<span class="text"><?=$arItem['TEXT']?></span>
			</a>
		</div>
	<?endforeach?>
	<?php
		$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_DIR."include_areas/footer/pricelist.php",
				"AREA_FILE_RECURSIVE" => "N",
				"EDIT_MODE" => "html",
			),
			false,
			Array('HIDE_ICONS' => 'Y')
		);
	?>
</div>