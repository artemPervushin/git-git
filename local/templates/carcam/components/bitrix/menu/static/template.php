<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
$curPage = $APPLICATION->GetCurPage();
?>
<ul class="sitenav-menu">
	<?php foreach($arResult as $key=>$arItem):?><?
		$class = '';
		$bParent = false;
		if(!empty($arItem['ADDITIONAL_LINKS'])) {
			$class .= ' with-sub';
			$bParent = true;
		}
		if($arItem['SELECTED']) $class .= ' active';
		
	?><li class="sitenav-menu-item<?=$class?>">
		<a href="<?=$arItem['LINK']?>" <?=$arItem['PARAMS']['target']?>class="<?=$class?>">
			<span class="link-text"><?=$arItem['TEXT']?></span>
		</a>
	</li><?endforeach?>
</ul>