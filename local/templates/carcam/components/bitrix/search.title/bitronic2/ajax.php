<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult["CATEGORIES"]))
	return;

\Bitrix\Main\Localization\Loc::loadMessages(SITE_TEMPLATE_PATH . '/header.php');
?>
	<table>
	<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
		<?if(is_array($arCategory['ITEMS'])) 
		foreach($arCategory["ITEMS"] as $i => $arElement):?>
			<?if(isset($arResult["ELEMENTS"][$arElement["ITEM_ID"]])):
				$arItem = $arResult["ELEMENTS"][$arElement["ITEM_ID"]];
				$availableClass = !$arItem["CAN_BUY"] ? ' out-of-stock' : ($arItem['FOR_ORDER'] ? ' available-for-order' : ' in-stock');
				$availableClass = !$arItem['ON_REQUEST'] ? $availableClass : ' available-for-order';
				$availableClass = !$arItem["bOffers"]    ? $availableClass : '';
				?>
				<tr class="ajax-search-item category_<?=$category_id?><?=$availableClass?>">
					<td class="item-photo">
						<a href="<?= $arElement["URL"]?>"><img src="<?= $arElement["PICTURE"]?>" alt="<?= strip_tags($arElement["NAME"])?>"></a>
					</td>
					<td class="item-name">
						<a href="<?= $arElement["URL"]?>"><?= $arElement["NAME"]?></a>
					</td>
					<td class="item-price">
						<?if(!$arItem["bOffers"] && !$arItem['ON_REQUEST']):?>
							<?=CRZBitronic2CatalogUtils::getElementPriceFormat($arItem['MIN_PRICE']['CURRENCY'], $arItem['MIN_PRICE']['DISCOUNT_VALUE'], $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']);?>
						<?endif;?>
					</td>
					<td class="item-availability">
						<?if(!$arItem["bOffers"]):?>
							<span class="avail-dot when-in-stock"></span>
							<span class="avail-dot when-out-of-stock"></span>
							<span class="avail-dot when-available-for-order"></span>
						<?endif;?>
					</td>
					<td class="item-actions">
						<?if($arItem["bOffers"]):?>
							<a href="<?=$arElement['URL']?>" class="btn-buy btn-main small">
								<i class="flaticon-shopping109"></i>
								<span class="text"><?=GetMessage('BITRONIC2_SEARCH_TO_DETAIL')?></span>
							</a>
						<?else:?>
							<button class="btn-buy <?=($arItem['ON_REQUEST']?'request':'buy')?> btn-main small" data-product-id="<?=$arItem["ID"]?>"<?if($arItem['ON_REQUEST']):?>
								data-toggle="modal" data-target="#modal_contact_product" data-measure-name="<?=$arItem['CATALOG_MEASURE_NAME']?>"<?endif?>>
								<i class="flaticon-shopping109"></i>
								<span class="text"><?=GetMessage('BITRONIC2_SEARCH_ADD_TO_BASKET')?></span>
								<span class="text in-cart"><?=GetMessage("BITRONIC2_PRODUCT_IN_CART")?></span>
								<span class="text request"><?=GetMessage("BITRONIC2_PRODUCT_REQUEST")?></span>
							</button>
						<?endif;?>
						<? 
						/* TODO
						include '_/buttons/btn-action_to-wait.html';
						*/
						?>
					</td>
				</tr>
			<?elseif(isset($arElement["ITEM_ID"])):?>
				<tr class="ajax-search-item category_<?=$category_id?>">
					<td class="item-photo">
					<?if (!empty($arElement['PICTURE'])):?>
						<a href="<?= $arElement["URL"]?>"><img src="<?= $arElement["PICTURE"]?>" alt="<?= $arElement["NAME"]?>"></a>
					<?endif?>
					</td>
					<td class="item-name" colspan="4">
						<a href="<?= $arElement["URL"]?>"><?= $arElement["NAME"]?></a>
					</td>
				</tr>
			<?endif;?>
		<?endforeach;?>
	<?endforeach;?>
	</table>
	<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
		<div class="popup-ajax-footer category_<?=$category_id?>">
			<?if(is_array($arCategory['ITEMS']))
			foreach($arCategory["ITEMS"] as $i => $arElement):?>
				<?if(!isset($arElement["ITEM_ID"])):?>
					<a href="<?= $arElement["URL"]?>" class="btn-show-all"><?= $arElement["NAME"]?></a>
				<?endif?>
			<?endforeach;?>
		</div>
	<?endforeach;?>

<?
// echo "<pre style='text-align:left;'>";print_r($arResult);echo "</pre>";

