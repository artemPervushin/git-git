<?
$MESS["BITRONIC2_ACCESORIES_TITLE"] = "Не забудьте добавить к заказу";
$MESS["BITRONIC2_ACCESORIES_ALL"] = "Все аксессуары к этому товару";
$MESS["BITRONIC2_ACCESORIES_ADD_TO_ORDER"] = "Добавить к заказу";
$MESS["BITRONIC2_ACCESORIES_ADD_TO_COMPARE"] = "Добавить в сравнение";
$MESS["BITRONIC2_ACCESORIES_ADD_TO_FAVORITE"] = "Добавить в избранное";
$MESS["BITRONIC2_ACCESORIES_FROM"] = "от";
$MESS["BITRONIC2_ACCESORIES_TO_DETAIL"] = "Подробнее";
$MESS["BITRONIC2_ACCESORIES_ADD_TO_BASKET"] = "В корзину";
$MESS["BITRONIC2_ACCESORIES_ONECLICK"] = "Купить в 1 клик";
$MESS["BITRONIC2_BIGDATA_NOTAVAIL"] = "Нет в наличии";
$MESS["BITRONIC2_PRODUCT_IN_CART"] = "В корзине";

$MESS["BITRONIC2_HITS_SHOW"] = "Показать хиты";
$MESS["BITRONIC2_HITS_HIDE"] = "Не показывать хиты";
?>