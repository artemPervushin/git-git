function JCSmartFilter(ajaxURL, form_id, manual) {
	this.ajaxURL = ajaxURL;
	this.form = BX(form_id);
	this.timer = null;
	this.isFlying = false;
	this.manual = manual;
	this.$form = $(this.form);

	// save set filter params for catalog AJAX
	var values = [];
	this.gatherInputsValues(values, BX.findChildren(this.form, {'tag': 'input'}, true));
	values = this.values2post(values);
	if (typeof values.set_filter != "undefined" && typeof values.del_filter == "undefined") {
		RZB2.ajax.CatalogSection.filterParams = values;
	}

	// clear Button
	this.$form.on('click', '#del_filter', function (e) {
		e.preventDefault();
		var $this = $(this);
		smartFilter.form = BX.findParent(e.target, {'tag': 'form'});

		// hide filter results
		$('#modef, #flying-results-wrap').hide();

		// clear sliders
		$(smartFilter.form).find('[id^=slider_]').each(function () {
			var min = $(this).siblings('.range-slider-inputs').find('input.range-input-lower').data('range-min');
			var max = $(this).siblings('.range-slider-inputs').find('input.range-input-upper').data('range-max');
			$(this).val([min, max]);
		});
		// clear slider input
		$(smartFilter.form).find('input[type=text]:enabled').val('');
		// clear checkbox
		$(smartFilter.form).find('input[type=checkbox]').prop('checked', false).prop('disabled', false).parent().removeClass('disabled');
		// clear brands
		$('.brands-catalog .brand').removeClass('active disabled');

		// clear filter params
		RZB2.ajax.CatalogSection.filterParams = {};
		$this.addClass('disabled');
		YSFilterRemoveDisable = false;
		var sefDel = $this.data('sef-del');
		if (sefDel.length > 0) {
			RZB2.ajax.CatalogSection.SefSetUrl = sefDel;
			RZB2.ajax.params.REQUEST_URI = sefDel;
		}
		// start ajax
		RZB2.ajax.CatalogSection.Start();
	});

	// full-filter Button
	this.$form.on('click', '.btn-toggle-full-filter', function (e) {
		var _ = $(this);
		_.toggleClass('toggled');
		var filterFull = _.closest('.form_filter').find('.filter-full');
		var filterShort = _.closest('.form_filter').find('.filter-short');
		if (filterFull.hasClass('filter-opened')) {
			filterFull.removeClass('filter-opened');
			filterShort.removeClass('filter-opened');
			filterFull.velocity('slideUp');
		} else {
			filterFull.velocity('slideDown', {
				complete: function () {
					filterFull.addClass('filter-opened');
					filterShort.addClass('filter-opened');
				}
			});
		}

		return false;
	})
	.on('submit', function (e) {
		smartFilter.reload($(this).children('input:first'));
		return false;
	});
	if(this.manual) {
		$(document).on('click', '#flying-results-wrap .btn-show-results, #' + form_id + ' .show-results', function (e) {
			e.preventDefault();
			RZB2.ajax.CatalogSection.Start();
		});
	}
}

var YSFilterRemoveDisable = false;

JCSmartFilter.prototype.keyup = function (input) {
	if (this.timer)
		clearTimeout(this.timer);
	this.timer = setTimeout(BX.delegate(function () {
		YSFilterRemoveDisable = true;
		this.reload(input);
	}, this), 1000);
};

JCSmartFilter.prototype.click = function (checkbox) {
	if (this.timer)
		clearTimeout(this.timer);
	this.timer = setTimeout(BX.delegate(function () {
		YSFilterRemoveDisable = true;
		this.reload(checkbox);
	}, this), 1000);
};

JCSmartFilter.prototype.reload = function (input) {
	this.position = BX.pos(BX.findParent(input, {}), true);
	this.form = BX.findParent(input, {'tag': 'form'});
	if (this.form) {
		var values = [];
		values[0] = {name: 'ajax', value: 'y'};
		this.gatherInputsValues(values, BX.findChildren(this.form, {'tag': 'input'}, true));

		this.updateFilterTagsList(values);

		this.values = this.values2post(values);
		// update filter block
		BX.ajax.loadJSON(
			this.ajaxURL,
			this.values,
			BX.delegate(this.postHandler, this)
		);
	}
};

JCSmartFilter.prototype.postHandler = function (result) {
	if (!!result && !!result.ITEMS) {
		for (var PID in result.ITEMS) {
			var arItem = result.ITEMS[PID];
			if (arItem.PROPERTY_TYPE == 'N' || arItem.PRICE) {
			} else if (arItem.VALUES) {
				for (var i in arItem.VALUES) {
					var ar = arItem.VALUES[i];
					var control = BX(ar.CONTROL_ID);
					if (control) {
						var $brand;
						if (arItem.CODE == this.brandPropCode) {
							$brand = $('div[data-checkbox="' + ar.CONTROL_ID + '"]');
						}
						control.parentNode.className = 'checkbox-styled ' + (ar.DISABLED && !control.checked ? 'disabled' : '');
						if (ar.DISABLED && !control.checked) {
							control.setAttribute("disabled", "disabled");
							if ($brand) $brand.addClass('disabled');
						} else {
							control.removeAttribute("disabled");
							if ($brand) $brand.removeClass('disabled');
						}
						$brand = undefined;
					}
				}
			}
		}
		if (!YSFilterRemoveDisable)
			return;

		var modef = BX('modef');
		var modef_num = BX('modef_num');
		var modef_flight = $('#flying-results-wrap');
		var modef_flight_num = BX('modef_flight_num');
		var _ = this;

		if (modef && modef_num) {
			modef_num.innerHTML = result.ELEMENT_COUNT;

			if (modef.style.display == 'none')
				modef.style.display = 'block';
		}
		if (modef_flight.length && modef_flight_num) {
			modef_flight_num.innerHTML = result.ELEMENT_COUNT;
			modef_flight.css('top', this.position.top - this.form.offsetTop + 5);
			if (!this.isFlying) {
				modef_flight.velocity('fadeIn');
			}
			clearTimeout(this.isFlying);
			this.isFlying = setTimeout(function () {
				modef_flight.velocity('fadeOut');
				_.isFlying = false;
			}, 4000);
		}
	}

	RZB2.ajax.CatalogSection.filterParams = this.values;
	if('SEF_SET_FILTER_URL' in result && result['SEF_SET_FILTER_URL'].length > 0) {
		RZB2.ajax.CatalogSection.SefSetUrl = result['SEF_SET_FILTER_URL'];
	}

	//delete service var
	delete RZB2.ajax.CatalogSection.filterParams.ajax;

	RZB2.ajax.CatalogSection.filterParams.set_filter = 'y';
	if (!this.manual) {
		RZB2.ajax.CatalogSection.Start();
	}

	if (YSFilterRemoveDisable) {
		$('#del_filter').removeClass('disabled');
		this.$form.find('footer .show-results').removeClass('disabled');
	}

};

JCSmartFilter.prototype.gatherInputsValues = function (values, elements) {
	if (elements) {
		for (var i = 0; i < elements.length; i++) {
			var el = elements[i];
			if (el.disabled || !el.type)
				continue;

			switch (el.type.toLowerCase()) {
				case 'number':
				case 'text':
				case 'textarea':
				case 'password':
				case 'hidden':
				case 'select-one':
					if (el.value.length) {
						values[values.length] = {name: el.name, value: el.value, id: el.id, type: el.type};
						var $el = $(el);
						if ($el.hasClass('range-input-lower') || $el.hasClass('range-input-upper')) {
							values[values.length-1]['value'] = el.value.replace(/\s+/g, '');
						}
					}
					break;
				case 'radio':
				case 'checkbox':
					if (el.checked)
						values[values.length] = {name: el.name, value: el.value, id: el.id, type: el.type};
					break;
				case 'select-multiple':
					for (var j = 0; j < el.options.length; j++) {
						if (el.options[j].selected)
							values[values.length] = {name: el.name, value: el.options[j].value, id: el.id, type: el.type};
					}
					break;
				default:
					break;
			}
		}
	}
};

JCSmartFilter.prototype.values2post = function (values) {
	var post = [];
	var current = post;
	var i = 0;
	while (i < values.length) {
		var p = values[i].name.indexOf('[');
		if (p == -1) {
			current[values[i].name] = values[i].value;
			current = post;
			i++;
		} else {
			var name = values[i].name.substring(0, p);
			var rest = values[i].name.substring(p + 1);
			if (!current[name])
				current[name] = [];

			var pp = rest.indexOf(']');
			if (pp == -1) {
				//Error - not balanced brackets
				current = post;
				i++;
			}
			else if (pp == 0) {
				//No index specified - so take the next integer
				current = current[name];
				values[i].name = '' + current.length;
			}
			else {
				//Now index name becomes and name and we go deeper into the array
				current = current[name];
				values[i].name = rest.substring(0, pp) + rest.substring(pp + 1);
			}
		}
	}
	return post;
};
JCSmartFilter.prototype.updateFilterTagsList = function (values) {
	var checkboxValues = [];
	var checkboxIdList = [];
	var obTagsList = $('.tags-list');
	for (var key in values) {
		if (typeof values[key].id != 'undefined' && typeof values[key].type != 'undefined' && values[key].type == 'checkbox') {
			checkboxValues[checkboxValues.length] = values[key];
			checkboxIdList[checkboxIdList.length] = values[key].id;
		}
	}

	// delete tag in tags-list
	obTagsList.find('.tag button').each(function () {
		if ($.inArray($(this).attr('data-input-id'), checkboxIdList) < 0) {
			$(this).parent('.tag').remove();
		}
	});
	if (!obTagsList.find('.tag button').length) {
		obTagsList.parent('.tags').addClass('hide');
	}

	// add tag in tags-list
	for (var key in checkboxValues) {
		if (!$('.tags-list .tag button[data-input-id=' + checkboxValues[key].id + ']').length) {
			var html = '<div class="tag">' +
				'<span class="tag-text">' + $('#catalog-filter-form #' + checkboxValues[key].id).siblings('span').text() + '</span>' +
				'<button class="btn-closebtn" data-input-id="' + checkboxValues[key].id + '"><span class="btn-text"></span></button>' +
				'</div> ';
			obTagsList.append(html).parent('.tags').removeClass('hide');
		}
	}
};

function createSlider(params) {
	// PRICE SLIDER STARTS HERE
	var minPriceLimit = Number(params.VALUES.MIN);
	var maxPriceLimit = Number(params.VALUES.MAX);

	var currentMin = Number((params.HTML_VALUES.MIN.length) ? params.HTML_VALUES.MIN : params.VALUES.MIN);
	var currentMax = Number((params.HTML_VALUES.MAX.length) ? params.HTML_VALUES.MAX : params.VALUES.MAX);

	function filterPricePips(value, type) {
		if (value === minPriceLimit || value === maxPriceLimit) {
			return 1;
		}
		return 2;
	}

	var numPipsDef = 5;
	var numPips = ((maxPriceLimit - minPriceLimit) > numPipsDef) ? numPipsDef : (maxPriceLimit - minPriceLimit) + 1;

	var $sliderObj = $('#' + params.SLIDER_ID);
	if (typeof $sliderObj.data('noUiSlider') != "undefined") return;

	$sliderObj.noUiSlider({
		start: [currentMin, currentMax],
		connect: true,
		behaviour: 'snap',
		step: 1,
		range: {
			'min': Math.floor(minPriceLimit),
			'max': Math.ceil(maxPriceLimit)
		},
		format: wNumb({
			decimals: 0,
			thousand: ' '
		})
	}).noUiSlider_pips({
		mode: 'count',
		values: numPips,
		density: 50,
		filter: filterPricePips
	}).data(
		'noUiSlider', 'noUiSlider'
	);
	$sliderObj.Link('lower').to('-inline-<div class="handle-inner"></div>', function (value) {
		// The tooltip HTML is 'this', so additional
		// markup can be inserted here.
		$(this).html(
			'<div class="arrow"></div><div class="stripes"></div> \
			<span class="text">' + value + '</span>'
		);
	});
	$sliderObj.Link('lower').to($('#' + params.INPUT_ID.MIN));

	$sliderObj.Link('upper').to('-inline-<div class="handle-inner"></div>', function (value) {

		// The tooltip HTML is 'this', so additional
		// markup can be inserted here.
		$(this).html(
			'<div class="arrow"></div><div class="stripes"></div> \
			<span class="text">' + value + '</span>'
		);
	});
	$sliderObj.Link('upper').to($('#' + params.INPUT_ID.MAX));

	// this str for not filter by this slider before we change this slider
	if (!params.HTML_VALUES.MIN.length)
		$('#' + params.INPUT_ID.MIN).val('');
	if (!params.HTML_VALUES.MAX.length)
		$('#' + params.INPUT_ID.MAX).val('');

	$sliderObj.on('change', function () {
		smartFilter.keyup(BX(params.INPUT_ID.MIN));
	});
}