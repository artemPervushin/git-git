<?
$MESS ['BITRONIC2_BCSF_FILTER_TITLE'] = "Фильтр";
$MESS ['BITRONIC2_BCSF_FILTER_TITLE_SUB'] = "Подбор по параметрам";
$MESS ['BITRONIC2_BCSF_FILTER_FROM'] = "От";
$MESS ['BITRONIC2_BCSF_FILTER_TO'] = "До";
$MESS ['BITRONIC2_BCSF_SET_FILTER'] = "Показать товары";
$MESS ['BITRONIC2_BCSF_DEL_FILTER'] = "Сбросить все фильтры";
$MESS ['BITRONIC2_BCSF_FILTER_COUNT'] = "Выбрано: #ELEMENT_COUNT#";
$MESS ['BITRONIC2_BCSF_FILTER_SHOW'] = "Расширенный фильтр";
$MESS ['BITRONIC2_BCSF_FILTER_HIDE'] = "Свернуть фильтр";
$MESS ['BITRONIC2_BCSF_YOUR_CHOISE'] = "Вы выбрали:";
$MESS ['BITRONIC2_BCSF_FIND_GOODS'] = "Найдено товаров:";

$MESS ['BITRONIC2_BCSF_'] = "Найдено товаров:";
$MESS ['BITRONIC2_BCSF_AVAILABLE'] = "Наличие";
$MESS ['BITRONIC2_BCSF_AVAILABLE_MAX'] = "В наличии";
$MESS ['BITRONIC2_BCSF_AVAILABLE_MIN'] = "Нет в наличии";
?>