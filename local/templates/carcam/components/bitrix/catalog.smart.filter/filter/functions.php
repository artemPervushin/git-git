<?
if(!function_exists('showFilterItem'))
{
function showFilterItem($arItem, &$arBoolean, $arParams, $obTemplate)
{
	//echo '<pre>', var_export($arItem, 1), '</pre>';
	$bBrandBlock = ('Y' == $arParams['BRAND_HIDE'] && $arItem['CODE'] == $arParams['BRAND_PROP_CODE']);

	if(isset($arItem["PRICE"]))
	{
		$type = "PRICE";
	}
	elseif($arItem["PROPERTY_TYPE"] == "N" )
	{
		$type = "SLIDER";
	}
	else
		$type = "CHECKBOX";
	
	$bExpanded = !array_key_exists('DISPLAY_EXPANDED', $arItem) || ($arItem['DISPLAY_EXPANDED'] == 'Y');
	$bShowImg = in_array($arItem['DISPLAY_TYPE'], array('G','H'));
	$bShowText = in_array($arItem['DISPLAY_TYPE'], array('F','H','K','P','R'));
	switch($type)
	{
		case "CHECKBOX":
			if(!empty($arItem["VALUES"]))
			{
				?><div class="filter-section allow-multiple-expanded <?=$bExpanded ? 'expanded' : ''?>"<?=($bBrandBlock?' style="display:none"':'')?>>
					<header>
						<span class="text"><?=$arItem['NAME']?>:</span>
						<?if(!empty($arItem['FILTER_HINT'])):?> 
						<sup class="help" title="<?=$arItem['FILTER_HINT']?>" data-tooltip>?</sup>
						<?endif?>
					</header>
					<div class="expand-content">
						<?foreach($arItem["VALUES"] as $keyVal => $arValue):
							
							if(!$arBoolean['bActiveFilters'] && $arValue["CHECKED"])
							{
								$arBoolean['bActiveFilters'] = true;
							}?>
							<label class="checkbox-styled">
								<input 
									type="checkbox"
									value="<?=$arValue["HTML_VALUE"]?>"
									name="<?echo $arValue["CONTROL_NAME"]?>"
									id="<?echo $arValue["CONTROL_ID"]?>"
									<?echo $arValue["CHECKED"]? 'checked="checked"': ''?>
									<?echo $arValue["DISABLED"] && !$arValue["CHECKED"]? 'disabled="disabled"': ''?>
									onclick="smartFilter.click(this)"					
								/><?

								if (!$bBrandBlock): ?>

								<span class="checkbox-content">
									<i class="flaticon-check14"></i>
									<?if($bShowImg && isset($arValue["FILE"]['SRC']) && !empty($arValue["FILE"]['SRC'])):?>
										<img 
											<?if(!$bShowText):?>
												data-tooltip title="<?=$arValue["VALUE"];?>"
												data-placement="right"
											<?endif?>
											alt="<?=$arValue["VALUE"];?>" 
											height="20" 
											src="<?=CResizer2Resize::ResizeGD2($arValue["FILE"]['SRC'], $arParams['RESIZER_FILTER'])?>"
										>
									<?endif?>
									<?if($bShowText):?>
										<?=$arValue["VALUE"];?><?/* TODO <sup>0</sup> */?>
									<?endif?>
								</span><?

								endif ?>

							</label>
						<?endforeach;?>	
					</div>
				</div><?
			}
			else
			{
				return false;
			}
		break;
		
		case "SLIDER":
		// break;
		
		case "PRICE":
			if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
				return false;
				
			$sliderId = "slider_".((isset($arItem["PRICE"])) ? 'price_' : '').$arItem['ID'];
			
			$min = (empty($arItem["VALUES"]["MIN"]["HTML_VALUE"])) ? "" : round($arItem["VALUES"]["MIN"]["HTML_VALUE"]);
			$max = (empty($arItem["VALUES"]["MAX"]["HTML_VALUE"])) ? "" : round($arItem["VALUES"]["MAX"]["HTML_VALUE"]);
			if ($min != '' || $max != '') $arBoolean['bActiveFilters'] = true;
			?><div class="filter-section allow-multiple-expanded <?=$bExpanded ? 'expanded' : ''?>">
				<header>
					<span class="text"><?=$arItem["NAME"]?>:</span>
					<?if(!empty($arItem['FILTER_HINT'])):?> 
						<sup class="help" title="<?=$arItem['FILTER_HINT']?>" data-tooltip>?</sup>
					<?endif?>
				</header>
				<div class="expand-content">
					<div class="range-slider price-slider" id="<?=$sliderId?>"></div>
					<div class="range-slider-inputs">
						<label class="filter-input-wrap start">
							<span class="text"><?=GetMessage('BITRONIC2_BCSF_FILTER_FROM')?></span>
							<input 
								type="text" 
								class="range-input-lower textinput"
								name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>" 
								value="<?echo $min?>"
								size="5"
								onkeyup="smartFilter.keyup(this)"
								data-range-min="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"
							>
						</label>
						<label class="filter-input-wrap end">
							<span class="text"><?=GetMessage('BITRONIC2_BCSF_FILTER_TO')?></span>
							<input 
								type="text" 
								class="range-input-upper textinput"
								name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
								value="<?echo $max?>"
								size="5"
								onkeyup="smartFilter.keyup(this)"
								data-range-max="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"
							>
							<?=(!empty($arItem["VALUES"]["MAX"]['CURRENCY']) 
								? $arParams['CONVERT_CURRENCY'] && !empty($arParams['CURRENCY_ID'])
									? CRZBitronic2CatalogUtils::getCurrencyTemplate($arParams['CURRENCY_ID'])
									: CRZBitronic2CatalogUtils::getCurrencyTemplate($arItem["VALUES"]["MAX"]['CURRENCY']) 
								: '')?>
						</label>
					</div><!-- /.slider-inputs -->
					
					<?$arSliderParams = array(
						"SLIDER_ID" => $sliderId,
						"VALUES" => array(
							'MIN'=>$arItem["VALUES"]["MIN"]["VALUE"],
							'MAX'=>$arItem["VALUES"]["MAX"]["VALUE"]
						),
						"HTML_VALUES" => array(
							'MIN'=>$arItem["VALUES"]["MIN"]["HTML_VALUE"],
							'MAX'=>$arItem["VALUES"]["MAX"]["HTML_VALUE"]
						),
						"INPUT_ID" => array(
							'MIN'=>$arItem["VALUES"]["MIN"]["CONTROL_ID"],
							'MAX'=>$arItem["VALUES"]["MAX"]["CONTROL_ID"]
						),
					);?>
					<script type="text/javascript">
						createSlider(<? echo CUtil::PhpToJSObject($arSliderParams, false, true); ?>);
					</script>
				</div>
			</div><?
		break;
	}

	if ($bBrandBlock):
		$obTemplate->setViewTarget('catalog_brands');
		foreach ($arItem['VALUES'] as $key => $arValue):
			$class = 'brand'
			       . ($arValue['CHECKED']  ? ' active'   : '')
			       . ($arValue['DISABLED'] ? ' disabled' : '');
			$file = CFile::ResizeImageGet($arValue["FILE"], array("width"=>120, "height"=>100));

				?><div class="<?=$class?>" data-checkbox="<?=$arValue['CONTROL_ID']?>">
					<span class="brand-img">
						<img src="<?=$file['src']?>" alt="<?=$arValue["VALUE"]?>">
					</span>
					<?//TODO <sup>43</sup>?>

				</div><?
		endforeach;
		$obTemplate->endViewTarget();
		return false;
	endif;
	
	return true;
}
}