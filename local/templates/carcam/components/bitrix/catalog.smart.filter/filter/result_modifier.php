<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if ($arParams['VISIBLE_PROPS_COUNT'] !== 0 && (!isset($arParams['VISIBLE_PROPS_COUNT']) || empty($arParams['VISIBLE_PROPS_COUNT']))) {
	$arParams['VISIBLE_PROPS_COUNT'] = 3;
}

$ob = new \CPHPCache();
$cacheString = md5(SITE_ID);
$cachePath = '/' . SITE_ID . '/CurSiteStores';
if ($ob->InitCache(360000, $cacheString, $cachePath)) {
	$arStores = $ob->GetVars();
} else if ($ob->StartDataCache()) {
	$arStores = array();
	global $CACHE_MANAGER;
	$CACHE_MANAGER->StartTagCache($cachePath);
	$rs = CCatalogStore::GetList(array(),
		array(
			'ACTIVE' => 'Y',
			'SITE_ID' => SITE_ID,
		),
		false, false,
		array('ID')
	);
	while ($ar = $rs->GetNext(false, false)) {
		$CACHE_MANAGER->RegisterTag("store_id_" . $ar['ID']);
		$arStores[md5($ar['ID'])] = 1;
	}
	$CACHE_MANAGER->RegisterTag("store_id_new");
	$CACHE_MANAGER->EndTagCache();
	$ob->EndDataCache($arStores);
}

global $rz_b2_options;
if (!empty($rz_b2_options['GEOIP']['STORES'])) {
	$arGeoStores = array();
	foreach ($rz_b2_options['GEOIP']['STORES'] as $storeId) {
		$hash = md5($storeId);
		if (isset($arStores[$hash])) {
			$arGeoStores[$hash] = 1;
		}
	}
	$arStores = $arGeoStores;
	unset($arGeoStores);
}
// sort prices on top
$arPrices = array();
foreach ($arResult['ITEMS'] as $key => &$arItem) {
	if ($arItem['PRICE']) {
		$arPrices[$key] = $arItem;
		unset($arResult['ITEMS'][$key]);
	}
	if ($arItem['CODE'] == 'STORE_AMOUNT_BOOL') {
		$arStoresVal = array();
		foreach ($arItem['VALUES'] as $vKey => $arVal) {
			if (isset($arStores[$arVal['URL_ID']])) {
				$arStoresVal[$vKey] = $arVal;
			}
		}
		$arItem['VALUES'] = $arStoresVal;
	}
}
unset($arItem);

$arResult['ITEMS'] = array_merge($arPrices, $arResult['ITEMS']);