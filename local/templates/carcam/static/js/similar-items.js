$.fn.equals = function(compareTo) {
  if (!compareTo || this.length != compareTo.length) {
    return false;
  }
  for (var i = 0; i < this.length; ++i) {
    if (this[i] !== compareTo[i]) {
      return false;
    }
  }
  return true;
};

function ys_slider_JSInit(ajax_page, site_id, slider_id, red_url){

	if(slider_id == "add2b_popup"){
		var __this__ = $('div#basket_slider');
		__this__.gupval = 0
		__this__.elem_width = 175
	}else{
		var __this__ = $('div#slider');
		__this__.gupval = 20
		__this__.elem_width = 240
	}

	__this__.speed = 200
	__this__.ajax_page = ajax_page
	__this__.site_id = site_id
	__this__.slider_id = slider_id
	__this__.red_url = red_url
	
		
	__this__.check_nav = function(th)
	{
		var button_left = th.find('.button7')
		var button_right = th.find('.button8')
		
		if(th.find('li.showed:first').prev().length!=1)
			button_left.addClass('disabled')
		else
			button_left.removeClass('disabled')
		
		if(th.find('li.showed:last').next().length!=1)
			button_right.addClass('disabled')
		else
			button_right.removeClass('disabled')
			
	}
	
	__this__.check_count = function(th)
	{
		if(slider_id == "add2b_popup"){
			var slw = th.parent().parent().width();
		}else{
			var slw = th.parent().width();
		}
		var count = Math.floor(slw / __this__.elem_width);
		var now_count = th.find('ul li.showed').length;
		if(now_count != count)
			if(now_count > count){
				var now = th.find('ul li.showed:last')
				for(var i=0;i<now_count-count;i++){
					now.removeClass('showed');
					now = now.prev()
				}
			}else{
				var now = th.find('ul li.showed:last')
				if(now.length!=1){
					th.find('ul li:lt(' + count + ')').addClass('showed');
				}else{
					for(var i=0;i<count-now_count;i++){
						now = now.next()
						now.addClass('showed').css('left','0').css('opacity', '1');
					}
					now_count = th.find('ul li.showed').length;
					if(now_count != count){
						now = th.find('ul li.showed:first')
						for(var i=0;i<count-now_count;i++){
							now = now.prev()
							now.addClass('showed').css('left','0').css('opacity', '1');
						}
					}
				}
			}
		__this__.check_nav(th)
	}
	
	__this__.bind_li = function(th)
	{
		th.find('ul li').unbind('hover').hover(function () {
			$(this).find('.item-popup').show();
			$(this).css({'z-index': 50});
			$(this).find('.item-popup').addClass('item-hover');
		}, function () {
			var openedMenu = $(this).find('.selectBox-menuShowing')
			if (openedMenu.length != 1) {
				$(this).css({'z-index': 1});
				$(this).find('.item-popup').removeClass('item-hover');		
				$(this).find('.item-popup').fadeOut();

			}
		});
	}
	
	__this__.ajax_loader_params = {
		loaderSymbols: ['0', '1', '2', '3', '4', '5', '6', '7'],
		loaderRate: 30
	}
	
	__this__.ajax_loader = function(obj)
	{
		obj.addClass("loader");
		obj.WAIT_STATUS = true;
		obj.WAIT_PARAM = __this__.ajax_loader_params;
		obj.WAIT_INDEX = 0;
		obj.WAIT_FUNC = function(){
			if(!obj) return;
			if(obj.WAIT_STATUS)
			{
				obj.html(obj.WAIT_PARAM.loaderSymbols[obj.WAIT_INDEX]);
				obj.WAIT_INDEX = obj.WAIT_INDEX < obj.WAIT_PARAM.loaderSymbols.length - 1 ? obj.WAIT_INDEX + 1 : 0;
				setTimeout(obj.WAIT_FUNC, obj.WAIT_PARAM.loaderRate);
			}
			else
				obj.removeClass("loader");
		};
		obj.WAIT_FUNC();
	}

	__this__.ajax_loader_stop = function(obj)
	{
		obj.WAIT_STATUS = false;
	}	
	
	__this__.ajax_li_check = function(th, name, now_page)
	{
		var pp = th.parent().parent()
		var count = 0
		count = parseInt(__this__.find('hidden[name="count"]').attr('data-count'));

		if(count/10 > now_page)
				return true;
		return false;
	}
	
	__this__.ajax_li = function(th, orientation)
	{
		var button_left = th.find('.button7')
		var button_right = th.find('.button8')
			
		if(orientation == "right")
		{
			var elem = th.find('li:last').not(".loader");
		
			if(!th.find('li.showed:last').equals(elem)) 
				return;

			var now_page = parseInt(elem.find('hidden[name="iNumPage"]').attr('data-page'));
			var name = th.attr('id').replace('block_', '');
			
			if(!__this__.ajax_li_check(th, name, now_page)) 
				return;
				var new_page = now_page+1;
			var ajax_elem = elem
			.clone()
			.removeClass('showed')
			
			elem.after(ajax_elem)
			
			button_right.old_text = button_right.html()
			__this__.ajax_loader(button_right)
		}else if(orientation == "left"){
			var elem = th.find('li:first').not(".loader");
		
			if(!th.find('li.showed:first').equals(elem)) 
				return;

			var now_page = parseInt(elem.find('hidden[name="iNumPage"]').attr('data-page'));
			var name = th.attr('id').replace('block_', '');
			
			if(now_page <= 1) return;
				var new_page = now_page-1;
			var ajax_elem = elem
			.clone()
			.removeClass('showed')
			
			elem.before(ajax_elem)
			
			button_left.old_text = button_left.html()
			__this__.ajax_loader(button_left)	

		}else	return;
		
		__this__.ajax_loader(ajax_elem)
		
		$.post(__this__.ajax_page, {
			'ys_ms_ajax_call':'y',
			'iNumPage': new_page,
			'red_url':__this__.red_url,
			'site_id':__this__.site_id,
			'slider_id':__this__.slider_id,
		}, function(data) {
		
			var appends = $(data).find('ul li')
			if(orientation == "right")
			{			
				elem.after(appends)
				var new_li = th.find('li.showed:last').next()
				th.find('li hidden[data-page=\"'+(new_page-2)+'\"]').parent().remove()
				__this__.ajax_loader_stop(button_right)
				button_right.html(button_right.old_text)
			}else if(orientation == "left"){
				elem.before(appends)
				var new_li = th.find('li.showed:first').prev()
				th.find('li hidden[data-page=\"'+(new_page+2)+'\"]').parent().remove()
				__this__.ajax_loader_stop(button_left)
				button_left.html(button_left.old_text)				
			}else	return;
			
			if(new_li.length==1){
				if(ajax_elem.hasClass('showed')){
					new_li.addClass('showed').css('opacity', '0')
					new_li.animate(
						{
							'opacity': '1'
						}, 
						__this__.speed,
						function(){__this__.normalize($(this))}
					);
				}
			}
			__this__.ajax_loader_stop(ajax_elem)
			ajax_elem.remove();
			
			appends.find("select").selectBox();
			__this__.check_nav(th)
			__this__.bind_li(th)
			__this__.add2basket_events(th)
			__this__.check_count(th);
		});	
		
		__this__.check_nav(th)
		
	}

	__this__.normalize = function(li)
	{
		li.css('opacity', '1').css('left','0')
	}	
	
	__this__.bind_nav = function(th)
	{
		var button_left = th.find('.button7')
		var button_right = th.find('.button8')
	
		button_left.unbind("click").click(function(){
			th.find('li').stop(true, true)
			var next_li = th.find('li.showed:first').prev()
			if(next_li.length!=1)
				return;
			var last_li = th.find('li.showed:last')
			var left = $(last_li).width()+__this__.gupval
			var all_li = th.find('li.showed:not(:last)')
			next_li.addClass('showed').css('opacity', '0').css('left','-'+left+'px')
			all_li.css('left','-'+left+'px')
			last_li.css('left','-'+left+'px')
			last_li.animate(
				{
					'left': 0+'px',
					'opacity': '0'
				}, 
				__this__.speed,
				function(){
					__this__.normalize($(this)) 
					$(this).removeClass('showed');
					__this__.check_nav(th)
					__this__.check_count(th);
				}
			);
			all_li.animate(
				{
					'left': 0+'px'
				}, 
				__this__.speed, 
				function(){	__this__.normalize($(this))}
			);
			next_li.animate(
				{
					'left': 0+'px',
					'opacity': '1'
				}, 
				__this__.speed,
				function(){	__this__.normalize($(this))}
			);
			__this__.ajax_li(th, "left")
		});
	
		button_right.unbind("click").click(function(){
			th.find('li').stop(true, true)
			var next_li = th.find('li.showed:last').next()
			if(next_li.length!=1)
				return;
			var all_li = th.find('li.showed:not(:first)')
			var first_li = th.find('li.showed:first')
			next_li.addClass('showed').css('opacity', '0')
			var left = $(first_li).width()+__this__.gupval
			first_li.animate(
				{
					'left': '-'+left+'px',
					'opacity': '0'
				}, 
				 __this__.speed,
				function(){
					__this__.normalize($(this))
					$(this).removeClass('showed');
					__this__.check_nav(th)
					__this__.check_count(th);
				}
			);
			all_li.animate(
				{
					'left': '-'+left+'px'
				}, 
				 __this__.speed, 
				function(){	__this__.normalize($(this)) }
			);
			next_li.animate(
				{
					'left': '-'+left+'px',
					'opacity': '1'
				}, 
				 __this__.speed,
				function(){	__this__.normalize($(this)) }
			);
			
			__this__.ajax_li(th, "right")
			
		});
		
		
	}

	__this__.add2basket_events = function(th)
	{
		th.find(".add2basket").attr('onclick','return ys_ms_ajax_add2basket(this);');
	}
	
	__this__.init = function(th)
	{
		if(!th)
			return;
		__this__.check_count(th);
		__this__.bind_nav(th);
		__this__.bind_li(th);
		__this__.add2basket_events(th);
		th.find("select").selectBox();
		$(window).resize(function(){
			__this__.check_count(th);
		});
	}
	
	__this__.init(__this__)
	
		Tipped.create(
		".star[title]," +
		".ys_close_add2b[title]", { skin: 'black' });
}	

function ys_ms_ajax_add2basket(self){
	if(!$(self).hasClass('button_in_basket')){
		var button = $(self)
		var id = button.attr('id').replace('ys-ms-','');
		var splitData = id.split('-');
		var iblock_id = splitData[0];
		var element_id = splitData[1];
		var href = button.attr('href');
		if($('.yen-bs-box').length > 0 &&  typeof SITE_TEMPLATE_PATH != "undefined" && typeof yenisite_bs_flyObjectTo != "undefined"){
			var action_add2b = $('#action_add2b').attr('value') ;
			var ob_post_params = JSON.parse('{"'+href.substr(href.indexOf('?')+1).split('&').join('","').split('=').join('":"')+'", "action":"ADD2BASKET", "id":"'+element_id+'", "iblock_id":"'+iblock_id+'","sessid":"'+BX.message.bitrix_sessid+'", "action_add2b":"'+action_add2b+'", "main_page":"Y"}');
			var url = SITE_TEMPLATE_PATH+'/ajax/add2basket.php';
			$.post(url, ob_post_params, function(data) {
				button.addClass("button_in_basket")
				var pic_src = $('#product_photo_'+element_id).attr('src');
				if($('#action_add2b').attr('value') == 'popup_window')
				{
					var arData = data.split('<!-- add2basket -->');
					$('.yen-bs-box').html(arData[0]);
					$('#add_2b_popup').html(arData[1]);
					$('#add_2b_popup').fadeIn('300');
					$('#mask').fadeIn('300');
				}
				else
				{
					var what = '#ys-ms-'+id+'-photo';
					var to = '.yen-bs-box';
					yenisite_bs_flyObjectTo(what, to);
					$('.yen-bs-box').html(data);
				}
			});
			return false;
		}else{
			return true;
		}
	}
	return false;
}