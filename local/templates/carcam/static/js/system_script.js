/* 	function for work with library jGrowl; script and css including in header.php  */
function jGrowl(text,theme,header) {
	if(theme=="error") {
		header = BX.message('JGROWL_ERROR');
	}

	$.jGrowl.defaults.position = 'bottom-left';		/* position of messages: top-left, top-right, bottom-left, bottom-right, center	*/
	$.jGrowl.defaults.check = 1000;					/* time in millisecond with which jGrowl will check posts to be removed from screen	*/
	$.jGrowl.defaults.closer = false;				/* show button 'Close all' 	*/
	$.jGrowl.closerTemplate = "Close all message";	/* message on button 'Close all'	*/
	$.jGrowl(text,{
		theme: 		theme,			/* theme of message. Create in 'bitronic_template/styles.css'	*/
		header: 	header,			/* title of message	*/
		life:		7000,			/* lifetime message in millisecond 	*/
		sticky: 	false,			/* closed only by user 	*/ 
		glue:		'after',		/* where show new message 'after' or 'before' last message 	*/
		//closeTemplate:	'',		/* symbol of button 'close'	*/
	});
}
/*	function for close user menu in header	*/
function yenisite_um_close() {
	if($('ul.user_menu').hasClass('opened')) {
		$('ul.user_menu').removeClass('opened').addClass('closed').fadeOut();
	}
}

function setSortFields(order, by) {
	$('#order_field').attr('value', order);
	$('#by_field').attr('value', by);
	document.forms['sort_form'].submit();
	return false;
}

function setViewField(view) {
	$('#view_field').attr('value', view);
	document.forms['sort_form'].submit();
	return false;
}

function setQuantity(id, operation) {
	var q = $(id).attr('value');
	if(operation == '-' && q > 1)
		q --;
	if(operation == '+' )
		q++;	
	$(id).attr('value', q);	
	$('#BasketRefresh').attr('value', 'Y');
	
	document.forms['basket_form'].submit();
}

function setQuantityTable(id, operation) {
	var q = $(id).attr('value');
	if(operation == '-' && q > 1)
		q --;
	if(operation == '+' )
		q++;	
	 $(id).attr('value', q);	
}

function setDelete(id) {
	$(id).attr('value', 'Y');	
	 $('#BasketRefresh').attr('value', 'Y');
	document.forms['basket_form'].submit();
}
function setDelay(id, val) {
	$(id).attr('value', val);	
	 $('#BasketRefresh').attr('value', 'Y');
	document.forms['basket_form'].submit();
}

function setPageCount(val) {
	var loc = window.location.pathname.toString();

	if (loc.indexOf('page_count-') === -1) {
		window.location.pathname = loc + 'page_count-' + val + '/';
	} else {
		window.location.pathname = loc.replace(new RegExp('page_count-(\\d+)', 'g'), 'page_count-' + val);
	}


	return false;
}

function YSErrorPlacement(error, element, popup)
{
    var elem = $(element),
    corners = ['left center', 'right center'],
    flipIt = elem.parents('td.right').length > 0 || elem.parents('span.right').length > 0;
 
    if(!error.is(':empty')) {
        elem.filter(':not(.valid)').qtip({
            overwrite: false,
            content: error,
            position: {
                my: corners[ flipIt ? 0 : 1 ],
                at: corners[ flipIt ? 1 : 0 ],
            },
            show: {
                event: false,
                ready: true
            },
            hide: true,
            style: {
                classes: 'qtip-red'
            }
        })
        .qtip('option', 'content.text', error);
        elem.css('border', '1px red solid');
    }
    else
    {
        elem.qtip('destroy');
        elem.css('border', '');
    }
	
	if(typeof(popup)!=='undefined')
		if (popup.validate().checkForm()) {
			$('.popup:visible button').removeClass("button_in_basket").removeAttr('disabled');
		}else{
			$('.popup:visible button').addClass("button_in_basket").attr('disabled','disabled');
		}
}

$(function() {
	
		// Handlers for view/sort change links
		if ($('input[name="ys-sef"]').length) {
			function viewSortHandlers() {
				fParams = []; // Filter params
				//len;
				setF = $('[name="sort_form"] input[name^=set_filter]');

				// Save filter parameters
				$('[name=sort_form] input[name^=arrFilter]').each(function() {
					fParams.push({name: $(this).attr('name'), val: $(this).val()});
				});
				 
				if (setF.length && setF.val().length) {
					fParams.push({name: 'set_filter', val: $('input[name=set_filter]').val()});
				}
				len = fParams.length;
			}
			$(document).ready(viewSortHandlers);
			$(document).ajaxComplete(viewSortHandlers);
			
			//$('body').off('click', '.f_view, .f_price, .f_name, .f_pop, .f_sales, .pager, .one-step');
			$('body').on('click', '.f_view, .f_price, .f_name, .f_pop, .f_sales, .pager, .one-step', function(e) {
				console.log('1');
				var el = $(e.target),
					next = el.next(),
					href = next.attr('href'),
					curr = $(e.currentTarget);

				if (el.hasClass('button11') || el.hasClass('button12') ||
					el.attr('href') !== undefined && !el.hasClass('nav-hidden')) {

					if (el.attr('href') !== undefined) {
						next = el;
						href = next.attr('href');
					}

					if ( curr.hasClass('f_view')
						||  curr.hasClass('f_price')
						|| curr.hasClass('f_name')
						|| curr.hasClass('f_pop')
						|| curr.hasClass('f_sales')) {

						if ( href.charAt(href.length - 1) !== '/' ) {
							href += '/';
							next.attr('href', href);
						}
					}

					if (setF.length || $('input[name=smart-filter-params]').length) {
						href += '?';

						for (var i = 0; i < len; i += 1) {
							href += fParams[i].name + '=' + fParams[i].val;
							if (i != len - 1) {
								href += '&';
							}
						}
					}
					
					next.attr('href', encodeURI(href));
					e.preventDefault();
										
					window.location = encodeURI(href);
				}
			});
		} // if ($('input[name="ys-sef"]').length)
		
	$('.pager-block').on('click', function(e) {
		if ($(e.target).hasClass('nav-hidden')) {
			e.preventDefault();
		}
	});

	$("#search_select").change(function(){	    
	    var selectVal = $('#search_select :selected').val();	   
		$("#search_form").attr("action",  selectVal);
	});
	
	$(".s_submit").click(function(){
		$("#search_form").submit();
	});


	var minh = 0;
    $('.catalog-list li').each(function(){
        if(minh == 0 || $(this).height() > minh) {
        	minh = $(this).height();
        }
    });

    $('.catalog-list li').css('height', minh + 'px');

    $('a[href*="ADD2BASKET"]').find('button').click(function(){
		var par = $(this).parent();

		if(par.attr("href")) {
			window.location = par.attr("href");
		} else {
			par = $(this).parent().parent();
			window.location = par.attr("href");
		}
		return false;
	});
});

function YSstartButtonLoader(loaderObj)
{
    loaderObj.VALUE = loaderObj.val();
    loaderObj.WAIT_STATUS = true;
    loaderObj.SYMBOLS = ['0', '1', '2', '3', '4', '5', '6', '7'];
    loaderObj.WAIT_START = 0;
    loaderObj.WAIT_CURRENT = loaderObj.WAIT_START;
    loaderObj.Rate = 10;
    loaderObj.WAIT_FUNC = function(){
        if(loaderObj.WAIT_STATUS)
        {
            loaderObj.css('font-family', 'WebSymbolsLigaRegular');
            loaderObj.siblings('span.text').removeClass('show').addClass('hide');
            loaderObj.html(loaderObj.SYMBOLS[loaderObj.WAIT_CURRENT]);
            loaderObj.WAIT_CURRENT = loaderObj.WAIT_CURRENT < loaderObj.SYMBOLS.length-1 ? loaderObj.WAIT_CURRENT + 1 : loaderObj.WAIT_START;
            setTimeout(loaderObj.WAIT_FUNC, 1000 / loaderObj.Rate);
        }
        else
            loaderObj.removeClass('loader').parent().prop("disabled", false).removeClass('active').removeClass('disable');
    };
    
    loaderObj.addClass('loader').parent().prop("disabled", true).addClass('active').addClass('disable');
    loaderObj.WAIT_FUNC();
}

function YSstopButtonLoader(loaderObj)
{
    loaderObj.WAIT_STATUS = false;
}