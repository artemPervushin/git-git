var gulp = require('gulp'),
	cssmin = require('gulp-cssmin'),
	rename = require('gulp-rename'),
	watch = require('gulp-watch');

gulp.task('css-s', function () {
    gulp.src('css/s.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('css'));
});

gulp.task('css-theme', function () {
    gulp.src('css/themes/theme_orange-flat-full.css')
        .pipe(cssmin())
        .pipe(rename({basename: 'theme_orange-flat'}))
        .pipe(gulp.dest('css/themes'));
});

gulp.task('watch', function () {
    gulp.watch('css/s.css', ['css-s']);
    gulp.watch('css/themes/theme_orange-flat-full.css', ['css-theme']);
});

gulp.task('default', ['watch']);