var system = require('system');

var urlToPrint = system.args[1]; 
var destinationFile = system.args[2]; 

var page = require('webpage').create();

page.paperSize = { 
	format: 'A4', 
	orientation: 'landscape', 
	margin: {
		top: '50px',
		right: '100px',
		bottom: '30px',
		left: '100px'
	},
	header: {
		height: '50px',
		contents: phantom.callback( function(pageNum, numPages) 
		{
			var today = new Date();
			var yyyy = today.getFullYear();
			var mm = today.getMonth() + 1;
			if( mm < 10 ) mm = '0' + mm;
			
			var dd = today.getDate();
			if( dd < 10 ) dd = '0' + dd; 
			
			var h = today.getHours();
			var m = today.getMinutes();
			if( m < 10 ) m = '0' + m;

			return "<span style='font-size: 9px;'><span style='float: left;'>" + dd + '.' + mm +'.' + yyyy + ' ' + h + ':' + m + "</span><div style='position: absolute; width: 100%; text-align: center;'>Прайс-лист КАРКАМ® Электроникс™</div></span>";
		})
  	},
	footer: {
		height: '40px',
		contents: phantom.callback( function(pageNum, numPages) 
		{
			return "<span style='font-size: 9px;'><span style='float:left'>" + urlToPrint + "</span><span style='float:right'>" + pageNum + '/' + numPages + '</span></span>';
		})
  	},
}

page.onLoadFinished = function( status ) 
{
	setTimeout( function() 
	{
		if ( status == 'success' ) 
		{
			page.render( destinationFile, { format: 'pdf' } );  
		}

		phantom.exit();
	}, 200); // wait till page does some js and reflowing before print
}

page.open( urlToPrint );
