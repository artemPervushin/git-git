<?

Class zimin_logic extends CModule
{
	var $MODULE_ID = 'zimin.logic';
	var $MODULE_VERSION = '1.0.3';
	var $MODULE_VERSION_DATE = '2017-03-20 13:13:13'; 
	var $MODULE_NAME = ' Печать PDF и проч';	// ' ' helps to put module first in a list			
	var $MODULE_DESCRIPTION = 'Кастомная логика для Каркама';
	
	var $PARTNER_NAME = 'Зимин Александр';
	var $PARTNER_URI = 'mailto:zimin@aleksandrzimin.com';


	function DoInstall()
	{
		RegisterModule( $this->MODULE_ID );
	}

	function DoUninstall()
	{
		UnRegisterModule( $this->MODULE_ID );
	}
}