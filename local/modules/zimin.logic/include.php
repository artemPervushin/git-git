<?

Class CZiminLogic
{
	public static function createAndSavePdf( $urlToPrint, $path )
	{
		$binary = dirname( __FILE__ ) . '/lib/ziminpdf';
		$script = dirname( __FILE__ ) . '/lib/printUrlToPdf.js';
		
		//fix. differentiate to substitue screen style with render style in a template
		$urlToPrint .= '?render';

		//render pdf to path
		$timeout = 7; //sec
		$errors = self::execute( $binary . ' ' . $script . ' ' . $urlToPrint . ' "' . $path . '"', null, $out, $out, $timeout );
		if( !$errors ) 
			return 'success';
		else 
			return 'Не удалось создать pdf по новым настройкам за ' . $timeout . ' секунд. Попробуйте еще раз<br><br>' . $errors ;
	}

	public static function returnPdf( $urlToPrint, $fileName )
	{
		$binary = dirname( __FILE__ ) . '/lib/ziminpdf';
		$script = dirname( __FILE__ ) . '/lib/printUrlToPdf.js';
		
		//fix. differentiate to substitue screen style with render style in a template
		$urlToPrint .= '?render';

		//create temporary file, unique name in read-write mode
		$file = tmpfile(); 
		$fileMeta = stream_get_meta_data( $file );
		$fileUri = $fileMeta['uri'];

		//render pdf to this file
		$timeout = 7; //sec
		//$errors = self::execute( './ziminpdf ./printUrlToPdf.js ' . $urlToPrint . ' ' . $fileUri, null, $out, $out, $timeout );
		$errors = self::execute( $binary . ' ' . $script . ' ' . $urlToPrint . ' ' . $fileUri, null, $out, $out, $timeout );
		if( !$errors )
		{
		    //success
		    
		    //give this file
		    header( 'Content-type: application/pdf' );
		    header( 'Content-Disposition: attachment; filename="' . $fileName . '.pdf"' );
		    header( 'Content-Transfer-Encoding: binary' );
		    readfile( $fileUri);

		    //close this file, thus preparing it for removal
		    fclose( $file ); 
		}
		else
		{
		    //fail
		    
		    header( 'Content-Type: text/html; charset=UTF-8' );
		    echo 'Не удалось создать pdf по новым настройкам за ' . $timeout . ' секунд. Попробуйте еще раз<br><br>' . $errors ;

		    //echo $out;
		}
	}

	private static function execute( $cmd, $stdin = null, &$stdout, &$stderr, $timeout = false )
	{
	    $pipes = array();
	    $process = proc_open(
	        $cmd,
	        array(array('pipe','r'),array('pipe','w'),array('pipe','w')),
	        $pipes
	    );
	    $start = time();
	    $stdout = '';
	    $stderr = '';

	    if(is_resource($process))
	    {
	        stream_set_blocking($pipes[0], 0);
	        stream_set_blocking($pipes[1], 0);
	        stream_set_blocking($pipes[2], 0);
	        fwrite($pipes[0], $stdin);
	        fclose($pipes[0]);
	    }

	    while(is_resource($process))
	    {
	        //echo ".";
	        $stdout .= stream_get_contents($pipes[1]);
	        $stderr .= stream_get_contents($pipes[2]);

	        if($timeout !== false && time() - $start > $timeout)
	        {
	            proc_terminate($process, 9);
	            return 1;
	        }

	        $status = proc_get_status($process);
	        if(!$status['running'])
	        {
	            fclose($pipes[1]);
	            fclose($pipes[2]);
	            proc_close($process);
	            return $status['exitcode'];
	        }

	        usleep(100000);
	    }

	    return 1;
	}
}