<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Список официальных представителей продукции «Каркам»");
$APPLICATION->SetTitle("Где купить?");
?>
    <main class="container about-page">
        <div class="row">
            <div class="col-xs-12">
                <h1><? $APPLICATION->ShowTitle(false) ?></h1>

                
	<style type="text/css">
		ul { margin: 0; padding: 0 }
		ul, ol { list-style:none; }
		#masthead { position:relative; z-index:2; border-bottom: 1px solid #C5C5C5\0; }
		#footer_outer { position:relative; z-index:2; }
		.ru-wrapper { background:url(images/bg_skelatal_weave.png); }
		.hero {  position:relative; background:#fff; box-shadow:2px 2px 16px rgba(0, 0, 0, 0.5);margin-bottom: 20px; padding: 0; }
		.hero img { max-width:100%; border: 1px solid #fff; }
		.headline { position: absolute; top:30px; right:30px; font-size:0; }
		.headline h2 { display:inline-block; *display:inline; margin-bottom: 0px;
		zoom:1; height:34px; padding:0 20px; font-size:20px; line-height:34px; text-transform:uppercase; background:rgba(156, 48, 2, 0.9)!important; filter:Alpha(opacity=90); background:#9c3022; color:#fff; }
		.headline .line-1 { background:rgba(220, 41, 30, 0.9)!important; filter:Alpha(opacity=90); background:#dc291e; vertical-align:top; }
		.headline i { display:inline-block; *display:inline;
		zoom:1; width:0; height:0; border-width:0 0 34px 34px; border-style:solid; border-color:rgba(117, 7, 0, 0.9) transparent; border-color:#750000 transparent\0; }
		zoom:1; width:142px; margin: 10px 20px 0 0; font-weight:bold; }
		.bbox { width:500px; margin: 20px 0 20px 15px; border:1px solid #fff; background:#fff url(images/bg_diagonales_decalees.png); box-shadow:2px 2px 16px rgba(0, 0, 0, 0.3); }
		.bbox h3 { margin:0px 0px 0px -15px; display:inline-block; *display:inline;
		zoom:1; height:30px; padding:0 20px; font-size:14px; line-height:30px; text-transform:uppercase; background:rgba(220, 41, 30, 1)!important; filter:Alpha(opacity=100); background:#dc291e; color:#fff; }
		.bbox i { margin: 0 0 0 -15px; padding:5px 0 0; display:block; width:0; height:0; border-width:0 0 15px 15px; border-style:solid; border-color:rgba(117, 7, 0, 1) transparent; border-color:#750000 transparent\0; }
		.bbox ul { margin:10px; }
		.bbox li { background: url(images/dot.gif) 0 7px no-repeat; padding: 0 0 10px 10px; color:#333; font-size:11px; }
		.ccontainer { 
            z-index:1;
            padding:20px;
            margin:0 auto;
            background:#fff;
            box-shadow:0 0 15px rgba(0, 0, 0, 0.3);
            background:url(images/bg_subtle_dots.png);
            border-left:1px solid rgba(255, 250, 250, 1);
            border-right:1px solid rgba(255, 250, 250, 1); color: #666;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 12px;
			position: relative;
		}
        @media screen and (max-width: 991px) {
            .ccontainer {
                padding: 0;
            }
            .hero {
                margin-bottom: 0;
            }
        }
		#recmag{display: none;}
		.img{border: none !important}
	</style>

	<div class="ru-wrapper">
	  <div class="ccontainer">
	    <div class="hero"><img src="images/hero2.jpg" alt="СПИСОК РЕКОМЕНДОВАННЫХ МАГАЗИНОВ">
	      <div class="headline hidden-xs hidden-sm"> <i></i>
	        <h2 class="line-1" style="font-family:Arial, Helvetica, sans-serif;">СПИСОК РЕКОМЕНДОВАННЫХ МАГАЗИНОВ
	</h2>
	        <br>
	        <h2 style="font-family:Arial, Helvetica, sans-serif;">НА ЯНДЕКС.МАРКЕТ</h2>
	      </div>
	    </div>
	    <ul class="recommended-shops row">
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="http://www.carcam.ru/" target="_blank"><img src="/upload/logo_carcam_offc.png" class="img" alt=""></a></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><a href="http://www.av808.ru/" target="_blank"><img src="/upload/logo-to-buy/32.jpg" class="img" alt=""></a></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="/upload/logo-to-buy/2.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="images/mmarkt.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="images/m-video.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="images/eld.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="images/tech.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="images/sila.png" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="/upload/logo-to-buy/5.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="/upload/logo-to-buy/11.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="/upload/logo-to-buy/24.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="/upload/logo-to-buy/25.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="images/logotype.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="/upload/logo-to-buy/30.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="/upload/partners/naviburg.png" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="excam_logo_for_carcam.jpg" class="img" alt=""></li>
			<li class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="image/digital-teka.jpg" class="img" alt=""></li>
		</ul>
	    <div class="bbox hidden-xs hidden-sm"> <i></i>
	      <h3  style="font-family:Arial, Helvetica, sans-serif;">Преимущества покупки в рекомендованных  магазинах:</h3>
	      <ul>
	        <li>Только сертифицированные продукты</li>
	        <li>Широкий ассортимент новинок  </li>
	        <li>Специальные акции и программы </li>
	        <li>Высокое качество обслуживания и  сервиса </li>
	        <li>Техническая поддержка и гарантия  на всю продукцию</li>
	      </ul>
	    </div>
	    <div class="hidden-xs hidden-sm" style="position: absolute; top: 450px; right: 77px;">
	   		<img src="images/Yandex.png">
	    </div>
	  </div>
</div>
	
<div style="clear: both; width: 100%; height: 50px"></div>	
                
                <style>
                    #action { padding: 0; width: 1222px; margin-left: 15px; clear: both !important; }
                    #action ul { list-style: none; padding: 0; }
                    #action li { display: inline; margin: 3px 7px !important; text-decoration: none !important; }
                    #action a { width: 284px; height: 60px; border: none !important; margin-bottom: 20px; }
                    .img {
                        padding: 3px;
                        /*border: 1px solid #D8D8D8 !important;*/
                    }
                    .ys_article {
                        margin-left: -210px;
                    }
                    h3 {
                        font-size: 20px;
                        color: #333333;
                        text-align: left;
                    }
                </style>

                <div style="text-align: center;">
                    <p style="color: #333333; font-size: 20px; line-height:18px; text-align: center;">
                        Наши фирменные магазины и точки выдачи продукции
                    </p>
                </div>

                <!--  all maps   -->
                <script type="text/javascript" src="/upload/jquery.lightbox-0.5.js"></script>
                <script type="text/javascript">
                    jQuery(function()
                    {
                        jQuery(".li a").lightBox();
                    });
                </script>
                <style>
                    /**
                     * jQuery lightBox plugin
                     * This jQuery plugin was inspired and based on Lightbox 2 by Lokesh Dhakar (http://www.huddletogether.com/projects/lightbox2/)
                     * and adapted to me for use like a plugin from jQuery.
                     * @name jquery-lightbox-0.5.css
                     * @author Leandro Vieira Pinho - http://leandrovieira.com
                     * @version 0.5
                     * @date April 11, 2008
                     * @category jQuery plugin
                     * @copyright (c) 2008 Leandro Vieira Pinho (leandrovieira.com)
                     * @license CCAttribution-ShareAlike 2.5 Brazil - http://creativecommons.org/licenses/by-sa/2.5/br/deed.en_US
                     * @example Visit http://leandrovieira.com/projects/jquery/lightbox/ for more informations about this jQuery plugin
                     */
                    #jquery-overlay {
                        position: absolute;
                        top: 0;
                        left: 0;
                        z-index: 90;
                        width: 100%;
                        height: 500px;
                    }
                    #jquery-lightbox {
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 100%;
                        z-index: 100;
                        text-align: center;
                        line-height: 0;
                    }
                    #jquery-lightbox a img { border: none; }
                    #lightbox-container-image-box {
                        position: relative;
                        background-color: #fff;
                        width: 250px;
                        height: 250px;
                        margin: 0 auto;
                    }
                    #lightbox-container-image { padding: 10px; }
                    #lightbox-loading {
                        position: absolute;
                        top: 40%;
                        left: 0%;
                        height: 25%;
                        width: 100%;
                        text-align: center;
                        line-height: 0;
                    }
                    #lightbox-nav {
                        position: absolute;
                        top: 0;
                        left: 0;
                        height: 100%;
                        width: 100%;
                        z-index: 10;
                    }
                    #lightbox-container-image-box > #lightbox-nav { left: 0; }
                    #lightbox-nav a { outline: none; }
                    #lightbox-nav-btnPrev, #lightbox-nav-btnNext {
                        width: 49%;
                        height: 100%;
                        zoom: 1;
                        display: block;
                    }
                    #lightbox-nav-btnPrev {
                        left: 0;
                        float: left;
                    }
                    #lightbox-nav-btnNext {
                        right: 0;
                        float: right;
                    }
                    #lightbox-container-image-data-box {
                        font: 10px Verdana, Helvetica, sans-serif;
                        background-color: #fff;
                        margin: 0 auto;
                        line-height: 1.4em;
                        overflow: auto;
                        width: 100%;
                        padding: 0 10px 0;
                    }
                    #lightbox-container-image-data {
                        padding: 0 10px;
                        color: #333333;
                    }
                    #lightbox-container-image-data #lightbox-image-details {
                        width: 70%;
                        float: left;
                        text-align: left;
                    }
                    #lightbox-image-details-caption { font-weight: bold; }
                    #lightbox-image-details-currentNumber {
                        display: block;
                        clear: left;
                        padding-bottom: 1.0em;
                    }
                    #lightbox-secNav-btnClose {
                        width: 66px;
                        float: right;
                        padding-bottom: 0.7em;
                    }
                    ul {
                        list-style: none;
                        /*margin-left: -20px;*/
                    }
                    .li {
                        float: left;
                        margin: 0 7px 7px 7px;
                    }
                    .li a {
                        text-decoration: none;
                        border: none;
                    }
                    .li a:hover {
                        text-decoration: none;
                        border: none;
                    }
                    h3 {
                        font-size: 20px;
                        color: #333333;
                        text-align: left;
                    }
                </style>
                <?php
                    $geo = explode('/', $_COOKIE["YS_GEO_IP_CITY"]);
                    $map_data = '';
                    $city = isset($geo[2])? $geo[2]:'Санкт-Петербург';

                    if (CModule::IncludeModule("iblock")) {
                        
                        // Поиск раздела
                        $sections = CIBlockSection::GetList(Array("SORT" => "­­ASC"), Array("IBLOCK_ID" => 157, "NAME" => $city), false, Array("ID", "NAME", "UF_ZOOM", "UF_LAT", "UF_LON"));
                        if ($section = $sections->GetNext()) {
                            
                            // Устновка центра и масштаба карты
                            $map_data['google_lat']   = $section["UF_LAT"]?   floatval($section["UF_LAT"]):55.751480318208;
                            $map_data['google_lon']   = $section["UF_LON"]?   floatval($section["UF_LON"]):37.59977671393;
                            $map_data['google_scale'] = $section["UF_ZOOM"]?  intval($section["UF_ZOOM"]) :9;

                            // Поиск точек самовывоза
                            $elements = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 157, "SECTION_ID" => $section["ID"]), false, false, Array("PROPERTY_GEO_TYPE", "PROPERTY_GEO_PHONE", "PROPERTY_GEO_TIME", "PROPERTY_GEO_MAP_POINT", "PROPERTY_ADDRESS"));
                            
                            // Формирование массива данных для отображения точек на карте
                            while($element = $elements->GetNext()) {
                                $coordinates = explode(',', $element['PROPERTY_GEO_MAP_POINT_VALUE']);
                                $map_data['PLACEMARKS'][] = Array(
                                    'TYPE' => $element['PROPERTY_GEO_TYPE_VALUE'],
                                    'TEXT' => $element['PROPERTY_ADDRESS_VALUE'],
                                    'PHONE' => $element['PROPERTY_GEO_PHONE_VALUE'],
                                    'TIME' => $element['PROPERTY_GEO_TIME_VALUE'],
                                    'LON' => floatval($coordinates[0]),
                                    'LAT' => floatval($coordinates[1]),
                                );
                            }
                        }
                    }
                ?>
                <? $APPLICATION->IncludeComponent(
	"bitrix:map.google.view", 
	"map", 
	array(
		"COMPONENT_TEMPLATE" => "map",
		"INIT_MAP_TYPE" => "ROADMAP",
		"MAP_DATA" => serialize($map_data),
		"MAP_HEIGHT" => "300",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"CONTROLS" => array(
			0 => "SMALL_ZOOM_CONTROL",
		),
		"OPTIONS" => array(
			0 => "ENABLE_SCROLL_ZOOM",
			1 => "ENABLE_DBLCLICK_ZOOM",
			2 => "ENABLE_DRAGGING",
			3 => "ENABLE_KEYBOARD",
		)
	),
	false
); ?>
                <br/>
                <h3>Единый федеральный центр приема заказов: <span style="white-space:nowrap;">8 (800) 555-68-08</span></h3>
                <a href="#" class="region_points_toggle btn-main hidden-md hidden-lg">Показать список магазинов <i class="icon-angle-double-down"></i></a>
                <script>
                    $('.region_points_toggle').on('click', function(e) {
                        e.preventDefault();
                        if ($('.region_points').is(':visible'))
                            $('.region_points_toggle').html('Показать список магазинов <i class="icon-angle-double-down"></i>');
                        else
                            $('.region_points_toggle').html('Скрыть список магазинов <i class="icon-angle-double-up"></i>');
                        $('.region_points').slideToggle();
                    });
                </script>
                <?php if (!empty($map_data)): ?>
                <div class="region_points">
                    <table class="table">
                        <tr>
                            <td style="font-size: 12pt; font-weight: bold;">Адрес:</td>
                            <td style="font-size: 12pt; font-weight: bold;">Тип магазина:</td>
                            <td style="font-size: 12pt; font-weight: bold;">Телефон:</td>
                            <td style="font-size: 12pt; font-weight: bold;">Режим работы магазина:</td>
                        </tr>
                        <?php foreach ($map_data['PLACEMARKS'] as $placemark): ?>
                        <tr>
                            <td><?= $placemark['TEXT'] ?></td>
                            <td><?= $placemark['TYPE'] ?></td>
                            <td><?= $placemark['PHONE'] ?></td>
                            <td><?= $placemark['TIME'] ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </main>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>