<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Проверка");
$APPLICATION->SetTitle("Проверка на подлинность");
?> <main class="container about-page">
	<div class="row">
		<div class="col-xs-12">
	<blockquote style="margin: 0px 0px 0px 40px; border: none; padding: 0px;"> 
  <div> 
    <p>Пожалуйста заполните всю информацию о покупке!</p>
   </div>
 </blockquote><blockquote style="margin: 0px 0px 0px 40px; border: none; padding: 0px;"> 
  <div> 
    <div class="gbook_otvet">Просим обратить ваше внимание на то, чтобы все поля были заполнены! Спасибо.</div>
   </div>
 
  <div class="gbook_otvet"> 
      <br>
     </div>
 </blockquote> <?$APPLICATION->IncludeComponent(
	"api:main.feedback", 
	".default", 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "",
		"INSTALL_IBLOCK" => "N",
		"USE_CAPTCHA" => "Y",
		"USE_HIDDEN_PROTECTION" => "Y",
		"USE_PHP_ANTISPAM" => "N",
		"PHP_ANTISPAM_LEVEL" => "1",
		"REPLACE_FIELD_FROM" => "Y",
		"INCLUDE_JQUERY" => "N",
		"VALIDTE_REQUIRED_FIELDS" => "N",
		"INCLUDE_PLACEHOLDER" => "N",
		"INCLUDE_PRETTY_COMMENTS" => "N",
		"INCLUDE_FORM_STYLER" => "N",
		"HIDE_FORM_AFTER_SEND" => "N",
		"SCROLL_TO_FORM_IF_MESSAGES" => "N",
		"SCROLL_TO_FORM_SPEED" => "1000",
		"UNIQUE_FORM_ID" => "544e23dabba2b",
		"OK_TEXT" => "Спасибо, ваше обращение #TICKET_ID# принято.",
		"EMAIL_TO" => "service-manager@carcam.ru",
		"DISPLAY_FIELDS" => array(
		),
		"REQUIRED_FIELDS" => array(
		),
		"CUSTOM_FIELDS" => array(
			0 => "Город покупки@input@text",
			1 => "Название магазина@input@text",
			2 => "Интернет-сайт продавца@input@text",
			3 => "Дата покупки@input@date",
			4 => "Название модели@input@text",
			5 => "Серийный номер@input@text",
			6 => "ВАШ E-mail@input@email@required",
			7 => "ВАШЕ имя@input@text",
			8 => "ВАШ телефон@input@text",
			9 => "",
		),
		"BRANCH_ACTIVE" => "N",
		"SHOW_FILES" => "N",
		"ADMIN_EVENT_MESSAGE_ID" => array(
			0 => "77",
		),
		"USER_EVENT_MESSAGE_ID" => array(
			0 => "78",
		),
		"HIDE_FIELD_NAME" => "N",
		"TITLE_DISPLAY" => "N",
		"FORM_TITLE" => "",
		"FORM_TITLE_LEVEL" => "1",
		"FORM_STYLE_TITLE" => "",
		"FORM_STYLE" => "text-align:left;",
		"FORM_STYLE_DIV" => "overflow:hidden;padding:5px;",
		"FORM_STYLE_LABEL" => "display: block;min-width:175px;margin-bottom: 3px;float:left;",
		"FORM_STYLE_TEXTAREA" => "padding:3px 5px;min-width:380px;min-height:150px;",
		"FORM_STYLE_INPUT" => "min-width:220px;padding:3px 5px;",
		"FORM_STYLE_SELECT" => "min-width:232px;padding:3px 5px;",
		"FORM_STYLE_SUBMIT" => "",
		"FORM_SUBMIT_CLASS" => "",
		"FORM_SUBMIT_ID" => "",
		"FORM_SUBMIT_VALUE" => "Проверить",
		"USER_AUTHOR_FIO" => "",
		"USER_AUTHOR_NAME" => "",
		"USER_AUTHOR_LAST_NAME" => "",
		"USER_AUTHOR_SECOND_NAME" => "",
		"USER_AUTHOR_EMAIL" => "",
		"USER_AUTHOR_PERSONAL_MOBILE" => "",
		"USER_AUTHOR_WORK_COMPANY" => "",
		"USER_AUTHOR_POSITION" => "",
		"USER_AUTHOR_PROFESSION" => "",
		"USER_AUTHOR_STATE" => "",
		"USER_AUTHOR_CITY" => "",
		"USER_AUTHOR_WORK_CITY" => "",
		"USER_AUTHOR_STREET" => "",
		"USER_AUTHOR_ADRESS" => "",
		"USER_AUTHOR_PERSONAL_PHONE" => "",
		"USER_AUTHOR_WORK_PHONE" => "",
		"USER_AUTHOR_FAX" => "",
		"USER_AUTHOR_MAILBOX" => "",
		"USER_AUTHOR_WORK_MAILBOX" => "",
		"USER_AUTHOR_SKYPE" => "",
		"USER_AUTHOR_ICQ" => "",
		"USER_AUTHOR_WWW" => "",
		"USER_AUTHOR_WORK_WWW" => "",
		"USER_AUTHOR_MESSAGE_THEME" => "",
		"USER_AUTHOR_MESSAGE" => "",
		"USER_AUTHOR_NOTES" => "",
		"AJAX_MODE" => "N",
		"SHOW_CSS_MODAL_AFTER_SEND" => "N",
		"CSS_MODAL_HEADER" => "Информация",
		"CSS_MODAL_FOOTER" => "<a id=\"bxid_576849\" >Разработка модуля</a> - Тюнинг Софт",
		"CSS_MODAL_CONTENT" => "<p>Модуль <b>расширенная форма обратной связи битрикс с вложением</b> предназначен для отправки сообщений с сайта, включая код CAPTCHA и скрытую защиту от спама, и отличается от стандартной формы Битрикс:
<br> - <b>отправкой файлов вложениями или ссылками на файл</b>,
<br> - <b>встроенным конструктором форм,</b>
<br> - скрытой защитой от спама,
<br> - работой нескольких форм на одной странице,
<br> - встроенным фирменным валидатором форм,
<br> - 4 встроенными WEB 2.0 шаблонами,
<br> - дополнительными полями со своим именованием,
<br> - и многими другими функциями...
<br>подробнее читайте на странице модуля <a id=\"bxid_863974\" >Расширенная форма обратной связи</a>
</p>",
		"UUID_LENGTH" => "10",
		"UUID_PREFIX" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
		</div>
	</div>
</main>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>