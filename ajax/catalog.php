<?
use Bitrix\Main\Loader;
include_once "include_stop_statistic.php";

// for sef of standart bitrix component
$_SERVER["REQUEST_URI"] = !empty($_REQUEST["REQUEST_URI"]) ? $_REQUEST["REQUEST_URI"] : $_SERVER["REQUEST_URI"];
$_SERVER["SCRIPT_NAME"] = !empty($_REQUEST["SCRIPT_NAME"]) ? $_REQUEST["SCRIPT_NAME"] : $_SERVER["SCRIPT_NAME"];

if(isset($_GET['ajax_basket']) || (isset($_POST["rz_ajax"]) && $_POST["rz_ajax"] === "y"))
{	
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}else{
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
}
include_once($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/lang/".LANGUAGE_ID."/header.php");


include_once "include_module.php";

if (Loader::IncludeModule('yenisite.core')) {
	$params = \Yenisite\Core\Ajax::getParams('bitrix:catalog', false, SITE_DIR . 'catalog/?rz_update_catalog_parameters_cache=Y');
}
if(!is_array($params) || empty($params)) {
	die("[ajax died] loading params");
}

//fill $_GET & $_REQUEST from REQUEST_URI
if (($searchStart = strpos($_SERVER['REQUEST_URI'], '?')) !== false) {
     $search      = substr($_SERVER['REQUEST_URI'], $searchStart+1);

    $arCheckURIParams = array(
    	'rz_all_elements',
    	$params['OFFER_VAR_NAME']
    );

	$arGet = explode('&', $search);
	foreach ($arGet as $param) {
		$param = explode('=', $param);
		if (!in_array($param[0], $arCheckURIParams)) continue;
		
		$_GET[$param[0]] = $param[1];
		$_REQUEST[$param[0]] = $param[1];
	}
	unset($arGet, $param, $search);
}

include_once "include_options.php";

//file_put_contents( $_SERVER['DOCUMENT_ROOT'].'/catalogParams.txt', print_r( $params, true ) );

//show catalog page
$APPLICATION->IncludeComponent("bitrix:catalog", "", $params, false);
?>
