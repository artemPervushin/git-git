<?
use Bitrix\Main\Loader;
include_once "include_stop_statistic.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";

if(!\Bitrix\Main\Application::GetInstance()->GetContext()->GetRequest()->IsAjaxRequest())
{
	die();
}
include_once $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/lang/".LANGUAGE_ID."/header.php";

include_once "include_module.php";

if (Loader::IncludeModule('yenisite.core')) {
	$arParams = \Yenisite\Core\Ajax::getParams('bitrix:catalog', false, SITE_DIR . 'catalog/?rz_update_catalog_parameters_cache=Y');
}
if(!is_array($arParams) || empty($arParams)) {
	die("[ajax died] loading params");
}

include_once "include_options.php";
include_once $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/components/bitrix/catalog/.default/include/prepare_params_element.php";

//show catalog store amount
$APPLICATION->IncludeComponent("bitrix:catalog.store.amount", "tooltip", array(
	"PER_PAGE" => "100",
	"USE_STORE_PHONE" => $arPrepareParams["USE_STORE_PHONE"],
	"SCHEDULE" => $arPrepareParams["USE_STORE_SCHEDULE"],
	"USE_MIN_AMOUNT" => 'N',
	"MIN_AMOUNT" => $arPrepareParams["MIN_AMOUNT"],
	"ELEMENT_ID" => intval($_REQUEST['ITEM_ID']),
	"STORE_PATH"  =>  $arPrepareParams["STORE_PATH"],
	"MAIN_TITLE"  =>  $arPrepareParams["MAIN_TITLE"],
	"CACHE_TYPE" => $arPrepareParams["CACHE_TYPE"],
	"CACHE_TIME" => $arPrepareParams["CACHE_TIME"],
	"CACHE_GROUPS" => $arPrepareParams["CACHE_GROUPS"],
	'STORE_CODE' => $arPrepareParams["STORE_CODE"],
	'FIELDS' => array('DESCRIPTION'),
	'CONTAINER_ID_POSTFIX' => $_REQUEST['STORE_POSTFIX'],
	'STORE_DISPLAY_TYPE' => $arPrepareParams['STORE_DISPLAY_TYPE'],
	'STORES' => $arPrepareParams['STORES'],
	),
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
