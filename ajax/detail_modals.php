<?
include_once "include_stop_statistic.php";

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

include_once "include_module.php";

$arAjax = \Yenisite\Core\Tools::GetDecodedArParams($_REQUEST['ajax']);
if(empty($arAjax)) {
	die('[ajax died] cannot get ajax data');
}
$arParams = \Yenisite\Core\Ajax::getParams($arAjax['CMP'], $arAjax['TMPL'], $arAjax['PAGE']);
$arParams['AJAX_MODE'] = "";

\Yenisite\Core\Tools::encodeAjaxRequest($_REQUEST);
\Yenisite\Core\Tools::encodeAjaxRequest($_POST);
if(!empty($_REQUEST[$_REQUEST['FORM_CODE']]['PRICE'])) {
	$_REQUEST[$_REQUEST['FORM_CODE']]['PRICE'] = str_replace(' ', '', $_REQUEST[$_REQUEST['FORM_CODE']]['PRICE']);
}
if('Y' == $_REQUEST['CONVERT_CURRENCY'] && !empty($_REQUEST['FORM_CODE'])) {
	$arForm = &$_REQUEST[$_REQUEST['FORM_CODE']];
	if (!empty($arForm['CURRENCY']) && !empty($arForm['PRICE'])) {
		\Bitrix\Main\Loader::includeModule('currency');
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$baseC = CCurrency::GetBaseCurrency();
		if ($baseC != $arForm['CURRENCY']) {
			/** @noinspection PhpUndefinedClassInspection */
			$arForm['PRICE'] = CCurrencyRates::ConvertCurrency($arForm['PRICE'], $arForm['CURRENCY'], $baseC);
		}
	}
	$_POST = $_REQUEST;
}
if (isset($_REQUEST['FORM_CODE'])) unset($_REQUEST['FORM_CODE'], $_POST['FORM_CODE'], $_GET['FORM_CODE']);
unset($_REQUEST['ajax'], $_POST['ajax'], $_GET['ajax']);

$APPLICATION->IncludeComponent($arAjax['CMP'], $arAjax['TMPL'], $arParams);
