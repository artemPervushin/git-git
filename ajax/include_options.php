<?
global $rz_b2_options;

//fill rz_b2_options
$APPLICATION->IncludeComponent("yenisite:settings.panel", "empty", array(
		"SOLUTION" => $moduleId,
		"SETTINGS_CLASS" => $settingsClass,
		"GLOBAL_VAR" => "rz_b2_options",
		"EDIT_SETTINGS" => array()
	),
	false
);

$rz_b2_options['active-currency'] = $APPLICATION->IncludeComponent(
		"yenisite:currency.switcher",
		"empty",
		array(
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "86400",
			"CURRENCY_LIST" => array(),
			"DEFAULT_CURRENCY" => "BASE"
		),
		false
	);
?>
