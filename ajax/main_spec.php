<?
include_once "include_stop_statistic.php";

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

include_once "include_module.php";

include_once($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/lang/".LANGUAGE_ID."/header.php");

include_once "include_options.php";

if (CModule::IncludeModule('catalog') && CModule::IncludeModule('yenisite.geoipstore'))
{
	include $_SERVER["DOCUMENT_ROOT"].SITE_DIR."include_areas/header/geoip.php";
}
//show main spec
include $_SERVER["DOCUMENT_ROOT"].SITE_DIR."include_areas/index/main_spec.php";
