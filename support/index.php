<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поддержка");
?>
<main class="container about-page">
	<div class="row">
		<div class="col-xs-12">
<style>
	a {
		text-decoration: none;
		border-bottom: none;
		color: #000;

	}
	.service_menu li{
		/*float: left;*/
		list-style-type: none;
		padding: 0;
		text-align: center;
	}
	.service_menu li img {
		padding: 5px;
	}
  .ys_article {
      margin-left: -110px;
  }
	/*a:hover img  {

    border: 1px solid #f26522; /* Рамка при наведении на ссылку курсора мыши */

}*/
  </style>
	<ul class="service_menu">
		<li class="col-xs-6 col-sm-4  col-md-2 col-lg-2">
			<a href="/service/" ><img src="/support/img/service.png" title="Сервис" border="0" alt="1.png" width="130" height="130"  /></a>
			<div style="text-align:center;"><a href="/service/" >Сервис</a></div>
		</li>
        <li class="col-xs-6 col-sm-4  col-md-2 col-lg-2">
			<a href="/firmwares/" ><img src="/support/img/f-wares.png" title="Прошивки" border="0" alt="2.png" width="130" height="130"  /></a>
			<div style="text-align:center;"><a href="/firmwares/" >Прошивки</a></div>
		</li>
        <li class="col-xs-6 col-sm-4  col-md-2 col-lg-2">
			<a href="/documentation/" ><img src="/support/img/doc.png" title="Документация" border="0" alt="3.png" width="130" height="130"  /></a>
			<div style="text-align:center;"><a href="/documentation/" >Документация</a></div>
		</li>
        <li class="col-xs-6 col-sm-4  col-md-2 col-lg-2">
			<a href="/articles/" ><img src="/support/img/articles1.png" title="Документация" border="0" alt="icon_stat.jpg" width="130" height="130"  /></a>
			<div style="text-align:center;"><a href="/articles/" >Статьи</a></div>
		</li>
        <li class="col-xs-6 col-sm-4  col-md-2 col-lg-2">
			<a href="/faq/" ><img src="/support/img/faq.png" title="Документация" border="0" alt="icon_faq.png" width="130" height="130"  /></a>
			<div style="text-align:center;"><a href="/faq/" >FAQ</a></div>
		</li>
        <li class="col-xs-6 col-sm-4  col-md-2 col-lg-2">
			<a href="/verify/" ><img src="/support/img/verify.png" title="Проверка на подлинность" border="0" alt="icon_faq.png" width="130" height="130"  /></a>
			<div style="text-align:center;"><a href="/verify/" >Проверка на подлинность</a></div>
		</li>
	</ul>
<div style="height:15%;"></div>
		</div>
	</div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>