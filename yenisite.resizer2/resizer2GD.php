<?
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    
    CModule::IncludeModule('yenisite.resizer2');
    $file = CResizer2Resize::ResizeGD2(htmlspecialchars($_REQUEST['url']), htmlspecialchars($_REQUEST['set']));
	$file = str_replace('http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'], '' ,$file);
    LocalRedirect($file);
?>
