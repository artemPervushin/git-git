<div class="comment item active">
	<div class="text">
		Покупаю технику в этом магазине не первый раз. Нравится все. Выбор, обслуживание. Рекомендую всем знакомым и
		родственникам. Многие по моей рекомендации сделали заказ - очень довольны.
	</div>
	<div class="author">Константин Циолковский</div>
</div>
<div class="comment item">
	<div class="text">Отличный магазин! Низкие цены, хороший выбор. Еще ни разу не подводил.</div>
	<div class="author">Андрей Кутузов</div>
</div>
<div class="comment item">
	<div class="text">Рекомендую всем знакомым и родственникам. Многие по моей рекомендации сделали заказ - очень доволен.</div>
	<div class="author">Сергей Кукурузов</div>
</div>
<div class="comment item">
	<div class="text">Нравится все</div>
	<div class="author">Кутуз Андреев</div>
</div>