<?
global $rz_b2_options;
if (CModule::IncludeModule('iblock')):?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.brandblock", 
	"main_page", 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "10",
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => "",
		"PROP_CODE" => "BRANDS_REF",
		"WIDTH" => "120",
		"HEIGHT" => "100",
		"WIDTH_SMALL" => "21",
		"HEIGHT_SMALL" => "17",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"PATH_FOLDER" => SITE_DIR."catalog/",
		"CATALOG_FILTER_NAME" => "arrFilter",
		"HEADER" => "Мы предлагаем технику ведущих мировых брендов",
		'BRANDS_CLOUD' => $rz_b2_options['brands_cloud'],
		//"FILTER_NAME" => "arrFilter"
	),
	false
);?>
<?endif?>
