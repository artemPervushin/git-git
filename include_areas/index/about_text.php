<p>Производим электронику для обеспечения безопасности всех сфер вашей жизни.</p>
<p>«КАРКАМ Электроникс» отечественный производитель полного цикла. Вся техника нашей компании разрабатывается и проходит полный контроль качества под наблюдением специалистов мирового уровня.</p> 
<p>Среди нашей продукции представлены десятки моделей автомобильной электроники, от авторегистраторов до систем онлайн мониторинга. Также, в ассортименте нашей компании, вы сможете найти все необходимое для организации видеонаблюдения или системы охраны на объекте любой сложности.</p>
<a href="<?=SITE_DIR?>about/" class="link more-content">
	<div class="bullets">
		<span class="bullet">&bullet;</span><!-- 
		--><span class="bullet">&bullet;</span><!--
		--><span class="bullet">&bullet;</span>
	</div>
	<span class="text">
		Еще информация
	</span>
</a>








