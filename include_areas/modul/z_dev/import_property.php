<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
$filePath = 'import_property.txt';

$import_property = unserialize(file_get_contents($filePath));
$z_count_Elements = 0;
$IBLOCK_ID = 153;
//_::d($import_property);
foreach($import_property as $z_arrElement)
    //$z_arrElement = $import_property[18142];

{
    //Найти товар и получить его id
    $productId = getIDProduct($IBLOCK_ID, $z_arrElement);

    if($productId)
    {
        $z_count_Elements++;
        //имея id товара запишим новые свойства
        $z_arValueProperty = array();
        foreach($z_arrElement['PROPERTIES'] as $key => $z_arrProperty)
        {
            $z_IDProperty = addProperty($IBLOCK_ID, $z_arrProperty);

            if($z_IDProperty)
            { //получим id-свойства по названию
                switch($z_arrProperty['VALUES']['PROPERTY_TYPE'])
                {
                    case 'S'://TODO: Готов, Без Множественных
                        if($z_arrProperty['VALUES']['MULTIPLE'] == 'Y')
                        {
                            //_::d('НЕДОРАБОТАН ОБРАБОТЧИК с типами СТРОК для МНОЖЕСТВЕННЫХ');
                            if(is_array($z_arrProperty['VALUES']['VALUE']))
                            {//html

                                $arValueString = array();
                                foreach($z_arrProperty['VALUES']['VALUE'] as $z_Item)
                                {
                                    $arValueString[] = array(
                                        'VALUE' => $z_Item
                                    );
                                }
                                if(!empty($arValueString))
                                {
                                    $z_arValueProperty[$z_IDProperty] = $arValueString;
                                }
                            }
                            else
                            {
                                _::d($z_IDProperty, $z_arrProperty);
                                $z_arValueProperty[$z_IDProperty] = array('VALUE' => $z_arrProperty['VALUES']['VALUE']);
                            }
                        }
                        elseif($z_arrProperty['VALUES']['MULTIPLE'] == 'N')
                        {
                            if(is_array($z_arrProperty['VALUES']['VALUE']))
                            {//html
                                $z_arValueProperty[$z_IDProperty] = array(
                                    'VALUE' => array(
                                        'TYPE' => $z_arrProperty['VALUES']['VALUE']['TYPE'],
                                        'TEXT' => $z_arrProperty['VALUES']['VALUE']['TEXT']
                                    )
                                );
                            }
                            else
                            {
                                $z_arValueProperty[$z_IDProperty] = array('VALUE' => $z_arrProperty['VALUES']['VALUE']);
                            }
                        }
                        break;
                    case 'L'://СПИСОК ГОТОВ
                        if($z_arrProperty['VALUES']['MULTIPLE'] == 'Y')
                        {
                            $z_arValue = array();
                            foreach($z_arrProperty['VALUES']['VALUE'] as $z_ValueProperty)
                            {
                                $z_arValue[] = array('VALUE' => addEnumValue($z_IDProperty, $z_ValueProperty));
                            }

                            if(!empty($z_arValue))
                            {
                                $z_arValueProperty[$z_IDProperty] = $z_arValue;
                            }
                        }
                        elseif($z_arrProperty['VALUES']['MULTIPLE'] == 'N')
                        {
                            $z_arValueProperty[$z_IDProperty] = array('VALUE' => addEnumValue($z_IDProperty, $z_arrProperty['VALUES']['VALUE']));
                        }
                        break;
                    case 'E'://TODO: Готов, но не полностью
                        if($z_arrProperty['VALUES']['MULTIPLE'] == 'Y')
                        {
                            //_::d('НЕДОРАБОТАН ОБРАБОТЧИК с типами связи с элементами для МНОЖЕСТВЕННЫХ',$z_arrProperty,$productId);
                            $arValueString = array();
                            foreach($z_arrProperty['VALUES']['VALUE'] as $z_Item)
                            {
                                $arValueString[] = array(
                                    'VALUE' => $z_Item
                                );
                            }
                            if(!empty($arValueString))
                            {
                                $z_arValueProperty[$z_IDProperty] = $arValueString;
                            }
                        }
                        elseif($z_arrProperty['VALUES']['MULTIPLE'] == 'N')
                        {
                            $z_PropID = array();
                            if(($z_arrProperty['VALUES']['VALUE'] == '503') && ($z_arrProperty['VALUES']['CODE'] == 'PRODUCER'))
                            {
                                $z_arValueProperty[10696] = 160;
                            }
                            else
                            {
                                $z_arValueProperty[$z_IDProperty] = array('VALUE' => $z_arrProperty['VALUES']['VALUE']);
                            }
                        }
                        break;
                    case 'N':
                        break;
                    case 'F':
                        if($z_arrProperty['VALUES']['MULTIPLE'] == 'Y')
                        {
                            $PropID = array();
                            foreach($z_arrProperty['VALUES']['VALUE_SRC'] as $z_ValueProperty)
                            {
                                $PropID[] = array(
                                    'VALUE'       => CFile::MakeFileArray('http://carcam.ru' . $z_ValueProperty),
                                    "DESCRIPTION" => ""
                                );
                            }
                            if($PropID)
                            {
                                $z_arValueProperty[$z_IDProperty] = $PropID;
                            }
                        }
                        elseif($z_arrProperty['VALUES']['MULTIPLE'] == 'N')
                        {
                            $PropID = array();
                            $PropID[] = array(
                                'VALUE'       => CFile::MakeFileArray('http://carcam.ru' . $z_arrProperty['VALUES']['VALUE_SRC']),
                                "DESCRIPTION" => ""
                            );

                            if($PropID)
                            {
                                $z_arValueProperty[$z_IDProperty] = $PropID;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        if(!empty($z_arValueProperty))
        {
            foreach($z_arValueProperty as $propertyId => $arItem)
            {
                CIBlockElement::SetPropertyValueCode($productId, $propertyId, $arItem);
            }
        }
    }
}
_::d(count($import_property));
_::d($z_count_Elements);

function getIDProduct($iBlockId, $arValueProperty)
{
    $arrXML_ID = array(
        8824  => 'fe21651f-4277-11e5-ae86-448a5b29e640',
        8396  => 'fafcdf50-3b45-11e5-ae86-448a5b29e640',
        9310  => 'f6dcd5f7-4275-11e5-ae86-448a5b29e640',
        6225  => 'eb6586e7-10fc-11e5-ae86-448a5b29e640',
        4491  => 'e9bbdfb8-5857-11e4-82b9-448a5b29e640',
        9242  => 'e6d28272-5aab-11e5-b17b-00155d2fde05',
        8368  => 'e63bdfed-3b45-11e5-ae86-448a5b29e640',
        9392  => 'e3e747a0-3b7c-11e5-ae86-448a5b29e640',
        9410  => 'e120881e-3b7e-11e5-ae86-448a5b29e640',
        8209  => 'd9864737-4284-11e5-ae86-448a5b29e640',
        8818  => 'd61686b4-4277-11e5-ae86-448a5b29e640',
        7408  => 'd4d69fa6-dd00-11e3-498a-00224da7db8a',
        9389  => 'd29242a1-3b7c-11e5-ae86-448a5b29e640',
        8362  => 'c9ebb465-3c1a-11e5-ae86-448a5b29e640',
        9397  => 'c4f56410-3b7c-11e5-ae86-448a5b29e640',
        2366  => 'c1b9042a-425b-11e4-bf15-448a5b29e640',
        7410  => 'bf43fb0c-dd00-11e3-498a-00224da7db8a',
        9311  => 'bd41320d-4275-11e5-ae86-448a5b29e640',
        1406  => 'bc140238-0b4f-11e4-7891-00224da7db8a',
        2108  => 'ba299574-1659-11e4-bf24-448a5b29e640',
        2043  => 'b40e56b5-0e4c-11e4-bf24-448a5b29e640',
        8281  => 'ad3d3154-4fd6-11e5-b2dc-00155d2fde05',
        11557 => 'acbbd21e-b9ec-11e5-b2e3-00155d2fde05',
        9382  => 'aaf83df2-3b7c-11e5-ae86-448a5b29e640',
        804   => 'a6627190-caea-11e3-2186-00224da7db8a',
        2105  => 'a37bd938-1659-11e4-bf24-448a5b29e640',
        1706  => 'a05cb432-0e4c-11e4-bf24-448a5b29e640',
        5717  => '9f30a6e5-d3c2-11e4-beca-448a5b29e640',
        802   => '9d43abb0-caea-11e3-2186-00224da7db8a',
        8638  => '9bebe65e-4276-11e5-ae86-448a5b29e640',
        11558 => '98fd58ce-b9ec-11e5-b2e3-00155d2fde05',
        1416  => '95e6b466-1642-11e4-bf24-448a5b29e640',
        800   => '908a1fd0-caea-11e3-2186-00224da7db8a',
        1701  => '8c020ff9-0e4c-11e4-bf24-448a5b29e640',
        8860  => '8bf0fa52-7ed0-11e5-8f6d-00155d2fde05',
        1413  => '89c6b201-1642-11e4-bf24-448a5b29e640',
        7404  => '89170e0c-dd00-11e3-498a-00224da7db8a',
        8646  => '888f6436-4277-11e5-ae86-448a5b29e640',
        11559 => '876a6368-b9ec-11e5-b2e3-00155d2fde05',
        798   => '8545a89c-caea-11e3-2186-00224da7db8a',
        8346  => '8300951b-3c1a-11e5-ae86-448a5b29e640',
        8377  => '82344a21-3b46-11e5-ae86-448a5b29e640',
        1483  => '80235fa4-eb1e-11e3-4097-00224da7db8a',
        1481  => '801a770e-eb1e-11e3-4097-00224da7db8a',
        1479  => '8011a25a-eb1e-11e3-4097-00224da7db8a',
        1477  => '80089f84-eb1e-11e3-4097-00224da7db8a',
        1475  => '7fffb090-eb1e-11e3-4097-00224da7db8a',
        1473  => '7ff780aa-eb1e-11e3-4097-00224da7db8a',
        1471  => '7fedfcce-eb1e-11e3-4097-00224da7db8a',
        1469  => '7fe4f656-eb1e-11e3-4097-00224da7db8a',
        1467  => '7fdc2d8c-eb1e-11e3-4097-00224da7db8a',
        1465  => '7fd31a4e-eb1e-11e3-4097-00224da7db8a',
        1463  => '7fca565c-eb1e-11e3-4097-00224da7db8a',
        1461  => '7fc26082-eb1e-11e3-4097-00224da7db8a',
        1459  => '7fb975e4-eb1e-11e3-4097-00224da7db8a',
        1457  => '7fb12a92-eb1e-11e3-4097-00224da7db8a',
        1453  => '7faa8bec-eb1e-11e3-4097-00224da7db8a',
        1451  => '7fa3f444-eb1e-11e3-4097-00224da7db8a',
        1420  => '7f9d2524-eb1e-11e3-4097-00224da7db8a',
        2041  => '7ec20713-0e4c-11e4-bf24-448a5b29e640',
        796   => '7b6171b2-caea-11e3-2186-00224da7db8a',
        11578 => '76700482-b9ec-11e5-b2e3-00155d2fde05',
        9406  => '7618082c-3b7e-11e5-ae86-448a5b29e640',
        8354  => '74faf9fe-3c1a-11e5-ae86-448a5b29e640',
        794   => '70ca451c-caea-11e3-2186-00224da7db8a',
        7389  => '6aed96a7-2088-11e5-ae86-448a5b29e640',
        8813  => '6a7e6692-4278-11e5-ae86-448a5b29e640',
        792   => '68abf1a0-caea-11e3-2186-00224da7db8a',
        9380  => '685417a7-3b7c-11e5-ae86-448a5b29e640',
        5716  => '6829974d-d3c2-11e4-beca-448a5b29e640',
        11560 => '679ec72c-b9ec-11e5-b2e3-00155d2fde05',
        8330  => '65977498-3c1a-11e5-ae86-448a5b29e640',
        1699  => '64895e8e-0e4c-11e4-bf24-448a5b29e640',
        7412  => '614cefac-a163-11e4-8629-448a5b29e640',
        8649  => '60c622b0-4277-11e5-ae86-448a5b29e640',
        10332 => '5fe23612-5c4b-11e5-b17b-00155d2fde05',
        1127  => '5e8d42fb-4958-11e4-bf15-448a5b29e640',
        560   => '51013305-6ca4-11e3-8d21-74867a2cb288',
        551   => '51013304-6ca4-11e3-8d21-74867a2cb288',
        1839  => '51013302-6ca4-11e3-8d21-74867a2cb288',
        523   => '51013300-6ca4-11e3-8d21-74867a2cb288',
        533   => '510132ff-6ca4-11e3-8d21-74867a2cb288',
        570   => '510132fd-6ca4-11e3-8d21-74867a2cb288',
        500   => '510132f8-6ca4-11e3-8d21-74867a2cb288',
        673   => '510132f7-6ca4-11e3-8d21-74867a2cb288',
        2276  => '4ee2d947-1d62-11e4-bf24-448a5b29e640',
        2274  => '4ee2d946-1d62-11e4-bf24-448a5b29e640',
        2272  => '4ee2d945-1d62-11e4-bf24-448a5b29e640',
        2268  => '4ee2d944-1d62-11e4-bf24-448a5b29e640',
        2266  => '4ee2d943-1d62-11e4-bf24-448a5b29e640',
        2264  => '4ee2d942-1d62-11e4-bf24-448a5b29e640',
        2262  => '4ee2d941-1d62-11e4-bf24-448a5b29e640',
        2260  => '4ee2d940-1d62-11e4-bf24-448a5b29e640',
        2240  => '4ee2d93f-1d62-11e4-bf24-448a5b29e640',
        2237  => '4ee2d93e-1d62-11e4-bf24-448a5b29e640',
        2232  => '4ee2d93d-1d62-11e4-bf24-448a5b29e640',
        10394 => '4df709ac-5c4b-11e5-b17b-00155d2fde05',
        8652  => '4a56b820-4277-11e5-ae86-448a5b29e640',
        9400  => '4847ec46-3b7e-11e5-ae86-448a5b29e640',
        9378  => '46b5f08a-2153-11e5-ae86-448a5b29e640',
        6294  => '44e03b7e-dcff-11e3-498a-00224da7db8a',
        8935  => '404695d2-4278-11e5-ae86-448a5b29e640',
        6297  => '3fd88e78-9429-11e5-8f6d-00155d2fde05',
        1616  => '3f74321e-e252-11e3-a08f-00224da7db8a',
        11561 => '3da540ae-b9ec-11e5-b2e3-00155d2fde05',
        9387  => '3cdcd275-3b7e-11e5-ae86-448a5b29e640',
        8448  => '3c0a7845-3b47-11e5-ae86-448a5b29e640',
        6499  => '393ce01d-1344-11e5-ae86-448a5b29e640',
        10396 => '34582484-5c4b-11e5-b17b-00155d2fde05',
        9373  => '33ad4fb9-2153-11e5-ae86-448a5b29e640',
        9312  => '31e6b001-4276-11e5-ae86-448a5b29e640',
        11577 => '2e3445ca-b9ec-11e5-b2e3-00155d2fde05',
        8821  => '29b72020-4278-11e5-ae86-448a5b29e640',
        10337 => '26813e18-5c4b-11e5-b17b-00155d2fde05',
        4981  => '24b56744-8e43-11e3-3590-00224da7db8a',
        8656  => '2361aecd-4277-11e5-ae86-448a5b29e640',
        8438  => '2267c671-3b47-11e5-ae86-448a5b29e640',
        8945  => '20bb9764-4575-11e5-ae86-448a5b29e640',
        11576 => '1e343dce-b9ec-11e5-b2e3-00155d2fde05',
        1759  => '1c3e6124-eaff-11e3-c597-00224da7db8a',
        1765  => '1c2c83c8-eaff-11e3-c597-00224da7db8a',
        975   => '1c239830-eaff-11e3-c597-00224da7db8a',
        1770  => '1bfaf77c-eaff-11e3-c597-00224da7db8a',
        1753  => '1bee10fc-eaff-11e3-c597-00224da7db8a',
        1767  => '1be57302-eaff-11e3-c597-00224da7db8a',
        1751  => '1bc17f10-eaff-11e3-c597-00224da7db8a',
        1711  => '1bb89878-eaff-11e3-c597-00224da7db8a',
        1709  => '1baa1d3e-eaff-11e3-c597-00224da7db8a',
        6308  => '1b058174-d3c1-11e4-beca-448a5b29e640',
        8480  => '1aa6ac4e-5c4c-11e5-b17b-00155d2fde05',
        1680  => '18c75584-eb16-11e3-c597-00224da7db8a',
        8442  => '1783dd19-3b46-11e5-ae86-448a5b29e640',
        4493  => '17044d2e-5858-11e4-82b9-448a5b29e640',
        6015  => '16a04d7b-044b-11e5-ae86-448a5b29e640',
        10384 => '163c11ea-5c4b-11e5-b17b-00155d2fde05',
        5720  => '161017d8-e373-11e4-beca-448a5b29e640',
        8826  => '12dc68c6-4278-11e5-ae86-448a5b29e640',
        4479  => '1134132b-5857-11e4-82b9-448a5b29e640',
        6309  => '107c8a5e-d3c1-11e4-beca-448a5b29e640',
        8429  => '0ff51c52-3b47-11e5-ae86-448a5b29e640',
        7394  => '0fcdc000-dcff-11e3-498a-00224da7db8a',
        8477  => '0f2af8d4-5c4c-11e5-b17b-00155d2fde05',
        9078  => '0be87245-2153-11e5-ae86-448a5b29e640',
        8433  => '07b79b28-3b46-11e5-ae86-448a5b29e640',
        6011  => '05cbd2cb-044b-11e5-ae86-448a5b29e640',
        4498  => '04b73b1d-5857-11e4-82b9-448a5b29e640',
        8463  => '017efcbc-5c4c-11e5-b17b-00155d2fde05',
        8635  => '0090b4d9-4277-11e5-ae86-448a5b29e640'
    );
    $resIDProduct = null;
    $arFilter = array(
        "IBLOCK_ID" => $iBlockId,
        "ACTIVE"    => "Y",
        "XML_ID"    => $arrXML_ID[$arValueProperty['XML_ID']]
        /*"NAME" => $z_arrElement['NAME']*/
    );
    $arSelectFields = array("ID", "NAME", "XML_ID");
    $z_arrResElements = \SB\IBlock::getElements(array("SORT" => "ASC"), $arFilter, false, Array("nPageSize" => 2), $arSelectFields);

    if(count($z_arrResElements) == 1)
    {
        $resIDProduct = $z_arrResElements[0]['ID'];
    }
    else
    {
        $arFilter = array("IBLOCK_ID" => $iBlockId, "ACTIVE" => "Y", "NAME" => $arValueProperty['NAME']);
        $z_arrResElements = \SB\IBlock::getElements(array("SORT" => "ASC"), $arFilter, false, Array("nPageSize" => 2), $arSelectFields);
        if(count($z_arrResElements) == 1)
        {
            $resIDProduct = $z_arrResElements[0]['ID'];
        }
    }

    return $resIDProduct;
}

/*
 * Функция вернет первый ID найденного свойства
 * в противном случае вернет 0
 * $z_iblock_id - где искать (инфоблок)
 * $z_name_property - название свойства
 */
function z_GetIdProperty($z_iblock_id, $z_name_property)
{
    $z_arResult = 0;
    $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array(
        "ACTIVE"    => "Y",
        "IBLOCK_ID" => $z_iblock_id,
        "NAME"      => $z_name_property
    ));
    while($prop_fields = $properties->GetNext())
    {
        $z_arResult = $prop_fields['ID'];

        //_::d($prop_fields);
        return $z_arResult;
    }

    return $z_arResult;
}

function addEnumValue($propertyId, $value)
{
    $property_enums = CIBlockPropertyEnum::GetList(Array(
        "DEF"  => "DESC",
        "SORT" => "ASC"
    ), Array(
        "PROPERTY_ID" => $propertyId,
        "VALUE"       => $value
    ));

    $property_value_enum = $property_enums->GetNext();

    if($property_value_enum)
    {
        //Обновляем свойство элемента имеющимся ID
        $enumValueId = $property_value_enum['ID'];
    }
    else
    {
        //Добавляем новый  элемент в свойство и присваиваем его элементу
        $ibpenum = new CIBlockPropertyEnum;
        $enumValueId = $ibpenum->Add(Array(
            'PROPERTY_ID' => $propertyId,
            'VALUE'       => $value
        ));
    }

    return $enumValueId;
}

function addProperty($iBlockId, $arPropertyFields = array())
{

    $name = $arPropertyFields['NAME'];

    // $z_IDProperty = z_GetIdProperty($IBLOCK_ID, $z_arrProperty['NAME']);

    $propertyId = null;

    $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array(
        "ACTIVE"    => "Y",
        "IBLOCK_ID" => $iBlockId,
        "NAME"      => $name
    ));
    while($dbPropFields = $properties->GetNext())
    {
        $propertyId = $dbPropFields['ID'];
    }

    if(!$propertyId)
    {//если нет такого свойства добавим его
        $arFields = Array(
            "NAME"          => $name,
            "ACTIVE"        => "Y",
            "CODE"          => $arPropertyFields['VALUES']['CODE'],
            "PROPERTY_TYPE" => $arPropertyFields['VALUES']['PROPERTY_TYPE'],
            "MULTIPLE"      => $arPropertyFields['VALUES']['MULTIPLE'],
            "IBLOCK_ID"     => $iBlockId
        );

        $iblockproperty = new CIBlockProperty;
        $propertyId = $iblockproperty->Add($arFields);
    }

    return $propertyId;
}

?>
<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/epilog_after.php"); ?>