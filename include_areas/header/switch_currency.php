<?
if(\Bitrix\Main\Loader::includeModule('yenisite.core'))
{
global $rz_b2_options;
$rz_b2_options['active-currency'] = $APPLICATION->IncludeComponent(
		"yenisite:currency.switcher", 
		"bitronic2", 
		array(
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "86400",
			"CURRENCY_LIST" => array(),
			"DEFAULT_CURRENCY" => "BASE"
		),
		false
	);
}
?>