<? global $rz_b2_options; ?>
<? $APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
	"ROOT_MENU_TYPE" => "catalog",	// Тип меню для первого уровня
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_TIME" => "604800",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"VIEW_HIT" => $rz_b2_options["block_main-menu-elem"],
		"HITS_POSITION" => $rz_b2_options["menu-hits-position"],
		"RESIZER_SET" => "3",	// Набор для картинки
		"PRICE_CODE" => array(	// Тип цены
			0 => "BASE",
		),
		"COMPONENT_TEMPLATE" => "catalog",
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"HITS_COMPONENT" => "CATALOG",	// Компонент, с помощью которого будут отображаться товары-хиты
		"HITS_TYPE" => "SHOW",	// Критерий отбора товаров-хитов
	),
	false
);