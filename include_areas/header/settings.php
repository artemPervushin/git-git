<?
use Bitronic2\Mobile;
mobile::Init();
$setMobile = (mobile::isMobile()) ? 'Y' : 'N';
?>
<?$APPLICATION->IncludeComponent(
	"yenisite:settings.panel", 
	".default", 
	array(
		"SOLUTION" => "yenisite.bitronic2",
		"SETTINGS_CLASS" => "CRZBitronic2Settings",
		"GLOBAL_VAR" => "rz_b2_options",
		"EDIT_SETTINGS" => array(
		),
		"SET_MOBILE" => $setMobile,
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);