<?use Bitronic2\Mobile;
$method = (mobile::isMobile()) ? 'tel' : 'callto';?>

<div class="phone-wrap">
 <a itemprop="telephone" content="8 800 555-6-808" href="<?= $method; ?>:8-800-555-6-808" class="phone-link" data-tooltip="" title="Заказать звонок" data-placement="right">8 800 555-6-808</a>
</div>