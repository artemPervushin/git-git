<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"catalog_bottom", 
	array(
		"ROOT_MENU_TYPE" => "catalog",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "604800",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "catalog_bottom",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"RESIZER_SET" => "3",
		"HITS_COMPONENT" => "CATALOG",
		"HITS_TYPE" => "SHOW",
		"PRICE_CODE" => "",
		"TITLE" => ""
	),
	false
);