<?
if (\Bitrix\Main\Loader::IncludeModule("yenisite.feedback")) {
	$APPLICATION->IncludeComponent("yenisite:feedback.add", "modal_subscribe",
		array(
			"IBLOCK_TYPE" => "bitronic2_feedback",
			"IBLOCK" => "6",
			"SUCCESS_TEXT" => "Спасибо! В случае поступления товара на склад мы сообщим Вам.",
			"PRINT_FIELDS" => array(
				0 => "EMAIL",
				1 => "PRODUCT",
			),
			"ACTIVE" => "Y",
			"EVENT_NAME" => "FOUND_CHEAP",
			"EMAIL" => "EMAIL",
			"PHONE" => "PHONE",
		), false);
}
