<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "FAQ");
$APPLICATION->SetPageProperty("keywords_inner", "FAQ");
$APPLICATION->SetPageProperty("title", "FAQ");
$APPLICATION->SetPageProperty("keywords", "FAQ");
$APPLICATION->SetPageProperty("description", "FAQ");
$APPLICATION->SetTitle("Часто задаваемые вопросы");
?><main class="container about-page">
<div class="row">
	<div class="col-xs-12">
		 <style>
	.quest {
		text-decoration:underline;
		cursor: pointer;
	}

.answer {
    margin-top:-15px;
}

.ys_article {
      margin-left: -210px;
  }


</style> <script type="text/javascript">
$(document).ready(function(){
  $(".answer").hide(); 
 
  $(".quest").toggle(function(){
  $(this).addClass("active");
}, function () {
  $(this).removeClass("active");
});

  $(".quest").click(function() 
  {
    $(this).next(".answer").slideToggle(200); 
  });
});
</script>
		<p class="quest">
		На экране видеорегистратора горит треугольник с восклицательным знаком; видеорегистратор делает файлы защищенными от перезаписи. </p>
		<p class="answer">
			 Данные признаки указывают на то, что в аппарате активирован G-сенсор. Для устранения данных проблем в работе, функцию стоит отключить, т.к. G-сенсор предназначен для использования только в режиме парковки.
		</p>
		<p class="quest">
			 Где можно приобрести комплектующие товары и запасные части для продукции КАРКАМ?
		</p>
		<p class="answer">
			 Все товары КАРКАМ можно приобрести в Интернет-магазине. Узнать о наличии Вы можете по номеру 8(800) 555-6-808.
		</p>
		<p class="quest">
			 Не могу прошить видеорегистратор. Подскажите, как это сделать самостоятельно?
		</p>
		<p class="answer">
			 Для того чтобы выполнить прошивку аппарата самостоятельно, обратитесь к разделу с руководством по прошивкам <a href="/firmwares/">http://carcam.ru/firmwares/</a>
			Если Вы не смогли выполнить установку прошивки самостоятельно, обратитесь к специалистам горячей линии КАРКАМ по номеру 8(800) 555-6-808. Они помогут осуществить установку в режиме онлайн.
		</p>
		<p class="quest">
			 Как проверить подлинность видеорегистратора?
		</p>
		<p class="answer">
			 Проверить подлинность аппарата можно c помощью специального раздела на сайте <a href="/verify/">http://carcam.ru/verify/</a><br>
		</p>
		<p class="quest">
			 Куда сдавать устройство, если в моем городе нет сервисного центра?
		</p>
		<p class="answer">
			 Если в Вашем городе нет сервисного центра «КАРКАМ Электроникс», Вы можете воспользоваться Европейской Моделью Гарантийного Обслуживания. Для оформления такого вида гарантии Вам необходимо заполнить специальную форму на официальном сайте <a href="http://carcam.ru/garant/">http://carcam.ru/garant/</a>
		</p>
		<p class="quest">
			 Как обновить базу радаров для видеорегистратора?
		</p>
		<p class="answer">
			 База радаров, используемая в видеорегистраторах, является частью прошивки и обновляется с выходом ее новой версии. Если Вам необходимо дополнительно обозначить какие-то точки расположения фиксирующих устройств, Вы можете самостоятельно их установить, следуя инструкции пользователя.
		</p>
		<p class="quest">
			 Как узнать, является ли продавец официальным дилером КАРКАМ?
		</p>
		<p class="answer">
			 Чтобы проверить, является ли продавец официальным дилером КАРКАМ, обратитесь к списку на официальном сайте <a href="/buy/">http://carcam.ru/buy/</a>
		</p>
	</div>
</div>
 </main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>