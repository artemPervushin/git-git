<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ОПТ");
?><main class="container">
<h1>Сотрудничество</h1>
<div>
	<div style="margin-bottom: 10px;">
		 Компания «КАРКАМ Электроникс» активно расширяет свою дилерскую сеть и приглашает к взаимовыгодному сотрудничеству партнеров из разных регионов России, Белоруссии, Украины, Казахстана, Армении, Киргизии и других стран СНГ.
	</div>
	<div style="margin-bottom: 10px;">
		 Получить статус официального дилера может абсолютно любая компания или индивидуальный предприниматель, заинтересованные в продаже и продвижении оборудования под торговой маркой «КАРКАМ» в своем регионе.
	</div>
	<div style="margin-bottom: 10px;">
		 Мы рассматриваем субъекты оптово-розничной торговли (торговые сети, кибермаркеты, онлайн и оффлайн магазины и т.п.) и сферы услуг (установочные центры, монтажные организации, охранные предприятия и т.п.).
	</div>
	<div style="margin-bottom: 10px;">
		 От Вас - желание работать!
	</div>
	<div style="margin-bottom: 10px;">
		 От нас:
		<ul style="list-style-position: outside; list-style-type: disc;">
			<li>гибкая система скидок и бонусов;</li>
			<li>обучение и тренинги персонала;</li>
			<li>техподдержка;</li>
			<li>сервис-центры по всей РФ и СНГ;</li>
			<li>размещение информации о Вашей компании на официальном сайте;</li>
			<li>перенаправление клиентов;</li>
			<li>совместное участие в рекламных компаниях и тематических выставках;</li>
			<li>совместное участие в тендерах.</li>
		</ul>
	</div>
	<div style="margin-bottom: 10px;">
		 Пожалуйста, заполните и отправьте «Запрос на сотрудничество», и наши менеджеры свяжутся с Вами в ближайшее время.
	</div>
	<div>
		Спасибо!
	</div>
</div>
<div class="row">
	<div class="col-md-5">
		<h3 style="color: #ff8b00;font-size: 24px;text-align:center;line-height:24px;font-weight:normal;">Запрос на сотрудничество</h3>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"carcam",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => Array(),
		"WEB_FORM_ID" => 1
	)
);?>
	</div>
	<div class="col-md-7">
		<h3 style="color: #ff8b00;font-size: 24px;text-align:center;line-height:24px;font-weight:normal;">Отдел оптовых продаж</h3>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<p>
 <b>Руководитель оптового отдела<br>
					Косенков Дмитрий</b> <br>
 <i class="icon-phone-squared"></i>8 (800) 555-68-08 доб.601 <br>
 <i class="icon-mail"></i> <img width="87" src="/images/emails/opt.png" height="12" style="position: relative;">
				</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<p>
 <b>Божко Андрей</b> <br>
					Региональный менеджер <br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08, доб. 607 <br>
 <i class="icon-mail"></i> <img width="95" src="/images/emails/opt5.png" height="12" style="position: relative;">
				</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<p>
 <b>Павел Букин</b> <br>
					Региональный менеджер <br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08, доб. 605 <br>
 <i class="icon-mail"></i> <img width="95" src="/images/emails/opt1.png" height="12" style="position: relative;"> <br><i class="icon-icq"></i> 653-453-134 <br>
				</p>
			</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
				<p>
 <b>Павел Трофимов</b> <br>
					Региональный менеджер <br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08, доб. 603 <br>
 <i class="icon-mail"></i> <img width="95" src="/images/emails/opt4.png" height="12" style="position: relative;"> <br><i class="icon-icq"></i> 660-910-970 <br>
				</p>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<p>
 <b>Станислав Астафьев</b> <br>
					Региональный менеджер <br>
 <i class="icon-phone-squared"></i> 8 (800) 555-68-08, доб. 504 <br>
 <i class="icon-mail"></i> <img width="95" src="/images/emails/opt7.png" height="12" style="position: relative;"> <br><i class="icon-icq"></i> 408-058-681 <br>
				</p>
			</div>
		</div>
	</div>
</div>
 </main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>